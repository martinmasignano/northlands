<?php

namespace App\Http\Controllers;

class ContactusController extends Controller {

	public function show($slug)
    {
        $pageslug = 'contactus.' . (string)$slug; 
        // This means that your views must be in views/contactus/ folder
        if( view()->exists($pageslug)){
            return view($pageslug);
        }
        abort(404);
    }
}