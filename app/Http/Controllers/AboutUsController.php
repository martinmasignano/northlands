<?php

namespace App\Http\Controllers;

class AboutUsController extends Controller {
	
	public function index()
	{
		return view('aboutus/faculty/faculty');
	}

    public function show($slug)
    {
        $pageslug = 'aboutus.' . (string)$slug; 
        // This means that your views must be in views/aboutus/ folder
        if( view()->exists($pageslug)){
            return view($pageslug);
        }
        abort(404);
    }
}
