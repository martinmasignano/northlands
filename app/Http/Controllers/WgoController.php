<?php

namespace App\Http\Controllers;

class WgoController extends Controller {
	
	public function index()
	{
		return view('wgo/wgo');
	}

    public function show($slug)
    {
        $pageslug = 'wgo.' . (string)$slug; 
        // This means that your views must be in views/wgo/ folder
        if( view()->exists($pageslug)){
            return view($pageslug);
        }
        abort(404);
    }
}
