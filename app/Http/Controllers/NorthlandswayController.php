<?php

namespace App\Http\Controllers;

class NorthlandswayController extends Controller {

	public function show($slug)
    {
        $pageslug = 'thenorthlandsway.' . (string)$slug; 
        // This means that your views must be in views/thenorthlandsway/ folder
        if( view()->exists($pageslug)){
            return view($pageslug);
        }
        abort(404);
    }
    
}