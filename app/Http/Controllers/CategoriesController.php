<?php

namespace App\Http\Controllers;

class CategoriesController extends Controller {

	public function show($slug)
    {
        $pageslug = 'categories.' . (string)$slug; 
        // This means that your views must be in views/categories/ folder
        if( view()->exists($pageslug)){
            return view($pageslug);
        }
        abort(404);
    }

}