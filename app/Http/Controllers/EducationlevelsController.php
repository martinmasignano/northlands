<?php

namespace App\Http\Controllers;

class EducationlevelsController extends Controller {

	public function index()
	{
		return view('educationlevels');
	}

	public function show($slug)
    {
        $pageslug = 'educationlevels.' . (string)$slug; 
        // This means that your views must be in views/educationlevels/ folder
        if( view()->exists($pageslug)){
            return view($pageslug);
        }
        abort(404);
    }
}
