<?php

namespace App\Http\Controllers;

class ArticlesController extends Controller {

	public function faculty($slug)
    {
        $pageslug = 'articles/faculty.' . (string)$slug; 
        // views must be in views/articles/faculty folder
        if( view()->exists($pageslug)){
            return view($pageslug);
        }
        abort(404);
    }

    public function wgo($slug)
    {
        $pageslug = 'articles/wgo.' . (string)$slug; 
        // views must be in views/articles/wgo folder
        if( view()->exists($pageslug)){
            return view($pageslug);
        }
        abort(404);
    }

    public function characterBuilding($slug)
    {
    	$pageslug = 'articles/categories/character-building.' . (string)$slug;
		// views must be in views/articles/categories/character-building folder
		if( view()->exists($pageslug)){
            return view($pageslug);
        }
        abort(404);
    }

    public function digitalLearners($slug)
    {
    	$pageslug = 'articles/categories/digital-learners.' . (string)$slug;
	// views must be in views/articles/categories/digital-learners folder
		if( view()->exists($pageslug)){
            return view($pageslug);
        }
        abort(404);
    }

	public function globalCitizenship($slug)
    {
    	$pageslug = 'articles/categories/global-citizenship.' . (string)$slug;
		// views must be in views/articles/categories/global-citizenship folder
		if( view()->exists($pageslug)){
            return view($pageslug);
        }
        abort(404);
    }

   	public function heathyLifestyle($slug)
    {
    	$pageslug = 'articles/categories/healthy-lifestyle.' . (string)$slug;
		// views must be in views/articles/categories/healthy-lifestyle folder
		if( view()->exists($pageslug)){
            return view($pageslug);
        }
        abort(404);
    }

   	public function iblearnes($slug)
    {
    	$pageslug = 'articles/categories/iblearnes.' . (string)$slug;
		// views must be in views/articles/categories/iblearnes folder
		if( view()->exists($pageslug)){
            return view($pageslug);
        }
        abort(404);
    }

   	public function mentors($slug)
    {
    	$pageslug = 'articles/categories/mentors.' . (string)$slug;
		// views must be in views/articles/categories/mentors folder
		if( view()->exists($pageslug)){
            return view($pageslug);
        }
        abort(404);
    }
}