<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'IndexController@index');

Route::get('user/{id}', 'UserController@showProfile');

// Articles routes
// Faculty articles
Route::get('/articles/faculty/{slug}','ArticlesController@faculty');
// Wgo articles
Route::get('/articles/wgo/{slug}','ArticlesController@wgo');
// Character building articles
Route::get('/articles/categories/character-building/{slug}','ArticlesController@characterBuilding');
// Digital Learners Articles routes
Route::get('/articles/categories/digital-learners/{slug}','ArticlesController@digitalLearners');
// Global Citizenship Articles routes
Route::get('/articles/categories/global-citizenship/{slug}','ArticlesController@globalCitizenship');
// Healthy Lifestyle Articles routes
Route::get('/articles/categories/healthy-lifestyle/{slug}','ArticlesController@heathyLifestyle');
// IB Learnes Articles routes
Route::get('/articles/categories/iblearnes/{slug}','ArticlesController@iblearnes');
// Mentors of Knowledge Articles routes
Route::get('/articles/categories/mentors/{slug}','ArticlesController@mentors');

// About us routes
Route::get('/aboutus/faculty/faculty', 'AboutUsController@index');
Route::get('/aboutus/{slug}', 'AboutUsController@show');

// Education levels routes
Route::get('/educationlevels', 'EducationlevelsController@index');
Route::get('/educationlevels/{slug}', 'EducationlevelsController@show');

// Admissions routes
Route::get('/admissions', 'AdmissionsController@index');

// Contact us routes
Route::get('/contactus/{slug}', 'ContactusController@show');

// Login routes
Route::get('/login', 'LoginController@index');

// Categories routes
Route::get('/categories/{slug}', 'CategoriesController@show');

// The northlands way routes
Route::get('/thenorthlandsway/{slug}', 'NorthlandswayController@show');

// WGO routes
Route::get('/wgo','WgoController@index');

// Languages settings
Route::get('/{lang}', ['as'=>'lang.switch', 'uses'=>'LanguageController@switchLang']);

// Documentation routes
Route::get('/documentation/index', 'DocumentationController@index');
