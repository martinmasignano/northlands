{{-- Language set in lang/{language}/components/navbar.php --}}

{{-- Fixed navbar --}}
<nav class="navbar navbar-default navbar-fixed-top">
  	<div class="container-fluid">
    	<div class="navbar-header">
      		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false" aria-controls="navbar">
    			<span class="sr-only">Toggle navigation</span>
    			<span class="icon-bar"></span>
        		<span class="icon-bar"></span>
        		<span class="icon-bar"></span>
      		</button>
      		<a class="navbar-brand" href="{{ url('/') }}">
                {!! Html::image('./css/img/logo.svg', 'Northlands', array('class' => 'logo', 'width' => '100%')) !!}
            </a>
    	</div>
    	<div id="navbar" class="navbar-collapse collapse">
      		<ul class="nav navbar-nav">
    			<li class="{{ Request::is('aboutus/*') ? 'active' : '' }} dropdown">
          			<a href="{{ url('aboutus') }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
          				{!! trans('components/navbar.about') !!} <span class="caret"></span>
          			</a>
          			<ul class="dropdown-menu">
            			<li>
                    		<a href="{{ url('/aboutus/thenorthlandsway') }}">
                      			{!! trans('components/navbar.northlandsway') !!}
                    		</a>
                  		</li>
            			<li>
            				<a href="{{ url('/aboutus/authorities') }}">
            					{!! trans('components/navbar.authorities') !!}
            				</a>
        				</li>
            			<li>
            				<a href="{{ url('/aboutus/history') }}">
            					{!! trans('components/navbar.history') !!}
            				</a>
            			</li>
            			<li>
            				<a href="{{ url('/aboutus/ourschool') }}">
            					{{ trans('components/navbar.ourschool') }}
            				</a>
        				</li>
          			</ul>
        		</li>
    			<li class="{{ Request::is('educationlevels') ? 'active' : ''}}">
    				<a href="{{ url('educationlevels') }}">
    					{!! trans('components/navbar.levels') !!}
    				</a>
    			</li>
        		<li class="{{ Request::is('admissions') ? 'active' : ''}}">
        			<a href="{{ url('admissions') }}">
        				{!! trans('components/navbar.admissions') !!}
        			</a>
        		</li>
        		<li>
        			<a href="http://cloud.northlands.edu.ar/npoint/" target="_blank">
        				{{ trans('components/navbar.alumni') }}
        			</a>
        		</li>
        		<li class="{{ Request::is('contactus/*') ? 'active' : ''}} dropdown">
        			<a href="{{ url('contactus') }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
        				{!! trans('components/navbar.contact') !!} <span class="caret"></span>
        			</a>
        			<ul class="dropdown-menu">
        				<li>
        					<a href="{{ url('/contactus/info') }}">
        						{!! trans('components/navbar.contactinfo') !!}
        					</a>
        				</li>
        				<li>
        					<a href="{{ url('/contactus/jobopportunities') }}">
        						{!! trans('components/navbar.job') !!}
        					</a>
        				</li>
        			</ul>
        		</li>
        		<li class="dropdown">
        			<a href="{{ url('aboutus') }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{!! trans('components/navbar.login') !!} <span class="caret"></span></a>
        			<ul class="dropdown-menu">
            			<li>
            				<a href="{{ url('https://accounts.google.com/o/oauth2/auth?client_id=493713552412.apps.googleusercontent.com&response_type=code&scope=openid%20email&display=popup&redirect_uri=http://www.northlands.org.ar/parents/index.php&state=fc37e65a7707ffd0f78dc0f561c25887&login_hint=') }}" target="_blank">
            					{!! trans('components/navbar.parents') !!}
            				</a>
            			</li>
            			<li>
            				<a href="{{ url('https://accounts.google.com/o/oauth2/auth?client_id=493713552412.apps.googleusercontent.com&response_type=code&scope=openid%20email&display=popup&redirect_uri=http://www.northlands.org.ar/student/index.php&state=fc37e65a7707ffd0f78dc0f561c25887&login_hint=') }}" target="_blank">
            					{!! trans('components/navbar.students') !!}
            				</a>
            			</li>
            			<li>
            				<a href="{{ url('https://accounts.google.com/o/oauth2/auth?client_id=493713552412.apps.googleusercontent.com&response_type=code&scope=openid%20email&display=popup&redirect_uri=http://www.northlands.org.ar/staff/index_Staff.php&state=fc37e65a7707ffd0f78dc0f561c25887&login_hint=') }}" target="_blank">
            					{!! trans('components/navbar.staff') !!}
            				</a>
            			</li>
            			<li>
            				<a href="{{ url('mailto:soporteIT@northlands.edu.ar') }}">
            					{!! trans('components/navbar.help') !!}
            				</a>
        				</li>
          			</ul>
        		</li>
      		</ul>

      		<ul class="nav navbar-nav navbar-flags navbar-right">
        		@foreach (Config::get('languages') as $lang => $language)
                    @if ($lang != App::getLocale())
                        <li>
                            <a href="{{ route('lang.switch', $lang) }}">
                                @if ($language == 'Spanish')
                                    {!! Html::image('./css/img/flag-arg.svg', 'Northlands', array('class' => '', 'width' => '100%')) !!}
                                @else
                                    {!! Html::image('./css/img/flag-uk.svg', 'Northlands', array('class' => '', 'width' => '100%')) !!}
                                @endif
                            </a>
                        </li>
                    @else
                        <li>
                            <a href="{{ route('lang.switch', $lang) }}">
                                @if ($language == 'Spanish')
                                    {!! Html::image('./css/img/flag-arg.svg', 'Northlands', array('class' => '', 'width' => '100%')) !!}
                                @else
                                    {!! Html::image('./css/img/flag-uk.svg', 'Northlands', array('class' => '', 'width' => '100%')) !!}
                                @endif
                            </a>
                        </li>
                    @endif
                @endforeach
      		</ul>

      		<form class="navbar-nav navbar-form navbar-right">
                <div class="input-group">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <i class="fa fa-flip-horizontal fa-search"></i>
                        </button>
                    </span>
                    <input type="text" class="form-control" placeholder="{!! trans('components/navbar.search') !!}">
                </div>
			</form>
		</div>
  	</div>
</nav>
