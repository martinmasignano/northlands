{{-- Scripts que se incluyen en el sitio --}}


<script type="text/javascript" src="{{ asset('js/vendor/jquery.easing.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/vendor/jquery.eislideshow.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/vendor/bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/vendor/thumbscript1.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/vendor/vlbdata1.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/vendor/slick.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/vendor/masonry.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/northlands.js') }}"></script>
