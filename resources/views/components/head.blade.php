<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="description" content="IB, CAMBRIDGE, LAHC. NORTHLANDS is a co-educational, non-denominational School which offers the children of the Argentine and international community with an all-embracing, bilingual and form multiple perspectives education. Olivos &amp; Nordelta" />
<meta name="keywords" content="Northlands school in Buenos Aires, Colegio, bilingüe, inglés, en zona norte, nordelta, Argentina, Bilingual, Kindergarten, Primary and Secondary education. English, IB, Cambridge, bilingue, mixto, laico, en nordelta">

	<meta name="author" content="Martín Masignano - Patricia Lazara">
	<link rel="icon" href="favicon.ico">

	<title>Northlands School | Colegio Biling&uuml;e Ingl&eacute;s</title>
	
	<link type="text/css" rel="stylesheet" href="{{ asset('css/vlightbox1.css') }}">
	<link type="text/css" rel="stylesheet" href="{{ asset('css/visuallightbox.css') }}">
	<link type="text/css" rel="stylesheet" href="{{ asset('css/app.css') }}">

	<script type="text/javascript" src="{{ asset('js/vendor/jquery.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/vendor/visuallightbox.js') }}"></script>
</head>
