{{-- Language set in lang/{language}/components/footer.php --}}

<div class="footer">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-4 col-md-4 col-lg-4">
				<p class="copyright">{!! trans('components/footer.copyright') !!}</p>
			</div>
			<div class="col-sm-4 col-md-5 col-lg-4">
				<ul>
					<li>
						<a href="#" target="_blank">
							<i class="fa fa-linkedin-square fa-2x"></i>
						</a>
					</li>
					<li class="hidden-xs hidden-sm">
						<a href="{{ url('contactus/jobopportunities') }}">
							<span>{!! trans('components/footer.jobopportunities') !!}<span>
						</a>
					</li>
					<li class="hidden-xs hidden-sm">
						<span>|</span>
					</li>
					<li class="hidden-xs hidden-sm">
						<a href="{{ url('contactus/info') }}">
							<span>{!! trans('components/footer.contactus') !!}</span>
						</a>
					</li>
					<li>
						<a href="#" target="_blank">
							<i class="fa fa-facebook-square fa-2x"></i>
						</a>
					</li>
				</ul>
			</div>
			<div class="col-sm-4 col-md-3 col-lg-4">
				<a class="back-to-top" href="#top"><i class="fa fa-chevron-up"></i></a>
			</div>
		</div>
	</div>
</div>