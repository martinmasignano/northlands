@extends('layouts.articlecolumns')

@section('sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('img/articles/global-citizenship/culturas_L.jpg')}}" alt="" class="pull-right">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('img/articles/global-citizenship/culturas_P.jpg')}}" alt="" class="pull-right">
	</div>
@endsection

@section('content')
	<div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> > 
		<a href="{{ url('/categories/global-citizenship') }}">Ciudadanía Global</a> > 
			<span>Culturas Tradicionales y Contextos</span> 
	</div>

	<h1>Culturas Tradicionales y Contextos</h1>
	<div class="columns">
		<p class="item">Dentro del marco de nuestro programa de Desarrollo Personal y Social, en el Jardín buscamos desarrollar en cada uno de nuestros alumnos conciencia de sí mismo y de los demás. Aspiramos a que los niños internalicen gradualmente el cuidado y la valoración de sí mismos, el respeto y valoración de sus compañeros, de otros miembros de su comunidad y del mundo en general. Se busca favorecer el desarrollo de la propia identidad, la autoconfianza y la seguridad en sus capacidades. Buscamos que puedan convivir cooperativamente con otros dentro del marco de un mundo globalizado valorando particularidades, símbolos, tradiciones, culturas y credos diversos, sentando las bases para la comprensión profunda del otro.</p>

		<p class="item">En esta tarea cotidiana también consideramos necesario enseñar a valorar la diversidad, al “otro” en forma sistemática a través de unidades de indagación acordes a los intereses y capacidades de los niños en esta etapa. Kieran Egan en su libro “Mentes educadas” plantea que los niños de esta edad tienen una comprensión mística de la realidad y cita a Jerome Bruner afirmando que “se puede enseñar cualquier tema con eficacia y de una forma intelectualmente honesta a cualquier niño y en cualquier etapa del desarrollo”. Buscamos afirmar que tiene sentido brindar a los niños información acerca de lo que los hace únicos dentro de su cultura, aprovechando esta oportunidad para aprender sobre culturas y tradiciones de otros países y contextos.</p>

		<p class="item">Algunos ejemplos de este tipo de trabajo son nuestros “Special Days” en las salas de 3 o “Let´s Celebrate” en las Salas de 4. Este año decidimos profundizar aún más estos conceptos y pensamos en un “Concert” donde el Jardín en su totalidad investigó acerca de las tradiciones, danzas, arte y celebraciones de algunos países . Esto permitió vivenciar experiencias tales como una clase de yoga , cantar y bailar tango o escuchar relatos tradicionales de los países de origen de nuestra comunidad internacional.</p>

		<iframe class="item" src="https://player.vimeo.com/video/172152478" width="100%" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

		<iframe class="item" src="https://player.vimeo.com/video/172153635" width="100%" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
	</div>
@endsection



