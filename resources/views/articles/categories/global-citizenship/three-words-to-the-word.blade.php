@extends('layouts.articlecolumns')

@section('sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('img/articles/global-citizenship/threeW_L.jpg')}}" alt="" class="pull-right">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('img/articles/global-citizenship/threeW_P.jpg')}}" alt="" class="pull-right">
	</div>
@endsection

@section('content')
	<div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> > 
		<a href="{{ url('/categories/global-citizenship') }}">Ciudadanía Global</a> > 
			<span>Three words to the word</span> 
	</div>

	<h1>Three words to the word</h1>
	<div class="columns">
		<p>Students in Primary were inquiring into the concept of Identity: “what makes us individual and unique and also how we portray ourselves to others”. They watched a video project made by high school students in the United States called Three Words to the World. The idea is that you have only three words to send a message to the world. They were inspired to do the project themselves. They were also inquiring into the idea of global citizenship and thought about how their messages could be universal -meaningful for those both near and far, similar and different.</p>

		<iframe src="https://player.vimeo.com/video/133171416" width="100%" height="320" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
	</div>
@endsection



