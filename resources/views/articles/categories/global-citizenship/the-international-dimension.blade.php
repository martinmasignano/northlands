@extends('layouts.articlecolumnsfixed')

@section('left-sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('img/articles/global-citizenship/international_L.jpg')}}" alt="" class="pull-right">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('img/articles/global-citizenship/international_P.jpg')}}" alt="" class="pull-right">
	</div>
@endsection

@section('breadcrumbs')
	 <div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> > 
		<a href="{{ url('/categories/global-citizenship') }}">Ciudadanía Global</a> > 
			<span>The International Dimension</span> 
	</div>
@endsection

@section('content')
	<h1>The International Dimension</h1>
	<p>Chasing the shifting clouds students float unanchored, confused  by the ever-changing world around them…but…if given the opportunity to grow and learn within an internationally minded learning community students will be empowered to direct their own lives in positive, effective and meaningful ways. They need to be given the tools and opportunities to discover the world beyond their homes and classrooms and encouraged to make connections. This will facilitate their own life-long learning and make a difference to their communities and ultimately the wider world.</p>

	<p>Living and learning in a globalized, interdependent and rapidly changing world is a considerable challenge for students today. They are faced with significant and pressing global issues that will affect the way they live their lives. They need to be enabled to make informed decisions and take responsible action in approaching the changing global landscape.</p>

	<p>It is our responsibility as a school community to promote international perspectives in our children and students and to support an international dimension throughout the curriculum. The international dimension is about students recognizing the world’s interconnections, becoming aware of themselves and others and being able to understand and value their own and others’ perspectives.</p>

{{-- 	{!! Html::image('img/articles/global-citizenship/international-dimension.png', '', array('class' => 'img-responsive')) !!} --}}

	{!! Html::image('img/articles/global-citizenship/intercultural.jpg', '', array('class' => 'img-responsive')) !!}
	<br>
	<p>At NORTHLANDS we believe that encouraging an international dimension will benefit the students, the school, wider community and the world.  We are passionate about encouraging multiple perspectives and helping students to “form their own opinions and viewpoints and value those of others.” One of NORTHLANDS’ objectives is: ‘To develop an individual moral sense based on universal ethical principles.’</p>

	<p>Our well-established School values and our all-embracing Personal Social Education (PSE) programme help provide a caring, respectful and mutually supportive learning community in which students can grow into responsible citizens who are encouraged to develop and demonstrate, universal ethical and moral values. Each year we focus on one of our PSE axes that supports the social and personal development of our students and strengthens and upholds our School values.</p>

	<p>Last year, in the Primary school, we progressed the development of our international dimension in many different ways.  We put on different events for students focusing on cultures from around world, held staff meetings to help deepen understanding and completed displays that celebrated similarities and differences and cultural heritage. This year we are continuing to follow our action plan and will hold events for students with international themes, provide further development for staff and continue to highlight the international dimension in and beyond the classroom.  One of the ways we will do this is by using the IBO PYP Learner Profile to a greater degree within inquiry learning.</p>
	
	<p>“The learner profile aims to: Develop internationally minded people who recognizing our common humanity and shared guardianship of the planet help to create a better more peaceful world.” IBO. As Boix Mansilla and Jackson state, “The growing global interdependence that characterizes our time calls for a generation of individuals who can engage in effective global problem solving and participate simultaneously in local, national, and global civic life.</p>
@endsection

@section('right-sidebar')
	<iframe id="iframe_container" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" width="100%" height="400" src="https://prezi.com/embed/on0grzohugpy/?bgcolor=ffffff&amp;lock_to_path=0&amp;autoplay=0&amp;autohide_ctrls=0&amp;landing_data=bHVZZmNaNDBIWnNjdEVENDRhZDFNZGNIUE43MHdLNWpsdFJLb2ZHanI0aXduSkQ5dDZxV2RBaVJZWnZ3dXBHcFdBPT0&amp;landing_sign=RP3f4RXGK5m495BX5HfbT8Is7wSl0wuJ0ZyWIzFt_HY"></iframe>
@endsection



