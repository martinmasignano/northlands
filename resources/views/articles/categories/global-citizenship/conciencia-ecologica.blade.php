@extends('layouts.articlecolumns')

@section('sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('img/articles/global-citizenship/conciencia_L.jpg')}}" alt="" class="pull-right">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('img/articles/global-citizenship/conciencia_P.jpg')}}" alt="" class="pull-right">
	</div>
@endsection

@section('content')
	<div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> > 
		<a href="{{ url('/categories/global-citizenship') }}">Ciudadanía Global</a> > 
			<span>Conciencia Ecológica</span> 
	</div>

	<h1>Conciencia Ecológica</h1>
	<div class="columns">
		<p class="item">Uno de los desafíos más grandes en la enseñanza de las Ciencias Naturales es lograr que los alumnos puedan pensar y analizar un tema en distintos niveles y poder relacionar lo que ocurre en su entorno con lo que ocurre a escala global. Buscamos que los chicos identifiquen el impacto que tienen las distintas actividades del hombre y de las comunidades sobre los distintos ecosistemas del planeta para lograr despertar en ellos la conciencia ecológica que dé como resultado un mayor compromiso individual. Este objetivo es un verdadero reto ya que los alumnos deben comprender y dimensionar las consecuencias de nuestras acciones a pesar de que éstas muchas veces, no son visibles en el lugar o tiempo en que nos hallamos.</p>

		<p class="item lead">“Hoy más que nunca, la vida debe caracterizarse por un sentido de responsabilidad universal, no sólo entre naciones y entre humanos, sino entre humanos y cualquier otra forma de vida” Dalai Lama</p>

		<div class="item well green">
			<h4>¿La conciencia ecológica se aprende?</h4>
			<p>Todas las pequeñas acciones que llevamos a cabo con los distintos grupos forman parte de un proyecto de sustentabilidad que involucra a todo el Colegio y a la comunidad y tiene como objetivo concientizar sobre la importancia del cuidado del ambiente y el uso racional de los recursos naturales. Parte de las propuestas ecológicas que se brindan a los chicos están relacionadas con la enseñanza de hábitos sustentables mediante actividades concretas: técnicas de reciclado de productos con materiales que los chicos conocen, apagar la luz al salir del aula, cerrar la canilla mientras me cepillo los dientes, entre otras.</p>
		</div>

		<iframe class="item" src="https://player.vimeo.com/video/158629993" width="100%" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

		<p class="item lead">Ser parte de estos eventos y participar en estas campañas globales ayuda a los chicos a dimensionar la importancia que los problemas ambientales tienen y sentirse parte de la solución.</p>

	</div>
@endsection


