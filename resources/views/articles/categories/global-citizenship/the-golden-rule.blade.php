@extends('layouts.articlecolumns')

@section('sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('img/articles/global-citizenship/india_L.jpg')}}" alt="" class="pull-right">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('img/articles/global-citizenship/india_P.jpg')}}" alt="" class="pull-right">
	</div>
@endsection

@section('content')
	<div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> > 
		<a href="{{ url('/categories/global-citizenship') }}">Ciudadanía Global</a> > 
			<span>The Golden Rule</span> 
	</div>

	<h1>The Golden Rule</h1>
	<div class="columns">
		<p class="item lead">NORTHLANDS promotes the development of global citizenship in which we "not only value our own cultural identity but also appreciate the richness of diversity and promote international-mindedness.</p>

		<p class="item">This intercultural understanding and respect inspires us to assume a shared responsibility to contribute to a more sustainable and peaceful world". </p>

		<p calss="item">Each new Interreligious meeting, allow students to deepen the concept of diversity, promoting appreciation and respect for differences. Because we attend to cultural diversity, we are aware about the importance of getting to know the different aspects of each religion, its rites and celebrations, symbols and expressions. We hope that the Interreligious meeting allows our future graduates to approach to the knowledge of different faiths and to consolidate their learning skills as global citizens.</p>

		{!! Html::image('img/articles/global-citizenship/golden-rule.png', '', array('class' => 'item img-responsive')) !!}

		<iframe class="item" src="https://player.vimeo.com/video/69623455" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

	</div>
@endsection


