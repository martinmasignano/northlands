@extends('layouts.articlecolumns')

@section('sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('img/articles/global-citizenship/thimun_l.jpg')}}" alt="" class="pull-right">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('img/articles/global-citizenship/thimun_p.jpg')}}" alt="" class="pull-right">
	</div>
@endsection

@section('content')
	<div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> > 
		<a href="{{ url('/categories/global-citizenship') }}">Ciudadanía Global</a> > 
			<span>En Primera Persona</span> 
	</div>

	<h1>En Primera Persona</h1>
	<div class="columns">
		<p class="item lead">The THIMUN (The Hague International Model United Nations) conference is a five-day simulation of  the United Nations for secondary school students, which takes place at the end of January each year in the World Forum Convention Center in The Hague.</p>

		<p class="item">THIMUN now attracts over 3,500 students and teachers from around 200 schools located in countries as far apart as Norway, Australia, China and Ecuador. The students themselves originate from more than 100 different countries.</p>

		<p class="item">Attendants of THIMUN conferences aim to seek, through discussion, negotiation and debate, solutions to the various problems of the world.  Every year NORTHLANDS is represented by their students in this conference.</p>

		<p class="item">Juan Diego Serrano Ortega (ON 2015) participated in 2014 representing the Saudi Arabi comitee.</p>

		<iframe class="item" src="https://player.vimeo.com/video/141187228" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

		{!! Html::image('img/articles/global-citizenship/thimun.jpg', '', array('class' => 'item img-responsive')) !!}

	</div>
@endsection




