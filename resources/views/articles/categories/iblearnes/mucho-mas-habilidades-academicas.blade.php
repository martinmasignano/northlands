@extends('layouts.articlecolumns')

@section('sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('img/articles/ib-learnes/habilidades-land.jpg')}}" alt="">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('img/articles/ib-learnes/habilidades-port.jpg')}}" alt="">
	</div>
@endsection

@section('content')
	<div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> >
		<a href="{{ url('/categories/empowers-ib-learnes') }}">Potenciamos alumnos IB</a> >
			<span>Mucho más que habilidades académicas</span>
	</div>

	<h1>Mucho más que habilidades académicas</h1>
	<div class="columns">
		<p class="item">Las asignaturas de Artes en el programa permiten un alto grado de adaptabilidad a diferentes contextos culturales. Se hace hincapié en la creatividad en el contexto de una investigación práctica y disciplinada dentro de los géneros correspondientes. Todas las asignaturas fomentan la práctica crítica, reflexiva y bien fundamentada para  ayudar a los alumnos a comprender la naturaleza dinámica y cambiante de las artes, explorar la diversidad de las artes a través del tiempo, el espacio y las culturas, y expresarse con confianza y competencia.</p>

		<h3 class="item">Oedipus Rex</h3>
		<a class="item articles-link" target="_blank" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2015/so_OedipusRex/so_OedipusRex.php') }}">
			{!! Html::image('img/articles/ib-learnes/ib-article4-2.png', '', array('class' => 'item img-responsive')) !!}
		</a>

		<h3 class="item">Greek Mythology and Urban Art</h3>
		<a class="item articles-link" target="_blank" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2015/so_y11_IBLiterature/so_y11_IBLiterature.php') }}">
			{!! Html::image('img/articles/ib-learnes/ib-article4-1.png', '', array('class' => 'item img-responsive')) !!}
		</a>

	</div>
@endsection
