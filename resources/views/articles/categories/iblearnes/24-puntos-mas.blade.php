@extends('layouts.articlecolumns')

@section('sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('img/articles/ib-learnes/24-puntos-land.jpg')}}" alt="">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('img/articles/ib-learnes/24-puntos-port.jpg')}}" alt="">
	</div>
@endsection

@section('content')
	<div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> > 
		<a href="{{ url('/categories/empowers-ib-learnes') }}">Potenciamos alumnos IB</a> > 
			<span>24 puntos o más</span> 
	</div>

	<h1>24 puntos o más</h1>
	<div class="columns">
		<p class="item">Todos nuestros alumnos rinden el Diploma bilingüe completo del IB que, además de evaluar las habilidades académicas, promueve una actitud internacional y habilidades interculturales. Es importante destacar que en todo el mundo, menos del 30 % de los Diplomas IB son bilingues.</p>

		<p class="item">El Diploma del IB se otorga a alumnos que obtienen 24 puntos o más de un máximo de 45. Los alumnos deben reunir otros requerimientos del programa como TOK (Teoría del Conocimiento), CAS (Creatividad, Acción y Servicio) y el trabajo final de Monografía.</p>

		{!! Html::image('img/articles/ib-learnes/ib-article3-1.png', '', array('class' => 'item img-responsive')) !!}

		<iframe class="item" src="https://player.vimeo.com/video/64309159" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

	</div>
@endsection


