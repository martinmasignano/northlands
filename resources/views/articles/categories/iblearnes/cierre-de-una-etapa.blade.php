@extends('layouts.articlecolumns')

@section('sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('img/articles/ib-learnes/cierre-etapa-land.jpg')}}" alt="">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('img/articles/ib-learnes/cierre-etapa-port.jpg')}}" alt="">
	</div>
@endsection

@section('content')
	<div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> > 
		<a href="{{ url('/categories/empowers-ib-learnes') }}">Potenciamos alumnos IB</a> > 
			<span>Cierre de una etapa</span> 
	</div>

	<h1>Cierre de una etapa</h1>
	<div class="columns">
		<p class="item">Al llegar a Año 6 y como cierre de este ciclo, los alumnos llevan a cabo un extenso y profundo proyecto colaborativo denominado la Exposición del Programa de la Escuela Primaria (PEP) del IB. La exposición constituye una oportunidad única y significativa para que los alumnos demuestren los atributos del perfil de la comunidad IB que han desarrollado a lo largo del programa.</p>

		{!! Html::image('img/articles/ib-learnes/ib-article2-1.png', '', array('class' => 'item img-responsive')) !!}
		
		<iframe class="item" src="https://player.vimeo.com/video/131206706" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		<iframe class="item" src="https://player.vimeo.com/video/159823456" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		<iframe class="item" src="https://player.vimeo.com/video/120694318" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

	</div>
@endsection


