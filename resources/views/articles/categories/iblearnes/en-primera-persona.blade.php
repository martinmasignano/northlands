@extends('layouts.articlecolumns')

@section('sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('img/articles/ib-learnes/en-primera-persona-land.jpg')}}" alt="">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('img/articles/ib-learnes/en-primera-persona-port.jpg')}}" alt="">
	</div>
@endsection

@section('content')
	<div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> > 
		<a href="{{ url('/categories/empowers-ib-learnes') }}">Potenciamos alumnos IB</a> > 
			<span>En primera Persona</span> 
	</div>

	<h1>En primera Persona</h1>
	<div class="columns">
		<p class="item">Sybrand Veeger, graduado de Northlands 2014, nos visitó para contarnos los beneficios de ser parte del mundo IB y dar testimonio en la reunión de presentación del Programa de la escuela primaria.</p>

		<iframe class="item" src="https://player.vimeo.com/video/143611552" width="100%" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

		<h3 class="item">La voz de la comunidad del PD</h3>
		<iframe class="item" src="https://player.vimeo.com/video/160759661" width="100%" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

		<h3 class="item">Diploma</h3>
		<iframe class="item" src="https://player.vimeo.com/video/64309159" width="100%" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		
		{!! Html::image('img/articles/ib-learnes/ib-article1-1.png', '', array('class' => 'item img-responsive')) !!}

		{!! Html::image('img/articles/ib-learnes/ib-article1-2.png', '', array('class' => 'item img-responsive')) !!}

		{!! Html::image('img/articles/ib-learnes/ib-article1-3.png', '', array('class' => 'item img-responsive')) !!}

	</div>
@endsection


