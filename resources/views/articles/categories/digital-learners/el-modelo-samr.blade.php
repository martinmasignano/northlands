@extends('layouts.articlecolumns')

@section('sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('img/articles/digital-learners/samr_land.jpg')}}" alt="" class="pull-right">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('img/articles/digital-learners/samr_port.jpg')}}" alt="" class="pull-right">
	</div>
@endsection

@section('content')
	<div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> > 
		<a href="{{ url('/categories/digital-learners') }}">Aprendices digitales</a> > 
			<span>El modelo SAMR</span> 
	</div>

	<h1>El modelo SAMR</h1>
	<div class="columns">
		<p class="item">El modelo SAMR, propuesto por el Dr. Ruben Puentedura, explica cuatro fases en el uso de las nuevas tecnologías, basándose en el tipo de relación entre las actividades que se realizan con ellas.</p>

		<p class="item">Ruben R. Puenteduras describe el proceso de implementación en dos etapas dividas cada una en dos subetapas.</p>

		<p class="item">Sustitución: la tecnología es utilizada para hacer los mismo que hacíamos antes, solo que a través de la tecnología.</p>

		<p class="item">Aumento: se hacen algunas mejoras al proceso, la tecnología es un sustituto con solo algunos cambios en lo que hacemos.</p>

		<p class="item">Modificación: la tecnología es utilizada para crear asignaciones en las que el uso de las tecnologías es determinante para poder llevarlas a cabo.</p>

		<p class="item">Redefinición: las tareas asignadas solo pueden lograrse a través del uso de la tecnología.</p>

		<p class="item">El modelo SAMR parece un modelo simple, pero es una herramienta poderosa para ver realmente el grado de integración de las TIC en las actividades educativas, y si realmente aprovechamos al máximo las nuevas tecnologías o nos quedamos en un uso cómodo de ellas.</p>

		<p class="item">Piense en el modelo SAMR como una escalera para visualizar su planificación docente. El primer escalón es sustituir un proceso por otro que use tecnología; luego muévase al siguiente cambiando un poquito el proceso; seguidamente modifique todo el programa para dar lugar a aprovechar al máximo la tecnología.</p>

		<img class="item img-responsive" src="{{ url('/img/articles/digital-learners/sami.jpg') }}" alt="El modelo SAMI">

		<a href="https://www.commonsensemedia.org/videos/ruben-puentedura-on-applying-the-samr-model" class="item" target="_blank">Ruben Puentedura on Applying the SAMR Model</a>

		<p class="item">Referencias<br />El modelo SAMR: Aprendizaje profundo en contextos auténticos en <a target="_blank" href="http://2-learn.net/director/el-modelo-samr-aprendizaje-profundo-en-contextos-auntenticos/">http://2-learn.net/director/el-modelo-samr-aprendizaje-profundo-en-contextos-auntenticos/</a></p>

@endsection


