@extends('layouts.articlecolumns')

@section('sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('img/articles/digital-learners/youtoo_land.jpg')}}" alt="" class="pull-right">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('img/articles/digital-learners/youtoo_port.jpg')}}" alt="" class="pull-right">
	</div>
@endsection

@section('content')
	<div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> > 
		<a href="{{ url('/categories/digital-learners') }}">Aprendices digitales</a> > 
			<span>You too express</span> 
	</div>

	<h1>You too express</h1>
	<div class="columns">
		<p class="item">Un equipo multidisciplinario de NORTHLANDS guiados por el equipo de innovación Cocolab, bajo la coordinación y guía de Ezequiel Bachrach investigó qué demandaban al Colegio los alumnos. Se generaron más de 1000 ideas como respuesta a sus demandas, luego se evaluaron las propuestas para responder a ellas. Así en 2015 elegimos, diseñamos e implementamos, en consonancia con el 1:1 D Learning Project, You Too Express que invitó a Año 5 y Año 8 de ambas sedes a expresarse a través del uso de la tecnología con videos de 95 segundos (en referencia al aniversario del Colegio).</p>

		<p class="item">La demanda de nuestros alumnos era disponer de un lugar (espacio, oportunidad) de libre expresión, sin reglas, sin horarios, sin restricciones. Querían expresar lo que piensan, lo que dicen, lo que hacen y lo que sienten los chicos de su generación.  Y les propusimos YouTooExpress, un festival de expresión generacional a través de cortos en el marco de los 95 años del Colegio</p>

		<img src="{{ url('img/articles/digital-learners/youtooexpress.jpg')}}" alt="You too express" class="item" style="float: right;">
	</div>

	<div class="row">
		<div class="col-xs-4">
			<iframe src="https://player.vimeo.com/video/174651109" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		</div>
		<div class="col-xs-4">
			<iframe src="https://player.vimeo.com/video/174652591" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		</div>
		<div class="col-xs-4">
			<iframe src="https://player.vimeo.com/video/148862288" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-4">
			<iframe src="https://player.vimeo.com/video/174669857" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		</div>
		<div class="col-xs-4">
			<iframe src="https://player.vimeo.com/video/174672088" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		</div>
		<div class="col-xs-4">
			<iframe src="https://player.vimeo.com/video/174676560" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		</div>
	</div>
@endsection


