@extends('layouts.articlecolumns')

@section('sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('img/articles/digital-learners/vanguardia_land.jpg')}}" alt="" class="pull-right">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('img/articles/digital-learners/vanguardia_port.jpg')}}" alt="" class="pull-right">
	</div>
@endsection

@section('content')
	<div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> > 
		<a href="{{ url('/categories/digital-learners') }}">Aprendices digitales</a> > 
			<span>A La vanguardia de una educación Progresista</span> 
	</div>

	<h1>A La vanguardia de una educación Progresista</h1>
	<div class="columns">
		<p class="item">Uno de los objetivos macro de NORTHLANDS es “Ser en un Colegio de vanguardia en el uso efectivo del aprendizaje digital para realzar la enseñanza y el aprendizaje”.</p>

		<p class="item">En 2015 comenzamos la implementación del programa 1 a 1 entusiasmados por brindar a nuestros alumnos la oportunidad de avanzar en su aprendizaje digital dentro de un currículo integrado. Capacitamos a nuestros docentes y creamos el rol de “Facilitador Digital”, un docente especializado en la integración de la tecnología y los contenidos, dedicado a ayudar a nuestros docentes en este proceso. Incrementamos nuestro ancho de banda para responder a las demandas adicionales de navegación simultánea y creamos dos redes dedicadas exclusivamente al Programa D-Learning 1:1.</p>

		<iframe class="item" src="https://player.vimeo.com/video/148862288" width="100%" height="260" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		
		<p class="item">Esto garantiza seguridad, gestión eficiente y flexibilidad permitiendo a maestros y alumnos utilizar un servicio más rápido y trabajar en una red de colaboración segura.</p>

		<p class="item">En agosto 2015, comenzamos con el proyecto y más de 600 alumnos se conectaron a la red NORTHLANDS incrementando sus posibilidades de aprendizaje y experiencia a través de sus dispositivos.</p>

		<p class="item">El Programa sigue creciendo y todo el Colegio estará conectado a través del proyecto 1:1 para 2017</p>

		<p class="item">Estas acciones ratifican nuestro compromiso con el desarrollo de un perfil de egresado digitalmente consciente y dispuesto a enfrentar los retos de la ciudadanía global con responsabilidad.</p>

		<h3>El poder de la tecnología interactiva</h3>
		<iframe src="https://player.vimeo.com/video/148495541" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

		<h3>Mientras enseñamos, aprendemos</h3>
		<iframe src="https://player.vimeo.com/video/148495540" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

		<h3>Otra manera de evaluar</h3>
		<iframe src="https://player.vimeo.com/video/149288028" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

	</div>
@endsection


