@extends('layouts.articlecolumns')

@section('sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('img/articles/digital-learners/changing_land.jpg') }}" alt="" class="pull-right">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('img/articles/digital-learners/changing_port.jpg')}}" alt="" class="pull-right">
	</div>
@endsection

@section('content')
	<div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> > 
		<a href="{{ url('/categories/digital-learners') }}">Aprendices digitales</a> > 
			<span>Changing the way we teach and learn</span> 
	</div>

	<h1>Changing the way we teach and learn</h1>
	<div class="columns">
		<p class="item">We are living in an education revolution.  Much has changed in the way students are learning and we cannot expect them to learn well if we continue to teach reading, writing and mathematics solely by traditional means. Smartphones, notebooks and tablets combine many functions in one device. An ever-increasing number of apps allow users to perform multiple tasks anytime and anywhere. The possibilities and functions these devices provide make them essential in this tech-forward age. This means they can have a big impact on not only our day-to-day lives, but in the classroom, too.</p>

		<p class="item">We firmly believe our 1:1 D-Learning programme has given our students the opportunity to enhance the way they learn. Apart from the multimedia tools that make their production more attractive and memorable, the apps give them the chance to check their learning in real time, process facts in a meaningful way, create graphs that translate concepts into more concrete data and, above all, interact with information to turn it into knowledge.</p>
		
		<div class="item">
			<img class="img-responsive" src="{{ url('/img/articles/digital-learners/ipad.jpg') }}" alt="">

			<h3><a target="_blank" href="http://www.northlands.org.ar/extensive/N1589O753/Galleries2015/son_iPad/son_iPad.php">iThink, iCreate, iSpeak, iPad</a></h3>		
		</div>
		
		<div class="item">
			<img class="img-responsive" src="{{ url('/img/articles/digital-learners/art.jpg') }}" alt="">

			<h3><a target="_blank" href="http://www.northlands.org.ar/extensive/N1589O753/Galleries2015/so_y11_IBLiterature/so_y11_IBLiterature.php">Greek Mythology and Urban Art</a></h3>
		</div>
@endsection


