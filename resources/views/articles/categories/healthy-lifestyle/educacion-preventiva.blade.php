@extends('layouts.articlecolumns')

@section('sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('img/articles/healthy-lifestyle/tabaquismo-land.jpg')}}" alt="">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('img/articles/healthy-lifestyle/tabaquismo-port.jpg')}}" alt="">
	</div>
@endsection

@section('content')
@section('content')
	<div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> > 
		<a href="{{ url('/categories/healthy-lifestyle') }}">Estilo de vida saludable</a> > 
			<span>Educación Preventiva</span> 
	</div>

	<h1>Educación Preventiva</h1>
	<div class="columns">
		<p class="item">Cuidemos nuestra vida</p>

		<p class="item">Este proyecto de educación preventiva tiene como objetivo establecer conductas de concientización y autocuidado en los niños antes de que lleguen a la adolescencia. Basándonos en la idea de que los conocimientos adquiridos en el Colegio ayudan a formar un pensamiento crítico propio, esperamos que nuestros alumnos tomen decisiones inteligentes y duraderas para convertirse en agentes de transformación social.</p>

		<p class="item">Como cierre, los alumnos diseñaron una campaña gráfica y audiovisual para el día del aire puro 31 de Mayo, donde actuaron como agentes multiplicadores compartiendo con otros sus propios aprendizajes.</p>

		<img class="item img-responsive" src="{{ url('img/articles/healthy-lifestyle/tabaquismo.jpg') }}" alt="">
		
		{!! Html::image('img/articles/healthy-lifestyle/hl-article1-1.png', '', array('class' => 'item img-responsive')) !!}

		<div class="item well black">
			<h4>Tabaquismo</h4>
			
			{!! Html::image('img/articles/healthy-lifestyle/hl-article1-well.png', '', array('class' => 'img-responsive')) !!}

			<p>Este año los alumnos de Año 5 Nordelta trabajan principalmente la prevención del tabaquismo en el marco del estudio de los sistemas de nutrición del cuerpo humano y su interrelación. </p>

			<p>Los chicos indagan en el laboratorio sobre la anatomía de los sistemas respiratorio y circulatorio: observan un corazón, lo disecan e identifican cavidades y válvulas; arman un aparato de Funke para estudiar la fisiología normal del pulmón e investigan el recorrido del oxígeno desde el exterior hasta llegar a la célula que lo utilizará. </p>

			<p>Una vez sentadas las bases del buen funcionamiento de los sistemas se introduce el tabaquismo visto desde diferentes perspectivas: médica, histórica y social. Estudian los efectos del tabaco en el cuerpo, hacen un recorrido sobre las publicidades a lo largo del tiempo, estudian la ley antitabaco como sistema de protección y cómo las leyes y normas nos ayudan a vivir en comunidad. Corroboran mediante experimentos en el laboratorio la presencia de sustancias tóxicas en el humo del cigarrillo.</p>
		</div>
	</div>
@endsection


