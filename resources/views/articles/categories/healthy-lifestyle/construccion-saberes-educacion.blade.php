@extends('layouts.articlecolumns')

@section('sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('img/articles/healthy-lifestyle/evaluacion-land.jpg')}}" alt="">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('img/articles/healthy-lifestyle/evaluacion-port.jpg')}}" alt="">
	</div>
@endsection

@section('content')
	<div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> > 
		<a href="{{ url('/categories/healthy-lifestyle') }}">Estilo de vida saludable</a> > 
			<span>Construcción de saberes y evaluación</span> 
	</div>
	
	<h1>Construcción de saberes y evaluación</h1>
	<div class="columns">
		<p class="item">La evaluación de la Educación Física está integrada al proceso de enseñanza en el Nivel Inicial. Durante este proceso el docente y el alumno reflexionan acerca de las experiencias y sobre cómo éstas contribuyeron a la construcción de los saberes corporales, motrices, lúdicos y relacionales incluídos en el proyecto pedagógico o planificación. (Diseño Curricular para la Educación Física)</p>

		<p class="item">Este tipo de evaluación consiste en recoger evidencias que permiten mejorar el proceso de enseñanza/aprendizaje para luego planificar de manera focalizada y con un propósito definido a través del conocimiento de lo que el alumno ha comprendido.</p>

		<p class="item">Evidencias </p>
		<p class="item">A través de “listas de cojetos” registramos las habilidades y contenidos trabajados en cada periodo del año, indicando en qué instancia del aprendizaje se encuentra el alumno (Inicio, En proceso o En proceso avanzado). Esto surge a través de observaciones incidentales y/o intencionales que se vuelcan en una lista de indicadores de desempeño formulados a partir de las expectativas de logro determinadas para cada Sala.</p>

		<p class="item">Los contenidos corresponden a: </p>
		
		<ul class="item healthy-lifestyle-text">
			<li>La exploración de las posibilidades motrices,</li>
			<li>La toma de conciencia corporal en las diferentes acciones. La lateralidad en diferentes acciones motrices. </li>
			<li>El cuidado y la percepción del propio cuerpo y el de los otros.</li>
			<li>Las acciones motrices que impliquen ajustes visomotor, la coordinación dinámica general y específica.</li>
			<li>Las acciones motrices combinadas y secuenciadas.</li>
			<li>Y por último la acción de jugar. La asunción de roles, la aceptación de las reglas y del resultado.</li>
		</ul>

		{!! Html::image('img/articles/healthy-lifestyle/evaluacionPE.jpg', '', array('class' => 'item img-responsive')) !!}

		<div class="item well froly">
			<h4>Metacognición</h4>
			<p>Las Salas de 5 realizan una autoevaluación a través de una grilla (rúbrica de autoevaluación) con dibujos de las habilidades que se espera desarrollar en ese nivel. En ella, los niños señalan si dichas habilidades han sido alcanzadas eligiendo entre tres opciones: “está lograda”, “a veces lo logro” o “me cuesta pero lo estoy intentando”. En la rúbrica también se incluyen preguntas que traen a la conciencia el propio aprendizaje que le permiten al alumno focalizar en aquello que no ha logrado y en lo que debería hacer para lograrlo en el futuro.</p>

			<p>Nuestra responsabilidad está estrechamente ligada a la formulación de preguntas generadoras de conocimiento. Esta práctica pone en movimiento la retroalimentación por parte del maestro que permite a los niños apropiarse, no sólo de los contenidos sino también del proceso de aprender.</p>

			<p>La práctica de la retroalimentación genera un dialogo eficaz con los alumnos, un vínculo de confianza, comunicación fluida e intercambio de ideas, reflexiones, preguntas y dudas.</p>  
		</div>

	</div>
@endsection


