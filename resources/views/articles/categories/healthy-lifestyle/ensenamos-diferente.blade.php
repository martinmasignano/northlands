@extends('layouts.articlecolumns')

@section('sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('img/articles/healthy-lifestyle/run-land.jpg')}}" alt="">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('img/articles/healthy-lifestyle/run-port.jpg')}}" alt="">
	</div>
@endsection

@section('content')
	<div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> > 
		<a href="{{ url('/categories/healthy-lifestyle') }}">Estilo de vida saludable</a> > 
			<span>Enseñamos diferente</span> 
	</div>

	<h1>Enseñamos diferente</h1>
	<div class="columns">
		<p class="item">Fundamentalmente porque enseñamos para la vida y no simplemente para el éxito deportivo. Por lo tanto adecuamos las estrategias de enseñanza para que todos los alumnos encuentren un desafío de superación, se conecten con su cuerpo y la conciencia corporal sea nuestro modo de realizar una verdadera vivencia holística.</p>

		<p class="item">Nuestras clases están basadas en la enseñanza en valores y la metacognición con el propósito de que los alumnos adquieran competencias para la vida que puedan transferir a cualquier ámbito de desempeño; y se destaquen por la integridad de sus valores, para con ellos, los otros y el medio ambiente.</p>

		<h3 class="item">¿Qué hace que nuestras clases sean diferentes?</h3>

		<iframe class="item" src="https://player.vimeo.com/video/165455344" width="100%" height="240" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		
		<ul class="item healthy-lifestyle-text">
			<li>el análisis de video, análisis de sus prácticas,</li>
			<li>las reflexiones grupales,</li>
			<li>la enseñanza con pares,</li>
			<li>la autoevaluación y la co-evaluación,</li>
			<li>la toma de decisiones permanente,</li>
			<li>la resolución de problemas motores,</li>
			<li>el fuerte sentido de pertenencia, tanto con su House, como con los equipos del Colegio</li>
		</ul>
		
		<h3 class="item">Particularmente durante la etapa Primaria nos focalizamos en</h3>

		<p class="item">El seguimiento de nuestros alumnos para que cada uno logre mejorar a su máximo potencial.<br />
		El estímulo para que la práctica deportiva continúe más allá de la clase misma.<br />
		Las rubricas con indicadores de desempeño en cada deporte o actividad donde cada alumno puede proponerse metas y analizar sus logros.<br />
		El estímulo positivo para que nuestros alumnos disfruten de la actividad física en clase y se comprometan con sus equipos.<br />
		La evaluación para la mejora continua.<br />
		El trabajo en el sentido de pertenencia al House y al Colegio (Captains, carteleras, House Points, reflexiones grupales).<br />
		La importancia de representar al House, de vestir la camiseta de NORTHLANDS.</p>

		<h3 class="item">En Secundaria</h3>

		<p class="item">los proyectos interdisciplinarios (CAS-líderes, la carrera matemática, los juegos del mundo)<br />los proyectos específicos (pliometría, análisis de habilidades en rugby)<br />
		los proyectos sociales (Interhouse, Interschool, giras, carrera solidaria, etc.)<br />
		los proyectos de vida en la naturaleza y natación, deportes de situación, atletismo y cross country.<br />
		TLEC</p>

		<p class="item">(Tiempo Ludico y Expresión Corporal) lo recreativo, lo lúdico y el espacio de construcción colaborativa, los acuerdos, la negociación y el liderazgo)</p>

	</div>
@endsection


