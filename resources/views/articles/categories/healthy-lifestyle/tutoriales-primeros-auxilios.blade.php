@extends('layouts.articlecolumns')

@section('sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('img/articles/healthy-lifestyle/primeros-auxilios-land.jpg')}}" alt="">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('img/articles/healthy-lifestyle/primeros-auxilios-port.jpg')}}" alt="">
	</div>
@endsection

@section('content')
	<div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> > 
		<a href="{{ url('/categories/healthy-lifestyle') }}">Estilo de vida saludable</a> > 
			<span>Tutoriales de Primeros Auxilios</span> 
	</div>

	<h1>Tutoriales de Primeros Auxilios</h1>
	<div class="columns">
		<p class="item">es actuar rápidamente con técnicas y procedimientos para asistir al accidentado hasta que venga el médico o la ambulancia. Los primeros auxilios varían según la urgencia ya sea en reanimación cardiopulmonar (RCP), atragantamientos, hemorragias, heridas, fracturas, esguinces, intoxicaciones, golpes de calor, etc. </p>
		
		<p class="item">Por ello es muy importante saber cómo actuar en cada caso, desde una herida hasta salvar una vida.</p>

		<p class="item">Por este motivo, en NORTHLANDS todos los años los docentes y los padres que lo desean toman los cursos teórico-prácticos de RCP y atragantamiento.  Esto es especialmente importante dada la cantidad de alumnos y de adultos que asisten a la Institución, ya que debemos estar seguros de que podrán actuar adecuadamente en caso que haga falta ponerse en acción. </p>

		<p class="item">Hemos filmado una serie de videos de primeros auxilios que ayudarán a  los padres que asistieron a nuestros cursos a recordar lo que allí aprendieron. También preparamos  auras de realidad aumentada cuyas imágenes estarán por todo el Colegio, para que puedan acceder a ellos desde cualquier dispositivo Android o Apple.</p>

		<iframe class="item" src="https://player.vimeo.com/video/142245860" width="100%" height="213" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

		<iframe class="item" src="https://player.vimeo.com/video/142245861" width="100%" height="213" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

		<iframe class="item" src="https://player.vimeo.com/video/142245858" width="100%" height="213" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

	</div>
@endsection


