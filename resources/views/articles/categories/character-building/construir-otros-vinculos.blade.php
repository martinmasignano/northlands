@extends('layouts.articlecolumns')

@section('sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('img/articles/character-building/construir-land.jpg') }}" alt="">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('img/articles/character-building/construir-port.jpg') }}" alt="">
	</div>
@endsection

@section('content')
	<div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> > 
		<a href="{{ url('/categories/character-building') }}">Desarrollo del carácter</a> > 
			<span>Construir otros vínculos</span> 
	</div>

	<h1>Construir otros vínculos</h1>
	<div class="columns">
		<p class="item">A lo largo de su escolaridad, los alumnos de NORTHLANDS, se ven beneficiados por una experiencia significativa y enriquecedora: el reagrupamiento, que brinda a los niños la oportunidad de conocer nuevos amigos y de ser parte de un nuevo grupo. </p>

		<p class="item">La ventaja de dicha reorganización radica en la posibilidad que tienen todos los chicos de construir otros vínculos y de ser parte de nuevas dinámicas. </p>
		
		<p class="item">Sin perder de vista la importancia del grupo y de la identidad que este proporciona, esta nueva organización otorga la posibilidad de aprender a tejer redes vinculares dinámicas desde pequeños.</p>

		<p class="item">Para llevar a cabo esta reorganización con responsabilidad y a conciencia y con la intención de favorecer a todos nuestros alumnos, se trabaja con los niños en grupos tentativos que nos permiten observar dinámicas convenientes, o no, para armar los grupos definitivos. En ellos observamos básicamente la calidad de los encuentros entre pares que deberán ser buenos canales de conducción hacia óptimos aprendizajes tanto sociales como académicos. </p>
		
		<p class="item">Nuestra mirada está orientada al armado de grupos heterogéneos en lo que respecta a nivel académico, fortalezas y debilidades personales e intereses, con el objetivo de fomentar el enriquecimiento personal a partir de la diversidad.</p>

		{!! Html::image('img/articles/character-building/cb-article2-1.png', '', array('class' => 'item img-responsive')) !!}
	</div>
@endsection


