@extends('layouts.articlecolumns')

@section('sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('/img/articles/character-building/acompanar-land.jpg') }}" alt="">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('/img/articles/character-building/acompanar-port.jpg') }}" alt="">
	</div>
@endsection

@section('content')
	<div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> > 
		<a href="{{ url('/categories/character-building') }}">Desarrollo del carácter</a> > 
			<span>Acompañar el cambio</span> 
	</div>
	
	<h1>Acompañar el cambio</h1>
	<div class="columns">
		<p class="item">Acompañando el proceso que implica el periodo de adaptación en el Jardín, abordamos el eje: “Conciencia de uno mismo” (desarrollar la confianza en nosotros mismos a partir de un mejor conocimiento propio), dentro del programa de PSE (Personal and Social Education).</p>

		<p class="item">Luego del primer mes de clases presentamos al grupo de Sala de 5 una actividad con imágenes que representaban sentimientos (cansado, feliz, triste, tímido y nervioso). La propuesta consistía en que los chicos ubicaran sus nombres debajo de la imagen que mejor representaba cómo se sentían, con la posibilidad de fundamentar su elección, si ellos así lo querían.</p>

		<p class="item">La experiencia resultó muy rica e interesante ya que muchos de los niños pudieron expresar cómo se sentían y fundamentarlo. En algunos casos incluso, surgió la necesidad de buscar estrategias entre todos para que algún niño sintiera menos vergüenza o dejara de estar triste al despedirse de su mamá.</p>

		<p class="item">Unas semanas más tarde el grupo comenzaba sus clases de natación. Ante la ansiedad e inquietud que notamos en los chicos, volvimos a utilizar esta propuesta. En este caso les presentamos imágenes de: “nervioso, feliz, entusiasmado y asustado”. Antes de comenzar, una de las niñas sugirió también poner de “tímido” porque iban a conocer profesores nuevos.</p>
		
		<p class="item">Para nuestra sorpresa, el grupo volvió a mostrarse muy reflexivo y muchos más quisieron pasar a explicar la razón de su elección.</p>

		<p class="item">Las propuestas continuarán de acuerdo a las situaciones que se vayan presentando a lo largo del año para promover así, la reflexión y la posibilidad de cambio. De esta forma procuramos abordar la conciencia de los sentimientos de cada uno y la posibilidad de modificarlos de acuerdo a las sugerencias del grupo.</p>

		<p class="item">Maria Laura Windhausen,</p>
		<p class="item">Docente de Nivel Inicial, Olivos.</p>
		
		{!! Html::image('img/articles/character-building/cb-article1-1.png', '', array('class' => 'item img-responsive')) !!}

		{!! Html::image('img/articles/character-building/cb-article1-2.png', '', array('class' => 'item img-responsive')) !!}

	</div>
@endsection


