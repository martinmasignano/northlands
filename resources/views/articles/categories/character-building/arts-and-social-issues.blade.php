@extends('layouts.articlecolumns')

@section('sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('img/articles/character-building/arts-land.jpg') }}" alt="">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('img/articles/character-building/arts-port.jpg') }}" alt="">
	</div>
@endsection

@section('content')
	<div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> > 
		<a href="{{ url('/categories/character-building') }}">Desarrollo del carácter</a> > 
			<span>Arts and Social issues</span> 
	</div>

	<h1>Arts and Social issues</h1>
	<div class="columns">
		<p class="item">After observing and analysing examples of Realistic art pieces which evidence social issues, Year 8 students were asked to develop a realistic painting using mixed media.</p>
		
		<p class="item">The activity consisted in selecting and analysing an article that discusses a social aspect of our country reality and that reflects the values presented in our school's PSE program.</p>
		
		<p class="item">The students wrote a reflection on the topic chosen, explaining their idea, purpose and intention, looked for different visual references, sketched some possible compositions in black pencil, and used the IPAD application PICSART to plan their compositions combining images of interest.</p>
		
		<p class="item">The final outcome was an expressive work in relation to the chosen theme.</p>

		<a href="#" data-toggle="modal" data-target=".bs-example-modal-lg">
			{!! Html::image('img/articles/character-building/Art1.jpg', '', array('class' => 'item img-responsive')) !!}
		</a>
	</div>

	<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog">
		    <div class="modal-content">
		      	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

				  <!-- Wrapper for slides -->
				  <div class="carousel-inner">
						<div class="item active">
							<img src="{{ url('img/articles/character-building/Art1.jpg') }}" alt="">
						</div>

						<div class="item">
							<img src="{{ url('img/articles/character-building/Art2.jpg') }}" alt="">
						</div>

						<div class="item">
							<img src="{{ url('img/articles/character-building/Art3.jpg') }}" alt="">
						</div>

						<div class="item">
							<img src="{{ url('img/articles/character-building/Art4.jpg') }}" alt="">
						</div>

						<div class="item">
							<img src="{{ url('img/articles/character-building/Art5.jpg') }}" alt="">
						</div>

						<div class="item">
							<img src="{{ url('img/articles/character-building/Art6.jpg') }}" alt="">
						</div>

				  </div>
				  <!-- Controls -->
					<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
					</a>
					<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</div>
@endsection


