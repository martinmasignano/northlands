@extends('layouts.articlecolumns')

@section('sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('img/articles/character-building/diferente-land.jpg') }}" alt="">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('img/articles/character-building/diferente-port.jpg') }}" alt="">
	</div>
@endsection

@section('content')
	<div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> > 
		<a href="{{ url('/categories/character-building') }}">Desarrollo del carácter</a> > 
			<span>¿Diferente a mí?</span> 
	</div>

	<h1>¿Diferente a mí?</h1>
	<div class="columns">
		<p class="item">Entendemos la Conciencia de los Demás (uno de nuestros Ejes de PSE) como la capacidad de salir de uno mismo hacia el encuentro con un otro diferente a mí. Implica tomar conciencia de la existencia de la persona que está a mi lado entendiendo que el otro es un ser único y valioso. En este encuentro uno descubre que comparte similitudes así como aspectos en los que se diferencia. En NORTHLANDS afirmamos que estas diferencias son las que nos enriquecen y generan la oportunidad de aprender de los demás.</p>
		
		<p class="item">Con el fin de fortalecer estos conceptos el Colegio realiza la "Experiencia Audela”, una actividad enriquecedora que se lleva a cabo desde hace varios años. Esta actividad es organizada por Audela, una organización de la sociedad civil que busca concientizar y sensibilizar a la sociedad acerca de las habilidades que pueden desarrollar las personas con discapacidad.</p>

		<p class="item">Este año los alumnos EP2 participaron con gran entusiasmo de talleres en los que conocieron a personas con diferentes discapacidades, descubrieron cómo son sus vidas, sus desafíos diarios y se asombraron de forma positiva al reconocer todas las habilidades que lograron desarrollar.</p>

		<p class="item">Los alumnos a través de esta experiencia y gracias al trabajo previo que realizan junto a sus docentes logran reconocer que TODOS tenemos capacidades diferentes, todos tenemos talentos en los que nos destacamos y habilidades que nos cuesta más desarrollar; y es por esto que siempre debemos estar abiertos a recibir lo que el otro nos puede ofrecer para poder crecer como mejores seres humanos.</p>

		<iframe id="vp14DJ6c" class="item" title="Video Player" width="100%" height="260" frameborder="0" src="https://s3.amazonaws.com/embed.animoto.com/play.html?w=swf/production/vp1&e=1462195007&f=4DJ6cE0f5bHDxTceKPi1pw&d=0&m=p&r=260p&volume=100&start_res=260p&i=m&asset_domain=s3-p.animoto.com&animoto_domain=animoto.com&options=" allowfullscreen></iframe>

		<iframe id="vp1z4hj0" class="item" title="Video Player" width="100%" height="260" frameborder="0" src="https://s3.amazonaws.com/embed.animoto.com/play.html?w=swf/production/vp1&e=1462195096&f=z4hj0qtenLAvUjtUkdpa2A&d=0&m=p&r=260p&volume=100&start_res=260p&i=m&asset_domain=s3-p.animoto.com&animoto_domain=animoto.com&options=" allowfullscreen></iframe>

		<iframe id="vp1ULgTb" class="item" title="Video Player" width="100%" height="260" frameborder="0" src="https://s3.amazonaws.com/embed.animoto.com/play.html?w=swf/production/vp1&e=1462195236&f=ULgTbHCNNwpN7TVdD1EW0A&d=0&m=p&r=260p&volume=100&start_res=260p&i=m&asset_domain=s3-p.animoto.com&animoto_domain=animoto.com&options=" allowfullscreen></iframe>

	</div>
@endsection


