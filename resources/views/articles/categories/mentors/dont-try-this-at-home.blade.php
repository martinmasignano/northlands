@extends('layouts.articlecolumns')

@section('sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('img/articles/mentors/donttrythis_land.jpg')}}" alt="" class="pull-right">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('img/articles/mentors/donttrythis_port.jpg')}}" alt="" class="pull-right">
	</div>
@endsection

@section('content')
	<div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> > 
		<a href="{{ url('/categories/mentors-of-knowledge') }}">Mentores del conocimiento</a> > 
			<span>Dont't try this at home</span> 
	</div>

	<h1>Dont't try this at home</h1>
	<div class="columns">
		<p class="item">During the first unit of inquiry on Transformations and Changes, Year 6 students were faced to the following questions: How do we learn in Natural Science? How do we find out what we want to know? How is knowledge produced?<br />Everything begins with a question. Then, scientists think of what they expect the answer to the question might be and they develop a hypothesis.<br />The teacher showed Year 6 students what happened when Mentos candies were dropped into Diet Coke: a fountain effect was produced. Then, children were challenged to explain the cause of that cascade which surprised them with an explosion.<br />In groups, they elaborated several different hypotheses: some of them thought it was the combination of carbonated water in the coke and the mint in the candy; others predicted it was the combination of carbon dioxide, caffeine and the candy; or the combination of sugar in the coke and in the candy; others considered it depended on the temperature of the coke; others inferred that this cascade would occur in any type of coke or even in any type of soda or carbonated liquid.<br />Therefore, they designed different experiments to find out if their answers were right elaborating all the steps of their procedures.<br />That is what scientists do, they test their hypotheses.<br />The different groups asked for the different materials they would use: regular Coke, Diet Coke, Zero Coke, Sprite, carbonated water, mint leaves, sugar, cooking oil, ice, mint and strawberry Mentos candies, coffee, erlenmeyers, beakers, test tubes, corks, Bunsen burners, goggles. And then, tested their hypotheses by carrying out the experiments they had designed.<br />After all the experiments, they analysed their results and they elaborated their conclusions. Some of them discarded their hypothesis, others, verified them. They had learnt that if the hypothesis seems to be correct, it might become a theory. That means that most people accept the hypothesis as true.<br />They had also learnt that, sometimes, scientists discover new facts, and they have to change a theory. They might even decide to throw out a theory altogether and start over with a new hypothesis.<br />This is how Year 6 students learnt about the scientific method. Learning about the scientific method is learning how to learn.</p>

		<p class="item">María Inés Martinez, Year 6 Science Teacher</p>

		<img class="img-responsive" src="{{ url('img/articles/mentors/donttrythis.jpg')}}" alt="Don't try this at home">
		
		<div class="item well black-beware">
			<img class="img-absolute beware" src="{{ url('img/articles/mentors/explosivo.png')}}" alt="">
			<h4>BEWARE!</h4>
			<p>While working on their experiments, some new questions arose. They suddenly wondered if they would explode if they drank some Coke with a Mentos candy in their mouth. They asked their teacher to do it. The teacher did it. But, to their disappointment, she didn´t  explode, nor did a cascade come out of her mouth. She only described some fizz went on before swallowing the coke.<br />– Don´t try this at home, though.<br />You can end up with a stomach irritation.- </p>
		</div>
	</div>
@endsection


