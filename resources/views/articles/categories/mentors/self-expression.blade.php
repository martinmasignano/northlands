@extends('layouts.articlecolumns')

@section('sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('img/articles/mentors/selfexpressions_land.jpg')}}" alt="" class="pull-right">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('img/articles/mentors/selfexpressions_port.jpg')}}" alt="" class="pull-right">
	</div>
@endsection

@section('content')
	<div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> > 
		<a href="{{ url('/categories/mentors-of-knowledge') }}">Mentores del conocimiento</a> > 
			<span>Self Expression</span> 
	</div>

	<h1>Self Expression</h1>
	<div class="columns">
		<p class="item">IB Art students work throughout the last two years at School on the creation of a personal project that not only involves the continuity of the learning art at School but the expression of their personal self. Each one finds their personal style by the research on different artistic manifestations chosen for the purpose to help in their development of ideas, thoughts, creativity and technical competence. This is the reason why each student at the end of this process shows a series of works that are related in content and aesthetically; and on the other hand each student differs from the other class mates. </p>

		<p class="item">Our IB art students exhibit their work within the school community, showing their entire process and production with a Vernissage. This is a very important and joyful event that shows the end of a process which starts in Primary with the aim to develop the art skills and of the inner self.</p>

		<img src="img/articles/mentors/selfexpressions1.jpg" alt="">
		<a href="#" class="item articles-link">
			<img class="item img-responsive" src="{{ url('img/articles/mentors/selfexpressions.jpg')}}" alt="">
		</a>
	</div>
@endsection


