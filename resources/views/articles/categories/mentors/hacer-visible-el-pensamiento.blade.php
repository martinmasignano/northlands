@extends('layouts.articlecolumns')

@section('sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('img/articles/mentors/metacognicion_port.jpg')}}" alt="" class="pull-right">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('img/articles/mentors/metacognicion_port.jpg')}}" alt="" class="pull-right">
	</div>
@endsection

@section('content')
	<div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> > 
		<a href="{{ url('/categories/mentors-of-knowledge') }}">Mentores del conocimiento</a> > 
			<span>Hacer visible el pensamiento</span> 
	</div>

	<h1>Hacer visible el pensamiento</h1>
	<h3>Un libro sobre metacognición</h3>
	<div class="columns">
		<p class="item">Metacognición<br />
		<span class="mentors-text">(En pocas palabras)<br />
		Es el conocimiento que uno tiene acerca del propio proceso de aprendizaje y pensamiento. Es el preguntarse cómo aprendemos y pensamos Es la conciencia de la propia actividad cognitiva</span></p>

		<p class="item">Docentes, asesoras y la Dirección de nuestro Nivel Inicial de la sede Nordelta publicó, junto con la Universidad de San Andrés, el libro "Reflexión y Metacognición. Experiencias metacognitivas en el nivel inicial".</p>

		<p class="item">Publicado por la editorial AIQUE, este es el primer libro de su tipo producido colaborativamente por personal docente de una institución educativa. …si bien existe mucha bibliografía sobre metacognición,  casi no existen experiencias registradas para Nivel Inicial.  En NORTHLANDS aplicamos el trabajo metacognitivo de manera natural y casi “espontánea” ya que forma parte de la columna vertebral de nuestra forma de enseñar, en ese contexto decidimos avanzar un poco más y dejar un registro escrito de nuestras experiencias aúlicas para compartirlas con la comunidad educativa” explicó Susie Arndt, Directora de Nivel Inicial Nordelta y líder del proyecto de este libro.</p>

		<p class="item">¿Se puede trabajar la metacognición en Nivel Inicial? ¿A partir de qué edad podríamos hacerlo? ¿Con qué propósito? Estas fueron las preguntas iniciales que se planteó el equipo para comenzar a indagar sobre el tema. Como se describe en el capítulo 1, luego de muchas instancias de estudio, capacitación, investigación y reflexión, pasaron a experimentar en las aulas.<br />A través de estas experiencias descubrieron que cuando se trabaja metacognitivamente con los alumnos más pequeños se los ayuda a construir un grado mayor de autonomía, autoestima y seguridad. “Pensamos que, sin perder la especificidad del nivel inicial y respetando el lugar del espacio lúdico esencial en este momento evolutivo, incluir estrategias de enseñanza desde un enfoque que favorezca el desarrollo metacognitivo, tiene la ventaja de estimular la generación de  procesos de pensamiento más conscientes y eficaces en nuestros alumnos”.</p>

		<p class="item">“Enseñamos a los niños a pensar, a reflexionar sobre cómo aprenden, qué tareas pueden resolver mejor, qué  estrategias deberán utilizar para resolver una tarea específica, cómo deben planificar su tiempo y la mejor forma de resolución de una tarea. Pensamos que, en la complejidad del mundo actual, cuanto más temprano comencemos a desarrollar habilidades de pensamiento y dominio personal de las emociones, más oportunidades tendrá el alumno de aprovechar los saberes que se le presenten en los sucesivos niveles del sistema, de formar su capacidad de elegir y decidir, su juicio crítico y sus competencias para interactuar en forma empática y colaborativa con los demás”.</p>

		<p class="item">El equipo de escritores espera  que el libro estimule la creatividad del lector y lo invite a generar nuevas prácticas que aporten al enriquecimiento de la enseñanza en el nivel inicial y por qué no, en la escuela primaria.</p>

		<div class="item well yellow">
			<h4>Un trabajo colaborativo</h4>
			<p>Susana I. Arndt, Virginia Barceló, Leila Hordh, María Victoria Somoza, Alejandra Batu, Patricia Cairnie, Rosana Cescut, Constanza Fernández Garay, Paula Martin, María Florencia Duda, Bárbara Pereyra, María Revol, María Carolina Bernengo, Verónica Van Kregten, Claudia Andrea Varela, Geraldine Kaczor y María Teresa Gómez.</p>
		</div>

		<img src="{{ url('img/articles/mentors/metacognicion.jpg')}}" alt="" class="img-responsive">
		
		<iframe class="item" src="https://player.vimeo.com/video/170626539" width="100%" height="260" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

		<p class="item">Agradecemos a:	
		- Rebeca Anijovich y a Graciela Cappelletti, del equipo de UdeSA que nos capacitó en metacognición.<br />
		-Rebeca y a Elizabeth Gothelf que nos ayudaron con la organización, escritura y edición del libro<br />
		-A los docentes de Nivel Inicial por su esfuerzo y compromiso con el proyecto.</p>

	</div>
@endsection


