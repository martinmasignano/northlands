@extends('layouts.articlecolumns')

@section('sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('img/articles/mentors/metacognicion-forum_land.jpg')}}" alt="" class="pull-right">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('img/articles/mentors/metacognicion-forum_port.jpg')}}" alt="" class="pull-right">
	</div>
@endsection

@section('content')
	<div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> > 
		<a href="{{ url('/categories/mentors-of-knowledge') }}">Mentores del conocimiento</a> > 
			<span>Open Forum - </span> 
	</div>

	<h1>¿Cómo desarrollar <br/>
	la Metacognición * <br />
	en nuestros hijos?</h1>
	<div class="columns">
		
		<div class="item well yellow" style="background-image:url('{{ asset('/css/img/bg-mentors.svg') }}'); background-repeat: no-repeat; background-color: #fbbc00;">
			<h4><strong>APRENDER A APRENDER</strong></h4>
			<h4><em>Un concepto innovador en educación</em>
</h4>

			<p>En la complejidad del mundo actual, cuanto más temprano desarrollen habilidades de pensamiento y dominio personal de emociones, más oportunidades tendrán nuestros hijos de formar su capacidad de elegir, su juicio crítico y sus competencias para interactuar con los demás.</p>
		</div>
		<p class="item">Expondrán las autoras de <strong>"Reflexión y Metacognición”</strong>, el libro que Docentes, asesoras y la Dirección de Nivel Inicial de NORTHLANDS Nordelta publicó, junto con la Universidad de San Andrés.
</p><br />
		<a href="https://docs.google.com/forms/d/e/1FAIpQLSdcqKTVI4tod8bP3_Jr4aZkZHdyu7E9cLJZYFhhvilQkA1PDw/viewform"><img src="{{ url('img/articles/mentors/openForum.svg')}}" alt="Open Forum" class="img-responsive"></a>
		<br/>
		<h6><strong>* Metacognición</strong> (En pocas palabras)	Es el conocimiento que uno tiene acerca del propio proceso de aprendizaje y pensamiento. Es el preguntarse cómo aprendemos y pensamos Es la conciencia de la propia actividad cognitiva</h6>
	
		<div class="item well grey"><p> Artículo relacionado: <a href="http://www.northlands.edu.ar/articles/categories/mentors/hacer-visible-el-pensamiento">Hacer visible el pensamiento</a></p></div>

	</div>

		
@endsection


