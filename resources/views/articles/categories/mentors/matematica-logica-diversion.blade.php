@extends('layouts.articlecolumns')

@section('sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('img/articles/mentors/matematica_land.jpg')}}" alt="" class="pull-right">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('img/articles/mentors/matematica_port.jpg')}}" alt="" class="pull-right">
	</div>
@endsection

@section('content')
	<div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> > 
		<a href="{{ url('/categories/mentors-of-knowledge') }}">Mentores del conocimiento</a> > 
			<span>¿Matemática + Lógica = diversión?</span> 
	</div>

	<h1>¿Matemática + Lógica = diversión?</h1>
	<div class="columns">
		<p class="item">El conteo de una colección de objetos supone: el conocimiento de la serie numérica oral, la asignación de un número –y sólo uno- a cada objeto contado, reconocer que el último número mencionado representa la cantidad de objetos de todo el grupo y que no refiere sólo a ese objeto en particular.  Tal como aparece documentado en numerosos trabajos de investigación realizados en diferentes países, coordinar todos estos aspectos lleva a los niños muchos años de su desarrollo y de su escolaridad.<br />En Sala de 3, los conocimientos numéricos son bien diversos y el juego compartido es una ocasión de mucho enriquecimiento que pone a los alumnos en situación de interactuar con los procedimientos y las ideas de otros. Una condición para el avance de estos conocimientos es sostener el juego durante un tiempo prolongado, de modo tal que lleguen a dominar las estrategias que despliegan en él.</p>

		<iframe class="item" src="https://player.vimeo.com/video/142256161" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

		<div class="item well orange">
			<h4>EL JUEGO DE LAS ZANAHORIAS</h4>
			<p>En mesas de a cuatro, nuestros pequeños juegan a “Zanahorias”</p>
			<p>¿Qué conocimientos numéricos entran en escena para jugarlo?</p>
			<p>Por un lado, necesitan identificar cuántas zanahorias señala la aguja. Esta cantidad (de 1 a 4) aparece  identificada de dos maneras: mediante una colección de zanahorias dibujadas y con la escritura numérica correspondiente. Algunos niños sólo recurren a la identificación del dígito anotado, otros cuentan los dibujos, sobre todo cuando la cantidad es mayor que dos. Luego, es necesario  “cosechar” esa cantidad de zanahorias de la huerta propia.<br />
			Algunos niños, al ver más de una zanahoria, dicen directamente que se trata de dos (como sinónimo de plural) o de tres (tanto para tres o cuatro) porque advierten que son más que dos. En otros casos, al contar sobre la zanahorias, cometen errores en la correspondencia entre cada objeto contado y la enunciación del nombre de un número. Otros leen directamente el número o cuentan respetando la  asignación de un número a cada zanahoria. A la hora de recolectar, especialmente cuando son más de dos, algunos juntan varias, luego corrigen contándolas, otros las cuentan de entrada, otros las cuentan con errores de conteo, etc.<br />Los errores mencionados forman parte del proceso de aprendizaje de la serie numérica y su utilización en situaciones de conteo, proceso que involucra a todo el Nivel Inicial y a los primeros años de la escuela Primaria. Contar es una actividad muy compleja que los adultos tenemos muy “naturalizada”.<br />Nuestra intención es que los niños comiencen a utilizar los números y se movilicen relaciones que potencien esa construcción de conocimientos.</p>
		</div>

		<img src="{{ url('img/articles/mentors/matematica.jpg')}}" alt="" class="item img-responsive">

		<div class="item well light-green">
			<h4>TALLERES EN PRIMARIA</h4>
			<p>En los talleres semanales de Matemática se alcanzó un interesante vínculo: unir la Matemática y la Lógica con lo lúdico, trasformando las horas de juego en momentos creativos, de gran participación y utilidad.<br />La propuesta  es  el juego que permite, además del disfrute, un tipo de análisis intelectual cuyas características son muy semejantes a las que presenta el desarrollo matemático:</p>
			<p>- Relaciona la matemática con una situación generadora de diversión.<br/>
			- Favorece  la  resolución de problemas usando diferentes procedimientos: explorar, analizar, comparar.<br/>
			- Utiliza el error como oportunidad de aprendizaje.<br/>
			- Busca el diálogo, el debate de ideas, fomentando el respeto hacia la diversidad de opiniones.<br/>
			- Promueve el ingenio, la creatividad y la imaginación.<br/>
			- Favorece el trabajo-juego en equipo, dando la oportunidad a que cada miembro pueda tener la posibilidad de explicar el procedimiento.<br/>
			- El juego permite la retroalimentación inmediata.</p>

			<p>Fanny Morales, Vicedirectora y Asesora de matemática, Primaria Olivos.</p>
		</div>
	</div>
@endsection


