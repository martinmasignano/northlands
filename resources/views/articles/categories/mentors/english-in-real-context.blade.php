@extends('layouts.articlecolumns')

@section('sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('img/articles/mentors/english_land.jpg')}}" alt="" class="pull-right">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('img/articles/mentors/english_port.jpg')}}" alt="" class="pull-right">
	</div>
@endsection

@section('content')
	<div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> > 
		<a href="{{ url('/categories/mentors-of-knowledge') }}">Mentores del conocimiento</a> > 
			<span>English in real context</span> 
	</div>

	<h1>English in real context</h1>
	<div class="columns">
		<p class="item">English in Secondary school at NORTHLANDS means developing worthy skills of communication that will stay with the students for ever. It means learning to use English in real contexts and situations. It means thinking critically and owning up to opinions and conclusions about our reality. And most importantly, it entails giving the students a tool that brings them closer to international contexts, and to international interests, since the beginning of their lives.</p>

		<p class="item">This wondrous path begins in Year 7, when students start to gain more independence as a result of their passage to Secondary school. And little by little, this independence gives them the opportunity to explore the current issues of the world in English, which in turn gives them both instruction and ability in the use of the language. This is a journey that takes them through the best works of literature, a wide variety of writing genres, the exposure to real English on a daily basis, opportunities to obtain invaluable resources in their oral production, and the ability to think critically in the target language. In this way, the preparation for the IGCSE exams in conjunction with the IB diploma gives the students integral academic and personal education. </p>

		<p class="item">But academia is not our only purpose. Where is the personal side to all this?  In line with the School values, <br/><span class="text-danger">the students learn to develop their own opinions about the world and about themselves in English. They learn and practise those values that we, in our own homes and as members of a family, try to forge in the young. </span><br/>Through Literature, we teach the NORTHLANDS students to be aware of and be responsible for themselves, we teach them to appreciate kind and quality of the works we study, we show them that it is important to care for our neighbour through the themes we explore, we foster their ability to become more sensitive and empathic with the world that surrounds them. And thus, this is what ties our purposes together. Through doing, we learn. And this is the NORTHLANDS way. So in the class, both academic learning and personal development come to one.</p>
		
		<p class="item">In conclusion, English at NORTHLANDS Secondary school is an opportunity to think, value, and share opinions; to learn, use and enjoy the language; and to develop ourselves and grow with the language of internationality. It is our purpose to grant the students a means of communication that will help them lead a life with more and better opportunities in the future. This is simply why we are hard at work.</p>

		<h3><a target="_blank" href="http://www.northlands.edu.ar/extensive/N1589O753/Galleries2015/son_iPad/son_iPad.php">iThink, iCreate, iSpeak, iPad</a></h3>		

		<h3><a target="_blank" href="http://www.northlands.edu.ar/extensive/N1589O753/Galleries2015/so_GeneralKnowledge/so_GeneralKnowledge.php">General Knowledge Interhouse<br /><small>2015 Secondary</small></a></h3>
	</div>
@endsection


