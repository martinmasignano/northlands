@extends('layouts.articlecolumns')

@section('sidebar')
	<div class="visible-xs-block visible-sm-block">
		<img src="{{ url('img/articles/mentors/tooyoungtolearn_land.jpg')}}" alt="" class="pull-right">
	</div>
	<div class="hidden-xs hidden-sm">
		<img src="{{ url('img/articles/mentors/tooyoungtolearn_port.jpg')}}" alt="" class="pull-right">
	</div>
@endsection

@section('content')
	<div class="breadcrumb">
		<a href="{{ url('/') }}">Inicio</a> > 
		<a href="{{ url('/categories/mentors-of-knowledge') }}">Mentores del conocimiento</a> > 
			<span>Too young to learn</span> 
	</div>

	<h1>Too young to learn</h1>
	<div class="columns">
		<p class="item mentors-text text-center">Teaching the habits of mind in Kindergarten can contribute significantly in the task of getting both teachers and students to reflect on the fascinating area of how we think, learn and work in the classroom. It has the potential of helping us to understand the transforming power of metacognition and the use of the language of thinking in our everyday work.</p>

		<p class="item">According to Art Costa, not only do the Habits of Mind help make cognitive activities more successful, but when the teacher alerts the students to which habits are being applied, or when the teacher proposes other habits, then the students are able, through reflection and self-evaluation, to begin to see how to apply the habits to all areas of study.</p>

		<p class="item">Students begin to realize that they need to think communally, persist in the process and use other habits to resolve specific problems or situations that they encounter in the process of learning.  When they complete a task, they are invited to think about the way they thought through the problem as well as how to express their emotions through the habits such as responding with wonder and awe or interest in new ideas, persisting through continual effort to complete the task, applying humour, among others.</p>

		<p class="item">The habits can be expressed through words using the language of thinking and emotions.  Each habit is represented by a few descriptive words and a picture, since visualization contributes to the learning process.<br />I decided to teach the following habits of mind as part of our annual Project to include and apply metacognition strategies in our everyday work as teachers, using a simpler language.</p>

		<p class="item">During the presentation of different activities in the classroom, I have established the routine of asking the students which habits of the mind they thought were necessary before starting the activity and then again during the activity.<br/> Very quickly they started to incorporate the words, LISTEN, THINK, PERSIST, and we are now working to complete the list.</p>

		<p class="item">El equipo de escritores espera  que el libro estimule la creatividad del lector y lo invite a generar nuevas prácticas que aporten al enriquecimiento de la enseñanza en el nivel inicial y por qué no, en la escuela primaria.</p>

		<iframe src="https://player.vimeo.com/video/167463272" width="100%" height="240" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

		<iframe src="https://player.vimeo.com/video/167465035" width="100%" height="240" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

		<div class="item well yellow">
			<h4>How to teach them?</h4>
			<p>I started to incorporate the habit, “Listen” into my class.  I showed the students a picture of an ear.<br />I asked them what it might mean and quickly the word LISTEN was suggested.  I asked the students to think about different situations when we need to listen attentively and why it was important to do so.  The group started to practise this habit in the classroom and to listen to each other carefully.<br />When the habit “Persist” was introduced, I showed the students a picture of a mountain with a flag on top of it.<br />They were captivated immediately and I asked them what it might mean but the students could only describe what they saw and not interpret the meaning of the picture so I decided to create a dramatization of the picture in order to help them understand this habit of mind.</p>
		</div>
	</div>
@endsection


