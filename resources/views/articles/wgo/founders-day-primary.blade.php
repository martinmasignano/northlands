@extends('layouts.articlecolumns')

@section('sidebar')
	{!! Html::image('img/wgo/galleries/FoundersDayPrimaria/WGO_FoundersDayPrimaria.jpg', '', array('class' => '')) !!}
@endsection

@section('content')
	<h1>Founder's Day / <i>Día de la Fundadora</i></h1>
	<h2><i>Primary Olivos</i></h2>
	<h3><i><a href="{{ url('wgo') }}">What's going on?</a></i></h3>
	
	<div class="columns">
		<p>On March 3 we honoured our roots by remembering Mrs. Mary Parczweski, Headmistress of NORTHLANDS from 1969 to 1982.</p>
		<p>The Year 6 captains also referred to the ladies who gave origin to our houses.</p>

		<p>El 31 de marzo honramos nuestras raíces recordando a la Sra. Mary Parczewski, Directora de NORTHLANDS desde 1969 hasta 1982.</p>
		<p><i>Los capitanes de Año 6 también hicieron referencia a las señoras que dieron origen a nuestras houses.</i></p>
	</div>
@endsection

@section('gallery')
	<div class="row">
		<div class="galleries-container">
			<div class="gallery-slider">
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayPrimaria/imagen_01.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayPrimaria/imagen_02.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayPrimaria/imagen_03.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayPrimaria/imagen_04.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayPrimaria/imagen_05.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayPrimaria/imagen_06.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayPrimaria/imagen_07.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayPrimaria/imagen_08.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayPrimaria/imagen_09.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayPrimaria/imagen_10.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayPrimaria/imagen_11.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayPrimaria/imagen_12.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayPrimaria/imagen_13.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayPrimaria/imagen_14.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayPrimaria/imagen_15.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayPrimaria/imagen_16.jpg', '', array('class' => 'img-slider')) !!}
				</div>
			</div>
		</div>
	</div>
@endsection
