@extends('layouts.articlecolumns')

@section('sidebar')
	{!! Html::image('img/wgo/galleries/FoundersDayKinderSecundaria/WGO_FoundersDay.jpg', '', array('class' => '')) !!}
@endsection

@section('content')
	<h1>Founder's Day / <i>Día de la Fundadora</i></h1>
	<h2><i>Kindergarten & Secondary Nordelta</i></h2>
	<h3><i><a href="{{ url('wgo') }}">What's going on?</a></i></h3>

	<div class="columns">
		<p>Kindergarten and Secondary pupils celebrate together Founder's day.</p>

		<p><i>Alumnos de Nivel Inicial y Secundaria conmemoran juntos el día de nuestra Fundadora.</i></p>

		<iframe src="https://player.vimeo.com/video/162580192" width="100%" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
	</div>
@endsection

@section('gallery')
	<div class="row">
		<div class="galleries-container">
			<div class="gallery-slider">
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayKinderSecundaria/imagen_01.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayKinderSecundaria/imagen_02.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayKinderSecundaria/imagen_03.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayKinderSecundaria/imagen_04.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayKinderSecundaria/imagen_05.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayKinderSecundaria/imagen_06.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayKinderSecundaria/imagen_07.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayKinderSecundaria/imagen_08.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayKinderSecundaria/imagen_09.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayKinderSecundaria/imagen_10.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayKinderSecundaria/imagen_11.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayKinderSecundaria/imagen_12.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayKinderSecundaria/imagen_13.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayKinderSecundaria/imagen_14.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayKinderSecundaria/imagen_15.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayKinderSecundaria/imagen_16.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayKinderSecundaria/imagen_17.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayKinderSecundaria/imagen_18.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayKinderSecundaria/imagen_19.jpg', '', array('class' => 'img-slider')) !!}
				</div>

				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/FoundersDayKinderSecundaria/imagen_20.jpg', '', array('class' => 'img-slider')) !!}
				</div>
			</div>
		</div>
	</div>
@endsection


