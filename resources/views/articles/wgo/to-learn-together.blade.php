@extends('layouts.articlecolumns')

@section('sidebar')
    <div class="visible-xs-block visible-sm-block">
        <img src="{{ url('img/wgo/galleries/ToLearnTogether/WGO_ToLearnTogether_L.jpg')}}" alt="To learn together">
    </div>
    <div class="hidden-xs hidden-sm">
        <img src="{{ url('img/wgo/galleries/ToLearnTogether/WGO_ToLearnTogether_P.jpg')}}" alt="To learn together">
    </div>
@endsection

@section('content')
	<h1>To learn together / <i>Aprender juntos</i></h1>
	<h2><i>May 2016 / Year 2 - Primary Olivos</i></h2>
	<h3><i><a href="{{ url('wgo') }}">What's going on?</a></i></h3>

	<div class="columns">
		<p>On Thursday 12 May we had the pleasant visit of Mr Reeves to our Year 2 violin class. He participated with us of learning how to hold the arch and the correct position of his feet on the rug. The students were in charge of teaching him and explaining how to do it. We enjoyed sharing our class with him. ¡Thank you Mr Reeves for your visit!</p>

		<p><i>El jueves 12 de mayo recibimos la grata visita del Sr. Reeves a nuestra clase de violín de Año 2. Participó con nosotros de la toma del arco y de la posición de los pies para tocar en la alfombrita. Los alumnos fueron quienes le enseñaron y le explicaron cómo realizarlo. Disfrutamos de compartir la clase con él. ¡Gracias Sr. Reeves por la visita!</i></p>
	</div>
@endsection

@section('gallery')
	<div class="row">
		<div class="galleries-container">
			<div class="gallery-slider">
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/ToLearnTogether/imagen_01.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/ToLearnTogether/imagen_02.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/ToLearnTogether/imagen_03.jpg', '', array('class' => 'img-slider')) !!}
				</div>
			</div>
		</div>
	</div>
@endsection

