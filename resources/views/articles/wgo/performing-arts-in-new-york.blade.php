@extends('layouts.articlecolumns')

@section('sidebar')
	{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/WGO_PerformingArtsNewYork.jpg', '', array('class' => '')) !!}
@endsection

@section('content')
	<h1>Performing Arts in New York / <i>Artes Interpretativas en Nueva York</i></h1>
	<h2><i>Secondary Olivos & Nordelta</i></h2>
	<h3><i><a href="{{ url('wgo') }}">What's going on?</a></i></h3>

	<div class="columns">
		<p>The purpose of the trip was to complement the different areas of the IB by attending an intensive study programme on Theatre and Visual Arts.</p>

		<p>The programme included outings to different Broadway and Off Broadway productions, thus appreciating different theatrical traditions of the world in New York, where theatre abounds in quantity and quality. We visited universities such as Parsons, Pratt and academies such as NY Film Academy, NY Broadway Dance Centre and Art Student League.</p>

		<p>The objectives of this trip also involve familiarization with the culture, practicing oral English, appreciating a cosmopolitan city such as New York, touring its principal touristy attractions, museums and the different neighbourhoods.</p>

		<p><i>El viaje tuvo como propósito complementar las diferentes áreas del IB asistiendo a un intensivo programa de estudios de Artes Visuales y Teatro.</i></p>

		<p><i>El programa incluyó salidas al teatro (Broadway y off Broadway) como así también distintas tradiciones teatrales del mundo presentadas en Nueva York, donde el teatro abunda tanto en cantidad como en calidad.</i></p>
		
		<p><i>Visitamos universidades como Parsons, Pratt y academias como New York Film Academy, Broadway Dance Center y el Student League.</i></p>

		<p><i>El objetivo de este viaje también involucra familiarizarse con otra cultura, practicar el idioma Inglés, apreciar una ciudad cosmopolita, visitando los puntos turísticos de interés, museos y diferentes zonas de Nueva York.</i></p>
	</div>
@endsection

@section('gallery')
	<div class="row">
		<div class="galleries-container">
			<div class="gallery-slider">
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_01.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_02.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_03.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_04.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_05.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_06.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_07.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_08.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_09.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_10.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_11.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_12.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_13.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_14.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_15.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_16.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_17.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_18.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_19.jpg', '', array('class' => 'img-slider')) !!}
				</div>

				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_20.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_21.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_22.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_23.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_24.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_25.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_26.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_27.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_28.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_29.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_30.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_31.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_32.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_33.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_34.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_35.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_36.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_37.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_38.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_39.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_40.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_41.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_42.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_43.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_44.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_45.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_46.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_47.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_48.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_49.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_50.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_51.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_52.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_53.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_54.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_55.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_56.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_57.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_58.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_59.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_60.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_61.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_62.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_63.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_64.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_65.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_66.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_67.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_68.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_69.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_70.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_71.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_72.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_73.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_74.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/PerformingArtsNewYorkSecundaria/imagen_75.jpg', '', array('class' => 'img-slider')) !!}
				</div>
			</div>
		</div>
	</div>
@endsection

