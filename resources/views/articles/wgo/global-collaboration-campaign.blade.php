@extends('layouts.articlecolumns')

@section('sidebar')
	{!! Html::image('img/wgo/galleries/GlobalCollaborationCampaign/WGO_GlobalCollaborationCampaign.jpg', '', array('class' => '')) !!}
@endsection

@section('content')	
	<h1>Global Collaboration Campaign  / <i>Campaña de Colaboración Global</i></h1>
	<h2><i>2015-2016 - Primary Olivos</i></h2>
	<h3><i><a href="{{ url('wgo') }}">What's going on?</a></i></h3>

	<div class="columns">
		<p>We are excited to announce a Global Collaboration campaign with three other IB PYP schools called “IB learners connect for Global Issues 2015”. During last winter 2015, a teacher from NORTHLANDS-Olivos, Ms Helena Griffin, visited Westlake Academy, in Westlake, Texas, where an International Mindedness Educator Symposium was hosted. The goal throughout the workshop was to learn how to flatten our classrooms into a global learning environment, break down prejudices and to collaborate and connect with other IB schools around the world. We are proud to be partnering with Westlake Academy, Instituto Anglo Britanico, in Monterrey Mexico, and Pinkerton, Coppell throughout the 2015-2016 school years.</p>

		<p>The three IB schools first sent a “global digital handshake” to the learners from our 5th grade classes and also constructed a common website for posting along with face-to-face opportunities for skyping or google hangouts as we address global issues. Eventually, through choice and passion for a common global concern, the learners collaborated together working towards a project to showcase their knowledge, solutions for the problems worldwide, and personal reflections.</p>

		<p>This collaboration will involve research concerning today’s global issues, sharing blogs, creating a project, posting pictures, SKYPING and Google Hangouts.</p>

		<p>The case selected was <a href="https://www.limbsinternational.org/" target="blank">Limbs International</a>. Inspired by the IB persuasive commercials the students created for common cause for our school has collected usd 2522. We now estimate that this sum will cater for around 8 prothesis, making the lives of childern in need of one, more pleasant.</p>

		<p>Thanks to NORTHLANDS’ school community support last Service Day in April we believe that we have given an important step towards the development of our learners in becoming a global citizen.</p>

		<p><i>Estamos muy entusiasmados de anunciar una campaña de Colaboración Global con otros tres colegios IB PEP denominado “Alumnos IB se conectan para temas Globales 2015”. En el invierno del 2015 una docente de NORTHLANDS Olivos, la Sra. Helena Griffin, visitó Westlake Academy en Westlake, Texas, donde se realizaba un Simposio sobre el Educador con Mentalidad Internacional. El objetivo del taller fue aprender a ‘aplanar nuestras aulas’ (eliminar los límites de las paredes mediante el uso de la tecnología) para convertirlas en ambientes de aprendizaje global, romper prejuicios y colaborar y conectar con otros colegios IB alrededor del mundo. Estamos orgullosos de asociarnos con Westlake Academy, el Instituto Anglo Británico en Monterrey, México, y con Pinkerton, Coppell, durante los años escolares de 2015 y 2016.</i></p>

		<p><i>México, y con Pinkerton, Coppell, durante los años escolares de 2015 y 2016. Los tres colegios IB primero enviaron un “apretón de manos digital global” a los alumnos de nuestro Año 5 y también construyeron un sitio web común para posteo junto con oportunidades para comunicaciones cara a cara por Skype o hangouts de Google mientras abordamos temas globales. Finalmente, a través de la elección y pasión por una preocupación global común, los alumnos colaboraron juntos trabajando hacia un proyecto para mostrar su conocimiento, soluciones a problemas mundiales y reflexiones personales.</i></p>

		<p><i>Esta colaboración comprendió la indagación sobre cuestiones globales actuales, el compartir blogs, la creación de un proyecto, el posteo de fotos, comunicación por Skype y por Google Hangouts.</i></p>
		
		<p><i>El caso elegido fue <a href="https://www.limbsinternational.org/" target="blank">Limbs International</a>. Inspirados por el IB, los convincentes comerciales creados por los alumnos para una causa común para nuestro colegio han reunido u$s 2522. Ahora estimamos que esta cifra cubrirá alrededor de 8 prótesis, haciendo más placentera la vida de niños que los necesitan.</i></p>

		<p><i>Gracias al apoyo de la comunidad del colegio NORTHLANDS al último Service Day en abril, creemos que hemos dado un paso importante hacia el desarrollo de nuestros alumnos para que se conviertan en ciudadanos globales.</i></p>

		<iframe src="https://player.vimeo.com/video/164991754" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		
	</div>
@endsection

@section('gallery')
	<div class="row">
		<div class="galleries-container">
			<div class="gallery-slider">
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/GlobalCollaborationCampaign/imagen_01.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/GlobalCollaborationCampaign/imagen_02.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/GlobalCollaborationCampaign/imagen_03.jpg', '', array('class' => 'img-slider')) !!}
				</div>
			</div>
		</div>
	</div>
@endsection


