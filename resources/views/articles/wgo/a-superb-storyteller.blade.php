@extends('layouts.articlecolumns')

@section('sidebar')
	{!! Html::image('img/wgo/galleries/ASuperbStorytellerSecundariaPrimaria/WGO_SuperbStoryteller.jpg', '', array('class' => '')) !!}
@endsection

@section('content')
	<h1>A Superb storyteller / <i>Una narradora Magnifica</i></h1>
	<h2><i>Olivos & Nordelta</i></h2>
	<h3><i><a href="{{ url('wgo') }}">What's going on?</a></i></h3>

	<div class="columns">
		<p>On March 14 & 15, Canadian award winning author Vicki Grant talked to our students from Year 5 to Year 8 about her novels and what goes into creating them. She gave them useful tips to use in their own creative writings and was a stimulation to read books for fun.</p>

		<p><i>El 14 y 15 de marzo la premiada escritora canadiense Vicki Grant, conversó con los alumnos de Años 5 a 8 acerca de sus novelas y el proceso de escribirlas. Les dio interesantes consejos para que utilicen en sus propias escrituras creativas y fue un estímulo para motivar la lectura recreativa.</i></p>
	</div>
@endsection

@section('gallery')
	<div class="row">
		<div class="galleries-container">
			<div class="gallery-slider">
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/ASuperbStorytellerSecundariaPrimaria/imagen_01.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/ASuperbStorytellerSecundariaPrimaria/imagen_02.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/ASuperbStorytellerSecundariaPrimaria/imagen_03.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/ASuperbStorytellerSecundariaPrimaria/imagen_04.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/ASuperbStorytellerSecundariaPrimaria/imagen_05.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/ASuperbStorytellerSecundariaPrimaria/imagen_06.jpg', '', array('class' => 'img-slider')) !!}
				</div>
			</div>
		</div>
	</div>
@endsection
