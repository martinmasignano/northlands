@extends('layouts.articlecolumns')

@section('sidebar')
    <div class="visible-xs-block visible-sm-block">
        <img src="{{ url('img/wgo/galleries/2015PrizeGivingCeremony/WGO_2015PrizeGivingCeremony_L.jpg')}}" alt="2015 Prize Giving Ceremony">
    </div>
    <div class="hidden-xs hidden-sm">
        <img src="{{ url('img/wgo/galleries/2015PrizeGivingCeremony/WGO_2015PrizeGivingCeremony_P.jpg')}}" alt="2015 Prize Giving Ceremony">
    </div>
@endsection

@section('content')
	<h1>2015 Prize Giving Ceremony / <i>Ceremonia de Entrega de Premios 2015</i></h1>
	<h2><i>Primary Olivos</i></h2>
	<h3><i><a href="{{ url('wgo') }}">What's going on?</a></i></h3>
	
	<div class="columns">
		<p>2015 Olivos Primary Prize Giving was held on December 17th. Year 6 flag bearers and escorts, Sophia Kehayoglu, Siena Torres, Alicia Rothuizen, Alix Daireaux, Tristán Lacase, Santos de Elía, Máximo Mander, Inés Leguizamón and Malena Laborda Moreno, transferred the Flags of Ceremony to Year 5 pupils who would then be the flag bearers and escorts in our Opening Ceremony in 2016.

		<p>The National Flag was handed to Magdalena Delfino, Valentina Murphy Brown and Marco Brizzio. The Buenos Aires Province Flag was handed to Simón Nofal, Candelaria Murphy Brown and Tomas Llorente. The School Flag was handed to Blanca Sahores, Milo Urrea Torres and Pablo Budai.
		
		<p>These were students who had an outstanding academic performance, demonstrated outstanding commitment towards School values and/or showed integrity and friendship consistently.
		
		<p>Year 1 to Year 6 prizes, All Round Excellence, Excellence in English, Excellence in Spanish, Effort and Progress and Friendship and Service, were given out together with PE colours and house points.
		
		<p>Also, a special prize was awarded to Year 6 student Santos De Elía for his creative writing productions which were commendable throughout the whole year.
		
		<p>The School Choir performed three songs (Music is My Life, Auld Lang Syne, Señales de Tránsito) and both Mr Nick Reeves and Ms Elsa González addressed the audience and shared farewell words.
		
		<p>Prize winner parents may access <a href="https://drive.google.com/a/northlands.edu.ar/folderview?id=0BwfRAS0qRkPZeTNFUlA1a0ZQTnM&usp=sharing">this link to download pictures in high resolution.</a>

		<p><i>El 17 de diciembre último se llevó a cabo la entrega de Premios de fin año. Escoltas y abanderados de Año 6, Sophia Kehayoglu, Siena Torres, Alicia Rothuizen, Alix Daireaux, Tristán Lacase, Santos de Elía, Máximo Mander, Inés Leguizamón and Malena Laborda Moreno, realizaron el pasaje de las Banderas de Ceremonia a los alumnos de Año 5 que serían los escoltas y abanderados en la Ceremonia de Inauguración del ciclo escolar 2016.</i></p>

		<p><i>La Bandera Nacional fue entregada a Magdalena Delfino, Valentina Murphy Brown and Marco Brizzio. La Bandera Provincial fue entregada a Simón Nofal, Candelaria Murphy Brown and Tomas Llorente. La Bandera del Colegio fue entregada a Blanca Sahores, Milo Urrea Torres and Pablo Budai.</i></p>

		<p><i>Estos son alumnos que tuvieron un excelente desempeño académico, evidenciaron un compromiso destacado con los valores del colegio y/o demostraron integridad y amistad en forma consistente.</i></p>

		<p><i>Durante la ceremonia se entregaron los premios de All Round Excellence, Excellence in English, Excellence in Spanish, Effort and Progress and Friendship and Service, junto con los PE colours y house points de Año 1 a Año 6.</i></p>

		<p><i>Además, el alumno de Año 6, Santos De Elía, recibió una mención especial por sus destacadas producciones escritas realizadas a lo largo del año.</i></p>

		<p><i>El Coro del colegio interpretó tres piezas musicales: Music is My Life, Auld Lang Syne y Señales de Tránsito. Luego, el Sr Nick Reeves y la Sra Elsa González compartieron palabras de despedida con todos los presents. Los familiares de los alumnos premiados pueden <a href="https://drive.google.com/a/northlands.edu.ar/folderview?id=0BwfRAS0qRkPZeTNFUlA1a0ZQTnM&usp=sharing">clickear este link para acceder a las imágenes en alta resolución.</a></i></p>

	</div>
@endsection

@section('gallery')
	<div class="row">
		@include('galleries/2015-prize-giving-ceremony/index')
	</div>
@endsection

@section('related')
	<div class="row">
		<h3>Related Galleries /<em>Galerías Relacionadas</em></h3>
		<div class="wgo-grid">
			@include ('wgo/wgoblocks/2015-prize-giving-ceremony')
		</div>
	</div>
@endsection

