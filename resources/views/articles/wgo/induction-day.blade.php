@extends('layouts.articlecolumns')

@section('sidebar')
	{!! Html::image('img/wgo/galleries/InductionDay/WGO_InductionDay.jpg', '', array('class' => '')) !!}
@endsection

@section('content')
	<h1>Induction Day / <i>Día de Inducción</i></h1>
	<h2><i>Secondary Olivos & Nordelta</i></h2>
	<h3><i><a href="{{ url('wgo') }}">What's going on?</a></i></h3>

	<div class="columns">
		<p>Like every year, we welcome students from Year 7 to the Secondary section. We are aware of the changes involved in taking this step and that is why we think it is important to give them a special place and a warm welcome. To do this, we have organized a breakfast in order to begin to know each other, play, talk and have fun. The kids were as happy and anxious as we were. It was a great day!!! We share some pictures.</p>

		<p><i>Como todos los años, les damos la bienvenida a los alumnos de Año 7 en la sección secundaria. Sabemos del cambio que implica dar este paso y es por ello que nos parece importante darles un lugar especial y recibirlos cálidamente. Por esto hemos organizado un desayuno con el objetivo de poder comenzar a conocernos mutuamente, jugar, conversar y divertirnos mucho. Los chicos estaban felices y expectantes al igual que nosotros. ¡¡¡Fue un gran día!!! Aquí, algunas fotos.</i></p>
	</div>
@endsection

@section('gallery')
	<div class="row">
		<div class="galleries-container">
			<div class="gallery-slider">
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_01.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_02.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_03.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_04.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_05.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_06.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_07.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_08.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_09.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_10.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_11.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_12.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_13.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_14.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_15.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_16.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_17.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_18.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_19.jpg', '', array('class' => 'img-slider')) !!}
				</div>

				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_20.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_21.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_22.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_23.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_24.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_25.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_26.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_27.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_28.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_29.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_30.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_31.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_32.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_33.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_34.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_35.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_36.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_37.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_38.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_39.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_40.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_41.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_42.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_43.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_44.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_45.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_46.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_47.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_48.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_49.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_50.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_51.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/InductionDay/imagen_52.jpg', '', array('class' => 'img-slider')) !!}
				</div>
			</div>
		</div>
	</div>
@endsection

