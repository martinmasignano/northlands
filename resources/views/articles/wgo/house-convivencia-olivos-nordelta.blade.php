@extends('layouts.articlecolumns')

@section('sidebar')
	{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaNordelta/WGO_HouseConvivencia.jpg', '', array('class' => '')) !!}
@endsection

@section('content')
	<h1>House Convivencia / <i>Convivencia de Houses</i></h1>
	<h2><i>Olivos & Nordelta</i></h2>
	<h3><i><a href="{{ url('wgo') }}">What's going on?</a></i></h3>

	<div class="columns">
		<p>On the 15th of March the Interhouse Secondary Convivencia took place in Nordelta. At first, the captains had to talk about the life of these incredible women and the reason why Mrs. Brightman chose them as models to be followed. Last year we realized that many people didn´t know basically who they were and why were they chosen representatives of the School moto Friendship and Service. Then they voted for the year leaders of each House and shared a breakfast together.</p>

		<p>Later, each house had already been divided in three groups, taking into account the different student abilities in order to accomplish the work the best they could. One group was in charge of working in the drama festival. The idea was to make a change, from the start. Instead of having one captain writing the script alone, we challenged them to try to work the idea together. Thirty creative minds can do better than one. Captains who in the beginning were afraid that maybe many people would like to go to the other groups, couldn´t believe how everybody got involved, participated, were fascinated with the final outcome and by then end were able to divide the work in teams responsible of the different areas that involved the creation of a `play.</p>

		<p>A second group was in charge of looking for significant thoughts or ideas that spoke about House Spirit. Once they had the idea they had to paint them on wooden plates to be hanged around the school to remind students in their everyday life about what house spirit is about.</p>

		<p>The third group had to create mobiles with the colours of the house and decorated them with concepts making reference to school spirit and ideals. We are highly satisfied with the final result. But we are really pleased to see that students could enjoy the activity with commitment and responsibility. There was a captain in charge of each of the groups and were challenged to put into practice their leadership resources in order to accomplish the objective.</p>

		<p><i>El 15 de Marzo tuvo en lugar en Nordelta la Convivencia de houses de la sección secundaria.</i></p>

		<p><i>Primero cada house se reunió con todos sus miembros para hablar de quiénes fueron estas mujeres y por qué Mrs. Brightman las eligió como modelos a seguir. El año pasado descubrimos que muchos alumnos desconocían la vida de estos seres ejemplares y por lo tanto el motivo por el cual habían sido elegidas como representantes del lema del colegio Friendship and Service. Luego se votaron los líderes de año y todo el house compartió un desayuno al aire libre.</i></p>

		<p><i>Después del desayuno se había dividido con anterioridad el house en tres grupos, teniendo en cuenta las habilidades de los alumnos para el mejor cumplimiento de la tarea a realizar.</i></p>

		<p><i>Uno de los grupos iba estar a cargo del drama festival. La idea este año fue correr un riesgo y cambiar la dinámica y trabajar la idea de forma conjunta. Treinta mentes creativas puede más que una. Al principio los capitanes a cargo de este grupo tenían de temor de que muchos quisieran pasarse, grande fue sus asombros al ver que todos se comprometían y participaban. Quedaron muy satisfechos con el resultado final, no sólo conformes con la idea lograda sino que pudieron armar los distintos grupos de trabajo que se harían cargo de la creación del play.</i></p>

		<p><i>Un segundo grupo tuvo a cargo la tarea de buscar ideas y pensamientos que hicieran referencia al House Spirit. Una vez que definían la idea debían pintarlas en placas de madera que luego serán colgadas alrededor de la escuela para que los alumnos tengan presente en su vida cotidiana la implicancia del significado de House Spirit.</i></p>

		<p><i>El tercer grupo, tenía como objetivo crear móviles con los colores del house y decorarlos con conceptos que hicieran referencia a House Spirit e ideales.</i></p>

		<p><i>Estamos sumamente satisfechos con los resultados logrados. Estamos muy contentos ya que los alumnos no sólo disfrutaron de la actividad, sino que lo hicieron con compromiso y responsabilidad. Había un capitán a cargo de cada actividad, desafiándolos a desarrollar sus habilidades y recursos como líderes para llevar adelante la tarea.</i></p>
	</div>
@endsection

@section('gallery')
	<div class="row">
		<div class="galleries-container">
			<div class="gallery-slider">
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaNordelta/imagen_01.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaNordelta/imagen_02.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaNordelta/imagen_03.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaNordelta/imagen_04.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaNordelta/imagen_05.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaNordelta/imagen_06.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaNordelta/imagen_07.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaNordelta/imagen_08.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaNordelta/imagen_09.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaNordelta/imagen_10.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaNordelta/imagen_11.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaNordelta/imagen_12.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaNordelta/imagen_13.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaNordelta/imagen_14.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaNordelta/imagen_15.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaNordelta/imagen_16.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaNordelta/imagen_17.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaNordelta/imagen_18.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaNordelta/imagen_19.jpg', '', array('class' => 'img-slider')) !!}
				</div>

				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaNordelta/imagen_20.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaNordelta/imagen_21.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaNordelta/imagen_22.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaNordelta/imagen_23.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaNordelta/imagen_24.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaNordelta/imagen_25.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaNordelta/imagen_26.jpg', '', array('class' => 'img-slider')) !!}
				</div>
			</div>
		</div>
	</div>
@endsection


