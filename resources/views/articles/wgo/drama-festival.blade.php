@extends('layouts.articlecolumns')

@section('sidebar')
	{!! Html::image('img/wgo/galleries/DramaFestival/WGO_DramaFestival.jpg', '', array('class' => '')) !!}
@endsection

@section('content')
	<h1>Drama Festival</h1>
	<h2><i>April 2016 - Secondary Olivos</i></h2>
	<h3><i><a href="{{ url('wgo') }}">What's going on?</a></i></h3>

	<div class="columns">
		<p>The 2016 Drama Festival took place in Olivos on Monday, April 25. This year, each one of the four Houses wrote an original play inspired on a different superhero. Nightingale won both Best direction and Overall production. After months of hard work we could enjoy some excellent performances.</p>
		
		<p><i>El Drama Festival 2016 tuvo lugar en Olivos el lunes 25 de abril. Este año cada una de las cuatro Houses escribió un texto original inspirado en diferentes superhéroes. Nightingale ganó tanto el premio a la Mejor dirección como el de Producción general. Después de varios meses de arduo trabajo pudimos disfrutar de algunas excelentes representaciones.</i></p>
	</div>
@endsection

@section('gallery')
	<div class="row">
		<div class="galleries-container">
			<div class="gallery-slider">
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_01.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_02.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_03.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_04.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_05.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_06.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_07.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_08.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_09.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_10.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_11.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_12.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_13.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_14.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_15.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_16.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_17.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_18.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_19.jpg', '', array('class' => 'img-slider')) !!}
				</div>

				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_20.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_21.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_22.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_23.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_24.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_25.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_26.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_27.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_28.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_29.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_30.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_31.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_32.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_33.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_34.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_35.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_36.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_37.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_38.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_39.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_40.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_41.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_42.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_43.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_44.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_45.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_46.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_47.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_48.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_49.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_50.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_51.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_52.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_53.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_54.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_55.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_56.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_57.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_58.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_59.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/DramaFestival/imagen_60.jpg', '', array('class' => 'img-slider')) !!}
				</div>
			</div>
		</div>
	</div>
@endsection

