@extends('layouts.articlecolumns')

@section('sidebar')
	{!! Html::image('img/wgo/galleries/Chaco2015AssemblySecundaria/imagen_03.jpg', '', array('class' => '')) !!}
@endsection

@section('content')
	<h1>Chaco 2015 Assembly</h1>
	<h2><i>Secondary Nordelta</i></h2>
	<h3><i><a href="{{ url('wgo') }}">What's going on?</a></i></h3>

	<div class="columns">
		<p>In July last year, almost one hundred girls and boys from both sites of NORTHLANDS School travelled to five rural schools in the thick Chaco forest to live for a week amongst students and teachers from there. For many it was the first participation in this project and for others the opportunity to repeat it and be reunited with the communities to stengthen bonds and make progress with the activities devised during the year. A valuable service and learning experience that this year celebrates its 25th anniversary.</p>

		<p><i>En Julio del año pasado, cerca de cien chicas y chicos del colegio NORTHLANDS de ambas sedes viajaron a cinco escuelas rurales en el impenetrable Chaqueño, para convivir durante una semana con alumnos y maestros de allá. Para muchos fue la primera participación en este proyecto y para otros la posibilidad de repetirla y reencontrarse nuevamente con las comunidades, para reforzar los vínculos y avanzar con las actividades diseñadas durante el año. Una valiosa experiencia de servicio y aprendizaje que este año cumple su 25º Aniversario.</i></p>

		<p>It is our responsibility as a school community to promote international perspectives in our children and students and to support an international dimension throughout the curriculum. The international dimension is about students recognizing the world’s interconnections, becoming aware of themselves and others and being able to understand and value their own and others’ perspectives.</p>

		<p>See the projects, the schools, the scenery and all the spirit of the "Chaco 2015"project.</p>

		<p>Mirá los proyectos, las escuelas, los paisajes y todo el espíritu del proyecto "Chaco 2015"</p>

		<iframe src="https://player.vimeo.com/video/161192513" width="100%" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		
	</div>
@endsection

@section('gallery')
	<div class="row">
		<div class="galleries-container">
			<div class="gallery-slider">
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/Chaco2015AssemblySecundaria/imagen_01.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/Chaco2015AssemblySecundaria/imagen_02.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/Chaco2015AssemblySecundaria/imagen_03.jpg', '', array('class' => 'img-slider')) !!}
				</div>
			</div>
		</div>
	</div>
@endsection


