@extends('layouts.articlecolumns')

@section('sidebar')
    <div class="visible-xs-block visible-sm-block">
        <img src="{{ url('img/wgo/galleries/AbilityToCommunicate/WGO_Ability2Communicate_L.jpg')}}" alt="Ability to communicate">
    </div>
    <div class="hidden-xs hidden-sm">
        <img src="{{ url('img/wgo/galleries/AbilityToCommunicate/WGO_Ability2Communicate_P.jpg')}}" alt="Ability to communicate">
    </div>
@endsection

@section('content')
	<h1>Ability to communicate / <i>Habilidad para comunicarnos</i></h1>
	<h2><i>April 2016 - Nordelta Secondary</i></h2>
	<h3><i><a href="{{ url('wgo') }}">What's going on?</a></i></h3>

	<div class="columns">
		<p>Under the PSE program, with Year 9 students, we began the school year working on the proposed axis: Ability to communicate. From the search into music they got in touch with what identified them. Then, they worked to put it into words and images, and finished the activity creating a flyer which communicates who they are, how they feel and what are they going through.</p>

		<p><i>En el marco del programa de PSE, con los alumnos de Año 9, comenzamos el año escolar trabajando sobre el eje propuesto: Habilidad para comunicarnos. A partir de la indagación en la música que les gusta fuimos poniéndonos en contacto con eso que los identifica. Luego, trabajamos en poder ponerlo en palabras e imágenes, y finalizamos creando un flyer que comunique quienes somos, cómo nos sentimos, qué nos pasa.</i></p>
	</div>
@endsection

@section('gallery')
	<div class="row">
		<div class="galleries-container">
			<div class="gallery-slider">
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/AbilityToCommunicate/imagen_01.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/AbilityToCommunicate/imagen_02.png', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/AbilityToCommunicate/imagen_03.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/AbilityToCommunicate/imagen_04.jpg', '', array('class' => 'img-slider')) !!}
				</div>
			</div>
		</div>
	</div>
@endsection


