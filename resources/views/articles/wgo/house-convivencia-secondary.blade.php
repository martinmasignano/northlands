@extends('layouts.articlecolumns')

@section('sidebar')
	{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/WGO_HouseConvivenciaOlivos.jpg', '', array('class' => '')) !!}
@endsection

@section('content')
	<h1>House 'Convivencia' / <i>Convivencia de Houses</i></h1>
	<h2><i>Secondary Olivos</i></h2>
	<h3><i><a href="{{ url('wgo') }}">What's going on?</a></i></h3>

	<div class="columns">
		<p>This year, the Olivos House Convivenciatook place on Wednesday, March 9 during the whole morning. At 8 am, there was an assembly for which the captains had prepared audiovisual presentations about the great women after whom the Houses were named. The members of each House, staff and students, shared breakfast in the senior building, and then the students carried out House activities until lunch time. It was an excellent opportunity for the House captains to strengthen their role as leaders.</p>

		<p><i>Este año el "Encuentro por Houses" de Olivos tuvo lugar el miércoles 9 de marzo durante toda la mañana. A las 8 hs hubo un assembly para el cual los capitanes habían preparado presentaciones audiovisuales sobre las grandes mujeres que dieron nombre a las Houses. Los miembros de cada House, docentes y alumnos, compartieron el desayuno en el edificio de secundaria y luego los alumnos realizaron actividades de House hasta el mediodía. Fue una excelente oportunidad para que los capitanes fortalecieran su rol como líderes.</i></p>

		<h5>Nightingale</h5>
		<iframe src="https://player.vimeo.com/video/159111887" width="100%" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

		<h5>Keller</h5>
		<iframe src="https://player.vimeo.com/video/159111884" width="100%" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		
		<h5>Fry</h5>
		<iframe src="https://player.vimeo.com/video/159111880" width="100%" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		
		<h5>Cavell</h5> 
		<iframe src="https://player.vimeo.com/video/159111877" width="100%" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
	</div>
@endsection

@section('gallery')
	<div class="row">
		<div class="galleries-container">
			<div class="gallery-slider">
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_01.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_02.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_03.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_04.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_05.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_06.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_07.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_08.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_09.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_10.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_11.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_12.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_13.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_14.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_15.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_16.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_17.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_18.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_19.jpg', '', array('class' => 'img-slider')) !!}
				</div>

				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_20.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_21.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_22.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_23.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_24.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_25.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_26.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_27.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_28.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_29.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_30.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_31.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_32.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_33.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_34.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_35.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/HouseConvivenciaSecundariaOlivos/imagen_36.jpg', '', array('class' => 'img-slider')) !!}
				</div>
			</div>
		</div>
	</div>
@endsection


