@extends('layouts.articlecolumns')

@section('sidebar')
    <div class="visible-xs-block visible-sm-block">
        <img src="{{ url('img/wgo/galleries/3rdPrizeCISArtProject/WGO_3rdPrizeCISArtProject_L.jpg')}}" alt="3rd Prize - CIS Art Project">
    </div>
    <div class="hidden-xs hidden-sm">
        <img src="{{ url('img/wgo/galleries/3rdPrizeCISArtProject/WGO_3rdPrizeCISArtProject_P.jpg')}}" alt="3rd Prize - CIS Art Project">
    </div>
@endsection

@section('content')
	<h1>3rd Prize - CIS Art Project / <i>3er Premio - Proyecto de Arte del CIS</i></h1>
	<h2><i>March - Secondary Nordelta</i></h2>
	<h3><i><a href="{{ url('wgo') }}">What's going on?</a></i></h3>

	<div class="columns">
		<p>At the end of last year CIS also launched an A​rt Contest inviting a large number of Schools around the world to show how their students embrace intercultural perspectives through creativity and self expression. Four hundred entries were received, among them works from Ignacio Beines Furcada in Year 11 Olivos and from Pía Volpino in Year 10 Nordelta and we are extremely proud to announce that Pía Volpino won the 3rd Prize and an Honourable Mention​. Apart from the creativity and high quality of her submitted work,​ Pía delivered a most articulate explanation of what images and social contrasts had inspired her work. It accurately depicted a global concern: the permanent manifestation of social inequality. She had the opportunity to share both the work sent to the contest and her portfolio with the advisors during the <a href="http://www.northlands.edu.ar/extensive/N1589O753/Galleries2016/kpson_CIS_visit/kpson_CIS_visit.php" target="_blank">CIS visit</a>.</p>
		<p><a href="http://www.northlands.edu.ar/extensive/N1589O753/Galleries2015/sn_IGCSE_art_PiaVolpino/sn_IGCSE_art_PiaVolpino.php" target="_blank">>>See more about Pía's project and its process</a></p>

		<p><i>A fines del año pasado, el CIS lanzó un concurso de arte invitando a un gran número de colegios de todo el mundo a mostrar la manera en que sus estudiantes adoptan perspectivas interculturales a través de la creatividad y la libre expresión. Hubo cuatrocientos concursantes, entre ellos Ignacio Beines Furcada de Año 11 de la sede Olivos y Pía Volpino de Año 10 de la sede Nordelta. Estamos muy orgullosos de su participación y compromiso y nos complace anunciar que en la ronda final Pía Volpino ganó el 3er Premio y recibió una mención de honor.</i></p>

		<p><i>Volpino de Año 10 de la sede Nordelta. Estamos muy orgullosos de su participación y compromiso y nos complace anunciar que en la ronda final Pía Volpino ganó el 3er Premio y recibió una mención de honor.</i></p>
		
		<p><i>Además de la calidad y creatividad de su trabajo, Pía formuló una elocuente explicación acerca de las imágenes y los contrastes sociales que la inspiraron. Representó una preocupación global con precisión: la manifestación permanente de la desigualdad social. Durante la <a href="http://www.northlands.edu.ar/extensive/N1589O753/Galleries2016/kpson_CIS_visit/kpson_CIS_visit.php" target="_blank">visita de los asesores del CIS​</a>, Pía tuvo la oportunidad de compartir con ellos el material enviado al concurso como también el portafolio de su obra.</i></p>

		<p><a href="http://www.northlands.edu.ar/extensive/N1589O753/Galleries2015/sn_IGCSE_art_PiaVolpino/sn_IGCSE_art_PiaVolpino.php" target="_blank">>>>Ver más acerca del proyecto de Pía y su proceso</a></p>
	</div>
@endsection

@section('gallery')
	<div class="row">
		@include('galleries/3rd-prize-cis-art-project/index')
	</div>
@endsection

