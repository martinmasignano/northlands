@extends('layouts.articlecolumns')

@section('sidebar')
	{!! Html::image('img/wgo/galleries/BigBrotherBigSister/WGO_BigBrother.jpg', '', array('class' => '')) !!}
@endsection

@section('content')
	<h1>Big Brother-Big Sister Programme / <i>Programa de Padrinazgo</i></h1>
	<h2><i>Primary Olivos</i></h2>
	<h3><i><a href="{{ url('wgo') }}">What's going on?</a></i></h3>

	<div class="columns">
		<p>Once again we have organised the Big Brother-Big Sister Programme for Year 6 and Year 1 pupils. This project aims to strengthen bonds between our students creating an atmosphere of friendship and harmony. For this, Year 6 and Year 1 students shared a meeting in which they played different group games. After that Year 1 children had some individual time with their Big Brother or Big Sister. Meet our Big Brothers/ Sisters and Little Brothers/ Sisters 2016!</p>

		<p><i>Una vez más hemos organizado el Programa de Padrinazgo de los alumnos de Año 6 a los alumnos de Año 1. Este proyecto tiene como fin fortalecer los vínculos entre nuestros alumnos generando un clima de compañerismo y armonía. Para esto, los alumnos de Año 6 y Año 1 compartieron un encuentro donde realizaron distintos juegos en grupo y luego compartieron un momento individual junto a sus padrinos. ¡Conozcan a nuestros padrinos y ahijados 2016! </i></p>
	</div>
@endsection

@section('gallery')
	<div class="row">
		<div class="galleries-container">
			<div class="gallery-slider">
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_01.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_02.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_03.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_04.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_05.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_06.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_07.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_08.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_09.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_10.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_11.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_12.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_13.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_14.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_15.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_16.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_17.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_18.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_19.jpg', '', array('class' => 'img-slider')) !!}
				</div>

				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_20.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_21.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_22.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_23.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_24.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_25.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_26.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_27.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_28.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_29.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_30.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1A/imagen_31.jpg', '', array('class' => 'img-slider')) !!}
				</div>

				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_01.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_02.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_03.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_04.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_05.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_06.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_07.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_08.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_09.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_10.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_11.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_12.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_13.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_14.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_15.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_16.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_17.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_18.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_19.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_20.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_21.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_22.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_23.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_24.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_25.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_26.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_27.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_28.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_29.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_30.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_31.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_32.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_33.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_34.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_35.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_36.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_37.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_38.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_39.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_40.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_41.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_42.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_43.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_44.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1B/imagen_45.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_01.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_02.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_03.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_04.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_05.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_06.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_07.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_08.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_09.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_10.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_11.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_12.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_13.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_14.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_15.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_16.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_17.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_18.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_19.jpg', '', array('class' => 'img-slider')) !!}
				</div>

				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_20.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_21.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_22.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_23.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_24.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_25.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_26.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_27.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_28.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_29.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_30.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_31.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_32.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_33.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_34.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_35.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_36.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_37.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_38.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_39.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_40.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_41.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_42.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_43.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_44.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_45.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_46.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_47.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_48.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_49.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_50.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_51.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_52.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/BigBrotherBigSister/BigBrothersY1C/imagen_53.jpg', '', array('class' => 'img-slider')) !!}
				</div>
			</div>
		</div>
	</div>
@endsection


