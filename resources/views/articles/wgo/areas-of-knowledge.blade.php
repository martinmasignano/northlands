@extends('layouts.articlecolumns')

@section('sidebar')
	{!! Html::image('img/wgo/galleries/AreasOfKnowledge/WGO_AreasOfKnowledge.jpg', '', array('class' => '')) !!}
@endsection

@section('content')
	<h1>Areas of Knowledge / <i>Áreas del conocimiento</i></h1>
	<h2><i>April 2016 - Primary Olivos</i></h2>
	<h3><i><a href="{{ url('wgo') }}">What's going on?</a></i></h3>
	
	<div class="columns">
		<p>Presentations for parents:  / <i>Presentaciones para padres:</i></p>

		<p><a href="https://drive.google.com/file/d/0B4hyrt-JZNKnNmZtLXB1X0hFTUlsMXE1dXpvXzlWX09SVzFZ/view?usp=sharing">Mathematics / <i>Matemática</i></a><br />
		<a href="https://drive.google.com/file/d/0BwfRAS0qRkPZNTRxS1F4TmliYzQ/view?usp=sharing">Spanish Language / <i>Prácticas del lenguaje</i></a></p>

		<p>On Tuesday 12 April we had an informative meeting on Spanish Language Practice and Mathematics with the respective Primary consultants of each of these subject areas.</p>

		<p><i>El martes 12 de abril tuvimos las reuniones informativas de Prácticas del lenguaje y Matemática con las respectivas asesoras de Primaria de cada una de estas áreas.</i></p>
	</div>
@endsection

@section('gallery')
	<div class="row">
		<div class="galleries-container">
			<div class="gallery-slider">
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/AreasOfKnowledge/imagen_01.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/AreasOfKnowledge/imagen_02.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/AreasOfKnowledge/imagen_03.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/AreasOfKnowledge/imagen_04.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/AreasOfKnowledge/imagen_05.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/AreasOfKnowledge/imagen_06.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/AreasOfKnowledge/imagen_07.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/AreasOfKnowledge/imagen_08.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/AreasOfKnowledge/imagen_09.jpg', '', array('class' => 'img-slider')) !!}
				</div>
			</div>
		</div>
	</div>
@endsection

