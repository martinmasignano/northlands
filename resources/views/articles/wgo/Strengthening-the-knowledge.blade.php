@extends('layouts.articlecolumns')

@section('sidebar')
	{!! Html::image('img/wgo/galleries/StrengtheningThe Knowledge/WGO_StrengtheningTheKnowledge.jpg', '', array('class' => '')) !!}
@endsection

@section('content')
	<h1>Strengthening the knowledge / <i>Reforzando el conocimiento</i></h1>
	<h2><i>April 2016 - Secondary Olivos & Nordelta</i></h2>
	<h3><i><a href="{{ url('wgo') }}">What's going on?</a></i></h3>

	<div class="columns">
		<p>As part of the IB Literature syllabus, we taught the play "The House of Bernarda Alba" by Federico García Lorca. Last Saturday we went to Belgrano Auditorium with Year 12 students from both sites to watch the play. The students were accompanied by members of the Spanish Department. It was a very successful evening which will most probably strengthen the knowledge and understanding of the play which the students will need to demonstrate in their November exam.</p>

		<p><i>Dentro de la Parte 2 del Programa de Literatura de IB incluimos la obra teatral de Federico García Lorca "La casa de Bernarda Alba". El sábado pasado asistimos con alumnos de Año 12 tanto de Olivos como de Nordelta a una representación de la obra realizada en el Auditorium de Belgrano. Los alumnos asistieron acompañados por integrantes del departamento de Lengua y fue una velada provechosa con la que esperamos reforzar el conocimiento de la pieza teatral a los efectos del examen final en noviembre próximo. </i></p>
	</div>
@endsection

@section('gallery')
	<div class="row">
		<div class="galleries-container">
			<div class="gallery-slider">
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/StrengtheningThe Knowledge/imagen_01.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/StrengtheningThe Knowledge/imagen_02.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/StrengtheningThe Knowledge/imagen_03.jpg', '', array('class' => 'img-slider')) !!}
				</div>
				<div class="gallery-slide">
					{!! Html::image('img/wgo/galleries/StrengtheningThe Knowledge/imagen_04.jpg', '', array('class' => 'img-slider')) !!}
				</div>
			</div>
		</div>
	</div>
@endsection

