@extends('layouts.gallery')
@section('section')
<section id="section1" class="grid-item">
@overwrite

@section('image')
    {!! Html::image('img/articles/mentors/metacognicion_thumb.jpg', '', array('class' => 'img-responsive')) !!}
@overwrite

@section('caption')
    <a class="caption" href="{{ url('articles/categories/mentors/hacer-visible-el-pensamiento') }}">
        <h3><strong>Hacer visible el pensamiento</strong></h3>
    </a>
@overwrite

@section('place')
    
@overwrite
