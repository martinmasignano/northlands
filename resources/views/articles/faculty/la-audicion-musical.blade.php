{{-- Language set in lang/{language}/aboutus/faculty/faculty/faculty-detail.php --}}

@extends('layouts.main')

@section('content_class','faculty-detail')
@section('content')

	<div class="row">
		<div class="col-xs-12">
			<div class="breadcrumb">
				<a href="{{ url('/') }}">Inicio</a> > 
				<a href="{{ url('/aboutus/faculty/faculty') }}">Equipo docente</a> > 
				<span>La audición musical de los más pequeños</span> 
			</div>
			<h1>La audición musical de los más pequeños</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-8">
			<div class="columns">
	    		<p>Fomentar la audición musical contribuye al desarrollo del oído, de la capacidad de atención y de la búsqueda estética. Ampliar el horizonte cultural y estimular el placer de escuchar música es uno de los objetivos que perseguimos en NORTHLANDS desde Nivel Inicial.</p>
                
                <div class="media">
                    <div class="media-left">
                        {!! Html::image('/img/faculty/baremboim.png', 'Daniel Baremboim. El sonido es vida. El poder de la música', array('class' => 'media-object')) !!}
                    </div>
                    <div class="media-body">
                        <p>Daniel Baremboim, en su libro El sonido es vida*, escribió: <i>“No basta con oír la música sino que también hay que escucharla para comprender la narración musical. Escuchar es oír acompañado de pensamiento, del mismo modo que el sentimiento es emoción acompañada de pensamiento”.</i></p>
                    </div>
                </div>

                <p>Nuestro oído es un órgano que está siempre alerta y conectado y, a pesar de esto, está menos desarrollado que la vista. El oído canaliza la escucha al cerebro que la decodifica para nuestra comprensión.</p>

                <p>Los niños están abiertos a cualquier estímulo que les ofrezcamos, todo lo absorben, entonces, ¿cuál es el desafío de guiarlos en el entrenamiento auditivo? Hay que expandir su apreciación del fenómeno sonoro y ofrecer a sus cerebros en crecimiento herramientas para comprender y poder nombrar aquello que oyen.</p>

                <p>El primer paso se da en el Jardín de Infantes: acostumbrarlos a que la música requiere atención.</p>

                <p>Distinguimos un momento y un lugar dentro de la clase y transmitimos una actitud de escucha, una manera de estar alertas y dispuestos. Baremboim escribe: <i>“La única condición para escuchar música es que no sea una actitud pasiva”</i>. En nuestras manos está brindarles música acorde a su edad y, poco a poco, complejizar las propuestas, presentarles algo nuevo, un instrumento, un sonido, música de culturas lejanas.</p>

	    		<iframe src="https://player.vimeo.com/video/178226128?title=0&byline=0&portrait=0" width="100%" height="260" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

	    		<p>A medida que crecen incorporamos preguntas que guían la audición: ¿qué instrumentos escuchan? ¿Cómo se dan cuenta? ¿Quiénes y cuántos cantan? ¿Son hombres, mujeres, niños? ¿Con qué elementos estarán imitando al viento en esta canción? ¿Qué nos contó la historia que cantaban?.</p>

                <p>Las preguntas repetidas los ayudan a estar preparados ante un nuevo estímulo musical para saber a qué tienen que prestar atención. En este punto es muy interesante el trabajo en grupo ya que cada chico aporta sus propias experiencias sonoras de acuerdo a lo que escucha en casa, a los instrumentos que conoce y entre todos suman conocimiento.</p>

                <p>A medida que crecen (6, 7, 8 años) ya pueden reconocer estilos y expresar sus gustos utilizando un vocabulario cada vez más preciso. <strong class="text-yellow">También comienzan a comprender que la música es inseparable de la cultura y de la época que la genera y que a través de ella podemos acercarnos a otros pueblos y otros pensamientos.</strong></p>

                <p>Los maestros de música damos el puntapié inicial ofreciéndoles experiencias musicales novedosas y guiándolos en la comprensión del valor de toda manifestación musical.</p>

                <div class="well yellow">
                    <p><i>“La capacidad de escuchar varias voces al mismo tiempo (…), la capacidad de recordar un tema que hizo su primera entrada (…) y luego reaparece a una luz diferente y la habilidad auditiva (…) son todas ellas cualidades que potencian la comprensión. Quizás el efecto acumulativo de estas habilidades y capacidades podría formar seres humanos más aptos para escuchar y entender varios puntos de vista al mismo tiempo, más capaces de valorar cuál es su lugar en la sociedad y en la historia, y seres humanos con más posibilidades de captar las similitudes entre todas las personas (…)”</i> (p 52). Baremboim. * BARENBOIM, D. BELACQUA, 2008</p>
                </div>
			</div>
		</div>

		<div class="col-md-4">
			<div class="sidebar sidebar-yellow">
				<div class="media">
					<div class="media-left">
						{!! Html::image('/img/faculty/marcela-hidalgo.png', 'Marcela Hidalgo', array('class' => 'media-object')) !!}
					</div>
					<div class="media-body">
						<h4 class="media-heading">Marcela Hidalgo</h4>
						<p>Docente de música nivel inicial y primaria Olivos</p>
					</div>
				</div>
				<p class="description">Marcela Hidalgo es Licenciada en Música de la UCA, Magister en Políticas educativas de la UTDT y ha realizado un Perfeccionamiento en documentación de experiencias pedagógicas en Pamplona, España. <br>
                Desde 1982 es Maestra de música del Nivel Inicial y de la Primaria de la sede Olivos y se ha dedicado también a la investigación publicando y exponiendo en diferentes congresos sus trabajaos sobre documentación de experiencias de clases y juego con material no estructurado en nivel inicial.</p>
			</div>

			<div class="sidebar-navigation">
				<h3>{!! trans('aboutus/faculty/faculty.header') !!}</h3>
				
				@include('aboutus/faculty/faculty-navigation')
			</div>
		</div>
	</div>	

@endsection
