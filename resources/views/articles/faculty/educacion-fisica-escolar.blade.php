{{-- Language set in lang/{language}/aboutus/faculty/faculty/faculty-detail.php --}}

@extends('layouts.main')

@section('content_class','faculty-detail')
@section('content')

	<div class="row">
		<div class="col-xs-12">
			<div class="breadcrumb">
				<a href="{{ url('/') }}">Inicio</a> > 
				<a href="{{ url('/aboutus/faculty/faculty') }}">Equipo docente</a> > 
				<span>La educación física escolar y la creatividad, oportunidades y estrategias didácticas</span> 
			</div>
			<h1>La educación física escolar y la creatividad, oportunidades y estrategias didácticas</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-8">
	    	<h4>Angie Mallo, coordinadora de Educación Física de Secundaria, disertó en el 7º Congreso Europeo de la Fédération Internationale d’Education Physique (FIEP) que tuvo lugar en Barcelona del 7 al 9 de junio de 2012.</h4>
            <div class="columns">

	    		<p>El tema de su disertación es “La educación física escolar y la creatividad, oportunidades y estrategias didácticas” aquí comparte algunos puntos de su ponencia:</p>

                <p>En el siglo pasado de la mano de la industria y la producción en serie, la enseñanza se enmarcó en ciertos parámetros enfocados a la enseñanza de contenidos y del trabajo escolar en masa. El nuevo siglo, en cambio, nos marca NO a la producción y SÍ a la diferenciación; para eso necesitamos enseñar desde la individualidad. Enseñar para observar y generar cambios.</p> 
                
                <p>Tenemos que pensar en formar seres diferentes, en potenciar las particularidades porque desde ellas se hará el cambio.</p>

	    		<blockquote>
                    <p class="text-center text-red">La creatividad ha sido considerada por psicólogos y filósofos como una alternancia entre dos modos muy diferentes de pensar: Un modo de análisis y un modo generador*.</p>
                </blockquote>

				<p>Un individuo cuando participa en un pensamiento analítico, es enfocado, limita su atención en el análisis. Cuando pensamos creativamente generamos asociaciones, el pensamiento pareciera que es menos enfocado y la atención está más a la deriva, viajando entre conceptos que antes no se tenían en cuenta. La creatividad, entonces, puede caracterizarse por la capacidad de moverse de un modo de pensamiento a otro sin dificultad.</p>

				<p>Las habilidades que requiere el ciudadano del nuevo siglo transcienden nuestra imaginación. El mundo está cambiando a gran velocidad y las certidumbres son parte del pasado.</p>

				<p>Enseñar para este paradigma es difícil y desafiante. Pero es el cambio y el futuro un tejido sin costura, somos parte y artífices, ya no bastan las buenas prácticas para tener un horizonte  “…la innovación de futuro es pensar en la brecha de oportunidad que se tendrá si se trae ese pensamiento de futuro al hoy” (Govindarajan 2011) En este marco la creatividad es una habilidad clave a desarrollar en las escuelas, tanto en enseñantes como en aprendices. </p>

				<p><sup>*(Howard-Jones,Paul A./ Winfieldb, M. & Crimminsb, G., 2008)pág.4</sup></p>
			</div>
		</div>

		<div class="col-md-4">
			<div class="sidebar sidebar-red">
				<div class="media">
					<div class="media-left">
						{!! Html::image('/img/faculty/angie-mayo.png', 'Angie Mayo', array('class' => 'media-object')) !!}
					</div>
					<div class="media-body">
						<h4 class="media-heading">Angie Mallo</h4>
						<p>Coordinadora Educación Física Secundaria</p>
					</div>
				</div>
				<p class="description">Angie es Profesora de Educación Física (ISEF) y Licenciada en Enseñanza de Educación Fisica (CAECE). Ha realizado un Postgrado en RRHH (UB) y una Especialización en educación y nuevas tecnologías (FLACSO). Le encanta trabajar e investigar para el desarrollo de una escuela integral que forme ciudadanos críticos, reflexivos, al servicio de progreso social y humanitario, ocupados por transformar al planeta en un espacio amigable y sustentable para desarrollarnos como personas y comunidad. Angie es desde 2008, coordinadora de Educación Física de Nivel Secundario de NORTHLANDS.</p>
			</div>

			<div class="sidebar-navigation">
				<h3>{!! trans('aboutus/faculty/faculty.header') !!}</h3>
				
				@include('aboutus/faculty/faculty-navigation')
			</div>
		</div>
	</div>	

@endsection
