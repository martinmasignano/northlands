{{-- Language set in lang/{language}/aboutus/faculty/faculty/faculty-detail.php --}}

@extends('layouts.main')

@section('content_class','faculty-detail')
@section('content')

	<div class="row">
		<div class="col-xs-12">
			<div class="breadcrumb">
				<a href="{{ url('/') }}">Inicio</a> > 
				<a href="{{ url('/aboutus/faculty/faculty') }}">Equipo docente</a> > 
				<span>Trabajar de manera colaborativa</span> 
			</div>
			<h1>Juntos hacemos grandes cosas<br><small>¡trabajan de manera colaborativa!</small></h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-7">
			<div class="row">
				<div class="col-sm-6">
					<p>La Dirección nos propuso escribir un libro sobre Metacognición en el Nivel Inicial. De más está decir que fue para todos un gran desafío y nuestros  inicios en este camino llamado “trabajo colaborativo”.</p>

		    		<p>Pasados los primeros momentos organizativos comenzamos a construir este nuevo conocimiento compartiendo bibliografías e investigaciones y brindando ideas que sumaran para este objetivo común. Las habituales conversaciones de maestras se modificaron, para dar paso a un lenguaje común relacionado con el tema de nuestro futuro libro. Las reuniones dedicadas a intercambiar, discutir y revisar nuestras producciones escritas fueron la parte más enriquecedora de este proceso. Se trabajó de manera colaborativa para sugerir, compartir y evaluar los avances de cada una. Desde un comienzo apostamos a un modo de trabajo que generara y fomentara la reflexión conjunta, la discusión e identificación de posibles dificultades para colaborar en la búsqueda de soluciones. </p>

		    		<p>Esta experiencia nos permitió, no sólo ser autoras de un libro, sino descubrir la posibilidad de enriquecernos como profesionales a través del intercambio de ideas, experiencias y, sobre todo, del trabajo en equipo.</p>

		    		<p>Con el objetivo de continuar profundizando nuestro aprendizaje, la dirección nos planteó un nuevo desafío: grabar un DVD que muestre algunas de nuestras “buenas” prácticas docentes. Las mismas serán observadas y evaluadas entre pares y luego seran compartidas con el resto del plantel.</p>
				</div>
				
				<div class="col-sm-6">
					<div class="panel">
						<div class="panel-body panel-yellow">
							
							<p>Algunos principios para lograr un verdadero trabajo colaborativo: 
							<br />
							<strong>INTERDEPENDENCIA POSITIVA:</strong> esfuerzo para beneficio del equipo.
							<br />
				    		<strong>PROCESAMIENTO GRUPAL:</strong> analizar qué acciones resultaron útiles para lograr el objetivo y cuáles no.
							<br />
				    		<strong>INTERACCIÓN ESTIMULADORA:</strong> promover el éxito del equipo.
							<br />
				    		<strong>RESPONSABILIDAD INDIVIDUAL Y GRUPAL:</strong> aprender juntos para desempeñarse mejor.
							<br />
				    		<strong>HABILIDADES INTERPERSONALES Y DE EQUIPO:</strong> saber ejercer dirección y tomar decisiones, crear clima de confianza.
							<br />
				    		Estos conceptos están interrelacionados y no pueden darse unos sin los otros si lo que se busca es el éxito del proyecto.</p>
						</div>
					</div>

					<h3>Related Galleries /<em>Galerías Relacionadas</em></h3>
					@include ('articles/articles-blocks/hacer-visible-el-pensamiento')
				</div>
			</div>
		</div>

		<div class="col-md-5">
			<h3>Autoras</h3>
			<div class="row">
				@include('aboutus/faculty/authors-navigation')
			</div>
		</div>
	</div>	
    	
@endsection
