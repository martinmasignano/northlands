{{-- Language set in lang/{language}/aboutus/faculty/faculty/faculty-detail.php --}}

@extends('layouts.main')

@section('content_class','faculty-detail')
@section('content')

	<div class="row">
		<div class="col-xs-12">
			<div class="breadcrumb">
				<a href="{{ url('/') }}">Inicio</a> > 
				<a href="{{ url('/aboutus/faculty/faculty') }}">Equipo docente</a> > 
				<span>Why innovate?</span> 
			</div>
			<h1>Why innovate?</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-8">
			<div class="columns">
	    		<p>Innovation is a process meant to spark off creative ideas and this can only happen when those who know the organization best, the ones who make it happen every day, work actively and collaboratively towards a creative process.  This takes place when judgment and prejudices are barred in the interest of new thoughts: passionate brainstorming enveloped in a spirit of teamwork, working together and communicating effectively.</p>

                <p>Creative innovation invites every participant to communicate cooperatively, to take on an active role putting their knowledge to imagine a new world of possibilities.  It also fosters the need to revisit and review well known processes to imagine at what point something new can be brought in, something different can be introduced, something positively disruptive could be welcome.</p>

                <p>As an institution who expects to be at the cutting edge of education for a globalized world, NORTHLANDS’ duty is to tutor not only informed but also alert citizens and leaders.  Alert meaning  “ready and able to imagine the future will surprise them with exponential change”. Although innovation cannot be scripted one idea will remain permanent: their need to contribute to the common good.</p> 

                <p>This is what innovation is about, the challenge to imagine the unthinkable, the need to cultivate nontraditional thoughts.</p>

                <p>It is the urge to think along different lines, to conjure up unconventional scenarios, to let our imagination flow without tightening it down to known circuits.</p>
                
                {!! Html::image('/img/faculty/innovation.png', 'Why innovate', array('class' => 'img-responsive')) !!}
	    		
                <p class="lead text-center">There is no organization that can say “no” to innovation.  In the words of Alvin Toffler “The illiterate of the 21st century will not be those who cannot read and write, but those who cannot learn, unlearn, and relearn. ” </p>

                <p>NORTHLANDS intends to be an institution made up of lifelong learners, both staff and students are committed to this objective. The bottom line message is related to the certainty that innovation is spurred by creativity and this is not necessarily a talent, but a skill which all the Schools in the 21st century are committed to developing.</p>
			</div>
		</div>

		<div class="col-md-4">
			<div class="sidebar sidebar-green">
				<div class="media">
					<div class="media-left">
						{!! Html::image('/img/faculty/marisa-perazzo.jpg', 'Marisa Perazzo', array('class' => 'media-object')) !!}
					</div>
					<div class="media-body">
						<h4 class="media-heading">Marisa Perazzo</h4>
						<p>Deputy Head</p>
					</div>
				</div>
				<p class="description">Born and educated in Argentina, she graduated as a Teacher of English at IES en Lenguas Vivas “Juan Ramón Fernández” where she became a teacher trainer, lecturer at Teacher Training College, and a Primary and Secondary Head of the English Department.  She holds a Master of Arts in Applied Linguistics and TEFL from King’s College, University of London, and a Postgraduate Degree in Education Management from Universidad Torcuato Di Tella.  She later certified as an Organizational Coach at Universidad de San Andrés in a joint programme with Axialent.  Her academic experience at AACI includes positions such as Professional English Centre Coordinator, Cambridge English Team Leader, Branch Manager, IELTS exams Administrator and Centre Exams Manager for Cambridge English Exams.  In 2011 she joined NORTHLANDS as Deputy Head and was Acting Head from February to August 2014.   Ms. Perazzo led the process that allowed Northlands to become a member of the Council of International Schools (CIS) and chaired the Committee that led it to certify as an International School.  Marisa is also an IB workshop leader qualified to facilitate courses on Leadership at IB World Schools.</p>
			</div>

			<div class="sidebar-navigation">
				<h3>{!! trans('aboutus/faculty/faculty.header') !!}</h3>
				
				@include('aboutus/faculty/faculty-navigation')
			</div>
		</div>
	</div>	

@endsection
