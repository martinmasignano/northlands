{{-- Language set in lang/{language}/aboutus/faculty/faculty/faculty-detail.php --}}

@extends('layouts.main')

@section('content_class','faculty-detail')
@section('content')

	<div class="row">
		<div class="col-xs-12">
			<div class="breadcrumb">
				<a href="{{ url('/') }}">Inicio</a> > 
				<a href="{{ url('/aboutus/faculty/faculty') }}">Equipo docente</a> > 
				<span>Tecnología aplicada a la educación</span> 
			</div>
			<h1>Tecnología aplicada a la educación</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-8">
			<div class="columns">
	    		<h3>Cómo definirías el desarrollo tecnológico del Colegio?	</h3>

	    		<p>Cuando ingresé al Colegio ya estaba en marcha un plan de actualización y equipamiento de recursos tecnológicos (software y hardware), producto de una fuerte inversión económica más lo percibido en la última campaña de Recaudación de Fondos. A mi juicio, siempre hay muchísimo por hacer. Cuando yo empecé a trabajar en tecnología –siempre dentro del ámbito educativo- era más fácil dominar “todo” en el área. Te preguntaban por cualquier software  y vos lo tenías que conocer. Hoy es imposible. El “todo” de hoy es tan extenso y abarcativo que en un rol como el mío hay que tener la habilidad de focalizar en las cuestiones conceptuales y macro para fijar rumbos y diferenciar tendencias de modas.</p>

	    		<iframe src="https://player.vimeo.com/video/148862288" width="100%" height="260" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

	    		<h3>¿Cómo distinguís una moda de una tendencia?</h3>

				<p>El punto es su nivel de  impacto en la sociedad a mediano y largo plazo. Cuando apareció Internet se sabía que el concepto, la idea de comunicación e intercomunicación a nivel global haría que “explotara el mundo”.  En esta área lo que te da la experiencia es una visión un poco más global de hacia dónde deberíamos ir. Cuáles son las tendencias, qué cosas hay en el mercado que podemos implementar. Qué cosas filtrar, porque la tecnología permanentemente está produciendo modas que a veces se transforman en tendencias y otras que simplemente quedan ahí.</p>

				<p>Facebook por ejemplo, no el concepto de Red Social, es hoy una moda pero hay que ver si tiene una proyección a largo plazo, yo no sé si esto va a estar en 10 años.</p>

				<h3>¿Cuál es el rol del docente frente a una clase de nativos digitales?</h3>

				<p>Cuando se implementó la informática en las escuelas (fines de los 80s) lo que se enseñaba eran  los componentes de la máquina; después programación…pasaron muchos años para entender que lo que teníamos que hacer era dejar la computadora a los ingenieros y a los técnicos para empezar a usarla como una herramienta pedagógica. Como usuario no es necesario entender cuál es la función de un micro procesador,  lo que si es necesario es aprender a usarlo para los fines que hacen a tu tarea. Luego en los 90 pasamos al mundo de los utilitarios y algunas tendencias se fueron  al  extremo de suponer que la gente aprende a utilizar la tecnología por “ósmosis” y tampoco es así.</p>

				<p>Hoy lo que se busca es un punto medio, enseñar habilidades y competencias prácticas y de forma paralela incentivar a que todos hagan uso de las TIC (Tecnologías de la Información la Comunicación) como un recurso en la materia correspondiente. Los docentes en parte se pueden sentir amedrentados por el manejo natural que los chicos tienen frente a la tecnología. Los alumnos ven algo y se tiran a probar, no le tienen miedo. 

				<p>Es usual oír la frase “los chicos manejan mejor que yo tal o cual cosa…” Si, instrumentalmente pueden manejarlo mejor pero la mayoría de las veces no saben usar el recurso para fines que no sean de diversión. A la hora de entender conceptualmente qué cosas productivas podés hacer con la tecnología, necesitás alguien que oriente y guíe. Esa es la tarea del docente hoy, guiar, orientar y esto no pasa por ser un eximio usuario del recurso sino por conocer su potencial creativo y productivo.</p>

				<p>Con la incorporación del Aula Móvil (mueble con 30 notebooks y conexión a Internet que se puede transportar a cada aula) por ejemplo, lo que incentiva y sorprende a los alumnos es descubrir las aplicaciones constructivas y productivas de ese recurso en la asignatura. Y esto se repite con el resto de los recursos de tecnología en el Colegio.</p>
			</div>
		</div>

		<div class="col-md-4">
			<div class="sidebar">
				<div class="media">
					<div class="media-left">
						{!! Html::image('/img/faculty/daniel-magaldi.png', 'Daniel Magaldi', array('class' => 'media-object')) !!}
					</div>
					<div class="media-body">
						<h4 class="media-heading">Daniel Magaldi</h4>
						<p>Jefe del Depto. de Informática</p>
					</div>
				</div>
				<p class="description">*Daniel Magaldi es Jefe el Departamento de Informática de Secundaria (Nordelta y Olivos) desde febrero 2012. Paralelamente se desempeña como consultor independiente en TIC aplicadas a la educación. Es  docente universitario y supervisor de exámenes y coordinador de cursos de capacitación en tecnología para ESSARP. En Argentina tuvo a su cargo el desarrollo de varios proyectos de E-Learning y de implementación tecnológica en distintos ámbitos educativos. Participó de la función pública en el Ministerio de Educación del GCABA en la Dirección de Incorporación de Tecnologías. Fue también director de Recursos Tecnológicos en el Florida Day School y en el Colegio St. Mark´s. </p>
			</div>

			<div class="sidebar-navigation">
				<h3>{!! trans('aboutus/faculty/faculty.header') !!}</h3>
				
				@include('aboutus/faculty/faculty-navigation')
			</div>
		</div>
	</div>	

@endsection
