{{-- Language set in lang/{language}/aboutus/faculty/faculty/faculty-detail.php --}}

@extends('layouts.main')

@section('content_class','faculty-detail')
@section('content')

	<div class="row">
		<div class="col-xs-12">
			<div class="breadcrumb">
				<a href="{{ url('/') }}">Inicio</a> > 
				<a href="{{ url('/aboutus/faculty/faculty') }}">Equipo docente</a> > 
				<span>Coros y orquestas, pequeñas sociedades para formar grandes ciudadanos</span> 
			</div>
			<h1>Coros y orquestas, pequeñas sociedades para formar grandes ciudadanos</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-8">
			<div class="columns">
	    		<h3>La Vocación Musical</h3> 

                <p>Empecé a trabajar a los 17 años como preceptor, cuidando a los chicos del coro de primaria del colegio Rudolf Steiner de Florida. Cuando terminé el secundario en el Nacional Nº 8, comencé a estudiar Biología en la UBA. Como la música me interesaba mucho, decidí en paralelo prepararme para entrar en el conservatorio. Para eso tomaba clases de teoría musical en el sótano de un bar cercano al teatro San Martín (porque con mis 18 años era “grande” para hacer una carrera como músico) así que me esforcé para rendir libre y adelantar cinco años de conservatorio. Un día estaba camino a la facultad; volvía de una clase de teoría musical en la que me había ido tan bien que me bajé del colectivo y decidí - ahí, en ese instante- dejar la Biología y dedicarme de lleno a la música. Y no me equivoqué, al poco tiempo en la escuela R. Steiner me ofrecieron dictar música en todo el secundario y armar el coro de secundaria. </p>

	    		<h3>¿Te sentís cómodo con la pedagogía Waldorf?</h3>

                <p>Es que yo me formé como alumno y luego como docente bajo esa filosofía. En el colegio alemán fui docente de música en los tres niveles porque en la pedagogía Waldorf - por su misma esencia - el arte está en cualquier materia en cualquier momento. Entonces, por ejemplo, en una clase de historia si necesitan que vaya el docente de música para mostrar el contexto musical de determinado momento histórico, te sumás a la clase (y esto se da en todos los niveles). Para mí es “la manera” de hacer las cosas, lo tengo internalizado. Mi desafío en NORTHLANDS es bajarlo, teorizarlo para compartirlo como experiencia en las reuniones del departamento de música y con los jefes de otras áreas. La idea es plantear qué se podría hacer con la música, la plástica y el teatro en relación con la matemática, la historia, etcétera. Es más difícil explicarlo que hacerlo, transmitirlo es para mí un desafío que me entusiasma muchísimo y estoy seguro de que vamos a llegar a implementarlo. </p>

                <h3>¿Te gusta dirigir?</h3> 

                <p>Me apasiona. Cantando en coros descubrí que me encantaba la dinámica del trabajo coral. Estando en el conservatorio un profesor de dirección coral me invitó a ser su asistente y despertó mi interés por la dirección, tanto de coros como de orquestas. Tomé clases con el Mtro. Antonio Russo y fue él quien insistió en que empezara a dirigir mientras me formaba, pero como yo tenía grabada la frase de los libros que decían “no utilice a las orquestas como conejitos de indias de sus estudios… no me animaba. Fue mi mujer, Paula, quien me dijo un día “hay muchos chicos y grandes que quieren tocar y vos querés dirigir”… júntense. Júntalos y armá una orquesta”. Esto, sumado al interés de mis alumnos del R. Steiner por armar una orquesta, me decidieron a hacerlo. Así surgió “Buenos Aires Cámara”, mi primera orquesta de cuerdas con 15 ó 16 integrantes desde 16 años hasta los guías que eran ventiañeros.</p>

                <h3>¿Cuál es el rol del director?</h3>

                <p>El director es el medio entre el compositor, la obra y la orquesta. Para eso es necesario el estudio histórico, el análisis musical de las obras y conocer la época del compositor, para luego trabajar la interpretación de las obras y lograr la mayor fidelidad de lo que el compositor quiso transmitir. Hay que ser respetuoso de las técnicas de ensayo, ser muy “musical” (ir a la música, sentirla, ser honesto con lo que estás haciendo). Siempre se ve el rol de director como muy de “divo” pero en realidad el objetivo es que la música llegue al público. Si tu imagen se pone en el medio, algo no cierra.</p>

                <h3>Pero todos los directores tienen algo de artistas, deseo de “ser vistos”, de ego ¿o no?</h3> 

                <p>Si, tenés que tenerlo. Para cumplir el rol de director, tenés que ser un poco actor, tenés que expresar, tenés que llegar desde otro lado a la audiencia y al músico. Pero previo a eso está el verdadero trabajo: estudiar la obra, preparar los ensayos. Descubrir cuáles son los problemas que pudieran surgir y tratar de tener las soluciones. Y a la vez, tener la humildad suficiente para pedir cuando necesitás que alguien te explique o te enseñe algo, tener el respeto para reconocer cuán importantes son aquellos a los que estás dirigiendo y así saber que todos son una “parte” de un gran equipo.</p> 

                <h3>¿Qué aporta en la formación integral del alumno ser parte de un coro?</h3>

                <p>Tanto el coro como la orquesta son una gran excusa para formar ciudadanos que respeten, que escuchen, que ayuden, que sean tolerantes. Enseñarles a disfrutar de la vida y del arte son la base de formación de una persona. Un coro o una orquesta son sociedades en chiquito, en las que se aprende a respetar los momentos y el lugar que cada uno ocupa (a veces un rol principal, otras de acompañamiento) si ese aprendizaje se propagara a la vida diaria…viviríamos en una sociedad feliz, ordenada.</p>
			</div>
		</div>

		<div class="col-md-4">
            <div class="well yellow">
                {!! Html::image('/img/faculty/headphones.png', '', array('class' => '', 'style' => 'position: absolute; top: -33px; right: 0;')) !!}
                <h3>¿Qué música te gusta?</h3> 

                <p>Es más fácil decir qué no me gusta, porque me gusta todo. He tocado y cantado desde chamamé hasta heavy metal. Lo único que no me gusta es el reagge.</p>

                <p>De música local actual me gusta mucho la Versuit, Abel Pintos, Charly García y Lito Vitale. De afuera George Michael, que es el único que pudo sustituir en un escenario a Freddie Mercury, los Beatles, los Rollings y también el viejo rock sinfónico (Génesis, Yes, Pink Floyd).</p>

                <p>También hay muchas cosas que me gustan que jamás hubiese escuchado de no ser por los alumnos: Cold Play, por ejemplo, algunas canciones de One Direction porque cantan a cinco voces – que es difícil – y lo hacen bien (algunos tienen menos desarrollo vocal, porque son muy jóvenes, pero saben qué rol cumplir). </p>
            </div>

			<div class="sidebar sidebar-lightyellow">
				<div class="media">
					<div class="media-left">
						{!! Html::image('/img/faculty/leandro-valle.png', 'Leandro Valle', array('class' => 'media-object')) !!}
					</div>
					<div class="media-body">
						<h4 class="media-heading">Leandro Valle</h4>
						<p>Director de Artes Interpretativas</p>
					</div>
				</div>
				<p class="description">Profesor nacional de música, director coral, director de orquesta y estilista musical. Ha realizado una variedad de cursos que profundizan en el estudio de la música, instrumentos y dirección. Dirige varios coros y orquestas entre ellos el Coro de la E. S. de Comercio Carlos Pellegrini, el de la Iglesia Evangélica Alemana en Bs.As., el Coro de la Facultad de Ciencias Económicas UBA y la Orquesta Filarmónica de Quilmes. Director de Artes escénicas de NORTHLANDS desde 2014</p>
			</div>

			<div class="sidebar-navigation">
				<h3>{!! trans('aboutus/faculty/faculty.header') !!}</h3>
				
				@include('aboutus/faculty/faculty-navigation')
			</div>
		</div>
	</div>

@endsection
