{{-- Language set in lang/{language}/home/slider.php --}}

<div id="ei-slider" class="ei-slider">
    <ul class="ei-slider-large">
 
<!--  kpson_Kermesse2016 -->
<li>
                           <a href="http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/kpson_Kermesse2016/kpson_Kermesse2016.php" target="_blank"> <img src="./img/slider/kpson_Kermesse2016.jpg" alt="Year 6/Year 7 Bridging, Primary & Secondary" /> </a>
                            <div class="ei-title">
                                <h2><a href="http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/kpson_Kermesse2016/kpson_Kermesse2016.php" target="_blank">Kermesse y Evento Intercultural</a></h2>
                                <h3><strong>Kermesse & Intercultural Event</strong></h3>
                            </div>
                        </li>

<!-- Fin kpson_Kermesse2016 -->
<!--  ops_Bridging -->
<li>
                           <a href="http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/ops_Bridging/ops_Bridging.php" target="_blank"> <img src="./img/slider/ops_Bridging.jpg" alt="Year 6/Year 7 Bridging, Primary & Secondary" /> </a>
                            <div class="ei-title">
                                <h2><a href="http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/ops_Bridging/ops_Bridging.php" target="_blank">Articulación Año 6/Año 7, Primaria y Secundaria</a></h2>
                                <h3><strong>Year 6/Year 7 Bridging, </strong>Primary & Secondary</h3>
                            </div>
                        </li>

<!-- Fin ops_Bridging -->
<!--  wsb_Swimming_Seguridad -->
                             <li>
                           <a href="https://sites.google.com/a/northlands.edu.ar/physical-education/home/wsb/wsb-swimming-november" target="_blank"> <img src="./img/slider/wsb_Swimming_Seguridad.jpg" alt="Water Safety - Swimming Activity, Kindergarten, Primary & Secondary" /> </a>
                            <div class="ei-title">
                                <h2><a href="https://sites.google.com/a/northlands.edu.ar/physical-education/home/wsb/wsb-swimming-november" target="_blank">Seguridad en el medio acu&aacute;tico - Actividad Natación, Nivel Inicial, Primaria y Secundaria</a></h2>
                                <h3><strong>Water Safety - Swimming Activity, </strong>Kindergarten, Primary & Secondary</h3>
                            </div>
                        </li>

<!-- Fin wsb_Swimming_Seguridad -->
<!--  onps_OMA -->
                             <li>
                           <a href="http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/onps_OMA/onps_OMA.php" target="_blank"> <img src="./img/slider/onps_OMA.jpg" alt="OMA Ñandú, Primary & Secondary" /> </a>
                            <div class="ei-title">
                                <h2><a href="http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/onps_OMA/onps_OMA.php" target="_blank">OMA '&Ntilde;and&uacute;' - Instancias finales, Primaria y Secundaria</a></h2>
                                <h3><strong>OMA '&Ntilde;and&uacute;'

- Final Rounds, </strong>Primary & Secondary</h3>
                            </div>
                        </li>

<!-- Fin onps_OMA -->
<!--  po_PYPExhibition2016 
                             <li>
                           <a href="http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/po_PYPExhibition2016/po_PYPExhibition2016.php" target="_blank"> <img src="./img/slider/po_PYPExhibition2016.jpg" alt="2016 IB PYP Exhibition, Primary" /> </a>
                            <div class="ei-title">
                                <h2><a href="http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/po_PYPExhibition2016/po_PYPExhibition2016.php" target="_blank">Exposisión IB PEP 2016, Primaria</a></h2>
                                <h3><strong>2016 IB PYP Exhibition, </strong>Primary</h3>
                            </div>
                        </li>

Fin po_PYPExhibition2016 -->

<!--  son_LaCumbre2016 -->
                             <li>
                           <a href="http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/son_LaCumbre2016/son_LaCumbre2016.php" target="_blank"> <img src="./img/slider/son_LaCumbre2016.jpg" alt="Choir Festival - La Cumbre, Córdoba - Secondary" /> </a>
                            <div class="ei-title">
                                <h2><a href="http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/son_LaCumbre2016/son_LaCumbre2016.php" target="_blank">Festival de coro - La Cumbre, Secundaria</a></h2>
                                <h3><strong>Choir Festival - La Cumbre, </strong>Secondary</h3>
                            </div>
                        </li>

<!-- Fin son_LaCumbre2016 -->
<!--  kpson_MaratonNordelta -->
                             <li>
                           <a href="http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/kpson_MaratonNordelta/kpson_MaratonNordelta.php" target="_blank"> <img src="./img/slider/kpson_MaratonNordelta.jpg" alt="NORTHLANDS en Maratón Nordelta 2016" /> </a>
                            <div class="ei-title">
                                <h2><a href="http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/kpson_MaratonNordelta/kpson_MaratonNordelta.php" target="_blank">NORTHLANDS en Maratón Nordelta 2016</a></h2>
                                <h3><strong>NORTHLANDS at <em>Maratón Nordelta 2016</em></strong></h3>
                            </div>
                        </li>

<!-- Fin kpson_MaratonNordelta -->
<!--  wsb_ADEFootball -->
                             <li>
                           <a href="https://sites.google.com/a/northlands.edu.ar/physical-education/home/wsb/wsb-football-24-10-al-29-10?pli=1" target="_blank"> <img src="./img/slider/wsb_ADEFootball.jpg" alt="ADE champions, Secondary" /> </a>
                            <div class="ei-title">
                                <h2><a href="https://sites.google.com/a/northlands.edu.ar/physical-education/home/wsb/wsb-football-24-10-al-29-10?pli=1" target="_blank">¡Felicitaciones al equipo Senior de Fútbol, campeones del Torneo ADE!, Secundaria</a></h2>
                                <h3><strong>Congratulations to our Senior Football Team, Champions at the ADE Tournament!!!</strong>, Secondary</h3>
                            </div>
                        </li>

<!-- Fin wsb_ADEFootball -->






    </ul><!-- ei-slider-large -->
    <ul class="ei-slider-thumbs">
        <li class="ei-slider-element">Current</li>
        <li><a href="#">Slide 1</a></li>
        <li><a href="#">Slide 2</a></li>
        <li><a href="#">Slide 3</a></li>
        <li><a href="#">Slide 4</a></li>
        <li><a href="#">Slide 5</a></li>
		<li><a href="#">Slide 6</a></li>
        <li><a href="#">Slide 7</a></li>
    </ul><!-- ei-slider-thumbs -->
</div><!-- ei-slider -->
<div class="slider-wgo">
    <a href="{{ url('/wgo') }}">{!! trans('home/slider.title') !!}</a>
</div>


