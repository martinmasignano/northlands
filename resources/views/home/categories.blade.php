{{-- Language set in lang/{language}/home/categories.php --}}

<div class="categories">


	<div class="col-xs-12 col-sm-6 col-md-4">
		{{-- Thinker --}}
		@include('home/categories-blocks/thinker')

		<div class="hidden-md hidden-lg">				
			@include('home/categories-blocks/header')
		</div>
	</div>

	{{-- Empowers IB learnes --}}
	<div class="col-xs-12 col-sm-6 col-md-4">
		@include('home/categories-blocks/iblearnes')
	</div>

	{{-- Global citizenship --}}
	<div class="col-xs-12 col-sm-6 col-md-4">
		@include('home/categories-blocks/globalcitizenship')
	</div>
	
	{{-- Header Desktop only --}}
	<div class="col-sm-6 col-md-4 hidden-xs hidden-sm">
		@include('home/categories-blocks/header')
	</div>

	{{-- character building --}}
	<div class="col-xs-12 col-sm-6 col-md-4">
		@include('home/categories-blocks/characterbuilding')
	</div>

	<div class="clearfix visible-md-block visible-lg-block"></div>

	<div class="col-xs-12 col-md-6">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-12 healthy-lifestyle">
				<div class="row">
					{{-- Mentors of knowledge --}}
					<div class="col-md-8">
						@include('home/categories-blocks/mentors')	
					</div>

					{{-- Healthy lifestyle Desktop only --}}
					<div class="col-md-4 hidden-xs hidden-sm">
						<div class="row category">
                            <div class="col-xs-12">
                                <div class="category-description">
                                    <a href="{{ url('categories/healthy-lifestyle') }}">
                                        <h3>{!! trans('home/categories.hlheader') !!}</h3>
                                    </a>
			                    	<a href="{{ url('/articles/categories/healthy-lifestyle/educacion-preventiva') }}">
			                    		<p>Educación preventiva</p>
			                    	</a>
                                </div>
                            </div>
						</div>
					</div>

					{{-- Digital learners --}}
					<div class="col-md-8 hidden-sm">
						@include('home/categories-blocks/digitallearners')
					</div>

					{{-- Healthy lifestyle Desktop Only --}}
					<div class="col-md-4 hidden-xs hidden-sm">
						<div class="row category healthy-lifestyle">
							<div class="col-xs-12">
                                <div class="row">
                                    <img class="img-responsive" src="./css/img/bg-healthy-lifestyle.svg" alt="{!! trans('home/categories.hlhead') !!}">
                                </div>
                            </div>
						</div>	
					</div>
				</div>
			</div>
			
			{{-- Healthy lifestyle Mobile only --}}
			<div class="col-xs-12 col-sm-6 hidden-md hidden-lg">
				@include('home/categories-blocks/healthylifestyle')
			</div>
		</div>
	</div>
	
	{{-- Slider --}}
	<div class="col-xs-12 col-sm-9 col-md-6 pull-right">
		<div class="row">
			@include('home/slider')
		</div>
	</div>
	
	{{-- Digital learners Mobile only --}}
	<div class="hidden-xs col-sm-3 hidden-md hidden-lg">
		<div class="row category digital-learners">
            <div class="col-xs-12">
                <div class="row">
                    <img class="img-responsive" src="./css/img/bg-digital-learners.svg" alt="{!! trans('home/categories.dlheader') !!}">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-12">	
                        <div class="category-description h150">
                            <a href="{{ url('categories/digital-learners') }}">
                                <h3>{!! trans('home/categories.dlheader') !!}</h3>
                            </a>
	                    	<a href="{{ url('articles/categories/digital-learners/vanguardia-educacion-progresista') }}">
                                <p>a la vanguardia de una educación progresista</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
		</div>		
	</div>
</div>
<div class="clearfix"></div>


