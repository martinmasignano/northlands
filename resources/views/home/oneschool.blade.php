{{-- Language set in lang/{language}/home/oneschool.php --}}

<div class="panel panel-default panel-oneschool">
	<div class="panel-body">
		<h3>
			<a href="{{ url('aboutus/ourschool') }}">{!! trans('home/oneschool.header') !!}</a>
		</h3>
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<video style="width: 100%;" controls preload>
                    <source src="{{ asset('./video/nordelta.mp4')}}" type="video/mp4">
                </video>
			</div>
			<div class="visible-xs-block">
				<div class="col-xs-12 col-sm-6 text-center">
					<a href="{{ url('aboutus/ourschool') }}">{!! trans('home/oneschool.nordelta') !!}</a>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6">
				<video style="width: 100%;" controls preload>
                    <source src="{{ asset('./video/olivos.mp4')}}" type="video/mp4">
                </video>
			</div>
			<div class="visible-xs-block">
				<div class="col-xs-12 col-sm-6 text-center">
					<a href="{{ url('aboutus/ourschool') }}">{!! trans('home/oneschool.olivos') !!}</a>
				</div>
			</div>
			<div class="hidden-xs">
				<div class="col-xs-12 col-sm-6 text-center">
					<a href="{{ url('aboutus/ourschool') }}">{!! trans('home/oneschool.nordelta') !!}</a>
				</div>
				<div class="col-xs-12 col-sm-6 text-center">
					<a href="{{ url('aboutus/ourschool') }}">{!! trans('home/oneschool.olivos') !!}</a>
				</div>
			</div>
		</div>
	</div>
</div>
