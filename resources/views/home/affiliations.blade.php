{{-- Language set in lang/{language}/home/affiliations.php --}}

<div class="panel panel-default panel-affiliations">
	<div class="panel-body">
		<h3>{!! trans('home/affiliations.header') !!}</h3>
		<div class="col-xs-6 col-sm-2 text-center">
			<div class="row">
				<a href="http://www.ibo.org/" target="_blank">
					<img src="{{ url('./css/img/logo-wsib.png') }}" alt="International Baccalaureate">
				</a>
			</div>
		</div>
		<div class="col-xs-6 col-sm-2 text-center">
			<div class="row">
				<a href="http://www.ibo.org/" target="_blank">
					<img src="{{ url('./css/img/logo-cambridge.jpg') }}" alt="Cambridge">
				</a>
			</div>
		</div>
		<div class="col-xs-6 col-sm-2 text-center">
			<div class="row">
				<a href="https://www.essarp.org.ar/" target="_blank">
					<img src="{{ url('./css/img/logo-essarp.png') }}" alt="English Speaking Scholastic Association of the River Plate">
				</a>
			</div>
		</div>
		<div class="col-xs-6 col-sm-2 text-center">
			<div class="row">
				<a href="http://www.lahc.net/home.htm" target="_blank">
					<img src="{{ url('./css/img/logo-lahc.png') }}" alt="Latin American Heads Conference">
				</a>
			</div>
		</div>
		<div class="col-xs-6 col-sm-2 text-center">
			<div class="row">
				<a href="http://www.cois.org/" target="_blank">
					<img src="{{ url('./css/img/logo-cis_member.png') }}" alt="Council of International Schools">
				</a>
			</div>
		</div>
		<div class="col-xs-6 col-sm-2 text-center">
			<div class="row">
				<a href="http://www.cois.org/" target="_blank">
					<img src="{{ url('./css/img/logo-cisla.jpg') }}" alt="Council of International Schools">
				</a>
			</div>
		</div>
	</div>
</div>
