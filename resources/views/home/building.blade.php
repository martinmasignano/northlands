{{-- Language set in lang/{language}/home/building.php --}}

<div class="panel panel-default panel-building">
	<div class="panel-body">
		<h3>{!! trans('home/building.header') !!}</h3>
		<div class="hidden-sm hidden-md hidden-lg">
			<a href="{{ url('aboutus/buildingourstory') }}">
				@if (App::getLocale() == 'en')
					<img class="img-responsive" src="./css/img/boh-mobile-en.jpg" alt="{!! trans('home/building.title') !!}">
				@else
					<img class="img-responsive" src="./css/img/boh-mobile-es.png" alt="{!! trans('home/building.title') !!}">
				@endif
			</a>
		</div>
		<div class="hidden-xs">
			<a href="{{ url('aboutus/buildingourstory') }}">
				@if (App::getLocale() == 'en')
					<img class="img-responsive" src="./css/img/boh-desktop-en.jpg" alt="{!! trans('home/building.title') !!}">
				@else 
					<img class="img-responsive" src="./css/img/boh-desktop-es.png" alt="{!! trans('home/building.title') !!}">
				@endif
			</a>
		</div>
	</div>
</div>
