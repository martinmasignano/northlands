{{-- Language set in lang/{language}/home/faculty.php --}}

<div class="panel panel-default panel-faculty">
	<div class="panel-body">
		<h3>
			<a href="{{ url('/aboutus/faculty/faculty') }}">{!! trans('home/faculty.header') !!}</a>
		</h3>
		<div class="row">
			<div class="col-sm-6">
				<a href="{{ url('/aboutus/faculty/faculty') }}">
					<div class="headmaster-quote">
						{{-- Quote --}}
						<div class="col-xs-12 col-sm-6 col-sm-push-6">
							<p><i>{!! trans('home/faculty.quote') !!}</i></p>
							<p><small>{!! trans('home/faculty.author') !!}</small></p>
						</div>
						{{-- Imagen de Nicholas --}}
						<div class="col-xs-12 col-sm-6 col-sm-pull-6">
							<div class="row">
								{!! Html::image('./css/img/nicholas-reeves.jpg', 'Nicholas Reeves', array('class' => 'img-responsive')) !!}
							</div>
						</div>
					</div>
				</a>
			</div>
			<div class="col-sm-6">
				@include('aboutus/faculty/faculty-navigation')
			</div>
		</div>
	</div>
</div>
