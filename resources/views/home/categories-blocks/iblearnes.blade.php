<div class="row">
	<div class="category ib-learnes">
		<div class="col-xs-6">
	        <div class="row">
	            <img class="img-responsive" src="./css/img/bg-ib-learnes.svg" alt="{!! trans('home/categories.ibheader') !!}">
	        </div>
	    </div>
	    <div class="col-xs-6">
	        <div class="row">
	            <div class="col-xs-12">
	                <div class="category-description">	
	                    <a href="{{ url('categories/empowers-ib-learnes') }}">
	                        <h3>{!! trans('home/categories.ibheader') !!}</h3>
	                    </a>
	                    <a href="{{ url('/articles/categories/iblearnes/en-primera-persona') }}">
                    		<p>En primera persona</p>
                    	</a>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>
