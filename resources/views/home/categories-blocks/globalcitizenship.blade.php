<div class="row">
	<div class="category global-citizenship">
	    <div class="col-xs-6 col-md-push-6">
	        <div class="row">
	            <div class="col-xs-12">
	                <div class="category-description">
	                    <a href="{{ url('categories/global-citizenship') }}">
	                        <h3>{!! trans('home/categories.gcheader') !!}</h3>
	                    </a>
	                    <a href="{{ url('articles/categories/global-citizenship/culturas-tradicionales-y-contextos') }}">
                    		<p>Culturas tradicionales y contextos</p>
                    	</a>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="col-xs-6 col-md-pull-6">
	        <div class="row">
	            <img class="img-responsive" src="./css/img/bg-global-citizenship.svg" alt="{!! trans('home/categories.gcheader') !!}">
	        </div>
	    </div>
	</div>
</div>
