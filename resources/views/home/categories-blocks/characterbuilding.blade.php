<div class="row">
	<div class="category character-building">
		<div class="col-xs-6 col-md-push-6">
	        <div class="row">
	            <img class="img-responsive" src="./css/img/bg-character-building.svg" alt="{!! trans('home/categories.cbheader') !!}">
	        </div>
	    </div>
	    <div class="col-xs-6 col-md-pull-6">
	        <div class="row">
	            <div class="col-xs-12">
	                <div class="category-description">
	                    <a href="{{ url('categories/character-building') }}">
	                        <h3>{!! trans('home/categories.cbheader') !!}</h3>
	                    </a>
                		<a href="{{ url('articles/categories/character-building/acompanar-el-cambio') }}">
                    		<p>Acompañar el cambio</p>
                    	</a>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>
