<div class="row">
	<div class="category mentors">
	    <div class="col-xs-6">
	        <div class="row">
	            <div class="col-xs-12">
	                <div class="category-description">
	                    <a href="{{ url('categories/mentors-of-knowledge') }}">
	                        <h3>{!! trans('home/categories.mkheader') !!}</h3>
	                    </a>
	                    <a href="{{ url('/articles/categories/mentors/hacer-visible-el-pensamiento') }}">
                    		<p>Hacer visible el pensamiento</p>
                    	</a>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="col-xs-6">
	        <div class="row">
	            <img class="img-responsive" src="./css/img/bg-mentors.svg" alt="{!! trans('home/categories.mkheader') !!}">
	        </div>
	    </div>
	</div>
</div>
