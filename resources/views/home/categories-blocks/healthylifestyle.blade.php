<div class="row">
	<div class="category healthy-lifestyle">
	    <div class="col-xs-6">
	        <div class="row">
	            <div class="col-xs-12">
	                <div class="category-description">
	                    <a href="{{ url('categories/healthy-lifestyle') }}">
	                        <h3>{!! trans('home/categories.hlheader') !!}</h3>
	                    </a>
	                    <a href="{{ url('articles/categories/healthy-lifestyle/educacion-preventiva') }}">
                    		<p>Educación preventiva</p>
                    	</a>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="col-xs-6">
	        <div class="row">
	            <img class="img-responsive" src="./css/img/bg-healthy-lifestyle.svg" alt="{!! trans('home/categories.hlheader') !!}">
	        </div>
	    </div>
	</div>
</div>
