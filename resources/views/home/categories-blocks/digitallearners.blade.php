<div class="row">
	<div class="category digital-learners">
		<div class="col-xs-6">
			<div class="row">
				<img class="img-responsive" src="./css/img/bg-digital-learners.svg" alt="{!! trans('home/categories.dlheader') !!}">
			</div>
		</div>
		<div class="col-xs-6">
			<div class="row">
				<div class="col-xs-12">	
					<div class="category-description">
	                    <a href="{{ url('categories/digital-learners') }}">
						    <h3>{!! trans('home/categories.dlheader') !!}</h3>
						</a>
                    	<a href="{{ url('articles/categories/digital-learners/vanguardia-educacion-progresista') }}">
                    		<p>a la vanguardia de una educación progresista</p>
                    	</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
