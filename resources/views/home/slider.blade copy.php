{{-- Language set in lang/{language}/home/slider.php --}}

<div id="ei-slider" class="ei-slider">
    <ul class="ei-slider-large">
        <li>
            <a href="{{url('articles/wgo/2015-prize-giving-ceremony')}}">
                <img src="./img/slider/kpson_95Anniversary.jpg" alt="image01" />
                <div class="ei-title">
                    <h2 class="ei-title-link">Ceremonia de Entrega de Premios 2015</h2>
                    <h3>2015 Prize Giving Ceremony</h3>
                </div>
            </a>
        </li>
        <li>
            <a href="{{url('articles/wgo/big-brother-big-sister')}}">
                <img src="./img/slider/son_LabSkills.jpg" alt="image02" />
                <div class="ei-title">
                	<h2 class="ei-title-link">Programa de Padrinazgo</h2>
                    <h3>Big Brother-Big Sister Programme</h3>
                </div>
            </a>
        </li>
        <li>
            <a href="{{url('articles/wgo/founders-day-primary')}}">
                <img src="./img/slider/son_ESSARPScienceFair.jpg" alt="image03"/>
                <div class="ei-title">
                    <h2 class="ei-title-link">Día de la Fundadora</h2>
                    <h3>Founder's Day</h3>
                </div>
            </a>
        </li>
        <li>
            <a href="{{url('articles/wgo/areas-of-knowledge')}}">
                <img src="./img/slider/bnw_MacoLizaso.jpg" alt="image04"/>
                <div class="ei-title">
                    <h2 class="ei-title-link">Áreas del conocimiento</h2>
                    <h3>Areas of Knowledge</h3>
                </div>
            </a>
        </li>
        <li>
            <a href="{{url('articles/wgo/global-collaboration-campaign')}}">
                <img src="./img/slider/sn_TeenAge.jpg" alt="image05"/>
                <div class="ei-title">
                	<h2 class="ei-title-link">Campaña de Colaboración Global</h2>
                    <h3>Global Collaboration Campaign</h3>
                </div>
            </a>
        </li>
        <li>
            <a href="{{url('articles/wgo/to-learn-together')}}">
                <img src="./img/slider/kpo_padrinos.jpg" alt="image06"/>
                <div class="ei-title">
                    <h2 class="ei-title-link">Aprender juntos</h2>
                   	<h3>To learn together</h3>
                </div>
            </a>
        </li>
        <li>
            <a href="{{url('articles/wgo/induction-day')}}">
                <img src="./img/slider/so_art_realism.jpg" alt="image07"/>
                <div class="ei-title">
					<h2 class="ei-title-link">Día de Inducción</h2>
                    <h3>Induction Day</h3>
                </div>
            </a>
        </li>
    </ul><!-- ei-slider-large -->
    <ul class="ei-slider-thumbs">
        <li class="ei-slider-element">Current</li>
        <li><a href="#">Slide 1</a></li>
        <li><a href="#">Slide 2</a></li>
        <li><a href="#">Slide 3</a></li>
        <li><a href="#">Slide 4</a></li>
        <li><a href="#">Slide 5</a></li>
		<li><a href="#">Slide 6</a></li>
        <li><a href="#">Slide 7</a></li>
    </ul><!-- ei-slider-thumbs -->
</div><!-- ei-slider -->
<div class="slider-wgo">
    <a href="{{ url('/wgo') }}">{!! trans('home/slider.title') !!}</a>
</div>


