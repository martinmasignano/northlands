{{-- Language set in lang/{language}/aboutus/authorities.php --}}

@extends('layouts.main')

@section('content_class','authorities')
@section('content')

	<div class="row">
		<div class="col-md-7">
			<h2>{!! trans('aboutus/authorities.header') !!}</h2>
			<p>{!! trans('aboutus/authorities.copy') !!}</p>
		</div>
		<div class="hidden-xs col-md-5">
			<h3>{!! trans('aboutus/authorities.boardtitle') !!}</h3>
			<ul>
				<li><strong>{!! trans('aboutus/authorities.president') !!}:</strong> Pablo Rasore.</li>
				<li><strong>{!! trans('aboutus/authorities.vice') !!}:</strong> Andrés Cazes.</li>
				<li><strong>{!! trans('aboutus/authorities.treasurer') !!}:</strong> Daniel Sielecki.</li>
				<li><strong>{!! trans('aboutus/authorities.secretary') !!}:</strong> Guadalupe Pasman.</li>
				<li><strong>{!! trans('aboutus/authorities.members') !!}:</strong>Marcos Clutterbuck, Alejandro Duhau, Roberto Helbling, Roberto Lavista Seguí, Marcela Rodrigo, Adrian Sucari.</li>
				<li><strong>{!! trans('aboutus/authorities.substitutesmembers') !!}:</strong> Lucila Fernie, Marcelo Salas Martinez.</li>
				<li><strong>{!! trans('aboutus/authorities.reviewer') !!}:</strong> Horacio Mantelini</li>
				<li><strong>{!! trans('aboutus/authorities.substitutereviewer') !!}:</strong> Jorge Perdomo.</li>
			</ul>
		</div>
	</div>
	{{-- Versión desktop --}}
	<div class="row hidden-xs">
		<div class="col-xs-12">
			<h3>{!! trans('aboutus/authorities.subtitleAuthorities') !!}</h3>
		</div>
		<div class="col-sm-6 col-md-4 col-lg-3">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body panel-body-bone">
						{{-- <a href="#" class="media" data-toggle="modal" data-target="#Modal1"> --}}
							<div class="media-left">
								{!! Html::image('./img/authorities/NickReeves.jpg', 'Nicholas Reeves', array('class' => 'media-object')) !!}
							</div>
							<div class="media-body">
								<h5 class="media-heading">{!! trans('aboutus/authorities.headmasterName') !!}</h5>
								<h6>{!! trans('aboutus/authorities.headmaster') !!}</h6>
							</div>
						{{-- </a> --}}
			    	</div>
				</div>
			</div>
		</div>

		<div class="col-sm-6 col-md-4 col-lg-3">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body panel-body-kangaroo">
						{{-- <a href="#" class="media" data-toggle="modal" data-target="#Modal2"> --}}
							<div class="media-left">
								{!! Html::image('./img/authorities/MarisaPerazzo.jpg', 'Marisa Perazzo', array('class' => 'media-object')) !!}
							</div>
							<div class="media-body">
								<h5 class="media-heading">{!! trans('aboutus/authorities.vicechancellorName') !!}</h5>
								<h6>{!! trans('aboutus/authorities.vicechancellor') !!}</h6>
							</div>
						{{-- </a> --}}
			    	</div>
				</div>
			</div>
		</div>

		<div class="col-sm-6 col-md-4 col-lg-3">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body panel-body-mischka">
						{{-- <a href="#" class="media" data-toggle="modal" data-target="#Modal3"> --}}
							<div class="media-left">
								{!! Html::image('./img/authorities/Mauge.jpg', 'María Eugenia Rodriguez', array('class' => 'media-object')) !!}
							</div>
							<div class="media-body">
								<h5 class="media-heading">{!! trans('aboutus/authorities.dafName') !!}</h5>
								<h6>{!! trans('aboutus/authorities.daf') !!}</h6>
							</div>
						{{-- </a> --}}
			    	</div>
				</div>
			</div>
		</div>

		<div class="col-sm-6 col-md-4 col-lg-3">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body panel-body-kangaroo">
						{{-- <a href="#" class="media" data-toggle="modal" data-target="#Modal4"> --}}
							<div class="media-left">
								{!! Html::image('./img/authorities/FlopySackmann.jpg', 'Florencia Sackmann Sala', array('class' => 'media-object')) !!}
							</div>
							<div class="media-body">
								<h5 class="media-heading">{!! trans('aboutus/authorities.devDirectorName') !!}</h5>
								<h6>{!! trans('aboutus/authorities.devDirector') !!}</h6>
							</div>
						{{-- </a> --}}
			    	</div>
				</div>
			</div>
		</div>

		<div class="col-sm-6 col-md-4 col-lg-3">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body panel-body-mischka">
						{{-- <a href="#" class="media" data-toggle="modal" data-target="#Modal5"> --}}
							<div class="media-left">
								{!! Html::image('./img/authorities/JorgeRey.jpg', 'Jorge Rey', array('class' => 'media-object')) !!}
							</div>
							<div class="media-body">
								<h5 class="media-heading">{!! trans('aboutus/authorities.sportsDirectorName') !!}</h5>
								<h6>{!! trans('aboutus/authorities.sportsDirector') !!}</h6>
							</div>
						{{-- </a> --}}
			    	</div>
				</div>
			</div>
		</div>

		<div class="col-sm-6 col-md-4 col-lg-3">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body panel-body-bone">
						{{-- <a href="#" class="media" data-toggle="modal" data-target="#Modal6"> --}}
							<div class="media-left">
								{!! Html::image('./img/authorities/LeandroValle.jpg', 'Leandro Valle', array('class' => 'media-object')) !!}
							</div>
							<div class="media-body">
								<h5 class="media-heading">{!! trans('aboutus/authorities.perfArtsName') !!}</h5>
								<h6>{!! trans('aboutus/authorities.perfArts') !!}</h6>
							</div>
						{{-- </a> --}}
			    	</div>
				</div>
			</div>
		</div>

		<div class="col-sm-6 col-md-4 col-lg-3">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body panel-body-bone">
						{{-- <a href="#" class="media" data-toggle="modal" data-target="#Modal7"> --}}
							<div class="media-left">
								{!! Html::image('./img/authorities/AleBatu.jpg', 'Alejandra Batu', array('class' => 'media-object')) !!}
							</div>
							<div class="media-body">
								<h5 class="media-heading">{!! trans('aboutus/authorities.kinderNDirectorName') !!}</h5>
								<h6>{!! trans('aboutus/authorities.kinderNDirector') !!}</h6>
							</div>
						{{-- </a> --}}
			    	</div>
				</div>
			</div>
		</div>

		<div class="col-sm-6 col-md-4 col-lg-3">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body panel-body-kangaroo">
						{{-- <a href="#" class="media" data-toggle="modal" data-target="#Modal8"> --}}
							<div class="media-left">
								{!! Html::image('./img/authorities/PoppyGilbert.jpg', 'Patricia Christensen de Gilbert', array('class' => 'media-object')) !!}
							</div>
							<div class="media-body">
								<h5 class="media-heading">{!! trans('aboutus/authorities.PrimaryNDirectorName') !!}</h5>
								<h6>{!! trans('aboutus/authorities.PrimaryNDirector') !!}</h6>
							</div>
						{{-- </a> --}}
			    	</div>
				</div>
			</div>
		</div>

		<div class="col-sm-6 col-md-4 col-lg-3">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body panel-body-mischka">
						{{-- <a href="#" class="media" data-toggle="modal" data-target="#Modal9"> --}}
							<div class="media-left">
								{!! Html::image('./img/authorities/ShaunHudson.jpg', 'Shaun Hudson', array('class' => 'media-object')) !!}
							</div>
							<div class="media-body">
								<h5 class="media-heading">{!! trans('aboutus/authorities.SecondaryNDirectorName') !!}</h5>
								<h6>{!! trans('aboutus/authorities.SecondaryNDirector') !!}</h6>
							</div>
						{{-- </a> --}}
			    	</div>
				</div>
			</div>
		</div>

		<div class="col-sm-6 col-md-4 col-lg-3">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body panel-body-kangaroo">
						{{-- <a href="#" class="media" data-toggle="modal" data-target="#Modal10"> --}}
							<div class="media-left">
								{!! Html::image('./img/authorities/VHenson.jpg', 'Vanesa Henson', array('class' => 'media-object')) !!}
							</div>
							<div class="media-body">
								<h5 class="media-heading">{!! trans('aboutus/authorities.kinderODirectorName') !!}</h5>
								<h6>{!! trans('aboutus/authorities.kinderODirector') !!}</h6>
							</div>
						{{-- </a> --}}
			    	</div>
				</div>
			</div>
		</div>

		<div class="col-sm-6 col-md-4 col-lg-3">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body panel-body-mischka">
						{{-- <a href="#" class="media" data-toggle="modal" data-target="#Modal11"> --}}
							<div class="media-left">
								{!! Html::image('./img/authorities/AdrianaGarciaP.jpg', 'Adriana García Posadas', array('class' => 'media-object')) !!}
							</div>
							<div class="media-body">
								<h5 class="media-heading">{!! trans('aboutus/authorities.PrimaryODirectorName') !!}</h5>
								<h6>{!! trans('aboutus/authorities.PrimaryODirector') !!}</h6>
							</div>
						{{-- </a> --}}
			    	</div>
				</div>
			</div>
		</div>

		<div class="col-sm-6 col-md-4 col-lg-3">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body panel-body-bone">
						{{-- <a href="#" class="media" data-toggle="modal" data-target="#Modal12"> --}}
							<div class="media-left">
								{!! Html::image('./img/authorities/hughesSofia.jpg', 'Sofia Hughes', array('class' => 'media-object')) !!}
							</div>
							<div class="media-body">
								<h5 class="media-heading">{!! trans('aboutus/authorities.SecondaryODirectorName') !!}</h5>
								<h6>{!! trans('aboutus/authorities.SecondaryODirector') !!}</h6>
							</div>
						{{-- </a> --}}
			    	</div>
				</div>
			</div>
		</div>
	</div>

	{{-- Versión mobile --}}
	<div class="row visible-xs-block">
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingOne">
				<h3 class="panel-title text-center">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
						{!! trans('aboutus/authorities.boardtitle') !!}
					</a>
				</h3>
			</div>
			<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
				<div class="panel-body">
					<ul>
		    			<li><strong>{!! trans('aboutus/authorities.president') !!}:</strong> Pablo Rasore.</li>
						<li><strong>{!! trans('aboutus/authorities.vice') !!}:</strong> Andrés Cazes.</li>
						<li><strong>{!! trans('aboutus/authorities.treasurer') !!}:</strong> Daniel Sielecki.</li>
						<li><strong>{!! trans('aboutus/authorities.secretary') !!}:</strong> Guadalupe Pasman.</li>
						<li><strong>{!! trans('aboutus/authorities.members') !!}:</strong>Marcos Clutterbuck, Alejandro Duhau, Roberto Helbling, Roberto Lavista Seguí, Marcela Rodrigo, Adrian Sucari.</li>
						<li><strong>{!! trans('aboutus/authorities.substitutesmembers') !!}:</strong> Lucila Fernie, Marcelo Salas Martinez.</li>
						<li><strong>{!! trans('aboutus/authorities.reviewer') !!}:</strong> Horacio Mantelini</li>
						<li><strong>{!! trans('aboutus/authorities.substitutereviewer') !!}:</strong> Jorge Perdomo.</li>
		    		</ul>
				</div>
			</div>
		</div>

		<div class="col-xs-12">
			<h3>{!! trans('aboutus/authorities.subtitleAuthorities') !!}</h3>
			<div class="row">
	    		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					<div class="panel panel-default">
						<div class="panel-heading panel-heading-bone" role="tab" id="headingTwo">
							<h5 class="panel-title">
								{{-- <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> --}}
									<div class="media-left">
										{!! Html::image('./img/authorities/NickReeves.jpg', 'Nicholas Reeves', array('class' => 'media-object')) !!}
									</div>
									<div class="media-body">
										<h5 class="media-heading">{!! trans('aboutus/authorities.headmasterName') !!}</h5>
										<h6>{!! trans('aboutus/authorities.headmaster') !!}</h6>
									</div>
								{{-- </a> --}}
							</h5>
						</div>
						{{-- <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
							<div class="panel-body">
								<p>{!! trans('aboutus/authorities.headmasterDescription') !!}</p>
								<p><a href="mailto:nreeves@northlands.edu.ar">nreeves@northlands.edu.ar</a></p>
							</div>
						</div> --}}
					</div>
					<div class="panel panel-default">
						<div class="panel-heading panel-heading-kangaroo" role="tab" id="headingThree">
							<h5 class="panel-title">
								{{-- <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> --}}
									<div class="media-left">
										{!! Html::image('./img/authorities/MarisaPerazzo.jpg', 'Marisa Perazzo', array('class' => 'media-object')) !!}
									</div>
									<div class="media-body">
										<h5 class="media-heading">{!! trans('aboutus/authorities.vicechancellorName') !!}</h5>
										<h6>{!! trans('aboutus/authorities.vicechancellor') !!}</h6>
									</div>
								{{-- </a> --}}
							</h5>
						</div>
						{{-- <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
							<div class="panel-body">
								<p>{!! trans('aboutus/authorities.vicechancellorDescription') !!}</p>
								<p><a href="mailto:mperazzo@northlands.edu.ar">mperazzo@northlands.edu.ar</a></p>
							</div>
						</div> --}}
					</div>
					<div class="panel panel-default">
						<div class="panel-heading panel-heading-mischka" role="tab" id="headingFour">
							<h5 class="panel-title">
								{{-- <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour"> --}}
									<div class="media-left">
										{!! Html::image('./img/authorities/Mauge.jpg', 'María Eugenia Rodriguez', array('class' => 'media-object')) !!}
									</div>
									<div class="media-body">
										<h5 class="media-heading">{!! trans('aboutus/authorities.dafName') !!}</h5>
										<h6>{!! trans('aboutus/authorities.daf') !!}</h6>
									</div>
								{{-- </a> --}}
							</h5>
						</div>
						{{-- <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
							<div class="panel-body">
								<p>{!! trans('aboutus/authorities.dafDescription') !!}</p>
								<p><a href="mailto:merodriguez@northlands.edu.ar">merodriguez@northlands.edu.ar</a></p>
							</div>
						</div> --}}
					</div>
					<div class="panel panel-default">
						<div class="panel-heading panel-heading-bone" role="tab" id="headingFive">
							<h5 class="panel-title">
								{{-- <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive"> --}}
									<div class="media-left">
										{!! Html::image('./img/authorities/FlopySackmann.jpg', 'Florencia Sackmann Sala', array('class' => 'media-object')) !!}
									</div>
									<div class="media-body">
										<h5 class="media-heading">{!! trans('aboutus/authorities.devDirectorName') !!}</h5>
										<h6>{!! trans('aboutus/authorities.devDirector') !!}</h6>
									</div>
								{{-- </a> --}}
							</h5>
						</div>
						{{-- <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
							<div class="panel-body">
								<p>{!! trans('aboutus/authorities.devDirectorDescription') !!}</p>
								<p><a href="mailto:fsackmannsala@northlands.edu.ar">fsackmannsala@northlands.edu.ar</a></p>
							</div>
						</div> --}}
					</div>
					<div class="panel panel-default">
						<div class="panel-heading panel-heading-kangaroo" role="tab" id="headingSix">
							<h5 class="panel-title">
								{{-- <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix"> --}}
									<div class="media-left">
										{!! Html::image('./img/authorities/JorgeRey.jpg', 'Jorge Rey', array('class' => 'media-object')) !!}
									</div>
									<div class="media-body">
										<h5 class="media-heading">{!! trans('aboutus/authorities.sportsDirectorName') !!}</h5>
										<h6>{!! trans('aboutus/authorities.sportsDirector') !!}</h6>
									</div>
								{{-- </a> --}}
							</h5>
						</div>
						{{-- <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
							<div class="panel-body">
								<p>{!! trans('aboutus/authorities.sportsDirectorDescription') !!}</p>
								<p><a href="mailto:jrey@northlands.edu.ar">jrey@northlands.edu.ar</a></p>
							</div>
						</div> --}}
					</div>
					<div class="panel panel-default">
						<div class="panel-heading panel-heading-mischka" role="tab" id="headingSeven">
							<h5 class="panel-title">
								{{-- <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven"> --}}
									<div class="media-left">
										{!! Html::image('./img/authorities/LeandroValle.jpg', 'Leandro Valle', array('class' => 'media-object')) !!}
									</div>
									<div class="media-body">
										<h5 class="media-heading">{!! trans('aboutus/authorities.perfArtsName') !!}</h5>
										<h6>{!! trans('aboutus/authorities.perfArts') !!}</h6>
									</div>
								{{-- </a> --}}
							</h5>
						</div>
						{{-- <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
							<div class="panel-body">
								<p>{!! trans('aboutus/authorities.perfArtsDescription') !!}</p>
								<p><a href="mailto:lvalle@northlands.edu.ar">lvalle@northlands.edu.ar</a></p>
							</div>
						</div> --}}
					</div>
					<div class="panel panel-default">
						<div class="panel-heading panel-heading-bone" role="tab" id="headingEight">
							<h5 class="panel-title">
								{{-- <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight"> --}}
									<div class="media-left">
										{!! Html::image('./img/authorities/AleBatu.jpg', 'Alejandra Batu', array('class' => 'media-object')) !!}
									</div>
									<div class="media-body">
										<h5 class="media-heading">{!! trans('aboutus/authorities.kinderNDirectorName') !!}</h5>
										<h6>{!! trans('aboutus/authorities.kinderNDirector') !!}</h6>
									</div>
								{{-- </a> --}}
							</h5>
						</div>
						{{-- <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
							<div class="panel-body">
								<p>{!! trans('aboutus/authorities.kinderNDirectorDescription') !!}</p>
								<p><a href="mailto:sarndtlamas@northlands.edu.ar">sarndtlamas@northlands.edu.ar</a></p>
							</div>
						</div> --}}
					</div>
					<div class="panel panel-default">
						<div class="panel-heading panel-heading-kangaroo" role="tab" id="headingNine">
							<h5 class="panel-title">
								{{-- <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine"> --}}
									<div class="media-left">
										{!! Html::image('./img/authorities/PoppyGilbert.jpg', 'Patricia Christensen de Gilbert', array('class' => 'media-object')) !!}
									</div>
									<div class="media-body">
										<h5 class="media-heading">{!! trans('aboutus/authorities.PrimaryNDirectorName') !!}</h5>
										<h6>{!! trans('aboutus/authorities.PrimaryNDirector') !!}</h6>
									</div>
								{{-- </a> --}}
							</h5>
						</div>
						{{-- <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
							<div class="panel-body">
								<p>{!! trans('aboutus/authorities.PrimaryNDirectorDescription') !!}</p>
								<p><a href="mailto:pchristensen@northlands.edu.ar">pchristensen@northlands.edu.ar</a></p>
							</div>
						</div> --}}
					</div>
					<div class="panel panel-default">
						<div class="panel-heading panel-heading-mischka" role="tab" id="headingTen">
							<h5 class="panel-title">
								{{-- <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="false" aria-controls="collapseTen"> --}}
									<div class="media-left">
										{!! Html::image('./img/authorities/ShaunHudson.jpg', 'Shaun Hudson', array('class' => 'media-object')) !!}
									</div>
									<div class="media-body">
										<h5 class="media-heading">{!! trans('aboutus/authorities.SecondaryNDirectorName') !!}</h5>
										<h6>{!! trans('aboutus/authorities.SecondaryNDirector') !!}</h6>
									</div>
								{{-- </a> --}}
							</h5>
						</div>
						{{-- <div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen">
							<div class="panel-body">
								<p>{!! trans('aboutus/authorities.SecondaryNDirectorDescription') !!}</p>
								<p><a href="mailto:agromick@northlands.edu.ar">agromick@northlands.edu.ar</a></p>
							</div>
						</div> --}}
					</div>
					<div class="panel panel-default">
						<div class="panel-heading panel-heading-bone" role="tab" id="headingEleven">
							<h5 class="panel-title">
								{{-- <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven"> --}}
									<div class="media-left">
										{!! Html::image('./img/authorities/VHenson.jpg', 'Vanesa Henson', array('class' => 'media-object')) !!}
									</div>
									<div class="media-body">
										<h5 class="media-heading">{!! trans('aboutus/authorities.kinderODirectorName') !!}</h5>
										<h6>{!! trans('aboutus/authorities.kinderODirector') !!}</h6>
									</div>
								{{-- </a> --}}
							</h5>
						</div>
						{{-- <div id="collapseEleven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEleven">
							<div class="panel-body">
								<p>{!! trans('aboutus/authorities.kinderODirectorDescription') !!}</p>
								<p><a href="mailto:vhenson@northlands.edu.ar">vhenson@northlands.edu.ar</a></p>
							</div>
						</div> --}}
					</div>
					<div class="panel panel-default">
						<div class="panel-heading panel-heading-kangaroo" role="tab" id="headingTwelve">
							<h5 class="panel-title">
								{{-- <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwelve" aria-expanded="false" aria-controls="collapseTwelve"> --}}
									<div class="media-left">
										{!! Html::image('./img/authorities/AdrianaGarciaP.jpg', 'Adriana García Posadas', array('class' => 'media-object')) !!}
									</div>
									<div class="media-body">
										<h5 class="media-heading">{!! trans('aboutus/authorities.PrimaryODirectorName') !!}</h5>
										<h6>{!! trans('aboutus/authorities.PrimaryODirector') !!}</h6>
									</div>
								{{-- </a> --}}
							</h5>
						</div>
						{{-- <div id="collapseTwelve" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwelve">
							<div class="panel-body">
								<p>{!! trans('aboutus/authorities.PrimaryODirectorDescription') !!}</p>
								<p><a href="mailto:egonzalez@northlands.edu.ar">egonzalez@northlands.edu.ar</a></p>
							</div>
						</div> --}}
					</div>
					<div class="panel panel-default">
						<div class="panel-heading panel-heading-mischka" role="tab" id="headingThirteen">
							<h5 class="panel-title">
								{{-- <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThirteen" aria-expanded="false" aria-controls="collapseThirteen"> --}}
									<div class="media-left">
										{!! Html::image('./img/authorities/hughesSofia.jpg', 'Sofia Hughes', array('class' => 'media-object')) !!}
									</div>
									<div class="media-body">
										<h5 class="media-heading">{!! trans('aboutus/authorities.SecondaryODirectorName') !!}</h5>
										<h6>{!! trans('aboutus/authorities.SecondaryODirector') !!}</h6>
									</div>
								{{-- </a> --}}
							</h5>
						</div>
						{{-- <div id="collapseThirteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
							<div class="panel-body">
								<p>{!! trans('aboutus/authorities.SecondaryODirectorDescription') !!}</p>
								<p><a href="mailto:shughes@northlands.edu.ar">shughes@northlands.edu.ar</a></p>
							</div>
						</div> --}}
					</div>
				</div>
			</div>
		</div>
	</div>	

	{{-- Modals --}}
	{{-- <div class="modal fade" id="Modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="media">
						<div class="media-left">
							{!! Html::image('./img/authorities/NickReeves.jpg', 'Nicholas Reeves', array('class' => 'media-object')) !!}
						</div>
						<div class="media-body">
							<h5 class="media-heading">{!! trans('aboutus/authorities.headmasterName') !!}</h5>
							<h6>{!! trans('aboutus/authorities.headmaster') !!}</h6>
						</div>
					</div>
				</div>
				<div class="modal-body">
					<p>{!! trans('aboutus/authorities.headmasterDescription') !!}</p>
					<p><a href="mailto:nreeves@northlands.edu.ar">nreeves@northlands.edu.ar</a></p>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="Modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="media">
						<div class="media-left">
							{!! Html::image('./img/authorities/MarisaPerazzo.jpg', 'Marisa Perazzo', array('class' => 'media-object')) !!}
						</div>
						<div class="media-body">
							<h5 class="media-heading">{!! trans('aboutus/authorities.vicechancellorName') !!}</h5>
							<h6>{!! trans('aboutus/authorities.vicechancellor') !!}</h6>
						</div>
					</div>
				</div>
				<div class="modal-body">
					<p>{!! trans('aboutus/authorities.vicechancellorDescription') !!}</p>
					<p><a href="mailto:mperazzo@northlands.edu.ar">mperazzo@northlands.edu.ar</a></p>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="Modal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="media">
						<div class="media-left">
							{!! Html::image('./img/authorities/Mauge.jpg', 'María Eugenia Rodriguez', array('class' => 'media-object')) !!}
						</div>
						<div class="media-body">
							<h5 class="media-heading">{!! trans('aboutus/authorities.dafName') !!}</h5>
							<h6>{!! trans('aboutus/authorities.daf') !!}</h6>
						</div>
					</div>
				</div>
				<div class="modal-body">
					<p>{!! trans('aboutus/authorities.dafDescription') !!}</p>
					<p><a href="mailto:merodriguez@northlands.edu.ar">merodriguez@northlands.edu.ar</a></p>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="Modal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="media">
						<div class="media-left">
							{!! Html::image('./img/authorities/FlopySackmann.jpg', 'Florencia Sackmann Sala', array('class' => 'media-object')) !!}
						</div>
						<div class="media-body">
							<h5 class="media-heading">{!! trans('aboutus/authorities.devDirectorName') !!}</h5>
							<h6>{!! trans('aboutus/authorities.devDirector') !!}</h6>
						</div>
					</div>
				</div>
				<div class="modal-body">
					<p>{!! trans('aboutus/authorities.devDirectorDescription') !!}</p>
					<p><a href="mailto:fsackmannsala@northlands.edu.ar">fsackmannsala@northlands.edu.ar</a></p>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="Modal5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="media">
						<div class="media-left">
							{!! Html::image('./img/authorities/JorgeRey.jpg', 'Jorge Rey', array('class' => 'media-object')) !!}
						</div>
						<div class="media-body">
							<h5 class="media-heading">{!! trans('aboutus/authorities.sportsDirectorName') !!}</h5>
							<h6>{!! trans('aboutus/authorities.sportsDirector') !!}</h6>
						</div>
					</div>
				</div>
				<div class="modal-body">
					<p>{!! trans('aboutus/authorities.sportsDirectorDesciption') !!}</p>
					<p><a href="mailto:jrey@northlands.edu.ar">jrey@northlands.edu.ar</a></p>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="Modal6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="media">
						<div class="media-left">
							{!! Html::image('./img/authorities/LeandroValle.jpg', 'Leandro Valle', array('class' => 'media-object')) !!}
						</div>
						<div class="media-body">
							<h5 class="media-heading">{!! trans('aboutus/authorities.perfArtsName') !!}</h5>
							<h6>{!! trans('aboutus/authorities.perfArts') !!}</h6>
						</div>
					</div>
				</div>
				<div class="modal-body">
					<p>{!! trans('aboutus/authorities.perfArtsDescription') !!}</p>
					<p><a href="mailto:lvalle@northlands.edu.ar">lvalle@northlands.edu.ar</a></p>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="Modal7" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="media">
						<div class="media-left">
							{!! Html::image('./img/authorities/AleBatu.jpg', 'Alejandra Batu', array('class' => 'media-object')) !!}
						</div>
						<div class="media-body">
							<h5 class="media-heading">{!! trans('aboutus/authorities.kinderNDirectorName') !!}</h5>
							<h6>{!! trans('aboutus/authorities.kinderNDirector') !!}</h6>
						</div>
					</div>
				</div>
				<div class="modal-body">
					<p>{!! trans('aboutus/authorities.kinderNDirectorDescription') !!}</p>
					<p><a href="mailto:sarndtlamas@northlands.edu.ar">sarndtlamas@northlands.edu.ar</a></p>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="Modal8" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="media">
						<div class="media-left">
							{!! Html::image('./img/authorities/PoppyGilbert.jpg', 'Patricia Christensen de Gilbert', array('class' => 'media-object')) !!}
						</div>
						<div class="media-body">
							<h5 class="media-heading">{!! trans('aboutus/authorities.PrimaryNDirectorName') !!}</h5>
							<h6>{!! trans('aboutus/authorities.PrimaryNDirector') !!}</h6>
						</div>
					</div>
				</div>
				<div class="modal-body">
					<p>{!! trans('aboutus/authorities.PrimaryNDirectorDescription') !!}</p>
					<p><a href="mailto:pchristensen@northlands.edu.ar">pchristensen@northlands.edu.ar</a></p>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="Modal9" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="media">
						<div class="media-left">
							{!! Html::image('./img/authorities/ShaunHudson.jpg', 'Shaun Hudson', array('class' => 'media-object')) !!}
						</div>
						<div class="media-body">
							<h5 class="media-heading">{!! trans('aboutus/authorities.SecondaryNDirectorName') !!}</h5>
							<h6>{!! trans('aboutus/authorities.SecondaryNDirector') !!}</h6>
						</div>
					</div>
				</div>
				<div class="modal-body">
					<p>{!! trans('aboutus/authorities.SecondaryNDirectorDescription') !!}</p>
					<p><a href="mailto:shudson@northlands.edu.ar">shudson@northlands.edu.ar</a></p>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="Modal10" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="media">
						<div class="media-left">
							{!! Html::image('./img/authorities/VHenson.jpg', 'Vanesa Henson', array('class' => 'media-object')) !!}
						</div>
						<div class="media-body">
							<h5 class="media-heading">{!! trans('aboutus/authorities.kinderODirectorName') !!}</h5>
							<h6>{!! trans('aboutus/authorities.kinderODirector') !!}</h6>
						</div>
					</div>
				</div>
				<div class="modal-body">
					<p>{!! trans('aboutus/authorities.kinderODirectorDescription') !!}</p>
					<p><a href="mailto:vhenson@northlands.edu.ar">vhenson@northlands.edu.ar</a></p>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="Modal11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="media">
						<div class="media-left">
							{!! Html::image('./img/authorities/AdrianaGarciaP.jpg', 'Adriana García Posadas', array('class' => 'media-object')) !!}
						</div>
						<div class="media-body">
							<h5 class="media-heading">{!! trans('aboutus/authorities.PrimaryODirectorName') !!}</h5>
							<h6>{!! trans('aboutus/authorities.PrimaryODirector') !!}</h6>
						</div>
					</div>
				</div>
				<div class="modal-body">
					<p>{!! trans('aboutus/authorities.PrimaryODirectorDescription') !!}</p>
					<p><a href="mailto:agarcia@northlands.edu.ar">agarcia@northlands.edu.ar</a></p>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="Modal12" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="media">
						<div class="media-left">
							{!! Html::image('./img/authorities/hughesSofia.jpg', 'Sofia Hughes', array('class' => 'media-object')) !!}
						</div>
						<div class="media-body">
							<h5 class="media-heading">{!! trans('aboutus/authorities.SecondaryODirectorName') !!}</h5>
							<h6>{!! trans('aboutus/authorities.SecondaryODirector') !!}</h6>
						</div>
					</div>
				</div>
				<div class="modal-body">
					<p>{!! trans('aboutus/authorities.SecondaryODirectorDescription') !!}</p>
					<p><a href="mailto:shughes@northlands.edu.ar">shughes@northlands.edu.ar</a></p>
				</div>
			</div>
		</div>
	</div> --}}

@endsection
