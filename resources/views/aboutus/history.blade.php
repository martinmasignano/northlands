{{-- Language set in lang/{language}/aboutus/history.php --}}

@extends('layouts.main')

@section('content_class','ourstory')
@section('content')

	<div class="row">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h1>{!! trans('aboutus/history.header') !!}</h1>
					
					<p>{!! trans('aboutus/history.text1') !!}</p>
					<p>{!! trans('aboutus/history.text2') !!}</p>
					<p>{!! trans('aboutus/history.text3') !!}</p>
					<br><br>
					<div class="row hidden-xs hidden-sm">
						<div class="col-xs-6">
							<iframe src="https://player.vimeo.com/video/132367510?byline=0&portrait=0" width="100%" height="200" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div>
						<div class="col-xs-6">
							<iframe src="https://player.vimeo.com/video/142891523?byline=0&portrait=0" width="100%" height="200" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div>
					</div>
				</div>
				{{-- Linea de tiempo --}}
				<div class="col-md-6">
					<h3 class="timeline-header text-center">{!! trans('aboutus/history.timeline') !!}</h3>
					<div class="timeline">
						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<div class="panel panel-yellow">
								<div class="panel-heading" role="tab" id="headingOne">
									<h4 class="row panel-title">
										<span class="col-xs-3">1920</span>
										<a class="col-xs-6" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Brightman</a>
										<span class="col-xs-3">1961</span>
									</h4>
								</div>

								<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body">
										@if (App::getLocale() == 'en')
											<img src="{{ asset('/img/timeline/brightman-en.svg') }}" alt="Brightman">
										@else
											<img src="{{ asset('/img/timeline/brightman-es.svg') }}" alt="Brightman">
										@endif
									</div>
								</div>
							</div>

							<div class="panel panel-orange">
								<div class="panel-heading" role="tab" id="headingTwo">
									<h4 class="row panel-title">
										<span class="col-xs-3">1961</span>
										<a class="col-xs-6 collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Wallace</a>
										<span class="col-xs-3">1968</span>
									</h4>
								</div>

								<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
									<div class="panel-body">
										@if (App::getLocale() == 'en')
											<img src="{{ asset('/img/timeline/wallace-en.svg') }}" alt="Wallace">
										@else
											<img src="{{ asset('/img/timeline/wallace-es.svg') }}" alt="Wallace">
										@endif
									</div>
								</div>
							</div>

							<div class="panel panel-red">
								<div class="panel-heading" role="tab" id="headingThree">
									<h4 class="row panel-title">
										<span class="col-xs-3">1969</span>
										<a class="col-xs-6 collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Parczewski</a>
										<span class="col-xs-3">1982</span>
									</h4>
								</div>

								<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
									<div class="panel-body">
										@if (App::getLocale() == 'en')
											<img src="{{ asset('/img/timeline/parczewski-en.svg') }}" alt="Parczewski">
										@else
											<img src="{{ asset('/img/timeline/parczewski-es.svg') }}" alt="Parczewski">
										@endif
									</div>
								</div>
							</div>

							<div class="panel panel-violet">
								<div class="panel-heading" role="tab" id="headingFour">
									<h4 class="row panel-title">
										<span class="col-xs-3">1983</span>
										<a class="col-xs-6 collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">Taylor</a>
										<span class="col-xs-3">1993</span>
									</h4>
								</div>

								<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
									<div class="panel-body">
										@if (App::getLocale() == 'en')
											<img src="{{ asset('/img/timeline/taylor-en.svg') }}" alt="Taylor">
										@else
											<img src="{{ asset('/img/timeline/taylor-es.svg') }}" alt="Taylor">
										@endif
									</div>
								</div>
							</div>

							<div class="panel panel-purple">
								<div class="panel-heading" role="tab" id="headingFive">
									<h4 class="row panel-title">
										<span class="col-xs-3">1994</span>
										<a class="col-xs-6 collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">Jackson</a>
										<span class="col-xs-3">1998</span>
									</h4>
								</div>

								<div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
									<div class="panel-body">
										@if (App::getLocale() == 'en')
											<img src="{{ asset('/img/timeline/jackson-en.svg') }}" alt="Jackson">
										@else
											<img src="{{ asset('/img/timeline/jackson-es.svg') }}" alt="Jackson">
										@endif
									</div>
								</div>
							</div>

							<div class="panel panel-blue">
								<div class="panel-heading" role="tab" id="headingSix">
									<h4 class="row panel-title">
										<span class="col-xs-3">1999</span>
										<a class="col-xs-6 collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">Price - Cabrera</a>
										<span class="col-xs-3">2004</span>
									</h4>
								</div>

								<div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
									<div class="panel-body">
										@if (App::getLocale() == 'en')
											<img src="{{ asset('/img/timeline/sprice-cabrera-en.svg') }}" alt="Price-Cabrera">
										@else
											<img src="{{ asset('/img/timeline/price-cabrera-es.svg') }}" alt="Price-Cabrera">
										@endif
									</div>
								</div>
							</div>

							<div class="panel panel-lightblue">
								<div class="panel-heading" role="tab" id="headingSeven">
									<h4 class="row panel-title">
										<span class="col-xs-3">2005</span>
										<a class="col-xs-6 collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">Gibbs</a>
										<span class="col-xs-3">2008</span>
									</h4>
								</div>

								<div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
									<div class="panel-body">
										@if (App::getLocale() == 'en')
											<img src="{{ asset('/img/timeline/gibbs-en.svg') }}" alt="Gibbs">
										@else
											<img src="{{ asset('/img/timeline/tgibbs-es.svg') }}" alt="Gibbs">
										@endif
									</div>
								</div>
							</div>

							<div class="panel panel-green">
								<div class="panel-heading" role="tab" id="headingEight">
									<h4 class="row panel-title">
										<span class="col-xs-3">2009</span>
										<a class="col-xs-6 collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">Magenta</a>
										<span class="col-xs-3">2013</span>
									</h4>
								</div>

								<div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
									<div class="panel-body">
										@if (App::getLocale() == 'en')
											<img src="{{ asset('/img/timeline/magenta-en.svg') }}" alt="Magenta">
										@else
											<img src="{{ asset('/img/timeline/smagenta-es.svg') }}" alt="Magenta">
										@endif
									</div>
								</div>
							</div>

							<div class="panel panel-gray">
								<div class="panel-heading" role="tab" id="headingNine">
									<h4 class="row panel-title">
										<span class="col-xs-3">2014</span>
										<a class="col-xs-6 collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">Reeves</a>
										<span class="col-xs-3">TODAY</span>
									</h4>
								</div>

								<div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
									<div class="panel-body">
										@if (App::getLocale() == 'en')
											<img src="{{ asset('/img/timeline/reeves-en.svg') }}" alt="Reeves">
										@else
											<img src="{{ asset('/img/timeline/reeves-es.svg') }}" alt="Reeves">
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection
