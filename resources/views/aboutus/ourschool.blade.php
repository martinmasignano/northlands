{{-- Language set in lang/{language}/aboutus/ourschool.php --}}

@extends('layouts.main')

@section('content_class','ourschool')
@section('content')

    <div class="row">
        <div class="col-xs-12 col-md-8">
            <h1>{!! trans('aboutus/ourschool.header') !!}</h1>
            <p>{!! trans('aboutus/ourschool.content') !!}</p> 
        </div>
        <div class="col-xs-12 col-md-4">
            <h3 class="ourschool-header">{!! trans('aboutus/ourschool.description') !!}</h3>
        </div>
    </div>

    <div class="col-xs-6 col-sm-3 col-md-2">
        <div class="row">
            @if (App::getLocale() == 'en')
                <img src="{{ asset('/img/ourschool/alumnos.svg') }}" alt="">
            @else
                <img src="{{ asset('/img/ourschool/alumnos-es.svg') }}" alt="">
            @endif
        </div>
    </div>

    <div class="col-xs-6 col-sm-3 col-md-2">
        <div class="row">
            @if (App::getLocale() == 'en')
                <img src="{{ asset('/img/ourschool/porcentajes.svg') }}" alt="">
            @else
                <img src="{{ asset('/img/ourschool/porcentajes-es.svg') }}" alt="">
            @endif
        </div>
    </div>
     
    <div class="col-xs-6 col-sm-3 col-md-2">
        <div class="row">
            @if (App::getLocale() == 'en')
                <img src="{{ asset('/img/ourschool/sedes.svg') }}" alt="">
            @else
                <img src="{{ asset('/img/ourschool/sedes-es.svg') }}" alt="">
            @endif
        </div>
    </div>

    <div class="col-xs-6 col-sm-3 col-md-2">
        <div class="row">
            @if (App::getLocale() == 'en')
                <img src="{{ asset('/img/ourschool/sedes-deportivas.svg') }}" alt="">
            @else
                <img src="{{ asset('/img/ourschool/sedes-deportivas-es.svg') }}" alt="">
            @endif
        </div>
    </div>

    <div class="visible-md-block visible-lg-block">
        <div class="col-md-2 col-lg-2">
            <div class="row">
                @if (App::getLocale() == 'en')
                    <img src="{{ asset('/img/ourschool/porcentaje-nordelta.svg') }}" alt="">
                @else
                    <img src="{{ asset('/img/ourschool/porcentaje-nordelta-es.svg') }}" alt="">
                @endif
            </div>
        </div>
        <div class="col-md-2 col-lg-2">
            <div class="row">
                @if (App::getLocale() == 'en')
                    <img src="{{ asset('/img/ourschool/hectareas-nordelta.svg') }}" alt="">
                @else
                    <img src="{{ asset('/img/ourschool/hectareas-nordelta-es.svg') }}" alt="">
                @endif
            </div>
        </div>
    </div> 
    
    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="row">
            <video style="width: 100%;" controls preload>
                <source src="{{ asset('./video/nordelta.mp4')}}" type="video/mp4">
            </video>
            <div class="visible-sm-block">
                <div class="col-xs-6">
                    <div class="row">
                        @if (App::getLocale() == 'en')
                        <img src="{{ asset('/img/ourschool/porcentaje-nordelta.svg') }}" alt="">
                    @else
                        <img src="{{ asset('/img/ourschool/porcentaje-nordelta-es.svg') }}" alt="">
                    @endif
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="row">
                        @if (App::getLocale() == 'en')
                            <img src="{{ asset('/img/ourschool/hectareas-nordelta.svg') }}" alt="">
                        @else
                            <img src="{{ asset('/img/ourschool/hectareas-nordelta-es.svg') }}" alt="">
                        @endif
                    </div>
                </div>
            </div>  
        </div>
    </div>

    <div class="visible-xs-block">
        <div class="col-xs-6 col-sm-3 col-md-2">
            <div class="row">
                @if (App::getLocale() == 'en')
                    <img src="{{ asset('/img/ourschool/porcentaje-nordelta.svg') }}" alt="">
                @else
                    <img src="{{ asset('/img/ourschool/porcentaje-nordelta-es.svg') }}" alt="">
                @endif
            </div>
        </div>
        <div class="col-xs-6 col-sm-3 col-md-2">
            <div class="row">
                @if (App::getLocale() == 'en')
                    <img src="{{ asset('/img/ourschool/hectareas-nordelta.svg') }}" alt="">
                @else
                    <img src="{{ asset('/img/ourschool/hectareas-nordelta-es.svg') }}" alt="">
                @endif
            </div>
        </div>
    </div>

    <div class="hidden-xs">
        <div class="col-xs-6 col-sm-3 col-md-2">
            <div class="row">
                @if (App::getLocale() == 'en')
                    <img src="{{ asset('/img/ourschool/porcentaje-olivos.svg') }}" alt="">
                @else
                    <img src="{{ asset('/img/ourschool/porcentaje-olivos-es.svg') }}" alt="">
                @endif
            </div>
        </div>
        <div class="col-xs-6 col-sm-3 col-md-2">
            <div class="row">
                @if (App::getLocale() == 'en')
                    <img src="{{ asset('/img/ourschool/hectareas-olivos.svg') }}" alt="">
                @else
                    <img src="{{ asset('/img/ourschool/hectareas-olivos-es.svg') }}" alt="">
                @endif
            </div>
        </div>
    </div>

    <div class="hidden-xs hidden-sm">   
        <div class="col-xs-6 col-sm-3 col-md-2">
            <div class="row">
                @if (App::getLocale() == 'en')
                    <img src="{{ asset('/img/ourschool/radio.svg') }}" alt="">
                @else
                    <img src="{{ asset('/img/ourschool/radio-es.svg') }}" alt="">
                @endif
            </div>
        </div>
        <div class="col-xs-6 col-sm-3 col-md-2">
            <div class="row">
                @if (App::getLocale() == 'en')
                    <img src="{{ asset('/img/ourschool/exalumnos.svg') }}" alt="">
                @else
                    <img src="{{ asset('/img/ourschool/exalumnos-es.svg') }}" alt="">
                @endif
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="row">
            <video style="width: 100%;" controls preload>
                <source src="{{ asset('./video/olivos.mp4')}}" type="video/mp4">
            </video>
        </div>
    </div>
    
    <div class="visible-xs-block">
        <div class="col-xs-6 col-sm-3 col-md-2">
            <div class="row">
                @if (App::getLocale() == 'en')
                    <img src="{{ asset('/img/ourschool/porcentaje-olivos.svg') }}" alt="">
                @else
                    <img src="{{ asset('/img/ourschool/porcentaje-olivos-es.svg') }}" alt="">
                @endif
            </div>
        </div>
        <div class="col-xs-6 col-sm-3 col-md-2">
            <div class="row">
                @if (App::getLocale() == 'en')
                    <img src="{{ asset('/img/ourschool/hectareas-nordelta.svg') }}" alt="">
                @else
                    <img src="{{ asset('/img/ourschool/hectareas-nordelta-es.svg') }}" alt="">
                @endif
            </div>
        </div>
    </div>

    <div class="hidden-md hidden-lg">
        <div class="col-xs-6 col-sm-3 col-md-2">
            <div class="row">
                @if (App::getLocale() == 'en')
                    <img src="{{ asset('/img/ourschool/radio.svg') }}" alt="">
                @else
                    <img src="{{ asset('/img/ourschool/radio-es.svg') }}" alt="">
                @endif
            </div>
        </div>
        <div class="col-xs-6 col-sm-3 col-md-2">
            <div class="row">
                @if (App::getLocale() == 'en')
                    <img src="{{ asset('/img/ourschool/exalumnos.svg') }}" alt="">
                @else
                    <img src="{{ asset('/img/ourschool/exalumnos-es.svg') }}" alt="">
                @endif
            </div>
        </div>
    </div>

    <div class="col-xs-6 col-sm-3 col-md-2">
        <div class="row">
            @if (App::getLocale() == 'en')
                <img src="{{ asset('/img/ourschool/comedor.svg') }}" alt="">
            @else
                <img src="{{ asset('/img/ourschool/comedor-es.svg') }}" alt="">
            @endif
        </div>
    </div>

    <div class="col-xs-6 col-sm-3 col-md-2">
        <div class="row">
            @if (App::getLocale() == 'en')
                <img src="{{ asset('/img/ourschool/cuerpo-academico.svg') }}" alt="">
            @else
                <img src="{{ asset('/img/ourschool/cuerpo-academico-es.svg') }}" alt="">
            @endif
        </div>
    </div>

    <div class="col-xs-6 col-sm-3 col-md-2">
        <div class="row">
            @if (App::getLocale() == 'en')
                <img src="{{ asset('/img/ourschool/administracion.svg') }}" alt="">
            @else
                <img src="{{ asset('/img/ourschool/administracion-es.svg') }}" alt="">
            @endif
        </div>
    </div>
@endsection
