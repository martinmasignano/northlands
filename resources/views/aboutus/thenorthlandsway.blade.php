{{-- Language set in lang/{language}/aboutus/northlandsway.php --}}

@extends('layouts.main')

@section('content_class','northlandsway')
@section('content')

    <div class="row">
        <div class="container">
            <p class="lead visible-xs-block">{!! trans('aboutus/northlandsway.copy') !!}</p>

            <div class="col-xs-12 col-sm-12 hidden-md hidden-lg">
                <div class="row">
                    <h1 class="text-center">{!! trans('aboutus/northlandsway.header') !!}</h1>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6">
                <div class="row">
                    <p class="lead hidden-xs">{!! trans('aboutus/northlandsway.copy') !!}</p>
                </div>
                <div class="row visible-md-block visible-lg-block">
                    <h1>{!! trans('aboutus/northlandsway.header') !!}</h1>
                </div>
                <div class="row hidden-xs">
                    <p>{!! trans('aboutus/northlandsway.description-1') !!}</p>
                    <p>{!! trans('aboutus/northlandsway.description-2') !!}</p>
                </div>
            </div>
            
            <div class="col-xs-12 col-sm-6">
                <div class="row">
                    @if (App::getLocale() == 'en')
                        <video class="ejes" autoplay controls loop preload>
                            <source src="{{ asset('./video/northlands_way_animation.mp4')}}" type="video/mp4">
                        </video>
                    @else
                        <video class="ejes" autoplay controls loop preload>
                            <source src="{{ asset('./video/northlands_way_animation_es.mp4')}}" type="video/mp4">
                        </video>
                    @endif
                        {{-- <source src="{{ asset('./video/northlands_way_animation.ogv')}}" type="video/ogg"> --}}
                </div>
            </div>
            
            <div class="col-xs-12 col-sm-6">
                <div class="row visible-xs-block">
                    <p>{!! trans('aboutus/northlandsway.description-1') !!}</p>
                    <p>{!! trans('aboutus/northlandsway.description-2') !!}</p>
                </div>
            </div>

            <div class="col-xs-12">
                <div class="row">
                    <div class="panel-axes">
                        <h3>{!! trans('aboutus/northlandsway.axes.title') !!}</h3>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row commitment">
                                    {{-- <a class="commitment" href="{{url('thenorthlandsway/commitment')}}"> --}}
                                        <div class="col-xs-3 col-sm-12 text-center">
                                            <i class="fa fa-3x fa-circle"></i>
                                        </div>
                                        <div class="col-xs-9 col-sm-12">
                                            <span>{!! trans('aboutus/northlandsway.commitment.title') !!}</span>
                                        </div>
                                    {{-- </a> --}}
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="row education">
                                    {{-- <a class="education" href="{{url('thenorthlandsway/education')}}"> --}}
                                        <div class="col-xs-3 col-sm-12 text-center">
                                            <span class="hidden-xs">{!! trans('aboutus/northlandsway.social.title') !!}</span>
                                            <img src="../css/img/half-circle.png" alt="">
                                            <span class="hidden-xs">{!! trans('aboutus/northlandsway.personal.title') !!}</span>
                                        </div>
                                        <div class="col-xs-9 col-sm-12">
                                            <div class="col-xs-4 visible-xs-block">
                                                <div class="row">
                                                    <span>{!! trans('aboutus/northlandsway.social.title') !!}</span><br />
                                                    <span>{!! trans('aboutus/northlandsway.personal.title') !!}</span>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 visible-xs-block">
                                                <span>{!! trans('aboutus/northlandsway.education.title') !!}</span>
                                            </div>
                                            <span class="hidden-xs">{!! trans('aboutus/northlandsway.education.title') !!}</span>
                                        </div>
                                    {{-- </a> --}}
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="row profile">
                                    {{-- <a class="profile" href="{{url('thenorthlandsway/profile')}}"> --}}
                                        <div class="col-xs-3 col-sm-12 text-center">
                                            <i class="fa fa-3x fa-circle"></i>
                                        </div>
                                        <div class="col-xs-9 col-sm-12">
                                            <span>{!! trans('aboutus/northlandsway.profile.title') !!}</span>
                                        </div>
                                    {{-- </a> --}}
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="row habits">
                                    {{-- <a class="habits" href="{{url('thenorthlandsway/habits')}}">     --}}
                                        <div class="col-xs-3 col-sm-12 text-center">
                                            <i class="fa fa-3x fa-circle"></i>
                                        </div>
                                        <div class="col-xs-9 col-sm-12">
                                            <span>{!! trans('aboutus/northlandsway.habits.title') !!}</span>
                                        </div>
                                    {{-- </a> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="clearfix"></div>

        </div>
    </div>

@endsection
