{{-- Language set in lang/{language}/aboutus/building.php --}}

@extends('layouts.main')

@section('content_class','building')
@section('content')

    <div class="row">
        <div class="col-xs-12 col-md-8">
            <h1>{!! trans('aboutus/building.header') !!}</h1>
            <p>{!! trans('aboutus/building.history1') !!}</p>
			<p>{!! trans('aboutus/building.history2') !!}</p>
        </div>
        <div class="col-xs-12 col-md-4">
            <h3 class="building-header text-center">{!! trans('aboutus/building.history3') !!}</h3>
        </div>
    </div>
	
	
	
	<div class="row visible-xs-block">
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			<div class="panel panel-default">
				<div class="panel-heading initial" role="tab" id="headingOne">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">{!! trans('aboutus/building.title1') !!}
							<br /><span>{!! trans('aboutus/building.description1') !!}</span>
						</a>
					</h4>
				</div>
				<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
					<a target="_blank" href="http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/kpson_BicentenarioIndependencia/kpson_BicentenarioIndependencia.php">
						<img class="img-responsive" src="{{ asset('/img/building/bicentenarioIndependencia.jpg') }}" alt="{!! trans('aboutus/building.header') !!}" alt="">
					</a>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading primary" role="tab" id="headingTwo">
					<h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">{!! trans('aboutus/building.title2') !!}
							<br /><span>{!! trans('aboutus/building.description2') !!}</span>
						</a>
					</h4>
				</div>
				<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
					<a target="_blank" href="http://www.northlands.org.ar/extensive/N1589O753/Galleries2015/d-LearningProject/d-LearningProject.php">
						<img class="img-responsive" src="{{ asset('/img/building/DlearningProject.jpg') }}" alt="{!! trans('aboutus/building.header') !!}" alt="">
					</a>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading secondary" role="tab" id="headingThree">
					<h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">{!! trans('aboutus/building.title3') !!}
							<br /><span>{!! trans('aboutus/building.description3') !!}</span>
						</a>
					</h4>
				</div>
				<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
					<a target="_blank" href="http://www.northlands.org.ar/extensive/N1589O753/Galleries2015/Intercultural/Intercultural.php">
						<img class="img-responsive" src="{{ asset('/img/building/intercultural2015.jpg') }}" alt="{!! trans('aboutus/building.header') !!}" alt="">
					</a>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading initial" role="tab" id="headingFour">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour">{!! trans('aboutus/building.title4') !!}
							<br /><span>{!! trans('aboutus/building.description4') !!}</span>
						</a>
					</h4>
				</div>
				<div id="collapseFour" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFour">
					<a target="_blank" href="http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/kpson_Chaco25/kpson_Chaco25.php">
						<img class="img-responsive" src="{{ asset('/img/building/Chaco25.jpg') }}" alt="{!! trans('aboutus/building.header') !!}" alt="">
					</a>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading primary" role="tab" id="headingFive">
					<h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">{!! trans('aboutus/building.title5') !!}
							<br /><span>{!! trans('aboutus/building.description5') !!}</span>
						</a>
					</h4>
				</div>
				<div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
					<a target="_blank" href="http://www.northlands.org.ar/extensive/N1589O753/Galleries2015/kpson_95Anniversary/kpson_95Anniversary.php">
						<img class="img-responsive" src="{{ asset('/img/building/95.jpg') }}" alt="{!! trans('aboutus/building.header') !!}" alt="">
					</a>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading secondary" role="tab" id="headingSix">
					<h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">{!! trans('aboutus/building.title6') !!}
							<br /><span>{!! trans('aboutus/building.description6') !!}</span>
						</a>
					</h4>
				</div>
				<div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
					<a target="_blank" href="http://cloud.northlands.org.ar/npoint/?p=960">
						<img class="img-responsive" src="{{ asset('/img/building/alumni2.jpg') }}" alt="{!! trans('aboutus/building.header') !!}" alt="">
					</a>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading initial" role="tab" id="headingSeven">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">{!! trans('aboutus/building.title7') !!}
							<br /><span>{!! trans('aboutus/building.description7') !!}</span>
						</a>
					</h4>
				</div>
				<div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
					<a target="_blank" href="http://www.northlands.org.ar/public/90anniversary/index_90anniversary.html">
						<img class="img-responsive" src="{{ asset('/img/building/90aniversario.jpg') }}" alt="{!! trans('aboutus/building.header') !!}" alt="">
					</a>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading primary" role="tab" id="headingEight">
					<h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">{!! trans('aboutus/building.title8') !!}
							<br /><span>{!! trans('aboutus/building.description8') !!}</span>
						</a>
					</h4>
				</div>
				<div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
					<a target="_blank" href="http://www.northlands.org.ar/public/BICENTENARIO/parentsBICENTENARIO2.html">
						<img class="img-responsive" src="{{ asset('/img/building/bicentenario2010.jpg') }}" alt="{!! trans('aboutus/building.header') !!}" alt="">
					</a>
				</div>
			</div>
		</div>
	</div>

	<div class="hidden-xs">
		<div class="row">
			<br />
			<div class="col-sm-3">
				<a target="_blank" href="http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/kpson_BicentenarioIndependencia/kpson_BicentenarioIndependencia.php">
					<img class="img-responsive" src="{{ asset('/img/building/bicentenarioIndependencia.jpg') }}" alt="{!! trans('aboutus/building.header') !!}" alt="">
					<div class="panel-heading initial">
						<h4 class="panel-title">
							{!! trans('aboutus/building.title1') !!}
								<br /><span>{!! trans('aboutus/building.description1') !!}</span>
						</h4>
					</div>
				</a>
			</div>

			<div class="col-sm-3">
				<a target="_blank" href="http://www.northlands.org.ar/extensive/N1589O753/Galleries2015/d-LearningProject/d-LearningProject.php">
					<img class="img-responsive" src="{{ asset('/img/building/DlearningProject.jpg') }}" alt="{!! trans('aboutus/building.header') !!}" alt="">
					<div class="panel-heading primary">
						<h4 class="panel-title">
							{!! trans('aboutus/building.title2') !!}
								<br /><span>{!! trans('aboutus/building.description2') !!}</span>
						</h4>
					</div>
				</a>
			</div>

			<div class="col-sm-3">
				<a target="_blank" href="http://www.northlands.org.ar/extensive/N1589O753/Galleries2015/Intercultural/Intercultural.php">
					<img class="img-responsive" src="{{ asset('/img/building/intercultural2015.jpg') }}" alt="{!! trans('aboutus/building.header') !!}" alt="">
					<div class="panel-heading secondary">
						<h4 class="panel-title">
							{!! trans('aboutus/building.title3') !!}
								<br /><span>{!! trans('aboutus/building.description3') !!}</span>
						</h4>
					</div>
				</a>
			</div>

			<div class="col-sm-3">
				<a target="_blank" href="http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/kpson_Chaco25/kpson_Chaco25.php">
					<img class="img-responsive" src="{{ asset('/img/building/Chaco25.jpg') }}" alt="{!! trans('aboutus/building.header') !!}" alt="">
					<div class="panel-heading initial">
						<h4 class="panel-title">
							{!! trans('aboutus/building.title4') !!}
								<br /><span>{!! trans('aboutus/building.description4') !!}</span>
						</h4>
					</div>
				</a>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3">
				<a href="http://www.northlands.org.ar/extensive/N1589O753/Galleries2015/kpson_95Anniversary/kpson_95Anniversary.php">
					<img target="_blank" class="img-responsive" src="{{ asset('/img/building/95.jpg') }}" alt="{!! trans('aboutus/building.header') !!}" alt="">
					<div class="panel-heading primary">
						<h4 class="panel-title">
							{!! trans('aboutus/building.title5') !!}
								<br /><span>{!! trans('aboutus/building.description5') !!}</span>
						</h4>
					</div>
				</a>
			</div>

			<div class="col-sm-3">
				<a target="_blank" href="http://cloud.northlands.edu.ar/npoint/?p=960">
					<img class="img-responsive" src="{{ asset('/img/building/alumni2.jpg') }}" alt="{!! trans('aboutus/building.header') !!}" alt="">
					<div class="panel-heading secondary">
						<h4 class="panel-title">
							{!! trans('aboutus/building.title6') !!}
								<br /><span>{!! trans('aboutus/building.description6') !!}</span>
						</h4>
					</div>
				</a>
			</div>

			<div class="col-sm-3">
				<a target="_blank" href="http://www.northlands.org.ar/public/90anniversary/index_90anniversary.html">
					<img class="img-responsive" src="{{ asset('/img/building/90aniversario.jpg') }}" alt="{!! trans('aboutus/building.header') !!}" alt="">
					<div class="panel-heading initial">
						<h4 class="panel-title">
							{!! trans('aboutus/building.title7') !!}
								<br /><span>{!! trans('aboutus/building.description7') !!}</span>
						</h4>
					</div>
				</a>
			</div>

			<div class="col-sm-3">
				<a target="_blank" href="http://www.northlands.org.ar/public/BICENTENARIO/parentsBICENTENARIO2.html">
					<img class="img-responsive" src="{{ asset('/img/building/bicentenario2010.jpg') }}" alt="{!! trans('aboutus/building.header') !!}" alt="">
					<div class="panel-heading primary">
						<h4 class="panel-title">
							{!! trans('aboutus/building.title8') !!}
								<br /><span>{!! trans('aboutus/building.description8') !!}</span>
						</h4>
					</div>
				</a>
			</div>
		</div>
		<br /><br />
	</div>

@endsection
