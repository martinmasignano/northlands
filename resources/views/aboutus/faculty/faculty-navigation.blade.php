<div class="faculty-navigation">
	<a href="{{ url('/articles/faculty/metacognicion') }}">
		{!! Html::image('/img/faculty/susie-arndt.jpg', 'Susan Arndt', array('class' => 'img-thumbnail')) !!}
	</a>

	<a href="{{ url('/articles/faculty/why-innovate') }}">
		{!! Html::image('/img/faculty/marisa-perazzo.jpg', 'Marisa Perazzo', array('class' => 'img-thumbnail')) !!}
	</a>

	<a href="{{ url('/articles/faculty/la-audicion-musical') }}">
		{!! Html::image('/img/faculty/marcela-hidalgo.png', 'Marcela Hidalgo', array('class' => 'img-thumbnail')) !!}
	</a>

	<a href="{{ url('/articles/faculty/la-vocacion-musical') }}">
		{!! Html::image('/img/faculty/leandro-valle.png', 'Leandro Valle', array('class' => 'img-thumbnail')) !!}
	</a>

	<a href="{{ url('articles/faculty/tecnologia-aplicada') }}">
		{!! Html::image('/img/faculty/daniel-magaldi.png', 'Daniel Magaldi', array('class' => 'img-thumbnail')) !!}
	</a>

	<a href="{{ url('/articles/faculty/educacion-fisica-escolar') }}">
		{!! Html::image('/img/faculty/angie-mayo.png', 'Angie Mayo', array('class' => 'img-thumbnail')) !!}
	</a>
</div>
