<div class="col-sm-6">
	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		
		<div class="panel panel-autoras">
			<div class="panel-heading" role="tab" id="headingOne">
				<div class="media">
					<div class="media-left">
						<img class="media-object" src="{{ url('img/faculty/susie-arndt.jpg') }}" alt="Susana I. Arndt">
					</div>
					<div class="media-body">
						<h4 class="media-heading panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Susana I. Arndt</a>
						</h4>
					</div>
				</div>
			</div>

			<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
				<div class="panel-body">Es Profesora de Inglés para la Enseñanza Primaria y para la Enseñanza Media y Profesora de Enseñanza Preescolar. Fue Profesora de Inglés en todos los niveles de la enseñanza y docente en el Nivel Inicial. Fue coordinadora del Programa de Enseñanza Primaria del “International Baccalauréate” y se desempeña como Directora de Nivel Inicial desde el año 1989.</div>
			</div>
		</div>

		<div class="panel panel-autoras">
			<div class="panel-heading" role="tab" id="headingTwo">
				<div class="media">
					<div class="media-left">
						<img class="media-object" src="{{ url('img/faculty/virginia-barcelo.jpg') }}" alt="Virginia Barceló">
					</div>
					<div class="media-body">
						<h4 class="media-heading panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseOne">Virginia Barceló</a>
						</h4>
					</div>
				</div>
			</div>

			<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
				<div class="panel-body">Es profesora de nivel inicial. Se desempeña como docente en el colegio NORTHLANDS, sede Nordelta, a cargo de la sala de 2. Estuvo a cargo de la coordinación de grupos de juego para niños de nivel maternal durante 5 años. Durante su permanencia en Brasil fue profesora de español para extranjeros.</div>
			</div>
		</div>

		<div class="panel panel-autoras">
			<div class="panel-heading" role="tab" id="headingThree">
				<div class="media">
					<div class="media-left">
						<img class="media-object" src="{{ url('img/authorities/AleBatu.jpg') }}" alt="Alejandra Batu">
					</div>
					<div class="media-body">
						<h4 class="media-heading panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseOne">Alejandra Batu</a>
						</h4>
					</div>
				</div>
			</div>

			<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
				<div class="panel-body">Es profesora de Nivel Inicial. Egresada del Profesorado de Nivel Inicial Normal Nº10 Juan Bautista Alberdi. Se desempeña como vicedirectora del Jardín de Infantes, NORTHLANDS, sede Nordelta. Ha ejercido la docencia en los niveles primario y jardín durante 25 años.</div>
			</div>
		</div>

		<div class="panel panel-autoras">
			<div class="panel-heading" role="tab" id="headingFour">
				<div class="media">
					<div class="media-left">
						<img class="media-object" src="{{ url('img/faculty/carolina-bernengo.jpg') }}" alt="Ma. Carolina Bernengo">
					</div>
					<div class="media-body">
						<h4 class="media-heading panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseOne">Ma. Carolina Bernengo</a>
						</h4>
					</div>
				</div>
			</div>

			<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
				<div class="panel-body">Es Profesora Nacional de Nivel Inicial egresada del Instituto Sara C. de Eccleston. Se desempeña como docente en el colegioNORTHLANDS (sede Nordelta) desde el año 2004. Actualmente cursa el Postítulo docente en la Universidad de San Andrès.</div>
			</div>
		</div>

		<div class="panel panel-autoras">
			<div class="panel-heading" role="tab" id="headingFive">
				<div class="media">
					<div class="media-left">
						<img class="media-object" src="{{ url('img/faculty/patricia-cairnie.jpg') }}" alt="Patricia Cairnie">
					</div>
					<div class="media-body">
						<h4 class="media-heading panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="true" aria-controls="collapseOne">Patricia Cairnie</a>
						</h4>
					</div>
				</div>
			</div>

			<div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
				<div class="panel-body">Es Profesora de Nivel Inicial. Egresada del Profesorado de Niel Inicial Sara C.Eccleston. Se desempeña como maestra de preescolar del Colegio NORTHLANDS, sede Nordelta. Ha ejercido la docencia en los niveles primario y jardín durante 34 años.</div>
			</div>
		</div>

		<div class="panel panel-autoras">
			<div class="panel-heading" role="tab" id="headingSix">
				<div class="media">
					<div class="media-left">
						<img class="media-object" src="{{ url('img/faculty/rosana-cescut.jpg') }}" alt="Rosana Cescut">
					</div>
					<div class="media-body">
						<h4 class="media-heading panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="true" aria-controls="collapseOne">Rosana Cescut</a>
						</h4>
					</div>
				</div>
			</div>

			<div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
				<div class="panel-body">Es Profesora Nacional de Educación Física. Egresada INEF General Belgrano. Se desempeña como profesora de educación física del colegio, sede NORTHLANDS, Nordelta. Haejercido la docencia en los niveles secundario, primario y jardín durante 24 años.</div>
			</div>
		</div>

		<div class="panel panel-autoras">
			<div class="panel-heading" role="tab" id="headingSeven">
				<div class="media">
					<div class="media-left">
						<img class="media-object" src="{{ url('img/faculty/flor-duda.jpg') }}" alt="María Florencia Duda">
					</div>
					<div class="media-body">
						<h4 class="media-heading panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="true" aria-controls="collapseOne">María Florencia Duda</a>
						</h4>
					</div>
				</div>
			</div>

			<div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
				<div class="panel-body">Es Profesora de nivel inicial egresada del Instituto Sara C. de Eccleston y cursó la carrera de Ciencias de la Educación en la Universidad Católica Argentina. Se desempeña como docente del nivel inicial en el colegio NORTHLANDS en su sede de Nordelta desde el año 2006.</div>
			</div>
		</div>

		<div class="panel panel-autoras">
			<div class="panel-heading" role="tab" id="headingEight">
				<div class="media">
					<div class="media-left">
						<img class="media-object" src="{{ url('img/faculty/constanza-garay.jpg') }}" alt="Constanza Fernández Garay">
					</div>
					<div class="media-body">
						<h4 class="media-heading panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="true" aria-controls="collapseOne">Constanza Fernández Garay</a>
						</h4>
					</div>
				</div>
			</div>

			<div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
				<div class="panel-body">Es Licenciada en Psicopedagogia. Egresada de la Universidad de Belgrano. Se desempeña como maestra de preescolar del Colegio NORTHLANDS, sede Nordelta. Ha ejercido la docencia en los niveles primario y jardín durante 20 años.</div>
			</div>
		</div>

		<div class="panel panel-autoras">
			<div class="panel-heading" role="tab" id="headingNine">
				<div class="media">
					<div class="media-left">
						<img class="media-object" src="{{ url('img/faculty/teresa-gomez.jpg') }}" alt="María Teresa Gómez">
					</div>
					<div class="media-body">
						<h4 class="media-heading panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="true" aria-controls="collapseOne">María Teresa Gómez</a>
						</h4>
					</div>
				</div>
			</div>

			<div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
				<div class="panel-body">Es Profesora de Ciencias de la Educación, Profesora de nivel inicial y de Nivel Primario. Actualmente se desempeña como personal directivo en el Instituto de Formación Docente de San Isidro. Es formadora de formadores y Asesora de Prácticas del Lenguaje del Colegio NORTHLANDS entre otras instituciones educativas. Trabaja en la producción de secuencias didácticas en el área de Prácticas del Lenguaje en diversos grupos de estudio e investigación.</div>
			</div>
		</div>

	</div>	
</div>

<div class="col-sm-6">
	<div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">

		<div class="panel panel-autoras">
			<div class="panel-heading" role="tab" id="heading2One">
				<div class="media">
					<div class="media-left">
						<img class="media-object" src="{{ url('img/faculty/leila-hordh.jpg') }}" alt="Leila Hordh">
					</div>
					<div class="media-body">
						<h4 class="media-heading panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse2One" aria-expanded="true" aria-controls="collapseOne">Leila Hordh</a>
						</h4>
					</div>
				</div>
			</div>

			<div id="collapse2One" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2One">
				<div class="panel-body">Es profesora de nivel inicial. Se desempeña como docente en el colegio NORTHLANDS, sede Nordelta, a cargo de la sala de 2. Es psicopedagoga y formó parte de un equipo de trabajo que se dedicaba al diagnóstico y seguimiento de niños con dificultades de aprendizaje.</div>
			</div>
		</div>

		<div class="panel panel-autoras">
			<div class="panel-heading" role="tab" id="heading2Two">
				<div class="media">
					<div class="media-left">
						<img class="media-object" src="{{ url('img/faculty/geraldine-kaczor.jpg') }}" alt="Geraldine Kaczor">
					</div>
					<div class="media-body">
						<h4 class="media-heading panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse2Two" aria-expanded="true" aria-controls="collapse2Two">Geraldine Kaczor</a>
						</h4>
					</div>
				</div>
			</div>

			<div id="collapse2Two" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2Two">
				<div class="panel-body">Es profesora de nivel Inicial y Primaria, y posee un Bachelor`s Degree in Spanish en la universidad de Florida (FAU). Actualmente se desempeña como docente bilingüe en el Colegio NORTHLANDS, sede Nordelta. Ha participado de conferencias internacionales sobre educación inicial. Ha ejercido la docencia por más de 20 años.</div>
			</div>
		</div>

		<div class="panel panel-autoras">
			<div class="panel-heading" role="tab" id="heading2Three">
				<div class="media">
					<div class="media-left">
						<img class="media-object" src="{{ url('img/faculty/paula-martin.jpg') }}" alt="Paula Martin">
					</div>
					<div class="media-body">
						<h4 class="media-heading panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse2Three" aria-expanded="true" aria-controls="collapse2Three">Paula Martin</a>
						</h4>
					</div>
				</div>
			</div>

			<div id="collapse2Three" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2Three">
				<div class="panel-body">Es Licenciada en Psicologia. Egresada de la Universidad de Belgrano. Se desempeña como maestra de preescolar del Colegio NORTHLANDS, sede Nordelta. Ha ejercido la docencia en los niveles primario y jardín durante 12 años.</div>
			</div>
		</div>

		<div class="panel panel-autoras">
			<div class="panel-heading" role="tab" id="heading2Four">
				<div class="media">
					<div class="media-left">
						<img class="media-object" src="{{ url('img/faculty/barbara-pereyra.jpg') }}" alt="Bárbara Pereyra">
					</div>
					<div class="media-body">
						<h4 class="media-heading panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse2Four" aria-expanded="true" aria-controls="collapse2Four">Bárbara Pereyra</a>
						</h4>
					</div>
				</div>
			</div>

			<div id="collapse2Four" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2Four">
				<div class="panel-body">Es Profesora del nivel inicial egresada del Centro Cultural Italiano. Se desempeña como docente del nivel inicial del Colegio NORTHLANDS en su sede de Nordelta desde el año 1998.</div>
			</div>
		</div>

		<div class="panel panel-autoras">
			<div class="panel-heading" role="tab" id="heading2Five">
				<div class="media">
					<div class="media-left">
						<img class="media-object" src="{{ url('img/faculty/maria-revol.jpg') }}" alt="María Revol">
					</div>
					<div class="media-body">
						<h4 class="media-heading panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse2Five" aria-expanded="true" aria-controls="collapse2Five">María Revol</a>
						</h4>
					</div>
				</div>
			</div>

			<div id="collapse2Five" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2Five">
				<div class="panel-body">Es Profesora en Educación Preescolar egresada del Instituto Alejandro Carbò (Pcia. de Córdoba). Se desempeña como docente en el colegio NORTHLANDS (sede  Nordelta) desde el año 2003. Actualmente cursa el Postítulo docente en la Universidad de San Andrès.</div>
			</div>
		</div>

		<div class="panel panel-autoras">
			<div class="panel-heading" role="tab" id="heading2Six">
				<div class="media">
					<div class="media-left">
						<img class="media-object" src="{{ url('img/faculty/victoria-somoza.jpg') }}" alt="María Victoria Somoza">
					</div>
					<div class="media-body">
						<h4 class="media-heading panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse2Six" aria-expanded="true" aria-controls="collapse2Six">María Victoria Somoza</a>
						</h4>
					</div>
				</div>
			</div>

			<div id="collapse2Six" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2Six">
				<div class="panel-body">Es profesora de nivel inicial. Se desempeña como docente en el colegio NORTHLANDS, sede Nordelta, a cargo de la sala de 2. Trabaja con niños desde hace más de 8 años.</div>
			</div>
		</div>

		<div class="panel panel-autoras">
			<div class="panel-heading" role="tab" id="heading2Seven">
				<div class="media">
					<div class="media-left">
						<img class="media-object" src="{{ url('img/faculty/veronica-van-kregten.jpg') }}" alt="Verónica Van Kregten">
					</div>
					<div class="media-body">
						<h4 class="media-heading panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse2Seven" aria-expanded="true" aria-controls="collapse2Seven">Verónica Van Kregten</a>
						</h4>
					</div>
				</div>
			</div>

			<div id="collapse2Seven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2Seven">
				<div class="panel-body">Es profesora de nivel inicial. Actualmente se desempeña como docente bilingüe en el Colegio NORTHLANDS, sede Nordelta. Ha ejercido la docencia y la dirección en el nivel inicial y primaria por más de 20 años.</div>
			</div>
		</div>

		<div class="panel panel-autoras">
			<div class="panel-heading" role="tab" id="heading2Eight">
				<div class="media">
					<div class="media-left">
						<img class="media-object" src="{{ url('img/faculty/claudia-varela.jpg') }}" alt="Claudia Andrea Varela">
					</div>
					<div class="media-body">
						<h4 class="media-heading panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse2Eight" aria-expanded="true" aria-controls="collapse2Eight">Claudia Andrea Varela</a>
						</h4>
					</div>
				</div>
			</div>

			<div id="collapse2Eight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2Eight">
				<div class="panel-body">Es Psicopedagoga y Licenciada en Psicología. Actualmente se desempeña como asesora de matemática y Ciencias Sociales del Colegio NORTHLANDS donde, a su vez, implementa el proyecto de Filosofía con niños.</div>
			</div>
		</div>

	</div>
</div>
