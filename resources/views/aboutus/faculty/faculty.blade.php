{{-- Language set in lang/{language}/aboutus/faculty/faculty/faculty.php --}}

@extends('layouts.main')

@section('content_class','faculty')
@section('content')

	<h1>{!! trans('aboutus/faculty/faculty.header') !!}</h1>
	<div class="row">
		<div class="col-sm-6">
			<p>{!! trans('aboutus/faculty/faculty.description1') !!}</p>
			<p>{!! trans('aboutus/faculty/faculty.description2') !!}</p>
		</div>
		<div class="col-sm-offset-1 col-sm-5">
			@if (App::getLocale() == 'en')
			<img src="{{ url('css/img/faculty-en.svg')}}" alt="Profiles">
			@else
			<img src="{{ url('css/img/faculty.svg')}}" alt="Perfiles">
			@endif
		</div>
	</div>

	<div class="profiles">
		<h3>{!! trans('aboutus/faculty/faculty.section-title') !!}</h3>
		<div class="col-xs-12 col-sm-6 col-md-3">
			<div class="row">
				<a class="media lightyellow" href="{{url('articles/faculty/metacognicion')}}">
					<div class="media-left">
						{!! Html::image('/img/faculty/susie-arndt.jpg', 'Susana I. Arndt', array('class' => 'media-object')) !!}
					</div>
					<div class="media-body">
						<h5 class="media-heading">Juntos hacemos grandes cosas</h5>
						<span>Susana I. Arndt</span>
					</div>
				</a>
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-3">
			<div class="row">
				<a class="media green" href="{{url('articles/faculty/why-innovate')}}">
					<div class="media-left">
						{!! Html::image('/img/faculty/marisa-perazzo.jpg', 'Marisa Perazzo', array('class' => 'media-object')) !!}
					</div>
					<div class="media-body">
						<h5 class="media-heading">Why innovate?</h5>
						<span>Marisa Perazzo</span>
					</div>
				</a>
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-3">
			<div class="row">
				<a class="media lightyellow" href="{{url('articles/faculty/la-audicion-musical')}}">
					<div class="media-left">
						{!! Html::image('/img/faculty/marcela-hidalgo.png', 'Marcela Hidalgo', array('class' => 'media-object')) !!}
					</div>
					<div class="media-body">
						<h5 class="media-heading">La audición musical de los más pequeños</h5>
						<span>Marcela Hidalgo</span>
					</div>
				</a>
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-3">
			<div class="row">
				<a class="media lightyellow" href="{{url('articles/faculty/la-vocacion-musical')}}">
					<div class="media-left">
						{!! Html::image('/img/faculty/leandro-valle.png', 'Leandro Valle', array('class' => 'media-object')) !!}
					</div>
					<div class="media-body">
						<h5 class="media-heading">La vocación musical</h5>
						<span>Leandro Valle</span>
					</div>
				</a>
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-3">
			<div class="row">
				<a class="media" href="{{url('articles/faculty/tecnologia-aplicada')}}">
					<div class="media-left">
						{!! Html::image('/img/faculty/daniel-magaldi.png', 'Daniel Magaldi', array('class' => 'media-object')) !!}
					</div>
					<div class="media-body">
						<h5 class="media-heading">Tecnología aplicada a la educación</h5>
						<span>Daniel Magaldi</span><br />
					</div>
				</a>
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-3">
			<div class="row">
				<a class="media red" href="{{url('articles/faculty/educacion-fisica-escolar')}}">
					<div class="media-left">
						{!! Html::image('/img/faculty/angie-mayo.png', 'Angie Mayo', array('class' => 'media-object')) !!}
					</div>
					<div class="media-body">
						<h5 class="media-heading">Educación física escolar</h5>
						<span>Angie Mallo</span>
					</div>
				</a>
			</div>
		</div>

	</div>

@endsection
