{{-- Language set in lang/{language}/contactus/info.php --}}

@extends('layouts.main')

@section('content')
	<h1>{!! trans('contactus/info.header') !!}</h1>	
	
	<div class="hidden-sm hidden-md hidden-lg">
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			<div class="panel panel-branch nordelta">
				<div class="row">
					<div class="panel-heading" role="tab" id="headingOne">
						<h2 class="panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">{{ trans('contactus/info.branch-nordelta') }}</a>
						</h2>
					</div>
					<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
						<div class="panel-body">
							<ul>
								<li>{{ trans('contactus/info.address') }} Av. De Los Colegios 680 - B1670NNN - Nordelta, Buenos Aires, Argentina.</li> 
								<li>{{ trans('contactus/info.phone') }} (54 11) 4871-2668 / 69 Fax:(54 11) 4871-2667</li>
								<li><a href="mailto:infonordel@northlands.edu.ar">infonordel@northlands.edu.ar</a></li>	
								<li><a href="mailto:admissionsnordelta@northlands.edu.ar">admissionsnordelta@northlands.edu.ar</a></li>
							</ul>
							<iframe src="https://www.google.com/maps/d/embed?mid=1fU3FxImpzyemmkKzzGLbllSVFxg&hl=es" width="100%" height="480"></iframe>
						</div>
					</div>
				</div>
			</div>
			<div class="panel panel-branch olivos">
				<div class="row">
					<div class="panel-heading" role="tab" id="headingTwo">
						<h2 class="panel-title">
							<a href="#collapseTwo" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="collapseTwo">
							{{ trans('contactus/info.branch-olivos') }}
							</a>
						</h2>
					</div>
					<div id="collapseTwo" class="collapse panel-collapse" role="tabpanel" aria-labelledby="headingTwo">
						<div class="panel-body">
							<ul>
								<li>{{ trans('contactus/info.address') }} Roma 1248 - B1636 CYT - Olivos, Zona Norte, Buenos Aires, Argentina.</li> 
								<li>{{ trans('contactus/info.phone') }} (54 11) 4711-8400 Fax: (54 11) 4711-8401</li>
								<li><a href="mailto:info@northlands.edu.ar">info@northlands.edu.ar</a></li>	
								<li><a href="mailto:admissionsolivos@northlands.edu.ar">admissionsolivos@northlands.edu.ar</a></li>
							</ul>
							<iframe src="https://www.google.com/maps/d/embed?mid=16fbNAVeUCVcc_Oiz6vun-Ep6LjI&hl=es" width="100%" height="480"></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row hidden-xs">
		<div class="col-md-6">
			<div class="panel panel-branch nordelta">
				<div class="row">
					<div class="panel-heading">
						<h2>{{ trans('contactus/info.branch-nordelta') }}</h2>
					</div>
					<div class="panel-body">
						<ul>
							<li>{{ trans('contactus/info.address') }} Av. De Los Colegios 680 - B1670NNN - Nordelta, Buenos Aires, Argentina.</li> 
							<li>{{ trans('contactus/info.phone') }} (54 11) 4871-2668 / 69 Fax:(54 11) 4871-2667</li>
							<li><a href="mailto:infonordel@northlands.edu.ar">infonordel@northlands.edu.ar</a></li>	
							<li><a href="mailto:admissionsnordelta@northlands.edu.ar">admissionsnordelta@northlands.edu.ar</a></li>
						</ul>
						<iframe src="https://www.google.com/maps/d/embed?mid=1fU3FxImpzyemmkKzzGLbllSVFxg&hl=es" width="100%" height="480"></iframe>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-branch olivos">
				<div class="row">
					<div class="panel-heading">
						<h2>{{ trans('contactus/info.branch-olivos') }}</h2>
					</div>
					<div class="panel-body">
						<ul>
							<li>{{ trans('contactus/info.address') }} Roma 1248 - B1636 CYT - Olivos, Zona Norte, Buenos Aires, Argentina.</li> 
							<li>{{ trans('contactus/info.phone') }} (54 11) 4711-8400 Fax: (54 11) 4711-8401</li>
							<li><a href="mailto:info@northlands.edu.ar">info@northlands.edu.ar</a></li>	
							<li><a href="mailto:admissionsolivos@northlands.edu.ar">admissionsolivos@northlands.edu.ar</a></li>
						</ul>
						<iframe src="https://www.google.com/maps/d/embed?mid=16fbNAVeUCVcc_Oiz6vun-Ep6LjI&hl=es" width="100%" height="480"></iframe>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection
