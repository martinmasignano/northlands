{{-- Language set in lang/{language}/contactus/jobs.php --}}

@extends('layouts.main')

@section('content')
	<h1>{!! trans('contactus/jobs.header') !!}</h1>	
	<div class="row">
		<div class="col-md-8">
			<p>{!! trans('contactus/jobs.paragraph1') !!}</p>
			<p>{!! trans('contactus/jobs.paragraph2') !!}</p>
			<p>{!! trans('contactus/jobs.paragraph3') !!}</p>
			<p>{!! trans('contactus/jobs.paragraph4') !!}</p>
		</div>
		<div class="col-md-4 hidden-xs hidden-sm">
			<div class="well join">
				<div class="panel-body">
					<iframe src="https://player.vimeo.com/video/142891523" width="100%" height="160" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					<p>{!! trans('contactus/jobs.joinus-text') !!}</p>
				</div>
				<div class="panel-footer">
					<a href="#">{!! trans('contactus/jobs.joinus-link') !!} >></a>
				</div>
			</div>
		</div>
	</div>
	<div class="row">

		{{-- Bloque de NORDELTA --}}
		<div class="col-sm-6">
			<div class="panel-group" id="accordionNordelta" role="tablist" aria-multiselectable="true">
				{{-- Acá van los puestos que se están buscando en NORDELTA --}}

				{{-- IT teacher NORDELTA --}}
				<div class="panel panel-branch nordelta">
					<div class="row">
						<div class="panel-heading" role="tab" id="ITteacher">
							
							{{-- Acá va el título del puesto de Librarian de NORDELTA --}}
							{{-- En inglés, el texto está en lang/en/contactus/jobs.php en donde dice nordelta-librarian-title --}}
							{{-- En español, el texto está en lang/es/contactus/jobs.php en donde dice nordelta-librarian-title --}}
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse" data-parent="#accordionNordelta" href="#collaspseITteacher" aria-expanded="true" aria-controls="collaspseITteacher">{!! trans('contactus/jobs.nordelta-ITteacher-title') !!}</a>
							</h4>
						</div>
						
						{{-- Acá va la descripción del puesto de Librarian de NORDELTA --}}
						{{-- En inglés, el texto está en lang/en/contactus/jobs.php en donde dice nordelta-librarian-text --}}
						{{-- En español, el texto está en lang/es/contactus/jobs.php en donde dice nordelta-librarian-text --}}
						<div id="collaspseITteacher" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ITteacher">
							<div class="panel-body">{!! trans('contactus/jobs.nordelta-ITteacher-text') !!}</div>
						</div>
					</div>
				</div>


				{{-- Head of School --}}
				<div class="panel panel-branch nordelta">
					<div class="row">
						<div class="panel-heading" role="tab" id="Head-of-School">
							
							{{-- Acá va el título del puesto de Head of School  --}}
							{{-- En inglés, el texto está en lang/en/contactus/jobs.php en donde dice nordelta-librarian-title --}}
							{{-- En español, el texto está en lang/es/contactus/jobs.php en donde dice nordelta-librarian-title --}}
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse" data-parent="#accordionNordelta" href="#collaspsenordelta-Head-of-School" aria-expanded="true" aria-controls="collaspsenordelta-Head-of-School">{!! trans('contactus/jobs.nordelta-Head-of-School-title') !!}</a>
							</h4>
						</div>
						
						{{-- Acá va la descripción del puesto de Librarian de NORDELTA --}}
						{{-- En inglés, el texto está en lang/en/contactus/jobs.php en donde dice nordelta-librarian-text --}}
						{{-- En español, el texto está en lang/es/contactus/jobs.php en donde dice nordelta-librarian-text --}}
						<div id="collaspsenordelta-Head-of-School" class="panel-collapse collapse" role="tabpanel" aria-labelledby="nordelta-Head-of-School">
							<div class="panel-body">{!! trans('contactus/jobs.nordelta-Head-of-School-text') !!}</div>
						</div>
					</div>
				</div>

				{{-- Librarian NORDELTA --}}
				<div class="panel panel-branch nordelta">
					<div class="row">
						<div class="panel-heading" role="tab" id="librarian">
							
							{{-- Acá va el título del puesto de Librarian de NORDELTA --}}
							{{-- En inglés, el texto está en lang/en/contactus/jobs.php en donde dice nordelta-librarian-title --}}
							{{-- En español, el texto está en lang/es/contactus/jobs.php en donde dice nordelta-librarian-title --}}
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse" data-parent="#accordionNordelta" href="#collaspseLibrarian" aria-expanded="true" aria-controls="collaspseLibrarian">{!! trans('contactus/jobs.nordelta-librarian-title') !!}</a>
							</h4>
						</div>
						
						{{-- Acá va la descripción del puesto de Librarian de NORDELTA --}}
						{{-- En inglés, el texto está en lang/en/contactus/jobs.php en donde dice nordelta-librarian-text --}}
						{{-- En español, el texto está en lang/es/contactus/jobs.php en donde dice nordelta-librarian-text --}}
						<div id="collaspseLibrarian" class="panel-collapse collapse" role="tabpanel" aria-labelledby="librarian">
							<div class="panel-body">{!! trans('contactus/jobs.nordelta-librarian-text') !!}</div>
						</div>
					</div>
				</div>

				{{-- Aclaración: tengo que hacer este if porque en inglés hay 2 puestos que están que no están en español. Por favor, evitar que haya textos o bloques en un idioma y no en otro --}}

				@if (App::getLocale() == 'en')
	

				{{-- IB Teacher NORDELTA --}}
				
				@else
				{{-- Estos puestos no están en español, por lo tanto no va nada --}}
				@endif

				{{-- Geography Teacher (Secondary NORDELTA) --}}
				<div class="panel panel-branch nordelta">
					<div class="row">
						<div class="panel-heading" role="tab" id="geographyTeacher">
							<h4 class="panel-title">
								
								{{-- Acá va el título del puesto Geography Teacher (Secondary NORDELTA) --}}
								{{-- En inglés, el texto está en lang/en/contactus/jobs.php en donde dice nordelta-geography-teacher-title --}}
								{{-- En español, el texto está en lang/es/contactus/jobs.php en donde dice nordelta-geography-teacher-title --}}
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionNordelta" href="#collapseGeographyTeacher" aria-expanded="false" aria-controls="collapseGeographyTeacher">
								{!! trans('contactus/jobs.nordelta-geography-teacher-title') !!}</a>
							</h4>
						</div>
						
						{{-- Acá va la descripción del puesto Geography Teacher (Secondary NORDELTA) --}}
						{{-- En inglés, el texto está en lang/en/contactus/jobs.php en donde dice nordelta-geography-teacher-text --}}
						{{-- En español, el texto está en lang/es/contactus/jobs.php en donde dice nordelta-geography-teacher-text --}}
						<div id="collapseGeographyTeacher" class="panel-collapse collapse" role="tabpanel" aria-labelledby="geographyTeacher">
							<div class="panel-body">{!! trans('contactus/jobs.nordelta-geography-teacher-text') !!}</div>
						</div>
					</div>
				</div>

			</div>
		</div>


		{{-- Bloque de OLIVOS --}}
		<div class="col-sm-6">
			<div class="panel-group" id="accordionOlivos" role="tablist" aria-multiselectable="true">
				{{-- Acá van los puestos que se están buscando en OLIVOS --}}
				
				{{-- Coordinator of Activities (Sede OLIVOS) --}}
				<div class="panel panel-branch olivos">
					<div class="row">
						<div class="panel-heading" role="tab" id="Coordinator of Activities">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse" data-parent="#accordionOlivos" href="#collapseSupplierHeading" aria-expanded="true" aria-controls="collapseSupplierHeading">{!! trans('contactus/jobs.olivos-Coordinator-of-Activities-title') !!}</a>
							</h4>
						</div>
						<div id="collapseSupplierHeading" class="panel-collapse collapse" role="tabpanel" aria-labelledby="supplierPayments">
							<div class="panel-body">{!! trans('contactus/jobs.olivos-Coordinator-of-Activities-text') !!}</div>
						</div>
					</div>
				</div>
				
				{{-- English and Literature Teacher (Secondary OLIVOS) --}}
				<div class="panel panel-branch olivos">
					<div class="row">
						<div class="panel-heading" role="tab" id="EnglishLiteratureTeacher">
							<h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionOlivos" href="#collapseEnglishLiteratureTeacher" aria-expanded="false" aria-controls="collapseEnglishLiteratureTeacher">{!! trans('contactus/jobs.olivos-english-literature-teacher-title') !!}</a>
							</h4>
						</div>
						<div id="collapseEnglishLiteratureTeacher" class="panel-collapse collapse" role="tabpanel" aria-labelledby="EnglishLiteratureTeacher">
							<div class="panel-body">{!! trans('contactus/jobs.olivos-english-literature-teacher-text') !!}</div>
						</div>
					</div>
				</div>
				
				{{-- Drama Teacher (Sede OLIVOS) --}}
				<div class="panel panel-branch olivos">
					<div class="row">
						<div class="panel-heading" role="tab" id="dramaTeacher">
							<h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionOlivos" href="#collapseDramaTeacher" aria-expanded="false" aria-controls="collapseDramaTeacher">{!! trans('contactus/jobs.olivos-drama-teacher-title') !!}</a>
							</h4>
						</div>
						<div id="collapseDramaTeacher" class="panel-collapse collapse" role="tabpanel" aria-labelledby="dramaTeacher">
							<div class="panel-body">{!! trans('contactus/jobs.olivos-drama-teacher-text') !!}</div>
						</div>
					</div>
				</div>

				{{-- Professor of Literature and Language (Secondary OLIVOS) --}}
				<div class="panel panel-branch olivos">
					<div class="row">
						<div class="panel-heading" role="tab" id="literatureLanguage">
							<h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionOlivos" href="#collapseLiteratureLanguage" aria-expanded="false" aria-controls="collapseLiteratureLanguage">{!! trans('contactus/jobs.olivos-literature-language-title') !!}</a>
							</h4>
						</div>
						<div id="collapseLiteratureLanguage" class="panel-collapse collapse" role="tabpanel" aria-labelledby="literatureLanguage">
							<div class="panel-body">{!! trans('contactus/jobs.olivos-literature-language-text') !!}</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="row hidden-md hidden-lg">
		<div class="well join">
			<div class="panel-body">
				<p>{!! trans('contactus/jobs.joinus-text') !!}</p>
			</div>
			<div class="panel-footer">
				<a href="#">{!! trans('contactus/jobs.joinus-link') !!} >></a>
			</div>
		</div>
	</div>
	
@endsection
