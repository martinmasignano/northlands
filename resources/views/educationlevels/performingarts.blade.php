{{-- Language set in lang/{language}/educationlevels/pa.php --}}

@extends('layouts.main')
@section('content_class','performing-arts')
@section('content')

	<div class="row">
		<div class="col-md-7">
			<h1>{!! trans('educationlevels/pa.header') !!}</h1>
			<p>{!! trans('educationlevels/pa.text1') !!}</p>
			<p>{!! trans('educationlevels/pa.text2') !!}</p>
		</div>
	</div>

	<div class="visible-xs-block">
		<div class="row">
			<div class="col-sm-4 visual-arts">
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					<div class="panel">
						<div class="panel-heading" role="tab" id="headingOne">
							<h4>
								<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">{{ trans('educationlevels/pa.visual-arts-title') }}</a>
							</h4>
						</div>

						<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body">
								<iframe src="https://player.vimeo.com/video/149396218" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
								<h5>Museum The City and The Arts - Nivel Inicial</h5>

								<iframe src="https://player.vimeo.com/video/183534501?title=0&byline=0&portrait=0" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
								<h5>Arte Plástica en Primaria</h5>

								<iframe src="https://player.vimeo.com/video/55288399" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
								<h5>Battle  - Secundaria</h5>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-4 dramatic-arts">
				<div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
					<div class="panel">
						<div class="panel-heading" role="tab" id="headingTwo">
							<h4>
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">{{ trans('educationlevels/pa.dramatic-arts-title') }}</a>
							</h4>
						</div>

						<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
							<div class="panel-body">
								<iframe src="https://player.vimeo.com/video/181940324" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
								<h5>2016 Kindergarten Concert</h5>

								<iframe src="https://player.vimeo.com/video/175555263?title=0&byline=0&portrait=0" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
								<h5>Acto Bicentenario</h5>

								<iframe src="https://player.vimeo.com/video/150785066" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
								<h5>TEEN AGE - Musical de Secundaria</h5>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-4 musical-arts">
				<div class="panel-group" id="accordion3" role="tablist" aria-multiselectable="true">
					<div class="panel">
						<div class="panel-heading" role="tab" id="headingThree">
							<h4>
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">{{ trans('educationlevels/pa.musical-arts-title') }}</a>
							</h4>
						</div>

						<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
							<div class="panel-body">
								<iframe src="https://player.vimeo.com/video/177558908" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
								<h5>Música en Nivel Inicial</h5>

								<iframe src="https://player.vimeo.com/video/145509894" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
								<h5>Festival de Coros ENCANTAR - Primaria</h5>

								<iframe src="https://player.vimeo.com/video/175555264" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
								<h5>Acto Bicentenario - Secundaria</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="hidden-xs">
		<div class="row">
			<div class="col-sm-4 visual-arts">
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					<div class="panel">
						<div class="panel-heading" role="tab" id="headingOne">
							<h4>
								<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">{{ trans('educationlevels/pa.visual-arts-title') }}</a>
							</h4>
						</div>

						<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body">
								<iframe src="https://player.vimeo.com/video/149396218" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
								<h5>Museum The City and The Arts - Nivel Inicial</h5>

								<iframe src="https://player.vimeo.com/video/183534501?title=0&byline=0&portrait=0" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
								<h5>Arte Plástica en Primaria</h5>

								<iframe src="https://player.vimeo.com/video/55288399" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
								<h5>Battle  - Secundaria</h5>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-4 dramatic-arts">
				<div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
					<div class="panel">
						<div class="panel-heading" role="tab" id="headingTwo">
							<h4>
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">{{ trans('educationlevels/pa.dramatic-arts-title') }}</a>
							</h4>
						</div>

						<div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
							<div class="panel-body">
								<iframe src="https://player.vimeo.com/video/181940324" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
								<h5>2016 Kindergarten Concert</h5>

								<iframe src="https://player.vimeo.com/video/175555263?title=0&byline=0&portrait=0" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
								<h5>Acto Bicentenario</h5>

								<iframe src="https://player.vimeo.com/video/150785066" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
								<h5>TEEN AGE - Musical de Secundaria</h5>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-4 musical-arts">
				<div class="panel-group" id="accordion3" role="tablist" aria-multiselectable="true">
					<div class="panel">
						<div class="panel-heading" role="tab" id="headingThree">
							<h4>
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">{{ trans('educationlevels/pa.musical-arts-title') }}</a>
							</h4>
						</div>

						<div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
							<div class="panel-body">
								<iframe src="https://player.vimeo.com/video/177558908" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
								<h5>Música en Nivel Inicial</h5>

								<iframe src="https://player.vimeo.com/video/145509894" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
								<h5>Festival de Coros ENCANTAR - Primaria</h5>

								<iframe src="https://player.vimeo.com/video/175555264" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
								<h5>Acto Bicentenario - Secundaria</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	@include('educationlevels/nav')
@endsection
