{{-- Language set in lang/{language}/educationlevels/pe.php --}}

@extends('layouts.main')
@section('content_class','pe')
@section('content')
	<h1>{!! trans('educationlevels/pe.header') !!}</h1>
	<div class="row">
		<div class="col-sm-8">
			<p>{!! trans('educationlevels/pe.text1') !!}</p>
			<p>{!! trans('educationlevels/pe.text2') !!}</p>
			<p>{!! trans('educationlevels/pe.text3') !!}</p>
			<div class="clearfix"></div>
			<div class="row">
				<div class="col-sm-6">
					<img src="{{ url('/css/img/pe-atletismo.jpg') }}" alt="Atletismo" class="img-responsive"><br />
					<img src="{{ url('/css/img/pe-kinder.jpg') }}" alt="Kinder" class="img-responsive"><br />
				</div>
				<div class="col-sm-6">
					<video autoplay loop preload>
                        <source src="{{ asset('./video/educacion_fisica.mp4')}}" type="video/mp4">
                    </video><br /><br />
                    <img src="{{ url('/css/img/pe-natacion.jpg') }}" alt="Natación" class="img-responsive"><br />
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="sidebar">
				<h3>{!! trans('educationlevels/pe.sidebar-heading') !!}</h3>
				<p>{!! trans('educationlevels/pe.sidebar-text1') !!}</p>
				<p>{!! trans('educationlevels/pe.sidebar-text2') !!}</p>
				<p>{!! trans('educationlevels/pe.sidebar-text3') !!}</p>
				<p>{!! trans('educationlevels/pe.sidebar-text4') !!}</p>
				<p>{!! trans('educationlevels/pe.sidebar-text5') !!}</p>
				<p>{!! trans('educationlevels/pe.sidebar-text6') !!}</p>
				<p>{!! trans('educationlevels/pe.sidebar-text7') !!}</p>
			</div>
		</div><br />
	</div>

	@include('educationlevels/nav')
@endsection
