{{-- Language set in lang/{language}/educationlevels/pse.php --}}

@extends('layouts.main')

@section('content')

	<h1>{!! trans('educationlevels/pse.header') !!}</h1>
	<p>{!! trans('educationlevels/pse.content') !!}</p>

	@include('educationlevels/nav')
@endsection