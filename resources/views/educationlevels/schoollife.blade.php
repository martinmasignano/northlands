{{-- Language set in lang/{language}/educationlevels/schoollife.php --}}

@extends('layouts.articlecolumns')

@section('sidebar')
    <div class="visible-xs-block">
        <img src="{{ url('/img/educationlevels/schoollife_L.jpg') }}" alt="">
    </div>
    <div class="hidden-xs">
        <img src="{{ url('img/educationlevels/schoollife_P.jpg') }}" alt="">
    </div>
@endsection

@section('content')
    <h1>{{ trans('educationlevels/schoollife.header') }}</h1>
    <p>{{ trans('educationlevels/schoollife.text') }}</p>
    
    <div class="row">
        <div class="col-sm-6">
            <div class="hidden-xs">
                <img class="img-responsive" src="{{ url('/img/educationlevels/schoollife1.jpg') }}" alt="">
            </div>
            <div class="row">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-branch">
                        <div class="panel-heading panel-houses" role="tab" id="headingOne">
                            <h4>
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">{{ trans('educationlevels/schoollife.houses-title') }}</a>
                            </h4>
                        </div>

                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                <p>{{ trans('educationlevels/schoollife.houses-text1') }}</p>
                                <p>{{ trans('educationlevels/schoollife.houses-text2') }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-branch">
                        <div class="panel-heading panel-captains" role="tab" id="headingTwo">
                            <h4>
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">{{ trans('educationlevels/schoollife.captains-title') }}</a>
                            </h4>
                        </div>

                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body">
                                <p>{{ trans('educationlevels/schoollife.captains-text1') }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-branch">
                        <div class="panel-heading panel-prefects" role="tab" id="headingThree">
                            <h4>
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">{{ trans('educationlevels/schoollife.prefects-title') }}</a>
                            </h4>
                        </div>

                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body">
                                <p>{{ trans('educationlevels/schoollife.prefects-text1') }}</p>
                                <p>{{ trans('educationlevels/schoollife.prefects-text2') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="hidden-xs">
                <img class="img-responsive" src="{{ url('/img/educationlevels/schoollife2.jpg') }}" alt="">
            </div>
            <div class="row">
                <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-branch">
                        <div class="panel-heading panel-houses" role="tab" id="headingFour">
                            <h4>
                                <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour">{{ trans('educationlevels/schoollife.headstudent-title') }}</a>
                            </h4>
                        </div>

                        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                            <div class="panel-body">
                                <p>{{ trans('educationlevels/schoollife.headstudent-text1') }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-branch">
                        <div class="panel-heading panel-captains" role="tab" id="headingFive">
                            <h4>
                                <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseFive" aria-expanded="true" aria-controls="collapseFive">{{ trans('educationlevels/schoollife.seniorcouncil-title') }}</a>
                            </h4>
                        </div>

                        <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                            <div class="panel-body">
                                <p>{{ trans('educationlevels/schoollife.seniorcouncil-text1') }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-branch">
                        <div class="panel-heading panel-prefects" role="tab" id="headingSix">
                            <h4>
                                <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseSix" aria-expanded="true" aria-controls="collapseSix">{{ trans('educationlevels/schoollife.studentcouncil-title') }}</a>
                            </h4>
                        </div>

                        <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                            <div class="panel-body">
                                <p>{{ trans('educationlevels/schoollife.studentcouncil-text1') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
	@include('educationlevels/nav')
@endsection
