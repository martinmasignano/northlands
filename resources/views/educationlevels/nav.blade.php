{{-- Language set in lang/{language}/educationlevels/nav.php --}}


<div class="panel-nav">
    <ul class="nav nav-pills nav-justified">
    	<li>
    		<a href="{{ url('/educationlevels/performingarts') }}">
    			{!! trans('educationlevels/nav.pa-link') !!}
    		</a>
    	</li>
    	<li>
    		<a href="{{ url('/educationlevels/internationalprogrammes') }}">
    			{!! trans('educationlevels/nav.ip-link') !!}
    		</a>
    	</li>
    	<li>
            {{-- <a href="{{ url('/educationlevels/pse') }}"> --}}
    		<a href="{{ url('http://www.northlands.org.ar/pse.php') }}" target="_blank">
    			{!! trans('educationlevels/nav.pse-link') !!}
    		</a>
    	</li>
    	<li>
    		<a href="{{ url('/educationlevels/aash') }}">
    			{!! trans('educationlevels/nav.aash-link') !!}
    		</a>
    	</li>
    	<li>
    		<a href="{{ url('/educationlevels/pe') }}">
    			{!! trans('educationlevels/nav.pe-link') !!}
    		</a>
    	</li>
    	<li>
    		<a href="{{ url('/educationlevels/schoollife') }}">
    			{!! trans('educationlevels/nav.sl-link') !!}
    		</a>
    	</li>
        <li>
            {{-- <a href="{{ url('/educationlevels/model11') }}"> --}}
    		<a href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/d-LearningProject/d-LearningProject.php') }}" target="_blank">
    			{!! trans('educationlevels/nav.11-link') !!}
    		</a>
    	</li>
    </ul>
</div>
