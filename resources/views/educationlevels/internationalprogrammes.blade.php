{{-- Language set in lang/{language}/educationlevels/internationalprogrammes.php --}}

@extends('layouts.main')
@section('content_class','international-programmes')
@section('content')
    <h1>{!! trans('educationlevels/internationalprogrammes.header') !!}</h1>
    <div class="row">
        <div class="col-sm-12 col-md-8">
            <div class="row">
                <div class="col-sm-4 col-md-3">
                    <img src="../css/img/logo-wsib.png" alt="">
                </div>
                <div class="col-sm-8 col-md-9">
                    <p>{!! trans('educationlevels/internationalprogrammes.intro') !!}</p>
                </div>
            </div>
        </div>
        <div class="visible-md-block visible-lg-block">
            <div class="col-md-4">
                <h3 class="ip-header text-center">
                    {!! trans('educationlevels/internationalprogrammes.ip-header') !!}
                </h3>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-8">
            <div class="row">
                <div class="col-sm-4">
                    <div class="pyp">
                        <h4>{!! trans('educationlevels/internationalprogrammes.pyp-header') !!}</h4>
                        <p>{!! trans('educationlevels/internationalprogrammes.pyp-text') !!}</p>
                    </div>
                    <div class="text-center">
                        <img src="../css/img/pep.png" alt="">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="igcse">
                        <h4>{!! trans('educationlevels/internationalprogrammes.igcse-header') !!}</h4>
                        <p>{!! trans('educationlevels/internationalprogrammes.igcse-text') !!}</p>
                    </div>
                    <div class="text-center">
                        <img src="../css/img/igcse.png" alt="">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="ibo">
                        <h4>{!! trans('educationlevels/internationalprogrammes.ibo-header') !!}</h4>
                        <p>{!! trans('educationlevels/internationalprogrammes.ibo-text') !!}</p>
                    </div>
                    <div class="text-center">
                        <img src="../css/img/diploma.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="visible-md-block visible-lg-block">
            <div class="col-md-4 text-center">
                <ul class="ip-logos">
                    <li>
                        <a href="http://www.ibo.org/" target="_blank">
                            <img src="{{ url('./css/img/logo-wsib.png') }}" alt="International Baccalaureate">
                        </a>
                    </li>
                    <li>
                        <a href="http://www.ibo.org/" target="_blank">
                            <img src="{{ url('./css/img/logo-cambridge.jpg') }}" alt="Cambridge">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.essarp.org.ar/" target="_blank">
                            <img src="{{ url('./css/img/logo-essarp.png') }}" alt="English Speaking Scholastic Association of the River Plate">
                        </a>
                    </li>
                    <li>
                        <a href="http://www.lahc.net/home.htm" target="_blank">
                            <img src="{{ url('./css/img/logo-lahc.png') }}" alt="Latin American Heads Conference">
                        </a>
                    </li>
                    <li>
                        <a href="http://www.cois.org/" target="_blank">
                            <img src="{{ url('./css/img/logo-cis_member.png') }}" alt="Council of International Schools">
                        </a>
                    </li>
                    <li>
                        <a href="http://www.cois.org/" target="_blank">
                            <img src="{{ url('./css/img/logo-cisla.jpg') }}" alt="Council of International Schools">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-12 text-center">
            <p class="disclaimer">{!! trans('educationlevels/internationalprogrammes.disclaimer') !!}</p>
        </div>
    </div>

    <div class="hidden-md hidden-lg">
        <div class="row">
            <div class="col-xs-12">
                <h3 class="ip-header text-center">
                    {!! trans('educationlevels/internationalprogrammes.header') !!}
                </h3>
            </div>
            <div class="col-xs-12 text-center">
                <ul class="ip-logos">
                    <li>
                        <a href="http://www.ibo.org/" target="_blank">
                            <img src="{{ url('./css/img/logo-wsib.png') }}" alt="International Baccalaureate">
                        </a>
                    </li>
                    <li>
                        <a href="http://www.ibo.org/" target="_blank">
                            <img src="{{ url('./css/img/logo-cambridge.jpg') }}" alt="Cambridge">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.essarp.org.ar/" target="_blank">
                            <img src="{{ url('./css/img/logo-essarp.png') }}" alt="English Speaking Scholastic Association of the River Plate">
                        </a>
                    </li>
                    <li>
                        <a href="http://www.lahc.net/home.htm" target="_blank">
                            <img src="{{ url('./css/img/logo-lahc.png') }}" alt="Latin American Heads Conference">
                        </a>
                    </li>
                    <li>
                        <a href="http://www.cois.org/" target="_blank">
                            <img src="{{ url('./css/img/logo-cis_member.png') }}" alt="Council of International Schools">
                        </a>
                    </li>
                    <li>
                        <a href="http://www.cois.org/" target="_blank">
                            <img src="{{ url('./css/img/logo-cisla.jpg') }}" alt="Council of International Schools">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    @include('educationlevels/nav')
@endsection

