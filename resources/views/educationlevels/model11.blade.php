{{-- Language set in lang/{language}/educationlevels/model11.php --}}

@extends('layouts.main')

@section('content')

	<h1>{!! trans('educationlevels/model11.header') !!}</h1>
	<p>{!! trans('educationlevels/model11.content') !!}</p>

	@include('educationlevels/nav')
@endsection
