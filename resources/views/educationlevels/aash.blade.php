{{-- Language set in lang/{language}/educationlevels/aash.php --}}

@extends('layouts.main')

@section('content')

	<div class="row visible-xs-block">
		<div class="col-xs-12">
			<h1>{!! trans('educationlevels/aash.header') !!}</h1>
			<p>{!! trans('educationlevels/aash.content') !!}</p>
			<img src="../css/img/activities.svg" alt="Activities" class="img-responsive">
			<video autoplay controls preload style="width: 100%">
                <source src="{{ asset('./video/activities.mp4')}}" type="video/mp4">
            </video>
			<img src="../css/img/power_or_ability_mobile.svg" alt="Power or Ability" class="img-responsive">
		</div>
	</div>
	
	<div class="hidden-xs">
		<div class="row">
			<div class="col-sm-8">
				<h1>{!! trans('educationlevels/aash.header') !!}</h1>
				<p>{!! trans('educationlevels/aash.content') !!}</p>

				<div class="row">
					<div class="col-sm-4">
						<img src="../css/img/power_or_ability_desktop.svg" alt="Power or Ability" class="img-responsive">
					</div>

					<div class="col-sm-8" style="margin-top: 40px;">
						<video autoplay controls preload style="width: 100%">
		                    <source src="{{ asset('./video/activities.mp4')}}" type="video/mp4">
		                </video>
					</div>
				</div>
			</div>

			<div class="col-sm-4">
				<img src="../css/img/activities.svg" alt="Activities" class="img-responsive">
			</div>
		</div>
	</div>

	@include('educationlevels/nav')
@endsection
