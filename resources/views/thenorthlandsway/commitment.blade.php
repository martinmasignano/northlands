{{-- Language set in lang/{language}/thenorthlandsway/commitment.php --}}

@extends('layouts.main')

@section('content')
	
	<h1>{!! trans('thenorthlandsway/commitment.header') !!}</h1>
	<p>{!! trans('thenorthlandsway/commitment.content') !!}</p>

@endsection