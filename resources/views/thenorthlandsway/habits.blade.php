{{-- Language set in lang/{language}/thenorthlandsway/habits.php --}}

@extends('layouts.main')

@section('content')
	
	<h1>{!! trans('thenorthlandsway/habits.header') !!}</h1>
	<p>{!! trans('thenorthlandsway/habits.content') !!}</p>

@endsection