{{-- Language set in lang/{language}/thenorthlandsway/profile.php --}}

@extends('layouts.main')

@section('content')

	<h1>{!! trans('thenorthlandsway/profile.header') !!}</h1>
	<p>{!! trans('thenorthlandsway/profile.content') !!}</p>

@endsection