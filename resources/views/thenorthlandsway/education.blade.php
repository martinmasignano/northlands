{{-- Language set in lang/{language}/thenorthlandsway/education.php --}}

@extends('layouts.main')

@section('content')
	
	<h1>{!! trans('thenorthlandsway/education.header') !!}</h1>
	<p>{!! trans('thenorthlandsway/education.content') !!}</p>

@endsection