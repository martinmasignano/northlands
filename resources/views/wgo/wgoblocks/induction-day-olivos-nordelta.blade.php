@extends('layouts.gallery')

{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
    <section class="grid-item section5 section6">
@overwrite

{{-- Imagen (Debe estar guardada en public_html/img/wgo/galleries/{nombre-de-la-galeria}) --}}
@section('image')
    <img class="img-responsive" src="img/wgo/galleries/InductionDay/INDEX_InductionDay.jpg" alt="Induction Day / Día de Inducción">
@overwrite

{{-- Título --}}
@section('caption')
	<a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/sn_y7_InductionDay/sn_y7_InductionDay.php') }}">
		<h4><strong>Induction Day</strong></h4>
		<p><strong><i>Día de Inducción</i></strong></p>
	</a>
@overwrite

{{-- Nivel y Sede --}}
@section('place')
	Secondary Olivos & Nordelta 
@overwrite
