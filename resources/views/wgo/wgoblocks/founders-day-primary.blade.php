@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section4">
@overwrite

@section('image')
	{!! Html::image('img/wgo/galleries/FoundersDayPrimaria/INDEX_FoundersDayPrimaria.jpg', '', array('class' => 'img-responsive')) !!}
@overwrite

@section('caption')
	<a class="caption" href="{{ url('articles/wgo/founders-day-primary') }}">
		<h4><strong>Founder's Day</strong></h4>
		<p><strong><i>Día de la Fundadora</i></strong></p>
	</a>
@overwrite

@section('place')
	Primary Olivos
@overwrite
