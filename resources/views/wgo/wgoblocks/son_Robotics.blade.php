@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section5 section6">
@overwrite

@section('image')
	<img class="img-responsive" src="img/wgo/galleries/son_Robotics/son_Robotics.jpg" alt="Developing programming skills / Desarrollando habilidades de programación">
@overwrite

@section('caption')
	<a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/son_Robotics/son_Robotics.php
') }}">
        <h4><strong>Developing programming skills</strong></h4>
        <p><strong><i>Desarrollando habilidades de programación</i></strong></p>
	</a>
@overwrite

@section('place')
	Secondary Olivos & Nordelta
@overwrite
