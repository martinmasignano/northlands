@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section1 section2">
@overwrite

@section('image')
	<img class="img-responsive" src="img/wgo/galleries/kon_KidsOlympicGames/kon_KidsOlympicGames.jpg" alt="Kids Olympic Games">
@overwrite

@section('caption')
	<a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/kon_KidsOlympicGames/kon_KidsOlympicGames.php') }}">
        <h4><strong>Kids Olympic Games
 </strong></h4>
        <p><strong><i>
</i></strong></p>
	</a>
@overwrite

@section('place')
	Kindergarten Nordelta & Olivos
@overwrite
