@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section5 section6">
@overwrite

@section('image')
	<img class="img-responsive" src="img/wgo/galleries/performing-arts-ny-olivos-nordelta/INDEX_PerformingArtsNewYork.jpg" alt="Performing Arts in New York / Artes Interpretativas en Nueva York">
@overwrite

@section('caption')
	<a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/son_PerformingArtsNY/son_PerformingArtsNY.php') }}">
        <h4><strong>Performing Arts in New York</strong></h4>
        <p><strong><i>Artes Interpretativas en Nueva York</i></strong></p>
	</a>
@overwrite

@section('place')
	Secondary Olivos & Nordelta
@overwrite
