@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section4">
@overwrite

@section('image')
	<img class="img-responsive" src="img/wgo/galleries/po_iPadShowcase_y2y3/po_iPadShowcase_y2y3.jpg" alt="NORTHLANDS iPad Showcase / Open Lesson - Clase abierta">
@overwrite

@section('caption')
	<a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/po_iPadShowcase_y2y3/po_iPadShowcase_y2y3.php') }}">
        <h4><strong>NORTHLANDS iPad Showcase Open Lesson </strong></h4>
        <p><strong><i>NORTHLANDS iPad Showcase

Open Lesson / Clase abierta</i></strong></p>
	</a>
@overwrite

@section('place')
	Primary Olivos
@overwrite
