@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section1 section2 section3 section4">
@overwrite

@section('image')
    <img class="img-responsive" src="img/wgo/galleries/onkp-PYP-Presentation/onkp-PYP-Presentation.jpg" alt="IB PYP Informative Meeting">
@overwrite

@section('caption')
    <a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/onkp_PYP_Presentation/onkp_PYP_Presentation.php') }}">
        <h4><strong>IB PYP Informative Meeting 
</strong></h4>
        <p><strong><i>Reunión Informativa sobre el PEP del IB
</i></strong></p>
    </a>
@overwrite

@section('place')
    Kindergarten and Primary
Olivos & Nordelta

@overwrite
