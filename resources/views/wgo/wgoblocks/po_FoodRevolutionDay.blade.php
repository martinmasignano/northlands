@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section4">
@overwrite

@section('image')
	<img class="img-responsive" src="img/wgo/galleries/po_FoodRevolutionDay/po_FoodRevolutionDay.jpg" alt="Celebrating Food Revolution Day / Celebrando Food Revolution Day">
@overwrite

@section('caption')
	<a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/po_FoodRevolutionDay/po_FoodRevolutionDay.php') }}">
        <h4><strong>Celebrating Food Revolution Day</strong></h4>
        <p><strong><i>Celebrando 'Food Revolution Day'</i></strong></p>
	</a>
@overwrite

@section('place')
	Primary Olivos
@overwrite
