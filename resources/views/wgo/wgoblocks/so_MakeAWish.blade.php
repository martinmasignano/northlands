@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section6">
@overwrite

@section('image')
	<img class="img-responsive" src="img/wgo/galleries/so_MakeAWish/so_MakeAWish.jpg" alt="Make a wish - Secondary Musical
/ Musical de Secundaria">
@overwrite

@section('caption')
	<a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/so_MakeAWish/so_MakeAWish.php
') }}">
        <h4><strong>“Make a wish” - Secondary Musical
</strong></h4>
        <p><strong><i>Musical de Secundaria</i></strong></p>
	</a>
@overwrite

@section('place')
	Secondary Olivos 
@overwrite
