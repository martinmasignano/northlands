@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section4 section3 section5 section6">
@overwrite

@section('image')
	<img class="img-responsive" src="img/wgo/galleries/onps2_OMA/onps2_OMA.jpg" alt="OMA 'Ñandú' - Final Rounds / - Instancias finales">
@overwrite

@section('caption')
	<a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/onps2_OMA/onps2_OMA.php') }}">
        <h4><strong>OMA 'Ñandú' - Final Rounds </strong></h4>
        <p><strong><i>Instancias finales</i></strong></p>
	</a>
@overwrite

@section('place')
	Nordelta & Olivos - Primary & Secondary
@overwrite
