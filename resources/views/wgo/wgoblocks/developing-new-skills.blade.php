@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section5">
@overwrite

@section('image')
    <img class="img-responsive" src="img/wgo/galleries/developing-new-skills-nordelta/sn_NewSkills.jpg" alt="Developing new skills / Desarrollando nuevas habilidades">
@overwrite

@section('caption')
    <a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/sn_NewSkills/sn_NewSkills.php') }}">
        <h4><strong>Developing new skills</strong></h4>
        <p><strong><i>Desarrollando nuevas habilidades</i></strong></p>
    </a>
@overwrite

@section('place')
    Secondary Nordelta
@overwrite
