@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section8">
@overwrite

@section('image')
    <img class="img-responsive" src="img/wgo/galleries/a-passion-for-skiing/skiing.jpg" alt="A passion for skiing, Balthazar and Tiziano / Apasionados por el ski, Balthazar y Tiziano">
@overwrite

@section('caption')
    <a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/PassionForSkiing/bnw_PassionForSkiing_2016.php') }}">
        <h4><strong>A passion for skiing, Balthazar and Tiziano</strong></h4>
        <p><strong><i>Apasionados por el ski, Balthazar y Tiziano</i></strong></p>
    </a>
@overwrite

@section('place')
    Más allá de NORTHLANDS
@overwrite
