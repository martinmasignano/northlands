@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section4">
@overwrite

@section('image')
	<img class="img-responsive" src="img/wgo/galleries/pn_OMA_InstanciasIniciales/pn_OMA_InstanciasIniciales.jpg" alt="Ñandú' Argentine Maths Olympiads - First rounds / Olimpíada Matemática Argentina Ñandú
- Instancias iniciales">
@overwrite

@section('caption')
	<a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/po_OMA_PrimeraRonda/po_OMA_PrimeraRonda.php
') }}">
        <h4><strong>Ñandú' Argentine Maths Olympiads - First rounds </strong></h4>
        <p><strong><i>Olimpíada Matemática Argentina Ñandú - Instancias iniciales</i></strong></p>
	</a>
@overwrite

@section('place')
	Primary Olivos
@overwrite
