@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section6">
@overwrite

@section('image')
	<img class="img-responsive" src="img/wgo/galleries/so_ESSARP_SoloistFestival/so_ESSARP_SoloistFestival.jpg" alt="Great performance, Delfina! / ¡Gran presentación, Delfina! ">
@overwrite

@section('caption')
	<a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/so_ESSARP_SoloistFestival/so_ESSARP_SoloistFestival.php
') }}">
        <h4><strong>Great performance, Delfina! </strong></h4>
        <p><strong><i>¡Gran presentación, Delfina! </i></strong></p>
	</a>
@overwrite

@section('place')
	Secondary Olivos
@overwrite
