@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section3 section4">
@overwrite

@section('image')
	<img class="img-responsive" src="img/wgo/galleries/wsb_LittleSports/wsb_LittleSports.jpg" alt="Little Sports 2016, Primary / 2016 Little Sports, Primaria">
@overwrite

@section('caption')
	<a class="caption" href="{{ url('https://sites.google.com/a/northlands.edu.ar/physical-education/home/wsb/wsb-little-sports-22-09') }}">
        <h4><strong>Little Sports 2016, Primary</strong></h4>
        <p><strong><i>2016 Little Sports, Primaria</i></strong></p>
	</a>
@overwrite

@section('place')
	Primary
@overwrite
