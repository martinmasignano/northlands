@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section2">
@overwrite

@section('image')
    <img class="img-responsive" src="img/wgo/galleries/playing-with-unstructured-material/ko_HeuristicGame_2016.jpg" alt="Playing with unstructured material / Juego con material no estructurado">
@overwrite

@section('caption')
    <a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/ko_HeuristicGame/ko_HeuristicGame.php') }}">
        <h4><strong>Playing with unstructured material</strong></h4>
        <p><strong><i>Juego con material no estructurado</i></strong></p>
    </a>
@overwrite

@section('place')
    Kindergarten Olivos
@overwrite
