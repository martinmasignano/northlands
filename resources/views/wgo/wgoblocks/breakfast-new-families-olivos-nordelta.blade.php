@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section1 section2 section3 section4 section5 section6">
@overwrite

@section('image')
	<img class="img-responsive" src="img/wgo/galleries/breakfast-new-families-olivos-nordelta/kpson_NewFamilies.jpg" alt="Breakfast for New Families / Desayuno para Familias Nuevas">
@overwrite

@section('caption')
	<a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/kpson_NewFamilies/kpson_NewFamilies.php') }}">
        <h4><strong>Breakfast for New Families</strong></h4>
        <p><strong><i>Desayuno para Familias Nuevas</i></strong></p>
	</a>
@overwrite

@section('place')
	Olivos & Nordelta
@overwrite
