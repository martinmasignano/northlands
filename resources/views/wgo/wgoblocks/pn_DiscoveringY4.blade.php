@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section3">
@overwrite

@section('image')
	<img class="img-responsive" src="img/wgo/galleries/pn_DiscoveringY4/pn_DiscoveringY4.jpg" alt="Discovering our Planet / Descubriendo nuestro planeta">
@overwrite

@section('caption')
	<a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/pn_DiscoveringY4/pn_DiscoveringY4.php') }}">
        <h4><strong>Discovering our Planet</strong></h4>
        <p><strong><i>Descubriendo nuestro planeta</i></strong></p>
	</a>
@overwrite

@section('place')
	Primary Nordelta
@overwrite
