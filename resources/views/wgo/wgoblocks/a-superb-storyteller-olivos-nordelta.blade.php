@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section3 section4">
@overwrite

@section('image')
	<img class="img-responsive" src="img/wgo/galleries/a-superb-storyteller-olivos-nordelta/INDEX_SuperbStoryteller.jpg" alt="A Superb storyteller / Una narradora Magnifica">
@overwrite

@section('caption')
	<a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/onps_VickyGrant/onps_VickyGrant.php') }}">
        <h4><strong>A Superb storyteller</strong></h4>
        <p><strong><i>Una narradora Magnifica</i></strong></p>
	</a>
@overwrite

@section('place')
	Olivos & Nordelta
@overwrite
