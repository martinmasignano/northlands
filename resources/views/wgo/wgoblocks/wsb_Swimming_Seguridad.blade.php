@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section1 section2 section3 section4 section5 section6">
@overwrite

@section('image')
	<img class="img-responsive" src="img/wgo/galleries/wsb_Swimming_Seguridad/wsb_Swimming_Seguridad.jpg" alt="Water Safety - Swimming Activity, / Seguridad en el medio acu&aacute;tico - Actividad Natación, Nivel Inicial, Primaria y Secundaria">
@overwrite

@section('caption')
	<a class="caption" href="{{ url('https://sites.google.com/a/northlands.edu.ar/physical-education/home/wsb/wsb-swimming-november') }}">
        <h4><strong>Water Safety - Swimming Activity</strong></h4>
        <p><strong><i>Seguridad en el medio acu&aacute;tico - Actividad Natación, Nivel Inicial, Primaria y Secundaria</i></strong></p>
	</a>
@overwrite

@section('place')
	Nordelta & Olivos
@overwrite
