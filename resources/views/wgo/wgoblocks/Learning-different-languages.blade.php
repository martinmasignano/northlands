@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section2">
@overwrite

@section('image')
	<img class="img-responsive" src="img/wgo/galleries/ko_Himno_Senas/ko_Himno_Senas.jpg" alt="OGlobal Citizenship - Learning different languages / Ciudadanía Global - Aprendiendo diferentes idiomas">
@overwrite

@section('caption')
	<a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/ko_Himno_Senas/ko_Himno_Senas.php

') }}">
        <h4><strong>Global Citizenship - Learning different languages </strong></h4>
        <p><strong><i>"Ciudadanía Global - Aprendiendo diferentes idiomas</i></strong></p>
	</a>
@overwrite

@section('place')
	Kindergarten Olivos
@overwrite
