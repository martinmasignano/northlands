@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section4">
@overwrite

@section('image')
	<img class="img-responsive" src="img/wgo/galleries/op_ChildrensDay2016/op_ChildrensDay2016.jpg" alt="Children´s Day Celebration / Celebración del Día del Niño
">
@overwrite

@section('caption')
	<a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/op_ChildrensDay2016/op_ChildrensDay2016.php
') }}">
        <h4><strong>Children´s Day Celebration </strong></h4>
        <p><strong><i>Celebración del Día del Niño
</i></strong></p>
	</a>
@overwrite

@section('place')
	Primary Olivos
@overwrite
