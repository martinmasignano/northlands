@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section6">
@overwrite

@section('image')
	<img class="img-responsive" src="img/wgo/galleries/chaco-2015-assembly-olivos/INDEX_Chaco2015Assembly.jpg" alt="Chaco 2015 Assembly">
@overwrite

@section('caption')
	<a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/os_Chaco2015Assembly/os_Chaco2015Assembly.php') }}">
        <h4><strong>Chaco 2015 Assembly</strong></h4>
        <p><strong><i></i></strong></p>
	</a>
@overwrite

@section('place')
	Secondary Olivos
@overwrite
