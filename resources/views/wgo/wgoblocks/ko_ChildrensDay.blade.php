@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section2">
@overwrite

@section('image')
	<img class="img-responsive" src="img/wgo/galleries/ko_ChildrensDay/ko_ChildrensDay.jpg" alt="Building the ability to play and creativeness / Construyendo habilidades lúdicas y creativas">
@overwrite

@section('caption')
	<a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/ko_ChildrensDay/ko_ChildrensDay.php') }}">
        <h4><strong>Building ability </strong></h4>
        <h5><strong><i>Construyendo habilidades</i></strong></h5>
	</a>
@overwrite

@section('place')
	Kindergarten Olivos
@overwrite
