@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section2">
@overwrite

@section('image')
    <img class="img-responsive" src="img/wgo/galleries/digital-learners-olivos/ko_DigitalLearners.jpg" alt="Digital Learners / Aprendices Digitales">
@overwrite

@section('caption')
    <a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/ko_DigitalLearners/ko_DigitalLearners.php') }}">
        <h4><strong>Digital Learners</strong></h4>
        <p><strong><i>Aprendices Digitales</i></strong></p>
    </a>
@overwrite

@section('place')
    Kindergarten Olivos
@overwrite
