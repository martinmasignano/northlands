@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section4 section6">
@overwrite

@section('image')
	<img class="img-responsive" src="img/wgo/galleries/ops_Bridging/ops_Bridging.jpg" alt="Year 6/Year 7 Bridging / Articulación Año 6/Año 7">
@overwrite

@section('caption')
	<a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/ops_Bridging/ops_Bridging.php') }}">
        <h4><strong>Year 6 - Year 7 Bridging
 </strong></h4>
        <p><strong><i>
Articulación Año 6 - Año 7</i></strong></p>
	</a>
@overwrite

@section('place')
	Primary & Secondary Olivos
@overwrite
