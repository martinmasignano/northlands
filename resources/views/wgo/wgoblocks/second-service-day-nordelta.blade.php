@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section5">
@overwrite

@section('image')
    <img class="img-responsive" src="img/wgo/galleries/second-service-day-nordelta/sn_2ndServiceDay.jpg" alt="Second Service Day / Segundo 'Service Day'">
@overwrite

@section('caption')
    <a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/sn_2ndServiceDay/sn_2ndServiceDay.php') }}">
        <h4><strong>Second Service Day</strong></h4>
        <p><strong><i>Segundo 'Service Day'</i></strong></p>
    </a>
@overwrite

@section('place')
    Secondary Nordelta
@overwrite
