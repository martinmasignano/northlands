@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section8">
@overwrite

@section('image')
    <img class="img-responsive" src="img/wgo/galleries/cristina-rodrigues/bnw_CristinaRodrigues_CITEI.jpg" alt="Cristina Rodrigues (IT Teacher) Member of CITEI Committe / Cristina Rodrigues (Docente de IT ) Miembro del Comité CITEI">
@overwrite

@section('caption')
    <a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/bnw_CristinaRodrigues_CITEI/bnw_CristinaRodrigues_CITEI.php') }}">
        <h4><strong>Cristina Rodrigues (IT Teacher) Member of CITEI Committe</strong></h4>
        <p><strong><i>Cristina Rodrigues (Docente de IT ) Miembro del Comité CITEI</i></strong></p>
    </a>
@overwrite

@section('place')
    Más allá de NORTHLANDS
@overwrite
