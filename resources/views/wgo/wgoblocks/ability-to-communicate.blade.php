@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section5">
@overwrite

@section('image')
	<img class="img-responsive" src="img/wgo/galleries/AbilityToCommunicate/INDEX_Ability2Communicate.jpg" alt="Ability to Communicate">
@overwrite

@section('caption')
	<a class="caption" href="{{ url('articles/wgo/ability-to-communicate') }}">
		<h3><strong>Ability to communicate</strong></h3>
		<p><strong><i>Habilidad para comunicarnos</i></strong></p>
	</a>
@overwrite

@section('place')
	Nordelta Secondary
@overwrite
