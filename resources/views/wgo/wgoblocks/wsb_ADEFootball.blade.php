@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section6 section5">
@overwrite

@section('image')
	<img class="img-responsive" src="img/wgo/galleries/wsb_ADEFootball/wsb_ADEFootball.jpg" alt="¡Felicitaciones al equipo Senior de Fútbol, campeones del Torneo ADE!, Secundaria / Congratulations to our Senior Football Team, Champions at the ADE Tournament!!!">
@overwrite

@section('caption')
	<a class="caption" href="{{ url('https://sites.google.com/a/northlands.edu.ar/physical-education/home/wsb/wsb-little-sports-22-09') }}">
        <h4><strong>Congratulations to our Senior Football Team, Champions at the ADE Tournament!!!</strong></h4>
        <p><strong><i>¡Felicitaciones al equipo Senior de Fútbol, campeones del Torneo ADE!, Secundaria </i></strong></p>
	</a>
@overwrite

@section('place')
	Primary
@overwrite
