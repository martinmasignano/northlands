@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section1">
@overwrite

@section('image')
    <img class="img-responsive" src="img/wgo/galleries/kn-TheFlyingNanny/kn_TheFlyingNanny.jpg" alt="Kindergarten Concert Nordelta">
@overwrite

@section('caption')
    <a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/kn_Concert2016/kn_Concert2016.php
') }}">
        <h4><strong>The Flying Nanny - Kindergarten Concert 
</strong></h4>
        <p><strong><i>
</i></strong></p>
    </a>
@overwrite

@section('place')
    Kindergarten Nordelta
@overwrite
