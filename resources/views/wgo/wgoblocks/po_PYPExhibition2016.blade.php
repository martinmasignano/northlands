@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section4">
@overwrite

@section('image')
	<img class="img-responsive" src="img/wgo/galleries/po_PYPExhibition2016/po_PYPExhibition2016.jpg" alt="2016 IB PYP Exhibition / Exposisión IB PEP 2016">
@overwrite

@section('caption')
	<a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/po_PYPExhibition2016/po_PYPExhibition2016.php') }}">
        <h4><strong>2016 IB PYP Exhibition </strong></h4>
        <p><strong><i>Exposisión IB PEP 2016</i></strong></p>
	</a>
@overwrite

@section('place')
	Primary Olivos
@overwrite
