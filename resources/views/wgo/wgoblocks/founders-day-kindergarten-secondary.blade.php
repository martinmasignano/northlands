@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section1 section5">
@overwrite

@section('image')
    <img class="img-responsive" src="img/wgo/galleries/founders-day-kindergarten-secondary/INDEX_FoundersDay.jpg" alt="Founder's Day / Día de la Fundadora">
@overwrite

@section('caption')
    <a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/ksn_FoundersDay/ksn_FoundersDay.php') }}">
        <h4><strong>Founder's Day</strong></h4>
        <p><strong><i>Día de la Fundadora</i></strong></p>
    </a>
@overwrite

@section('place')
    Kindergarten & Secondary Nordelta
@overwrite
