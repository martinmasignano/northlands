@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section4">
@overwrite

@section('image')
    <img class="img-responsive" src="img/wgo/galleries/global-collaboration-campaign-olivos/INDEX_GlobalCollaborationCampaign.jpg" alt="Global Collaboration Campaign  / Campaña de Colaboración Global">
@overwrite

@section('caption')
    <a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/po_LIMBSInternational/po_LIMBSInternational.php') }}">
        <h4><strong>Global Collaboration Campaign</strong></h4>
        <p><strong><i>Campaña de Colaboración Global</i></strong></p>
    </a>
@overwrite

@section('place')
    2015-2016 - Primary Olivos
@overwrite
