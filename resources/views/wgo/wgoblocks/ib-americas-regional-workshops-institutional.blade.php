@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section7">
@overwrite

@section('image')
    <img class="img-responsive" src="img/wgo/galleries/ib-americas-regional-workshops-institutional/TalleresRegionalesIBAmericas.jpg" alt="IB Americas Regional Workshops / Talleres Regionales de IB Américas">
@overwrite

@section('caption')
    <a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/TalleresRegionalesIBAmericas/TalleresRegionalesIBAmericas.php') }}">
        <h4><strong>IB Americas Regional Workshops</strong></h4>
        <p><strong><i>Talleres Regionales de IB Américas</i></strong></p>
    </a>
@overwrite

@section('place')
    Institucional
@overwrite
