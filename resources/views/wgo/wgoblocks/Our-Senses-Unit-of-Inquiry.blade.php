@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section2">
@overwrite

@section('image')
	<img class="img-responsive" src="img/wgo/galleries/ko_OurSenses/ko_OurSenses.jpg" alt="Our Senses - Unit of Inquiry  / Nuestros sentidos - Unidad de indagación">
@overwrite

@section('caption')
	<a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/ko_OurSenses/ko_OurSenses.php
') }}">
        <h4><strong>"Our Senses" - Unit of Inquiry </strong></h4>
        <p><strong><i>"Nuestros sentidos" - Unidad de indagación</i></strong></p>
	</a>
@overwrite

@section('place')
	Kindergarten Olivos
@overwrite
