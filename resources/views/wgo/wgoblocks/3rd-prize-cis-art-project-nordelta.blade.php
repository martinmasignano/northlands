@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section5">
@overwrite

@section('image')
	<img class="img-responsive" src="img/wgo/galleries/3rd-prize-cis-art-project-nordelta/INDEX_3rdPrizeCISArtProject.jpg" alt="3rd Prize - CIS Art Project / 3er Premio - Proyecto de Arte del CIS">
@overwrite

@section('caption')
	<a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/sn_3rdPriceCIS/sn_3rdPriceCIS.php') }}">
        <h4><strong>3rd Prize - CIS Art Project</strong></h4>
        <p><strong><i>3er Premio - Proyecto de Arte del CIS</i></strong></p>
	</a>
@overwrite

@section('place')
	Secondary Nordelta
@overwrite
