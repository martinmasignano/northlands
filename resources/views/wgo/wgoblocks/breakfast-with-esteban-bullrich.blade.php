@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section7">
@overwrite

@section('image')
    <img class="img-responsive" src="img/wgo/galleries/breakfast-with-esteban-bullrich/kpson_DesayunoConEBullrich.jpg" alt="Breakfast with Esteban Bullrich, Minister of Education / Desayuno con el Ministro de Educación Esteban Bullrich">
@overwrite

@section('caption')
    <a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/kpson_DesayunoConEBullrich/kpson_DesayunoConEBullrich.php') }}">
        <h4><strong>Breakfast with Esteban Bullrich, Minister of Education</strong></h4>
        <p><strong><i>Desayuno con el Ministro de Educación Esteban Bullrich</i></strong></p>
    </a>
@overwrite

@section('place')
    Institucional
@overwrite
