@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section1">
@overwrite

@section('image')
    <img class="img-responsive" src="img/wgo/galleries/first-unit-of-inquiry-nordelta/kn_FirstUnitOfInquiry.jpg" alt="First Unit of Inquiry / Primer unidad de indagación">
@overwrite

@section('caption')
    <a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/kn_FirstUnitOfInquiry/kn_FirstUnitOfInquiry.php') }}">
        <h4><strong>First Unit of Inquiry</strong></h4>
        <p><strong><i>Primer unidad de indagación</i></strong></p>
    </a>
@overwrite

@section('place')
    Kindergarten Nordelta
@overwrite
