@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section2">
@overwrite

@section('image')
	<img class="img-responsive" src="img/wgo/galleries/ko_ChangesinNature_Spring/ko_ChangesinNature_Spring.jpg" alt="Kindergarten Olivos / Cambios en la naturaleza: Primavera ">
@overwrite

@section('caption')
	<a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/ko_ChangesinNature_Spring/ko_ChangesinNature_Spring.php') }}">
        <h4><strong>Changes in Nature: Spring</strong></h4>
        <p><strong><i>Cambios en la naturaleza: Primavera </i></strong></p>
	</a>
@overwrite

@section('place')
	Kindergarten Olivos
@overwrite
