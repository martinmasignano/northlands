@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section2">
@overwrite

@section('image')
	<img class="img-responsive" src="img/wgo/galleries/ko_MyBody/ko_MyBody.jpg" alt="My Body / Mi cuerpo">
@overwrite

@section('caption')
	<a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/ko_MyBody/ko_MyBody.php') }}">
        <h4><strong>My Body</strong></h4>
        <h5><strong><i>Mi cuerpo</i></strong></h5>
	</a>
@overwrite

@section('place')
	Kindergarten Olivos
@overwrite
