@extends('layouts.gallery')

{{-- ID para el filtro WGO --}}
{{-- Listado de ID's para filtrar: <section id="section{NUMERO-DE-LA-LISTA}" class="grid-item">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
    <section class="grid-item section6">
@overwrite

{{-- Imagen (Debe estar guardada en public_html/img/wgo/galleries/{nombre-de-la-galeria}) --}}
@section('image')
    <img class="img-responsive" src="img/wgo/galleries/house-convivencia-secundaria-olivos/INDEX_HouseConvivenciaOlivos.jpg" alt="House 'Convivencia' / Convivencia de Houses">
@overwrite

{{-- Título --}}
@section('caption')
	<a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/so_HouseConvivencia/so_HouseConvivencia.php') }}">
		<h4><strong>House 'Convivencia'</strong></h4>
		<p><strong><i>Convivencia de Houses</i></strong></p>
	</a>
@overwrite

{{-- Nivel y Sede --}}
@section('place')
	Secundaria Olivos
@overwrite
