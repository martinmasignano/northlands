@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section5">
@overwrite

@section('image')
	{!! Html::image('img/wgo/galleries/Chaco2015AssemblySecundaria/INDEX_Chaco2015Assembly.jpg', '', array('class' => 'img-responsive')) !!}
@overwrite

@section('caption')
	<a class="caption" href="{{ url('articles/wgo/chaco-2015-assembly') }}">
		<h4><strong>Chaco 2015 Assembly</strong></h4>
		{{-- <p><strong><i>xxx</i></strong></p> --}}
	</a>
@overwrite

@section('place')
	Secondary Nordelta
@overwrite
