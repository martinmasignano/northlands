@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section5 section6">
@overwrite

@section('image')
    <img class="img-responsive" src="img/wgo/galleries/top-mark-in-argentina/son_IAC_TopMark.jpg" alt="Top Mark in Argentina in subjects and group awards (ICE) / Mayor puntaje de Argentina por materia y por grupo de materias (ICE)">
@overwrite

@section('caption')
    <a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/son_IAC_TopMark/son_IAC_TopMark.php') }}">
        <h4><strong>Top Mark in Argentina in subjects and group awards (ICE)</strong></h4>
        <p><strong><i>Mayor puntaje de Argentina por materia y por grupo de materias (ICE)</i></strong></p>
    </a>
@overwrite

@section('place')
    Secondary Olivos & Nordelta
@overwrite
