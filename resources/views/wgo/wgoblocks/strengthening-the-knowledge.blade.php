@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section5 section6">
@overwrite

@section('image')
	{!! Html::image('img/wgo/galleries/StrengtheningThe Knowledge/INDEX_StrengtheningTheKnowledge.jpg', '', array('class' => 'img-responsive')) !!}
@overwrite

@section('caption')
	<a class="caption" href="{{ url('articles/wgo/strengthening-the-knowledge') }}">
		<h4><strong>Strengthening the knowledge</strong></h4>
		<p><strong><i>Reforzando el conocimiento</i></strong></p>
	</a>
@overwrite

@section('place')
	Secondary Olivos & Nordelta
@overwrite
