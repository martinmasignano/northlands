@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section8">
@overwrite

@section('image')
	<img class="img-responsive" src="img/wgo/galleries/bnw_UniversityCounselor/bnw_UniversityCounselor.jpg" alt="University counselor invited as a panelist to IACAC / Consejero universitario invitado como panelista a IACAC">
@overwrite

@section('caption')
	<a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/bnw_UniversityCounselor/bnw_UniversityCounselor.php
') }}">
        <h4><strong>University counselor invited as a panelist to IACAC</strong></h4>
        <p><strong><i>Consejero universitario invitado como panelista a IACAC</i></strong></p>
	</a>
@overwrite

@section('place')
	Beyond NORTHLANDS' Walls
@overwrite
