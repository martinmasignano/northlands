@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section5 section6">
@overwrite

@section('image')
	<img class="img-responsive" src="img/wgo/galleries/son_ESSARP_CWC/son_ESSARP_CWC.jpg" alt="2016 ESSARP Creating Writing Competition / Competencia de Escritura Creativa de ESSARP 2016">
@overwrite

@section('caption')
	<a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/son_ESSARP_CWC/son_ESSARP_CWC.php
') }}">
        <h4><strong>2016 ESSARP Creating Writing Competition</strong></h4>
        <p><strong><i>Competencia de Escritura Creativa de ESSARP 2016</i></strong></p>
	</a>
@overwrite

@section('place')
	Secondary Olivos & Nordelta
@overwrite
