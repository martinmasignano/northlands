@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section7">
@overwrite

@section('image')
    <img class="img-responsive" src="img/wgo/galleries/fcd-visit-olivos-nordelta/kpson_FCD.jpg" alt="FCD Visit / Visita de FCD">
@overwrite

@section('caption')
    <a class="caption" href="{{ url('https://accounts.google.com/o/oauth2/auth?client_id=493713552412.apps.googleusercontent.com&response_type=code&scope=openid%20email&display=popup&redirect_uri=http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/kpson_FCD/kpson_FCD.php&state=fc37e65a7707ffd0f78dc0f561c25887&login_hint=
    ') }}">
        <h4><strong>FCD Visit</strong></h4>
        <p><strong><i>Visita de FCD</i></strong></p>
    </a>
@overwrite

@section('place')
    Olivos & Nordelta
@overwrite
