@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section3 section4 section7 ">
@overwrite

@section('image')
    <img class="img-responsive" src="img/wgo/galleries/professional-development/onp_ProfessionalDevelopment.jpg
" alt="IB PYP Professional Development / Desarrollo Profesional del IB PEP">
@overwrite

@section('caption')
    <a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/onp_ProfessionalDevelopment/onp_ProfessionalDevelopment.php

') }}">
        <h4><strong>IB PYP Professional Development</strong></h4>
        <p><strong><i>Desarrollo Profesional del IB PEP</i></strong></p>
    </a>
@overwrite

@section('place')
    Primary Nordelta
@overwrite
