@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section7">
@overwrite

@section('image')
    <img class="img-responsive" src="img/wgo/galleries/25-anniversary-chaco-project/kpson_Chaco25.jpg" alt="25 Anniversary Chaco Project Celebration / Celebración del 25 Aniversario Proyecto Chaco">
@overwrite

@section('caption')
    <a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/kpson_Chaco25/kpson_Chaco25.php') }}">
        <h4><strong>25 Anniversary Chaco Project Celebration</strong></h4>
        <p><strong><i>Celebración del 25 Aniversario Proyecto Chaco</i></strong></p>
    </a>
@overwrite

@section('place')
    Olivos & Nordelta
@overwrite
