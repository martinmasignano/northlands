@extends('layouts.gallery')
{{-- classes para el filtro WGO --}}
{{-- Listado de classes para filtrar: <section class=" grid-item section{NUMERO-DE-LA-LISTA}">
    1 = kinder nordelta
    2 = kinder olivos
    3 = primaria nordelta
    4 = primaria olivos
    5 = secundaria nordelta
    6 = secundaria olivos
    7 = institucional
    8 = beyond
--}}
@section('section')
<section class="grid-item section1 section2 section3 section4 section5 section6">
@overwrite

@section('image')
    <img class="img-responsive" src="img/wgo/galleries/careers-universities-choice/Class2014-CareersChoice.jpg" alt="Careers and Universities Choice / Elección de Carreras y Universidades">
@overwrite

@section('caption')
    <a class="caption" href="{{ url('http://www.northlands.org.ar/extensive/N1589O753/Galleries2016/kpson_CareerChoices_2015/kpson_CareerChoices_2015.php') }}">
        <h4><strong>Careers and Universities Choice</strong></h4>
        <p><strong><i>Elección de Carreras y Universidades</i></strong></p>
    </a>
@overwrite

@section('place')
    Olivos & Nordelta
@overwrite
