{{-- Language set in lang/{language}/educationlevels/wgo.php --}}

@extends('layouts.main')

@section('content_class','wgo')
@section('content')

	<div class="row">
		<div class="col-xs-12 col-md-8">
			<h1>{!! trans('educationlevels/wgo.title') !!}</h1>
		</div>
		<div class="col-xs-12 col-md-4">
			<select class="btn btn-block btn-default dropdown-toggle" id="wgo-filter">
				<option value="" default selected>{!! trans('educationlevels/wgo.default') !!}</option>
				<option value="all">{!! trans('educationlevels/wgo.all') !!}</option>
				<option value="section1">{!! trans('educationlevels/wgo.kinder-n') !!}</option>
				<option value="section2">{!! trans('educationlevels/wgo.kinder-o') !!}</option>
				<option value="section3">{!! trans('educationlevels/wgo.primary-n') !!}</option>
				<option value="section4">{!! trans('educationlevels/wgo.primary-o') !!}</option>
				<option value="section5">{!! trans('educationlevels/wgo.secondary-n') !!}</option>
				<option value="section6">{!! trans('educationlevels/wgo.secondary-o') !!}</option>
				<option value="section7">{!! trans('educationlevels/wgo.institutional') !!}</option>
				<option value="section8">{!! trans('educationlevels/wgo.beyond') !!}</option>
			</select>
		</div>
	</div>

	<div class="row">
		<div class="wgo-grid" id="wgo-grid">
			@include ('wgo/wgoblocks/kpson_Kermesse2016')
			@include ('wgo/wgoblocks/ops_Bridging')
			@include ('wgo/wgoblocks/onps2_OMA')
			<!--@include ('wgo/wgoblocks/po_PYPExhibition2016')-->
			@include ('wgo/wgoblocks/wsb_Swimming_Seguridad')
			@include ('wgo/wgoblocks/son_LaCumbre2016')
			@include ('wgo/wgoblocks/kpson_MaratonNordelta')
			@include ('wgo/wgoblocks/wsb_ADEFootball')
			@include ('wgo/wgoblocks/wsb_LittleSports')
			@include ('wgo/wgoblocks/kn_OpenForum')
			@include ('wgo/wgoblocks/po_InterschoolGeneralKnowledge')
			@include ('wgo/wgoblocks/ko_ChangesinNature_Spring')
			<!-- @include ('wgo/wgoblocks/pn_ChoralGathering')
			@include ('wgo/wgoblocks/pn_HealthyLiving')
			@include ('wgo/wgoblocks/pn_ChildrensDay')
			@include ('wgo/wgoblocks/pn_PaintedTshirts')
			@include ('wgo/wgoblocks/pn_CrearConciencia') -->
			@include ('wgo/wgoblocks/ko_ArtExhibition2016')
			@include ('wgo/wgoblocks/kn_4thServiceDay')
			@include ('wgo/wgoblocks/po_NewApps')
			@include ('wgo/wgoblocks/sn_CASexperiences')
			@include ('wgo/wgoblocks/ko_ChildrensDay')
			@include ('wgo/wgoblocks/sn_OutingY11')
			@include ('wgo/wgoblocks/ko_EcoTricycle')
			<!-- @include ('wgo/wgoblocks/pn_AssemblyY3') 
			@include ('wgo/wgoblocks/pn_DiscoveringY4') 
			@include ('wgo/wgoblocks/pn_VegetableGarden') -->
			@include ('wgo/wgoblocks/kindergarten-concert-nordelta')
			@include ('wgo/wgoblocks/peace-pals-international-olivos')
			@include ('wgo/wgoblocks/year-5-visits-FARGO-olivos')
			@include ('wgo/wgoblocks/teatro-colon-olivos')
			<!-- @include ('wgo/wgoblocks/pn_ProductiveCircuits') -->
			@include ('wgo/wgoblocks/po_iPadShowcase_y5y6')
			@include ('wgo/wgoblocks/po_JJOO_y6')
			@include ('wgo/wgoblocks/po_TreeDay')
			@include ('wgo/wgoblocks/so_InterTOK')
			@include ('wgo/wgoblocks/bnw_UniversityCounselor')
			@include ('wgo/wgoblocks/kn_SanMartin2016')
			@include ('wgo/wgoblocks/ko_Concert2016')
			@include ('wgo/wgoblocks/onps_OMA')
			<!-- @include ('wgo/wgoblocks/pn_AncientCivilizations')
			@include ('wgo/wgoblocks/pn_SanMartin') -->
			@include ('wgo/wgoblocks/po_3rdServiceDay')
			@include ('wgo/wgoblocks/so_ESSARP_SoloistFestival')
			@include ('wgo/wgoblocks/son_FutureForum2016')
			@include ('wgo/wgoblocks/son_OldNorthlandersVisit')
			@include ('wgo/wgoblocks/son_RosarioDelTala2016')
			@include ('wgo/wgoblocks/son_Universities')
			@include ('wgo/wgoblocks/kn_3rdServiceDay')
			@include ('wgo/wgoblocks/kn_ChildrensDay')
			@include ('wgo/wgoblocks/ko_UnstructuredMaterial')
			@include ('wgo/wgoblocks/kpson_2016ChacoClothesFair')
			@include ('wgo/wgoblocks/onp_OMA_MDQ')
			@include ('wgo/wgoblocks/op_ChildrensDay2016')
			@include ('wgo/wgoblocks/po_bicentenario2016')
			@include ('wgo/wgoblocks/pn_ConcertEP2_GoldenDream')
			@include ('wgo/wgoblocks/pn_DiaBandera')
			<!-- @include ('wgo/wgoblocks/pn_OMA_InstanciasIniciales')-->
			@include ('wgo/wgoblocks/kon_KidsOlympicGames')
			@include ('wgo/wgoblocks/po_SanMartin')
			@include ('wgo/wgoblocks/so_MakeAWish')
			@include ('wgo/wgoblocks/son_Chaco2016')
			@include ('wgo/wgoblocks/son_ESSARP_CWC')
			@include ('wgo/wgoblocks/son_IBDP_subjects')
			@include ('wgo/wgoblocks/son_NOA2016')
			@include ('wgo/wgoblocks/son_Robotics')
			@include ('wgo/wgoblocks/po_1stCommendationAssembly')
			@include ('wgo/wgoblocks/bnw_VanesaHarbek')
			@include ('wgo/wgoblocks/ko_MyBody')
			@include ('wgo/wgoblocks/kpson_BicentenarioIndependencia')
			@include ('wgo/wgoblocks/po_BookFestival')
			@include ('wgo/wgoblocks/po_EP2_concert')
			@include ('wgo/wgoblocks/po_FoodRevolutionDay')
			@include ('wgo/wgoblocks/po_iPadShowcase_y2y3')
			@include ('wgo/wgoblocks/po_y4y5_Assembly')
			@include ('wgo/wgoblocks/po_y5_CAEMSE')
			@include ('wgo/wgoblocks/sn_TallerAUDELA')
			@include ('wgo/wgoblocks/son_EMUN')
			@include ('wgo/wgoblocks/kn_2ndServiceDay')
			@include ('wgo/wgoblocks/kn_Camping')
			@include ('wgo/wgoblocks/po_OMA_PrimeraRonda')
			@include ('wgo/wgoblocks/kn-Epoca-Colonial')
			@include ('wgo/wgoblocks/Science-in-Kindergarten')
			@include ('wgo/wgoblocks/Second-Service-Day')
			@include ('wgo/wgoblocks/induction-day')
			@include ('wgo/wgoblocks/Creating Community')
			@include ('wgo/wgoblocks/Healthy-Habits')
			@include ('wgo/wgoblocks/Learning-different-languages')
			@include ('wgo/wgoblocks/Our-Senses-Unit-of-Inquiry')	
			@include ('wgo/wgoblocks/International-Certification')	
			@include ('wgo/wgoblocks/CIS-visit')	
			@include ('wgo/wgoblocks/IB-PYP-informative-meeting')
			@include ('wgo/wgoblocks/IB-PYP-professional-development')
			@include ('wgo/wgoblocks/reflection-and-service-nordelta')
			@include ('wgo/wgoblocks/commendation-assembly-nordelta')
			@include ('wgo/wgoblocks/ipad-showcase-nordelta')
			@include ('wgo/wgoblocks/literacy-week-nordelta')
			@include ('wgo/wgoblocks/second-service-day-olivos')
			@include ('wgo/wgoblocks/service-in-action-olivos')
			@include ('wgo/wgoblocks/friendship-day-year4-olivos')
			@include ('wgo/wgoblocks/camping-trip-year5-olivos')
			@include ('wgo/wgoblocks/second-service-day-nordelta')
			@include ('wgo/wgoblocks/drama-festival-nordelta')
			@include ('wgo/wgoblocks/developing-new-skills')
			@include ('wgo/wgoblocks/ability-to-communicate-nordelta')
			@include ('wgo/wgoblocks/to-connect-through-music')
			@include ('wgo/wgoblocks/international-awards-ceremony')
			@include ('wgo/wgoblocks/top-mark-argentina')
			@include ('wgo/wgoblocks/bautista-alonso')
			@include ('wgo/wgoblocks/cristina-rodrigues')
			@include ('wgo/wgoblocks/first-unit-to-inquiry')
			@include ('wgo/wgoblocks/toy-museum-nordelta')
			@include ('wgo/wgoblocks/digital-learners-olivos')
			@include ('wgo/wgoblocks/family-day')
			@include ('wgo/wgoblocks/friendship-day')
			@include ('wgo/wgoblocks/playing-with-unstructured-material')
			@include ('wgo/wgoblocks/FelipeRepresentArgentine')
			@include ('wgo/wgoblocks/ben-walden-at-northlands')
			@include ('wgo/wgoblocks/careers-universities-choice')
			@include ('wgo/wgoblocks/breakfast-with-esteban-bullrich')
			@include ('wgo/wgoblocks/fcd-visit-olivos-nordelta')
			@include ('wgo/wgoblocks/a-passion-for-skiing')
			@include ('wgo/wgoblocks/global-collaboration-campaign-olivos')
			@include ('wgo/wgoblocks/to-learn-together-may2016-year2-olivos')
			@include ('wgo/wgoblocks/drama-festival-april2016-olivos')
			@include ('wgo/wgoblocks/ib-americas-regional-workshops-institutional')
			@include ('wgo/wgoblocks/first-service-day-nordelta')
			@include ('wgo/wgoblocks/art-project-monstrosities-olivos')
			@include ('wgo/wgoblocks/world-health-day-olivos')
			@include ('wgo/wgoblocks/25-anniversary-chaco-project-celebration')
			@include ('wgo/wgoblocks/professional-development-olivos-nordelta')
			@include ('wgo/wgoblocks/founders-day-kindergarten-secondary')
			@include ('wgo/wgoblocks/house-convivencia-nordelta')
			@include ('wgo/wgoblocks/a-superb-storyteller-olivos-nordelta')
			@include ('wgo/wgoblocks/chaco-2015-assembly-olivos')
			@include ('wgo/wgoblocks/areas-of-knowledge-olivos')
			@include ('wgo/wgoblocks/founders-day-olivos')
			@include ('wgo/wgoblocks/3rd-prize-cis-art-project')
			@include ('wgo/wgoblocks/frankenstein-from-london-olivos')
			@include ('wgo/wgoblocks/strengthening-the-knowledge-olivos-nordelta')
			@include ('wgo/wgoblocks/performing-arts-ny-olivos-nordelta')
			@include ('wgo/wgoblocks/phasing-period-nordelta')
			@include ('wgo/wgoblocks/phasing-period-olivos')
			@include ('wgo/wgoblocks/breakfast-new-families-olivos-nordelta')
			@include ('wgo/wgoblocks/activities-fair-olivos-nordelta')
			@include ('wgo/wgoblocks/big-brother-big-sister-olivos')
			@include ('wgo/wgoblocks/2015-prize-giving-ceremony-olivos')
			@include ('wgo/wgoblocks/international-exams-results')
			@include ('wgo/wgoblocks/induction-day-olivos-nordelta')
			@include ('wgo/wgoblocks/house-convivencia-secundaria-olivos')
			{{-- @include ('wgo/wgoblocks/2015-prize-giving-ceremony')
			@include ('wgo/wgoblocks/big-brother-big-sister')
			@include ('wgo/wgoblocks/founders-day-primary')
			@include ('wgo/wgoblocks/areas-of-knowledge')
			@include ('wgo/wgoblocks/global-collaboration-campaign')
			@include ('wgo/wgoblocks/to-learn-together')
			@include ('wgo/wgoblocks/induction-day')
			@include ('wgo/wgoblocks/performing-arts-in-new-york')
			@include ('wgo/wgoblocks/house-convivencia-olivos-nordelta')
			@include ('wgo/wgoblocks/a-superb-storyteller')
			@include ('wgo/wgoblocks/house-convivencia-secondary')
			@include ('wgo/wgoblocks/chaco-2015-assembly')
			@include ('wgo/wgoblocks/3rd-prize-cis-art-project')
			@include ('wgo/wgoblocks/strengthening-the-knowledge')
			@include ('wgo/wgoblocks/ability-to-communicate')
			@include ('wgo/wgoblocks/drama-festival') --}}

		</div>
	</div>

@endsection
