{{-- Language set in lang/{language}/educationlevels.php --}}

@extends('layouts.main')
@section('content_class','educationlevels')
@section('content')
	
	<div class="row">
		<div class="col-xs-12 col-md-8">
			<h1>{!! trans('educationlevels.header') !!}</h1>
			<p>{!! trans('educationlevels.intro1') !!}</p>	
			<p>{!! trans('educationlevels.intro2') !!}</p>
		</div>
	</div>
	
	{{-- Versión Mobile --}}
	<div class="row visible-xs-block">
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			<div class="panel panel-default">
				<div class="panel-heading initial" role="tab" id="headingOne">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">{!! trans('educationlevels.level1') !!}
							<br /><span>{!! trans('educationlevels.level1.description') !!}</span>
						</a>
					</h4>
				</div>
				<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
					<a href="#" data-toggle="modal" data-target=".inicial-modal">
						<img class="img-responsive" src="{{ asset('/img/educationlevels/initial.jpg') }}" alt="{!! trans('educationlevels.header') !!}" alt="">
					</a>
					<div class="panel-body">
						<p>{!! trans('educationlevels.level1.text') !!}</p>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading primary" role="tab" id="headingTwo">
					<h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">{!! trans('educationlevels.level2') !!}
							<br /><span>{!! trans('educationlevels.level2.description') !!}</span>
						</a>
					</h4>
				</div>
				<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
					<a href="#" data-toggle="modal" data-target=".primary-modal">
						<img class="img-responsive" src="{{ asset('/img/educationlevels/primary.jpg') }}" alt="{!! trans('educationlevels.header') !!}" alt="">
					</a>

					<div class="panel-body">
						<p>{!! trans('educationlevels.level2.text') !!}</p>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading secondary" role="tab" id="headingThree">
					<h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">{!! trans('educationlevels.level3') !!}
							<br /><span>{!! trans('educationlevels.level3.description') !!}</span>
						</a>
					</h4>
				</div>
				<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
					<a href="#" data-toggle="modal" data-target=".secondary-modal">
						<img class="img-responsive" src="{{ asset('/img/educationlevels/secondary.jpg') }}" alt="{!! trans('educationlevels.header') !!}" alt="">
					</a>

					<div class="panel-body">
						<p>{!! trans('educationlevels.level3.text') !!}</p>
					</div>
				</div>
			</div>
		</div>
		
		<h3 class="edlevels-heading">{!! trans('educationlevels.proyect-title') !!}</h3>
		@if (App::getLocale() == 'en')
		<img src="{{ asset('/img/educationlevels/edlevels-mobile-en.svg') }}" alt="{!! trans('educationlevels.header') !!}">
		@else
		<img src="{{ asset('/img/educationlevels/edlevels-mobile.svg') }}" alt="{!! trans('educationlevels.header') !!}">
		@endif
	</div>

	{{-- Versión desktop --}}
	<div class="hidden-xs">
		@if (App::getLocale() == 'en')
			<div class="section" style="background-image: url('css/img/bg-educationlevels-sections-en.svg');">
		@else
			<div class="section" style="background-image: url('css/img/bg-educationlevels-sections.svg');">
		@endif

			@if (App::getLocale() == 'en')
			<div class="row initial" style="background-image: url('css/img/bg-educationlevels-initial-en.svg');">
			@else
			<div class="row initial" style="background-image: url('css/img/bg-educationlevels-initial.svg');">
			@endif
				<div class="section-inner">
					<div class="col-xs-4">
						<div class="row">
							<div class="img-container">
								<a href="#" data-toggle="modal" data-target=".inicial-modal">
									<img class="img-responsive" src="{{ asset('/img/educationlevels/initial.jpg') }}" alt="{!! trans('educationlevels.header') !!}" alt="">
								</a>
							</div>
						</div>
					</div>
					<div class="col-xs-5">
						<div class="info">
							<h3>{!! trans('educationlevels.level1') !!}<br />
							<small>{!! trans('educationlevels.level1.description') !!}</small></h3>
							<p>{!! trans('educationlevels.level1.text') !!}</p>
						</div>
					</div>
				</div>
			</div>
			
						
			@if (App::getLocale() == 'en')
			<div class="row primary" style="background-image: url('css/img/bg-educationlevels-primary-en.svg');">
			@else
			<div class="row primary" style="background-image: url('css/img/bg-educationlevels-primary.svg');">
			@endif
				<div class="section-inner">
					<div class="col-xs-4">
						<div class="row">
							<div class="img-container">
								<a href="#" data-toggle="modal" data-target=".primary-modal">
									<img class="img-responsive" src="{{ asset('/img/educationlevels/primary.jpg') }}" alt="{!! trans('educationlevels.header') !!}" alt="">
								</a>
							</div>
						</div>
					</div>
					<div class="col-xs-5">
						<div class="info">
							<h3>{!! trans('educationlevels.level2') !!}<br />
							<small>{!! trans('educationlevels.level2.description') !!}</small></h3>
							<p>{!! trans('educationlevels.level2.text') !!}</p>
						</div>
					</div>
				</div>
			</div>
			
			@if (App::getLocale() == 'en')
			<div class="row secondary" style="background-image: url('css/img/bg-educationlevels-secondary-en.svg');">
			@else
			<div class="row secondary" style="background-image: url('css/img/bg-educationlevels-secondary.svg');">
			@endif
				<div class="section-inner">
					<div class="col-xs-4">
						<div class="row">
							<div class="img-container">
								<a href="#" data-toggle="modal" data-target=".secondary-modal">
									<img class="img-responsive" src="{{ asset('/img/educationlevels/secondary.jpg') }}" alt="{!! trans('educationlevels.header') !!}" alt="">
								</a>
							</div>
						</div>
					</div>
					<div class="col-xs-5">
						<div class="info">
							<h3>{!! trans('educationlevels.level3') !!}<br />
							<small>{!! trans('educationlevels.level3.description') !!}</small></h3>
							<p>{!! trans('educationlevels.level3.text') !!}</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	{{-- Incluyo barra de navegación inferior --}}
	@include('educationlevels/nav')

	{{-- Modal initial --}}
	<div class="modal fade inicial-modal" tabindex="-1" role="dialog" aria-labelledby="modal-inicial">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<video autoplay controls preload style="width: 100%">
                    <source src="{{ asset('./video/inicial.mp4')}}" type="video/mp4">
                </video>
			</div>
		</div>
	</div>

	{{-- Modal primary --}}
	<div class="modal fade primary-modal" tabindex="-1" role="dialog" aria-labelledby="modal-primary">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<video autoplay controls preload style="width: 100%">
                    <source src="{{ asset('./video/primaria.mp4')}}" type="video/mp4">
                </video>
			</div>
		</div>
	</div>

	{{-- Modal secondary --}}
	<div class="modal fade secondary-modal" tabindex="-1" role="dialog" aria-labelledby="modal-secondary">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<video autoplay controls preload style="width: 100%">
                    <source src="{{ asset('./video/secundaria.mp4')}}" type="video/mp4">
                </video>
			</div>
		</div>
	</div>
@endsection
