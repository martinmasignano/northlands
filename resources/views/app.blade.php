{{-- Este es el archivo principal de la aplicación. 
Aquí se cargan todas las secciones y subsecciones. --}}

<!DOCTYPE html>
<html lang="en">
    {{-- Title, metatags y llamadas a las hojas de estilo. --}}
    @include('components/head')

    <body class="@yield('content_class')">
        {{-- Barra de navegación principal --}}
        @include('components/navbar')

        {{-- Todo el contenido del sitio se carga aquí. 
        Este es el contenedor principal. --}}
        <div class="content">
            @yield('main')
        </div>

        {{-- Sticky footer --}}
        @include('components/footer')
        
        {{-- Scripts necesarios para el correcto funcionamiento del sitio. --}}
        @include('components/scripts')
    </body>
</html>
