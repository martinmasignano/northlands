{{-- Home. Aquí se hacen las llamadas a las distintas secciones que la conforman --}}

{{-- Todo el contenido se incluye por medio de la función @yield('content') en layouts/main.blade.php --}}
@extends('layouts.main')

{{-- Ubico los elementos de la sección --}}
@section('content')
	{{-- Agrego la clase 'home' al body --}}
	@section('content_class','home')
	
	{{--Estructura de la home --}}
	<div class="row">	
		@include ('home/categories')
	</div>
	<div class="row">
		@include ('home/oneschool')

		@include ('home/faculty')

		@include ('home/building')

		@include ('home/affiliations')
	</div>

@stop
