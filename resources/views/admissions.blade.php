{{-- Language set in lang/{language}/admissions.php --}}

@extends('layouts.articlecolumns')

@section('content_class','admissions article')

@section('sidebar')
	<div class="visible-xs-block">
		<img src="css/img/admisiones_land.jpg" alt="" class="pull-right">
	</div>
	<div class="hidden-xs">
		<img src="css/img/admisiones_port.jpg" alt="" class="pull-right">
	</div>
@endsection

@section('content')
	
	<h1>{!! trans('admissions.header') !!}</h1>
	
	<div class="row">
		<div class="col-sm-6">
			<p>{!! trans('admissions.text1') !!}</p>
			<p>{!! trans('admissions.text2') !!}</p>

			<h2>{!! trans('admissions.schedule') !!}</h2>

			<p>{!! trans('admissions.text3') !!}</p>

			<ul>
				<li>{!! trans('admissions.date1') !!}</li>
				<li>{!! trans('admissions.date2') !!}</li>
			</ul>

			<h2>{!! trans('admissions.priority') !!}</h2>

			<p>{!! trans('admissions.text4') !!}</p>

			<ol>
				<li>{!! trans('admissions.priority1') !!}</li>
				<li>{!! trans('admissions.priority2') !!}</li>
			</ol>
		</div>

		<div class="col-sm-6">
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-branch initial">
					<div class="row">
						<div class="panel-heading" role="tab" id="headingOne">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">{!! trans('admissions.accordiontitle1') !!}</a>
							</h4>
						</div>
						<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body">
								<p>{!! trans('admissions.process.step1') !!} <a href="http://www.northlands.org.ar/admisionespub/frmContacto.aspx" target="_blank">{!! trans('admissions.process.step1.link') !!}</a></p>
								<p>{!! trans('admissions.process.step2a') !!} <a href="http://www.northlands.org.ar/admisiones/frmAdmisiones.aspx" target="_blank">{!! trans('admissions.process.step2.link') !!}</a> {!! trans('admissions.process.step2b') !!}</p>

								<ul>
									<li>{!! trans('admissions.process.substep1') !!}</li>
									<li>{!! trans('admissions.process.substep2') !!}</li>
									<li>{!! trans('admissions.process.substep3') !!}</li>
									<li>{!! trans('admissions.process.substep4') !!}</li>
								</ul>
								
								<h3>{!! trans('admissions.comunication.title') !!}</h3>
								<p>{!! trans('admissions.comunication.text1') !!}</p>
								<p>{!! trans('admissions.comunication.text2') !!}</p>
								
								{{-- Pregunto si es inglés o español y pongo los distintos links según el idioma --}}
								@if (App::getLocale() == 'en')
									<ul>
										<li>
											<a target="_blank" href="http://www.northlands.org.ar/webpublica/admission/handbooks/ManualPadresJardinInfantesNordelta2017.pdf">Kindergarten Nordelta >></a>
										</li>
										<li>
											<a target="_blank" href="http://www.northlands.org.ar/webpublica/admission/handbooks/ManualPadresPrimariaNordelta2017.pdf">Primary Nordelta >></a>
										</li>
										<li>
											<a target="_blank" href="http://www.northlands.org.ar/webpublica/admission/handbooks/ManualPadresSecundariaNordelta2017.pdf">Secondary Nordelta >></a>
										</li>
										<li>
											<a target="_blank" href="http://www.northlands.org.ar/webpublica/admission/handbooks/ManualPadresJardinInfantesOlivos2017.pdf">Kindergarten Olivos >></a>
										</li>
										<li>
											<a target="_blank" href="http://www.northlands.org.ar/webpublica/admission/handbooks/ManualPadresPrimariaOlivos2017.pdf">Primary Olivos >></a>
										</li>
										<li>
											<a target="_blank" href="http://www.northlands.org.ar/webpublica/admission/handbooks/ManualPadresSecundariaOlivos2017.pdf">Secondary Olivos >></a>
										</li>
									</ul>
								@else {{-- Si es español --}}
									<ul>
										<li>
											<a target="_blank" href="http://www.northlands.org.ar/webpublica/admission/handbooks/ManualPadresJardinInfantesNordelta2017.pdf">Nivel Inicial Nordelta >></a>
										</li>
										<li>
											<a target="_blank" href="http://www.northlands.org.ar/webpublica/admission/handbooks/ManualPadresPrimariaNordelta2017.pdf">Primaria Nordelta >></a>
										</li>
										<li>
											<a target="_blank" href="http://www.northlands.org.ar/webpublica/admission/handbooks/ManualPadresSecundariaNordelta2017.pdf">Secundaria Nordelta >></a>
										</li>
										<li>
											<a target="_blank" href="http://www.northlands.org.ar/webpublica/admission/handbooks/ManualPadresJardinInfantesOlivos2017.pdf">Nivel Inicial Olivos >></a>
										</li>
										<li>
											<a target="_blank" href="http://www.northlands.org.ar/webpublica/admission/handbooks/ManualPadresPrimariaOlivos2017.pdf">Primaria Olivos >></a>
										</li>
										<li>
											<a target="_blank" href="http://www.northlands.org.ar/webpublica/admission/handbooks/ManualPadresSecundariaOlivos2017.pdf">Secundaria Olivos >></a>
										</li>
									</ul>
								@endif
							</div>
						</div>
					</div>
				</div>

				<div class="panel panel-branch initial">
					<div class="row">
						<div class="panel-heading" role="tab" id="headingTwo">
							<h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">{!! trans('admissions.accordiontitle2') !!}</a>
							</h4>
						</div>
						<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
							<div class="panel-body">
								<p>{!! trans('admissions.family.text1') !!}</p>
								<p>{!! trans('admissions.family.text2') !!}</p>
								
								<ol>
									<li>{!! trans('admissions.family.information1') !!}</li>
									<li>{!! trans('admissions.family.information2') !!}</li>
								</ol>

								<h3>{!! trans('admissions.documents.title') !!}</h3>
								
								<p>{!! trans('admissions.documents.information1') !!}</p>

								<ol>
									<li>{!! trans('admissions.documents.requisits1') !!}</li>
									<li>{!! trans('admissions.documents.requisits2') !!}</li>
									<li>{!! trans('admissions.documents.requisits3') !!}
										<p>{!! trans('admissions.documents.information2') !!}</p>
										<ul>
											<li>{!! trans('admissions.documents.information.text1') !!}</li>
											<li>{!! trans('admissions.documents.information.text2') !!}</li>
											<li>{!! trans('admissions.documents.information.text3') !!}</li>
										</ul>
									</li>
									<li>{!! trans('admissions.documents.requisits4') !!}</li>
								</ol>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection
