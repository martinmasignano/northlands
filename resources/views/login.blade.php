{{-- Language set in lang/{language}/educationlevels.php --}}

@extends('layouts.main')

@section('content')

    <div class="row">
    	<div class="container">
		    <h1>{!! trans('login.header') !!}</h1>
		    <p>{!! trans('login.content') !!}</p>
    	</div>	
	</div>

@endsection