{{-- Language set in lang/{language}/categories/global-citizenship.php --}}

@extends('layouts.categories')

@section('content_class','categories global-citizenship')

@section('page-header')
	<div class="col-md-4">
		<h1>{!! trans('categories/global-citizenship.header') !!}</h1>
	</div>
	<div class="col-md-8">
		<p class="lead">{!! trans('categories/global-citizenship.lead') !!}</p>
	</div>
@endsection
    	
@section('content') 
	<div class="hidden-xs hidden-sm col-md-4">
		<div class="row">
    		<div class="thumbnail">
				{!! Html::image('img/articles/global-citizenship/gc-front.png', '', array('class' => 'img-responsive')) !!}
			</div>
		</div>
	</div>
	
	<div class="col-md-8">
		<div class="row">
			<div class="col-xs-12 col-sm-6">
	    		<div class="thumbnail">
					<a href="{{ url('articles/categories/global-citizenship/culturas-tradicionales-y-contextos') }}">
						{!! Html::image('img/articles/global-citizenship/gc-thumb-article1.png', 'Global Citizenship', array('class' => 'img-responsive')) !!}
					</a>
					<div class="caption">
						<a href="{{ url('articles/categories/global-citizenship/culturas-tradicionales-y-contextos') }}">
							<h3>{!! trans('categories/global-citizenship.caption-heading-1') !!}</h3>
						</a>
						<p>{!! trans('categories/global-citizenship.caption-text-1') !!}
							<a href="{{ url('articles/categories/global-citizenship/culturas-tradicionales-y-contextos') }}">+info</a>
						</p>
					</div>
				</div>
	    	</div>

	    	<div class="col-xs-12 col-sm-6">
	    		<div class="thumbnail">
					<a href="{{ url('articles/categories/global-citizenship/three-words-to-the-word') }}">
						{!! Html::image('img/articles/global-citizenship/gc-thumb-article2.png', 'Global Citizenship', array('class' => 'img-responsive')) !!}
					</a>
					<div class="caption">
						<a href="{{ url('articles/categories/global-citizenship/three-words-to-the-word') }}">
							<h3>{!! trans('categories/global-citizenship.caption-heading-2') !!}</h3>
						</a>
						<p>{!! trans('categories/global-citizenship.caption-text-2') !!}
							<a href="{{ url('articles/categories/global-citizenship/three-words-to-the-word') }}">+info</a>
						</p>
					</div>
				</div>
	    	</div>

	    	<div class="clearfix"></div>

	    	<div class="col-xs-12 col-sm-6">
	    		<div class="thumbnail">
					<a href="{{ url('articles/categories/global-citizenship/the-international-dimension') }}">
						{!! Html::image('img/articles/global-citizenship/international.jpg', 'Global Citizenship', array('class' => 'img-responsive')) !!}
					</a>
					<div class="caption">
						<a href="{{ url('articles/categories/global-citizenship/the-international-dimension') }}">
							<h3>{!! trans('categories/global-citizenship.caption-heading-3') !!}</h3>
						</a>
						<p>{!! trans('categories/global-citizenship.caption-text-3') !!}
							<a href="{{ url('articles/categories/global-citizenship/the-international-dimension') }}">+info</a>
						</p>
					</div>
				</div>
	    	</div>

	    	<div class="col-xs-12 col-sm-6">
	    		<div class="thumbnail">
					<a href="{{ url('articles/categories/global-citizenship/en-primera-persona') }}">
						{!! Html::image('img/articles/global-citizenship/gc-thumb-article4.png', 'Global Citizenship', array('class' => 'img-responsive')) !!}
					</a>
					<div class="caption">
						<a href="{{ url('articles/categories/global-citizenship/en-primera-persona') }}">
							<h3>{!! trans('categories/global-citizenship.caption-heading-4') !!}</h3>
						</a>
						<p>{!! trans('categories/global-citizenship.caption-text-4') !!}
							<a href="{{ url('articles/categories/global-citizenship/en-primera-persona') }}">+info</a>
						</p>
					</div>
				</div>
	    	</div>	

	    	<div class="clearfix"></div>

	    	<div class="col-xs-12 col-sm-6">
	    		<div class="thumbnail">
					<a href="{{ url('articles/categories/global-citizenship/the-golden-rule') }}">
						{!! Html::image('img/articles/global-citizenship/gc-thumb-article5.png', 'Global Citizenship', array('class' => 'img-responsive')) !!}
					</a>
					<div class="caption">
						<a href="{{ url('articles/categories/global-citizenship/the-golden-rule') }}">
							<h3>{!! trans('categories/global-citizenship.caption-heading-5') !!}</h3>
						</a>
						<p>{!! trans('categories/global-citizenship.caption-text-5') !!}
							<a href="{{ url('articles/categories/global-citizenship/the-golden-rule') }}">+info</a>
						</p>
					</div>
				</div>
	    	</div>

	    	<div class="col-xs-12 col-sm-6">
	    		<div class="thumbnail">
					<a href="{{ url('articles/categories/global-citizenship/conciencia-ecologica') }}">
						{!! Html::image('img/articles/global-citizenship/gc-thumb-article6.png', 'Global Citizenship', array('class' => 'img-responsive')) !!}
					</a>
					<div class="caption">
						<a href="{{ url('articles/categories/global-citizenship/conciencia-ecologica') }}">
							<h3>{!! trans('categories/global-citizenship.caption-heading-6') !!}</h3>
						</a>
						<p>{!! trans('categories/global-citizenship.caption-text-6') !!}
							<a href="{{ url('articles/categories/global-citizenship/conciencia-ecologica') }}">+info</a>
						</p>
					</div>
				</div>
	    	</div>

	    	<div class="clearfix"></div>
		</div>
	</div>
@endsection
