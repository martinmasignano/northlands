{{-- Language set in lang/{language}/categories/character-building.php --}}

@extends('layouts.categories')

@section('content_class','categories character-building')

@section('page-header')
	<div class="col-md-4">
		<h1>{!! trans('categories/character-building.header') !!}</h1>
	</div>
	<div class="col-md-8">
		<p class="lead">{!! trans('categories/character-building.lead') !!}</p>
	</div>
@endsection

@section('content')   
	<div class="hidden-xs hidden-sm col-md-4">
		<div class="row">
    		<div class="thumbnail">
				{!! Html::image('img/articles/character-building/objectivePSE2016.jpg', 'Character Building', array('class' => 'img-responsive')) !!}
			</div>
		</div>
	</div>
	
	<div class="col-md-8">
		<div class="row">
			<div class="col-xs-12 col-sm-6">
	    		<div class="thumbnail">
	    			<a href="{{ url('articles/categories/character-building/acompanar-el-cambio') }}">
						{!! Html::image('img/articles/character-building/cb-thumb-article1.png', 'Character Building', array('class' => 'img-responsive')) !!}
					</a>
					<div class="caption">
						<a href="{{ url('articles/categories/character-building/acompanar-el-cambio') }}">
							<h3>{!! trans('categories/character-building.caption-heading-1') !!}</h3>
						</a>
						<p>{!! trans('categories/character-building.caption-text-1') !!}
							<a href="{{ url('articles/categories/character-building/acompanar-el-cambio') }}">+info</a>
						</p>
					</div>
				</div>
	    	</div>

	    	<div class="col-xs-12 col-sm-6">
	    		<div class="thumbnail">
					<a href="{{ url('articles/categories/character-building/construir-otros-vinculos') }}">
						{!! Html::image('img/articles/character-building/cb-thumb-article2.png', 'Character Building', array('class' => 'img-responsive')) !!}
					</a>
					<div class="caption">
						<a href="{{ url('articles/categories/character-building/construir-otros-vinculos') }}">
							<h3>{!! trans('categories/character-building.caption-heading-2') !!}</h3>
						</a>
						<p>{!! trans('categories/character-building.caption-text-2') !!}
							<a href="{{ url('articles/categories/character-building/construir-otros-vinculos') }}">+info</a>
						</p>
					</div>
				</div>
	    	</div>

	    	<div class="clearfix"></div>
			
			<div class="col-xs-12 col-sm-6">
	    		<div class="thumbnail">
					<a href="{{ url('articles/categories/character-building/diferente-a-mi') }}">
						{!! Html::image('img/articles/character-building/cb-thumb-article3.png', 'Character Building', array('class' => 'img-responsive')) !!}
					</a>
					<div class="caption">
						<a href="{{ url('articles/categories/character-building/diferente-a-mi') }}">
							<h3>{!! trans('categories/character-building.caption-heading-3') !!}</h3>
						</a>
						<p>{!! trans('categories/character-building.caption-text-3') !!}
							<a href="{{ url('articles/categories/character-building/diferente-a-mi') }}">+info</a>
						</p>
					</div>
				</div>
	    	</div>

	    	<div class="col-xs-12 col-sm-6">
	    		<div class="thumbnail">
					<a href="{{ url('articles/categories/character-building/arts-and-social-issues') }}">
						{!! Html::image('img/articles/character-building/cb-thumb-article4.png', 'Character Building', array('class' => 'img-responsive')) !!}
					</a>
					<div class="caption">
						<a href="{{ url('articles/categories/character-building/arts-and-social-issues') }}">
							<h3>{!! trans('categories/character-building.caption-heading-4') !!}</h3>
						</a>
						<p>{!! trans('categories/character-building.caption-text-4') !!}
							<a href="{{ url('articles/categories/character-building/arts-and-social-issues') }}">+info</a>
						</p>
					</div>
				</div>
	    	</div>

	    	<div class="clearfix"></div>
		</div>
	</div>
@endsection
