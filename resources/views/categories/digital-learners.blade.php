{{-- Language set in lang/{language}/categories/digital-learners.php --}}

@extends('layouts.categories')

@section('content_class','categories digital-learners')

@section('page-header')
	<div class="col-md-4">
		<h1>{!! trans('categories/digital-learners.header') !!}</h1>
	</div>
	<div class="col-md-8">
		<p class="lead">{!! trans('categories/digital-learners.lead') !!}</p>
	</div>
@endsection

@section('content')     	
	<div class="hidden-xs hidden-sm col-md-4">
		<div class="row">
			<div class="thumbnail">
				<iframe src="https://player.vimeo.com/video/181958556" width="100%" height="240" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			</div>
		</div>
	</div>

	<div class="col-md-8">
		<div class="row">
			<div class="col-xs-12 col-sm-6">
	    		<div class="thumbnail">
					<a href="{{ url('articles/categories/digital-learners/vanguardia-educacion-progresista') }}">
						<img src="{{ asset('img/articles/digital-learners/vanguardia_thumb.jpg') }}" alt="" class="img-responsive">
					</a>
					<div class="caption">
						<a href="{{ url('articles/categories/digital-learners/vanguardia-educacion-progresista') }}">
							<h3>{!! trans('categories/digital-learners.caption-heading-1') !!}</h3>
						</a>
						<p>{!! trans('categories/digital-learners.caption-text-1') !!}
							<a href="{{ url('articles/categories/digital-learners/vanguardia-educacion-progresista') }}">+info</a>
						</p>
					</div>
				</div>
	    	</div>

	    	<div class="col-xs-12 col-sm-6">
	    		<div class="thumbnail">
					<a href="{{ url('articles/categories/digital-learners/you-too-express') }}">
						<img src="{{ asset('img/articles/digital-learners/youtoo_thumb.jpg') }}" alt="" class="img-responsive">
					</a>
					<div class="caption">
						<a href="{{ url('articles/categories/digital-learners/you-too-express') }}">
							<h3>{!! trans('categories/digital-learners.caption-heading-2') !!}</h3>
						</a>
						<p>{!! trans('categories/digital-learners.caption-text-2') !!}
							<a href="{{ url('articles/categories/digital-learners/you-too-express') }}">+info</a>
						</p>
					</div>
				</div>
	    	</div>

	    	<div class="clearfix"></div>
			
			<div class="col-xs-12 col-sm-6">
	    		<div class="thumbnail">
					<a href="{{ url('articles/categories/digital-learners/changing-the-way-we-teach-and-learn') }}">
						<img src="{{ asset('img/articles/digital-learners/changing_thumb.jpg') }}" alt="" class="img-responsive">
					</a>
					<div class="caption">
						<a href="{{ url('articles/categories/digital-learners/changing-the-way-we-teach-and-learn') }}">
							<h3>{!! trans('categories/digital-learners.caption-heading-3') !!}</h3>
						</a>
						<p>{!! trans('categories/digital-learners.caption-text-3') !!}
							<a href="{{ url('articles/categories/digital-learners/changing-the-way-we-teach-and-learn') }}">+info</a>
						</p>
					</div>
				</div>
	    	</div>

	    	<div class="col-xs-12 col-sm-6">
	    		<div class="thumbnail">
					<a href="{{ url('articles/categories/digital-learners/el-modelo-samr') }}">
						<img src="{{ asset('img/articles/digital-learners/samr_thumb.jpg') }}" alt="" class="img-responsive">
					</a>
					<div class="caption">
						<a href="{{ url('articles/categories/digital-learners/el-modelo-samr') }}">
							<h3>{!! trans('categories/digital-learners.caption-heading-4') !!}</h3>
						</a>
						<p>{!! trans('categories/digital-learners.caption-text-4') !!}
							<a href="{{ url('articles/categories/digital-learners/el-modelo-samr') }}">+info</a>
						</p>
					</div>
				</div>
	    	</div>

		</div>
	</div>
	<img class="hidden-xs hidden-sm absolute-bottom" src="{{ asset('img/articles/digital-learners/d-learning.png') }}" alt="">
@endsection
