{{-- Language set in lang/{language}/categories/global-citizenship.php --}}

@extends('layouts.categories')

@section('content_class','categories healthy-lifestyle')

@section('page-header')
	<div class="col-md-4">
		<h1>{!! trans('categories/healthy-lifestyle.header') !!}</h1>
	</div>
	<div class="col-md-8">
		<div class="row">
			<p class="lead">{!! trans('categories/healthy-lifestyle.lead') !!}</p>
		</div>
	</div>
@endsection
    	
@section('content') 
	<div class="hidden-xs hidden-sm col-md-4">
		<div class="row">
			<div class="thumbnail">
				{!! Html::image('img/articles/healthy-lifestyle/hl-front.png', 'Healthy Lifestyle', array('class' => '')) !!}
			</div>
		</div>
	</div>
	
	<div class="col-md-8">
		<div class="row">
			<div class="col-xs-12 col-sm-6">
	    		<div class="thumbnail">
					<a href="{{ url('articles/categories/healthy-lifestyle/educacion-preventiva') }}">
						{!! Html::image('img/articles/healthy-lifestyle/hl-thumb-article1.png', 'Healthy Lifestyle', array('class' => 'img-responsive')) !!}
					</a>
					<div class="caption">
						<a href="{{ url('articles/categories/healthy-lifestyle/educacion-preventiva') }}">
							<h3>{!! trans('categories/healthy-lifestyle.caption-heading-1') !!}</h3>
						</a>
						<p>{!! trans('categories/healthy-lifestyle.caption-text-1') !!}
							<a href="{{ url('articles/categories/healthy-lifestyle/educacion-preventiva') }}">+info</a>
						</p>
					</div>
				</div>
	    	</div>

	    	<div class="col-xs-12 col-sm-6">
	    		<div class="thumbnail">
					<a href="{{ url('articles/categories/healthy-lifestyle/tutoriales-primeros-auxilios') }}">
						{!! Html::image('img/articles/healthy-lifestyle/hl-thumb-article2.png', 'Healthy Lifestyle', array('class' => 'img-responsive')) !!}
					</a>
					<div class="caption">
						<a href="{{ url('articles/categories/healthy-lifestyle/tutoriales-primeros-auxilios') }}">
							<h3>{!! trans('categories/healthy-lifestyle.caption-heading-2') !!}</h3>
						</a>
						<p>{!! trans('categories/healthy-lifestyle.caption-text-2') !!}
							<a href="{{ url('articles/categories/healthy-lifestyle/tutoriales-primeros-auxilios') }}">+info</a>
						</p>
					</div>
				</div>
	    	</div>

	    	<div class="clearfix"></div>
			
			<div class="col-xs-12 col-sm-6">
	    		<div class="thumbnail">
					<a href="{{ url('articles/categories/healthy-lifestyle/construccion-saberes-educacion') }}">
						{!! Html::image('img/articles/healthy-lifestyle/hl-thumb-article3.png', 'Healthy Lifestyle', array('class' => 'img-responsive')) !!}
					</a>
					<div class="caption">
						<a href="{{ url('articles/categories/healthy-lifestyle/construccion-saberes-educacion') }}">
							<h3>{!! trans('categories/healthy-lifestyle.caption-heading-3') !!}</h3>
						</a>
						<p>{!! trans('categories/healthy-lifestyle.caption-text-3') !!}
							<a href="{{ url('articles/categories/healthy-lifestyle/construccion-saberes-educacion') }}">+info</a>
						</p>
					</div>
				</div>
	    	</div>

	    	<div class="col-xs-12 col-sm-6">
	    		<div class="thumbnail">
					<a href="{{ url('articles/categories/healthy-lifestyle/ensenamos-diferente') }}">
						{!! Html::image('img/articles/healthy-lifestyle/hl-thumb-article4.png', 'Healthy Lifestyle', array('class' => 'img-responsive')) !!}
					</a>
					<div class="caption">
						<a href="{{ url('articles/categories/healthy-lifestyle/ensenamos-diferente') }}">
							<h3>{!! trans('categories/healthy-lifestyle.caption-heading-4') !!}</h3>
						</a>
						<p>{!! trans('categories/healthy-lifestyle.caption-text-4') !!}
							<a href="{{ url('articles/categories/healthy-lifestyle/ensenamos-diferente') }}">+info</a>
						</p>
					</div>
				</div>
	    	</div>
		</div>
	</div>
@endsection
