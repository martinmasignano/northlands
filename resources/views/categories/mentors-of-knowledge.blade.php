{{-- Language set in lang/{language}/categories/global-citizenship.php --}}

@extends('layouts.categories')

@section('content_class','categories mentors')

@section('page-header')
	<div class="col-md-4">
		<h1>{!! trans('categories/mentors.header') !!}</h1>
	</div>
	<div class="col-md-8">
		<p class="lead">{!! trans('categories/mentors.lead') !!}</p>
	</div>
@endsection

@section('content') 
	<div class="hidden-xs hidden-sm col-md-4">
		<div class="row">
			<div class="thumbnail">
				<img src="{{ url('img/articles/mentors/mentors-index.svg') }}" alt="">
			</div>
		</div>
	</div>
	
	<div class="col-md-8">
		<div class="row">
	    	<div class="col-xs-12 col-sm-6">
	    		<div class="thumbnail">
					<a href="{{ url('articles/categories/mentors/hacer-visible-el-pensamiento') }}">
						<img class="img-responsive" src="{{ asset('img/articles/mentors/metacognicion_thumb.jpg') }}" alt="Hacer visible el pensamiento">
					</a>
					<div class="caption">
						<a href="{{ url('articles/categories/mentors/hacer-visible-el-pensamiento') }}">
							<h3>{!! trans('categories/mentors.caption-heading-1') !!}</h3>
						</a>
						<p>{!! trans('categories/mentors.caption-text-1') !!}
							<a href="{{ url('articles/categories/mentors/hacer-visible-el-pensamiento') }}">+info</a>
						</p>
					</div>
				</div>
	    	</div>

	    	<div class="col-xs-12 col-sm-6">
	    		<div class="thumbnail">
					<a href="{{ url('articles/categories/mentors/self-expression') }}">
						<img class="img-responsive" src="{{ asset('img/articles/mentors/selfexpressions_thumb.jpg') }}" alt="Self expression">
					</a>
					<div class="caption">
						<a href="{{ url('articles/categories/mentors/self-expression') }}">
							<h3>{!! trans('categories/mentors.caption-heading-2') !!}</h3>
						</a>
						<p>{!! trans('categories/mentors.caption-text-2') !!}
							<a href="{{ url('articles/categories/mentors/self-expression') }}">+info</a>
						</p>
					</div>
				</div>
	    	</div>

	    	<div class="clearfix"></div>

	    	<div class="col-xs-12 col-sm-6">
	    		<div class="thumbnail">
					<a href="{{ url('articles/categories/mentors/english-in-real-context') }}">
						<img class="img-responsive" src="{{ asset('img/articles/mentors/english_thumb.jpg') }}" alt="English in real context">
					</a>
					<div class="caption">
						<a href="{{ url('articles/categories/mentors/english-in-real-context') }}">
							<h3>{!! trans('categories/mentors.caption-heading-3') !!}</h3>
						</a>
						<p>{!! trans('categories/mentors.caption-text-3') !!}
							<a href="{{ url('articles/categories/mentors/english-in-real-context') }}">+info</a>
						</p>
					</div>
				</div>
	    	</div>

	    	<div class="col-xs-12 col-sm-6">
	    		<div class="thumbnail">
					<a href="{{ url('articles/categories/mentors/dont-try-this-at-home') }}">
						<img class="img-responsive" src="{{ asset('img/articles/mentors/donttrythis_thumb.jpg') }}" alt="Dont try this at home">
					</a>
					<div class="caption">
						<a href="{{ url('articles/categories/mentors/dont-try-this-at-home') }}">
							<h3>{!! trans('categories/mentors.caption-heading-4') !!}</h3>
						</a>
						<p>{!! trans('categories/mentors.caption-text-4') !!}
							<a href="{{ url('articles/categories/mentors/dont-try-this-at-home') }}">+info</a>
						</p>
					</div>
				</div>
	    	</div>
			
			<div class="clearfix"></div>

	    	<div class="col-xs-12 col-sm-6">
	    		<div class="thumbnail">
					<a href="{{ url('articles/categories/mentors/matematica-logica-diversion') }}">
						<img class="img-responsive" src="{{ asset('img/articles/mentors/matematica_thumb.jpg') }}" alt="Matemática + Lógica = Diversión?">
					</a>
					<div class="caption">
						<a href="{{ url('articles/categories/mentors/matematica-logica-diversion') }}">
							<h3>{!! trans('categories/mentors.caption-heading-5') !!}</h3>
						</a>
						<p>{!! trans('categories/mentors.caption-text-5') !!}
							<a href="{{ url('articles/categories/mentors/matematica-logica-diversion') }}">+info</a>
						</p>
					</div>
				</div>
	    	</div>

	    	<div class="col-xs-12 col-sm-6">
	    		<div class="thumbnail">
					<a href="{{ url('articles/categories/mentors/too-young-to-learn') }}">
						<img class="img-responsive" src="{{ asset('img/articles/mentors/tooyoungtolearn_thumb.jpg') }}" alt="Too young to learn">
					</a>
					<div class="caption">
						<a href="{{ url('articles/categories/mentors/too-young-to-learn') }}">
							<h3>{!! trans('categories/mentors.caption-heading-6') !!}</h3>
						</a>
						<p>{!! trans('categories/mentors.caption-text-6') !!}
							<a href="{{ url('articles/categories/mentors/to-young-to-learn') }}">+info</a>
						</p>
					</div>
				</div>
	    	</div>

	    	<div class="clearfix"></div>

		</div>
	</div>
			
@endsection
