{{-- Language set in lang/{language}/categories/iblearnes.php --}}

@extends('layouts.categories')

@section('content_class','categories ib-learnes')

@section('page-header')
	<div class="col-md-4">
		<h1>{!! trans('categories/iblearnes.header') !!}</h1>
	</div>
	<div class="col-md-8">
		<p class="lead">{!! trans('categories/iblearnes.lead') !!}</p>
	</div>
@endsection

@section('content')   	
	<div class="hidden-xs hidden-sm col-md-4">
		<div class="row">
			<div class="thumbnail">
				<h3>El perfil de la comunidad IB</h3>
				<iframe src="https://player.vimeo.com/video/66636889" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
				<h3>Connected</h3>
				<iframe src="https://player.vimeo.com/video/66630620" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
				<h3>Qué es la comunidad IB</h3>
				<iframe src="https://player.vimeo.com/video/92728163" width="100%" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			</div>
		</div>
	</div>
	
	<div class="col-md-8">
		<div class="row">
	    	<div class="col-xs-12 col-sm-6">
	    		<div class="thumbnail">
					<a href="{{ url('articles/categories/iblearnes/en-primera-persona') }}">
						{!! Html::image('img/articles/ib-learnes/ib-thumb-article1.png', '', array('class' => 'img-responsive')) !!}
					</a>
					<div class="caption">
						<a href="{{ url('articles/categories/iblearnes/en-primera-persona') }}">
							<h3>{!! trans('categories/iblearnes.caption-heading-1') !!}</h3>
						</a>
						<p>{!! trans('categories/iblearnes.caption-text-1') !!}
							<a href="{{ url('articles/categories/iblearnes/en-primera-persona') }}">+info</a>
						</p>
					</div>
				</div>
	    	</div>

	    	<div class="col-xs-12 col-sm-6">
	    		<div class="thumbnail">
					<a href="{{ url('articles/categories/iblearnes/cierre-de-una-etapa') }}">
						{!! Html::image('img/articles/ib-learnes/ib-thumb-article2.png', 'Empowers IB learnes', array('class' => 'img-responsive')) !!}
					</a>
					<div class="caption">
						<a href="{{ url('articles/categories/iblearnes/cierre-de-una-etapa') }}">
							<h3>{!! trans('categories/iblearnes.caption-heading-2') !!}</h3>
						</a>
						<p>{!! trans('categories/iblearnes.caption-text-2') !!}
							<a href="{{ url('articles/categories/iblearnes/cierre-de-una-etapa') }}">+info</a>
						</p>
					</div>
				</div>
	    	</div>
			
			<div class="clearfix"></div>

	    	<div class="col-xs-12 col-sm-6">
	    		<div class="thumbnail">
					<a href="{{ url('articles/categories/iblearnes/24-puntos-mas') }}">
						{!! Html::image('img/articles/ib-learnes/ib-thumb-article3.png', '', array('class' => 'img-responsive')) !!}
					</a>
					<div class="caption">
						<a href="{{ url('articles/categories/iblearnes/24-puntos-mas') }}">
							<h3>{!! trans('categories/iblearnes.caption-heading-3') !!}</h3>
						</a>
						<p>{!! trans('categories/iblearnes.caption-text-3') !!}
							<a href="{{ url('articles/categories/iblearnes/24-puntos-mas') }}">+info</a>
						</p>
					</div>
				</div>
	    	</div>

	    	<div class="col-xs-12 col-sm-6">
	    		<div class="thumbnail">
	    			<a href="{{ url('articles/categories/iblearnes/mucho-mas-habilidades-academicas') }}">
	    				{!! Html::image('img/articles/ib-learnes/ib-thumb-article4.png', '', array('class' => 'img-responsive')) !!}
	    			</a>
					<div class="caption">
						<a href="{{ url('articles/categories/iblearnes/mucho-mas-habilidades-academicas') }}">
							<h3>{!! trans('categories/iblearnes.caption-heading-4') !!}</h3>
						</a>
						<p>{!! trans('categories/iblearnes.caption-text-4') !!}
							<a href="{{ url('articles/categories/iblearnes/mucho-mas-habilidades-academicas') }}">+info</a>
						</p>
					</div>
				</div>
	    	</div>

	    	<div class="clearfix"></div>
	    </div>		
	</div>
@endsection
