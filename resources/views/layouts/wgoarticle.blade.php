@extends('app')
@section('content_class','wgo-article')

@section('main')
	<div class="row">
		<div class="container-fluid">
			<div class="row">
				{{-- Aquí se cargan los títulos de los WGO --}}
				<div class="col-sm-3">
					<div class="sidebar-article-left">
						@yield('sidebar')
					</div>
				</div>
				
				{{-- Texto, imágenes secundarias, videos --}}
				<div class="col-sm-9">
					<div class="main-article">
						@yield('content')
					</div>
				</div>
			</div>
			<div class="row">
				<div class="galleries-container">
					@yield('gallery')
				</div>
			</div>
		</div>
	</div>
@endsection