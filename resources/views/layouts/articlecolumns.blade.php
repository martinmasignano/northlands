@extends('app')
@section('content_class','article')

@section('main')
	<div class="row">
		<div class="container-fluid">
			<div class="row">
				{{-- Aquí se cargan las imágenes principales de los artículos --}}
				<div class="col-md-4">
					<div class="row">
						<div class="sidebar-article-left">
							@yield('sidebar')
						</div>
					</div>
				</div>
				
				{{-- Texto, imágenes secundarias, videos --}}
				<div class="col-md-8">
					@yield('breadcrumbs')

					<div class="main-article">
						@yield('content')
						@yield('gallery')
						@yield('related')
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection


