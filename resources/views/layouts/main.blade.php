@extends('app')

@section('main')
	<div class="row">
		<div class="container">
			@yield('content')
		</div>
	</div>
@endsection