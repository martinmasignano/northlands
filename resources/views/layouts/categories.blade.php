@extends('app')

@section('main')
	<div class="row">
		<div class="page-header">
			<div class="container">
				<div class="row">
					@yield('page-header')
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row">
				@yield('content')
			</div>
		</div>

		<div class="container">
			@include ('educationlevels/nav')
		</div>
	</div>
@endsection