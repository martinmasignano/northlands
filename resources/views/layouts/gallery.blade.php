@yield('section')
	<div class="thumbnail wgo-thumbnail">
		@yield('image')
		
		@yield('caption')
		<div class="row">
			<div class="place">
				@yield('place')
			</div>
		</div>
	</div>
</section>

