$(document).ready(function() {

	// Hambuger Icon animation
	$('.navbar-toggle').on('click', function () {
	    $(this).toggleClass('active');
  	});

  	// Back to top animation
  	$('.back-to-top').click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 500);
		return false;
	});

	// Home Slider
	$(function() {
        $('#ei-slider').eislideshow({
			animation     : 'center',
			autoplay      : true,
			slideshow_interval  : 3000,
			titlesFactor    : 0
        });
    });

    //Slick slider (para las galerías WGO)
    $('.gallery-slider').slick({
		autoplay: false,
		centerMode: true,
		centerPadding: '220px',
		infinite: true,
		dots: true,
		speed: 500,
		slidesToShow: 1,
		adaptiveHeight: true,
		responsive: 
		[
			{
				breakpoint: 992,
				settings: {
					centerPadding: '60px'
				}
			},
			{
				breakpoint: 768,
				settings: {
					centerPadding: '40px'
				}
			},
			{
			breakpoint: 480,
			settings: {
				centerPadding: '20px'
				}
			}
		]
	});

	//Slick slider para títulos de categorias en home
	$('.cb-slider, .dl-slider, .gc-slider, .hl-slider, .ib-slider, .mentors-slider').slick({
		autoplay: true,
		arrows: false,
		dots: false
	});

	// Select para WGO
	$(function() {
	    $('#wgo-filter').change(function() {
		  var section = $(this).val();

		  if (section === 'all') {
		    $('#wgo-grid > section').fadeIn(450);
		  } else {
		    $('#wgo-grid > section').fadeIn(450);
		    $('#wgo-grid > section').not('.' + section).hide();
		  }
		});
	});
});


