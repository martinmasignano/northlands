<?php

return [

    /*
    |--------------------------------------------------------------------------
    | info Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the info section.
    |
    */

    'header'         =>  'CONTACT US',
    'branch-nordelta'   =>  'NORDELTA SITE',
    'branch-olivos'   =>  'OLIVOS SITE',
    'address' => 'Address: ',
    'phone' => 'Telephone: ',
];
