<?php

return [

    /*
    |--------------------------------------------------------------------------
    | job oportunities Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the job opportunities section.
    |
    */

    'header'            =>  'JOB OPPORTUNITIES',

    'paragraph1'        =>   'NORTHLANDS School actively seeks for innovative, passionate and dedicated members of staff. ',

    'paragraph2'        => 'NORTHLANDS School needs to count on open-minded, versatile, flexible teachers, capable of having a global view of the organization, and of becoming guides for our students, so that the latter may eventually graduate with the necessary tools to become agents of change. ',

    'paragraph3'        => '',
    
    'paragraph4'        => 'We are currently recruiting for the following positions:',

    'joinus-text'       => 'Formá parte de nuestra Comunidad de Aprendizaje',
    'joinus-link'       => 'Ingresá tus datos aquí',

    'nordelta-position1-title' => 'Profesor/a de inglés (SECUNDARIA NORDELTA)',
    'nordelta-position1-text' => '13/06/2016 <br />NORTHLANDS hace extensiva la búsqueda de Profesor/a de inglés para su sede de Secundaria Nordelta. <br />El/la postulante debe contar con título oficial, y experiencia en exámenes IGCSE e idealmente IB. Disponibilidad full time. <br />Los interesados deben enviar CV indicando Referencia PROF.INGL. a <a href="rrhh@northlands.edu.ar">rrhh@northlands.edu.ar</a>', 

    'nordelta-position2-title' => 'Profesor/a bilingüe de Geografía (Secundaria NORDELTA)',
    'nordelta-position2-text' => '26/05/2016 <br />Secundaria Nordelta está en proceso de búsqueda de un/a Profesor/a de Geografía bilingüe. <br />El/la postulante debe tener titulo oficial. <br />Por favor enviar CV indicando Referencia PROF.GEO y pretensiones salariales a: <a href="rrhh@northlands.edu.ar">rrhh@northlands.edu.ar</a>', 

    'nordelta-position3-title' => 'Secretaria/o Pedagógica/o (Primaria NORDELTA)',
    'nordelta-position3-text' => 'texto', 

    'olivos-position1-title' => 'Auxiliar suplente de Inglés turno tarde para Year 6 (Primaria OLIVOS)',
    'olivos-position1-text' => '02/06/2016 <br />Auxiliar suplente de Inglés turno tarde para Year 6. <br />Experiencia previa en los años superiores de Primaria. <br />Disponibilidad de 12. 30 a 16.40 de lunes a viernes. <br />Por favor enviar CV indicando referencia a: <a href="rrhh@northlands.edu.ar">rrhh@northlands.edu.ar</a>', 

    'olivos-position2-title' => 'Profesor de Inglés  (Secundaria OLIVOS)',
    'olivos-position2-text' => 'texto',  
];
