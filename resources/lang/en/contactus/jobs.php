<?php

return [

    /*
    |--------------------------------------------------------------------------
    | job oportunities Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the job opportunities section.
    |
    */

    'header'            =>  'JOB OPPORTUNITIES',

    'paragraph1'        =>   'NORTHLANDS School actively seeks for innovative, passionate and dedicated members of staff. ',

    'paragraph2'        => 'NORTHLANDS School needs to count on open-minded, versatile, flexible teachers, capable of having a global view of the organization, and of becoming guides for our students, so that the latter may eventually graduate with the necessary tools to become agents of change. ',

    'paragraph3'        => '',
    
    'paragraph4'        => 'We are currently recruiting for the following positions:',

    'joinus-text'       => 'Formá parte de nuestra Comunidad de Aprendizaje',
    'joinus-link'       => 'Ingresá tus datos aquí',

    /* AQUÍ VAN LOS TEXTOS DE LOS PUESTOS LABORALES BUSCADOS */

      /* ITteacher NORDELTA */
    'nordelta-ITteacher-title' => 'IT teacher (Kindergarten Nordelta)',
    'nordelta-ITteacher-text' => '<small>7/11/2016</small><br>NORTHLANDS invites applications for the post of IT teacher in Kindergarten Nordelta. <br><br><strong>Requirements </strong><br>
<ul><li>- A dynamic and proactive attitude</li>
<li>- Bilingual (English/Spanish)</li>
<li>- Excellent interpersonal relationship skills</li>
<li>- Experience in kindergarten</li></ul><br>
Please submit your CV to <a href="mailto:rrhh@northlands.edu.ar">rrhh@northlands.edu.ar</a>, indicating INF. as reference.',
    
    /* Head of School */
    'nordelta-Head-of-School-title' => 'Head of School',
    'nordelta-Head-of-School-text' => '<small>28/10/2016</small><br>Reporting to the Board of Governors, the Head of School, as the Instructional Leader and Chief Executive Officer of the school has overall responsibility for the quality of learning and institutional strength. He/she will promote and model the school’s mission, vision and values and goals in all areas of school life so that students are educated in an environment of academic challenge and personal, social and emotional growth in preparation for a lifetime of learning and contribution to the community.<br><br>

<strong>Key Deliverables will be:</strong><br>
<ul><li>- Partner & Envision: Governance, Leadership, and Strategy</li>
<li>- Imagine & Create: Community of life-long learning</li>
<li>- Evolve & Inspire: Institutional Advancement</li>
<li>- Refine & Execute: Recruitment, engagement, and retention</li>
<li>- Supervise & Ensure: Dual campus alignment</li></ul><br>
<strong>Minimum Academic and Professional Qualifications:</strong>
<ul>
<li>- Demonstrated ability to guide the implementation, development and review of the curriculum in line with Accreditation, IB and National Standards;</li>
<li>- Previous success in international school management, as Head of School or Principal and demonstrated Board Governance experience and policy implementation;</li>
<li>- Demonstrated track record of the recruitment of capable and talented teaching staff, as well as staff management and appraisal;</li>
<li>- Inter-culturally sensitive with strong moral character and a willingness to embrace the Argentine culture;</li>
<li>- Proven experience in budget design and control combined with the ability to set objectives, plan strategically and monitor progress;</li>
<li>- Leadership and initiative, demonstrated team-building skills, personal drive and charisma, the ability to inspire confidence and earn the trust of all members of the school community;</li>
<li>- Excellent public relations skills and the ability to develop working relationships with educational authorities and business organisations;</li>
<li>- Adept in the use of technology in an educational setting, combined with a demonstrated support of technology integration across the curriculum;</li>
<li>- Higher Education Degree, M.A./M.Ed./M.Sc. or equivalent Advanced Degree required;</li>
<li>- Fluency in Spanish preferred.</li><br><br>
<strong>Terms and Conditions of the Appointment</strong><br>

An initial, 3-year, renewable contract offers a competitive compensation and benefits package, which is commensurate with experience and the responsibilities of the position.
<br><br><br>
<strong>APPLICATION PROCEDURE</strong>
<br>
CIS is delighted to announce that Northlands has retained CIS, represented by Joe Cornacchio and the CIS Leadership Search team, to lead the search for a Head of School.
<br><br>
Leaders who are interested in this opportunity, should email leadershipsearch@cois.org to request the full Leadership Search Services application procedure.
<br><br>
Candidates who already have an up-to-date leadership portfolio with CIS, should submit a cover letter of application.
<br><br>
If the School receives an outstanding application, the Selection Committee reserves the right to make an appointment before the closing date below.
<br><br><br>
<strong>APPLICATION DEADLINE</strong><br>

The deadline for applications is 11 November 2016.<br><br>

The review of files will begin as soon as applications are received.  Finalists will be selected by the Search Committee as early in the process as possible. Finalists will be invited to Argentina for final interview to meet with the Search Committee, Board and constituent groups of the Northlands community, with the intention of making an appointment shortly thereafter. Northlands operates on a Southern Hemisphere calendar and is open to a negotiated start date for the new Head of School, depending on their availability.',


    /* Librarian NORDELTA */
    'nordelta-librarian-title' => 'Librarian (NORDELTA Site)',
    'nordelta-librarian-text' => '<small>09/09/2016</small><br>Good spoken and written communication skills both in English and Spanish is mandatory.<br>Preferred availability from 12.00 to 16.30hs.<br>The task of a librarian is:<br>Library management<br>Support to staff and students on research both in internet and the bibliography<br>Support the spanish immersion program.<br>Those of you who are interested please send your CV stating BIBL. as the reference to <a href="mailto:rrhh@northlands.edu.ar">rrhh@northlands.edu.ar</a>',


    /* Geography Teacher (Secondary NORDELTA) */
    'nordelta-geography-teacher-title' => 'Geography Teacher (Secondary NORDELTA)',
    'nordelta-geography-teacher-text' => '<small>26/05/2016</small><br>Nordelta Secondary has an opening for a bilingual Geography teacher.<br>The candidate should hold a teaching degree.<br>Applicants can send their CV’s to <a href="mailto:rrhh@northlands.edu.ar">rrhh@northlands.edu.ar</a> stating PROF.GEO. as reference.',

    /* Coordinator of Activities (Sede OLIVOS) */
    'olivos-Coordinator-of-Activities-title' => ' Activities Coordinator (OLIVOS Site)',
    'olivos-Coordinator-of-Activities-text' => '<small>01/11/2016</small><br>NORTHLANDS invites applications for the post of AASH (Activities After School Hours) Coordinator in Olivos.
        Starting date: February 2017<br>
        Timetable: Monday through Friday from 10.30 – 18.00<br>
        The AASH Coordinator is part of Northlands´ teaching staff and as such will be an active member of the School’s community, showing commitment to the School’s values and its comprehensive education programme.<br><br>
        <strong>He/she will be responsible for coordinating all aspects of the AASH programme: </strong>
        <ul><li>- Communication and promotion of activities</li>
        <li>- Organization and management of courses</li> 
        <li>- Coordination with PE and Music departments </li>
        <li>- Communication with families</li>
        <li>- Ongoing program supervision</li></ul>
        <br>
        <strong>Requirements</strong> <br>
        <ul><li>- Efficient organizational skills</li>
        <li>- A dynamic and proactive attitude</li>
        <li>- Good command of the English language</li>
        <li>- Excellent interpersonal relationship skills</li>
        <li>- Proficient with MS Office applications</li></ul><br>

Application deadline: 11/11/16 <br>
Please submit your CV to <a href="mailto:rrhh@northlands.edu.ar">rrhh@northlands.edu.ar</a>, indicating COORD AASH as reference.',

    /* English and Literature Teacher (Secondary OLIVOS) */
    'olivos-english-literature-teacher-title' => 'English and Literature Teacher (Secondary OLIVOS)',
    'olivos-english-literature-teacher-text' => '<small>13/09/2016</small><br>The candidate must possess teaching credentials and prior experience in IGCSE / IB exam preparation.<br>Applicants should send their CV indicating PROF.INGL. in the email subject to <a href="mailto:rrhh@northlands.edu.ar">rrhh@northlands.edu.ar</a>',

    /* Drama Teacher (OLIVOS Site) */
    'olivos-drama-teacher-title' => 'Drama Teacher (OLIVOS Site)',
    'olivos-drama-teacher-text' => '<small>01/09/2016</small><br>The candidate must possess teaching credentials and prior experience in Drama IGCSE exam preparation (theory and practice).<br>Applicants should send their CV indicating PROF.TEATRO in the email subject to <a href="mailto:rrhh@northlands.edu.ar">rrhh@northlands.edu.ar</a>',

    /* Professor of Literature and Language (Secondary OLIVOS) */
    'olivos-literature-language-title' => 'Professor of Literature and Language (Secondary OLIVOS)',
    'olivos-literature-language-text' => 'Professor of Literature in Spanish with Argentine teaching credentials.<br>Must have experience teaching IB and IGCSE Literature and Language & Literature syllabi.<br>Some knowledge of English is preferred.<br>Those interested should send their CV stating PROF.L&L. as the reference to <a href="mailto:rrhh@northlands.edu.ar">rrhh@northlands.edu.ar</a>',
];
