<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Education levels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the Education levels section.
    |
    */

    'header'         =>  'Educational levels',    
    'intro1'         =>  'Our curricular project consolidates its international programme with the mandatory national education programme of the province of Buenos Aires.',
    'intro2'         =>  'Our students receive a comprehensive bilingual education from multiple perspectives in a  caring and learning environment and personal and social development fostering international-mindedness.',
    'proyect-title'  => 'CURRICULAR PROJECT',
    
    'level1'         => 'Kindergarten',
    'level1.description'    => '2 to 5 years old',
    'level1.text'   => 'We provide a healthy learning environment embracing academic, social and emotional well-being to promote the child’s holistic development.  By implementing a curriculum that fosters international-mindedness and builds strong personal values, we encourage curiosity and self-esteem.  Children naturally acquire the necessary skills to understand, develop and express their ideas both in English and Spanish.',
    
    'level2'        => 'Primary',
    'level2.description'    => '6 to 12 years old',
    'level2.text'   => 'A stimulating environment that promotes learning, the development of positive attitudes, confidence and self-discipline. By introducing a comprehensive curriculum focused on inquiry and discovery, students are encouraged to be active, caring and respectful of themselves and others and to participate as responsible citizens in the world around them.',
    
    'level3'        => 'Secondary', 
    'level3.description'    => '13 to 18 years old',
    'level3.text'   => 'A broad and flexible curriculum that caters to young people’s personal consolidation. High-quality education that deepens academic interest and curiosity, international-mindedness and appreciation of culture. Students develop critical thinking, creativity and the ability to work independently. Individual talents and global thinking are stimulated to better understand and participate in the interconnected world we live in.',
];
