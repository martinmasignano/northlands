<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Login Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the Login section.
    |
    */

    'header'         =>  'Login',
    'content' 	     =>	 'Content for Login',
];