<?php

return [

	/*
    |--------------------------------------------------------------------------
    | one school, two sites language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the one school, two sites names.
    |
    */
	
	'header'	=>	'ONE SCHOOL, TWO SITES',
    'olivos'      =>  'OLIVOS SITE',
    'nordelta'      =>  'NORDELTA SITE'
];
