<?php

return [

	/*
    |--------------------------------------------------------------------------
    | faculty language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the faculty names.
    |
    */
	
    'header'    =>  'FACULTY',
	'quote'		=>	'NORTHLANDS selects the best human capital for the personal, physical and academic development of our students.',
	'author'	=>	'Nicholas Reeves, Headmaster',
];
