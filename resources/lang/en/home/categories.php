<?php

return [

    /*
    |--------------------------------------------------------------------------
    | categories language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the categories names.
    |
    */
    
    'header'    =>  'This is the NORTHLANDS way',
    'subheader' =>  'Solid roots in the minds of the future',
    'ibheader'  =>  'EMPOWERS IB LEARNERS',
    'ibtext'    =>  'Online Reading Programme',
    'gcheader'  =>  'GLOBAL CITIZENSHIP',
    'gctext'    =>  'Connecting Classrooms',
    'cbheader'  =>  'CHARACTER BUILDING',
    'cbtext'    =>  'Making new friends',
    'mkheader'  =>  'MENTORS OF KNOWLEDGE',
    'mktext'    =>  'Don´t try this at home',
    'hlheader'  =>  'HEALTHY LIFESTYLE',
    'hltext'    =>  'First Aids tutorials',
    'dlheader'  =>  'DIGITAL LEARNERS',
    'dltext'    =>  'To expand the future of our students',
];
