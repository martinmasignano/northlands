<?php

return [

	/*
    |--------------------------------------------------------------------------
    | building language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the building names.
    |
    */
	
	'header'		=>	'BUILDING OUR HISTORY',
    'title'         =>  'Building our History'
];
