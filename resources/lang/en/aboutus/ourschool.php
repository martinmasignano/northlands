<?php

return [

	/*
    |--------------------------------------------------------------------------
    | Our school language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the Our school names.
    |
    */
	
    'header'                =>  'ONE SCHOOL, TWO SITES',
    'content'               =>  'NORTHLANDS is a non-confessional, coeducational, bilingual, IB World School with two sites situated in Zona Norte, Buenos Aires, Argentina. The School offers a very high standard of education to children between the ages of 2 and 18.',
    'description'           => 'KEY FACTS',
];
