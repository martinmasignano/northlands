<?php

return [

	/*
    |--------------------------------------------------------------------------
    | History language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the History names.
    |
    */
	
    'header'                =>  'BUILDING HISTORY',
    'history1'               =>  'In 2015, we celebrated our 95th Anniversary since the School’s foundation and we have decided to move forward towards our centenary in 2020, in a conscientious manner.',
    'history2'              => 'Building our history entails travelling together along a path of shared stories, friends and colleagues, of lessons learnt from our successes and failures, of feeling proud to be part of something bigger, part of a community that grows and amalgamates under the slogan "Friendship and Service"',
    'history3'              => 'MILESTONES',

    'title1'                => 'Bicentenario 9 de Julio',
    'description1'          => '1816 - 2016',
    
    'title2'                => 'Proyecto D-Learning',
    'description2'          => '2015',
    
    'title3'                => 'Intercultural',
    'description3'          => '2015',
    
    'title4'                => 'Proyecto Chaco',
    'description4'          => '25 años - 2015',
    
    'title5'                => '95 aniversario',
    'description5'          => '2015',

    'title6'                => 'Proyecto Historia',
    'description6'          => 'Alumni',

    'title7'                => '90 aniversario',
    'description7'          => '2010',

    'title8'                => 'Bicentenario 25 de Mayo',
    'description8'          => '1810 - 2010',

];
