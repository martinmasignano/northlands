<?php

return [

	/*
    |--------------------------------------------------------------------------
    | The Northlands way language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the The Northlands way names.
    |
    */
	
	'copy'					=>	'Northlands is a coeducational, bilingual IB World School that, through caring and innovative teaching, educates young people to the full extent of their individual potential. It is a school that values its Anglo-Argentine roots, while respecting all cultures, religions and nationalities. It offers an all-encompassing, warm, cordial and friendly environment which is reflected in its motto: Friendship and Service. Graduates are individuals whose integrity and moral values lead them to choose freely what is right.',
	'header'				=>	'This is the<br />NORTHLANDS way',
    'description-1'         =>  'To this purpose we offer a comprehensive education whereby body, mind and spirit are addressed as one from multiple perspectives.   NORTHLANDS provides  a learning environment for personal development and social education and its bilingual international programme invites us to reflect as global citizens.',
    'description-2'         =>  'The aim of our mission “to educate young people whose moral values lead them to choose freely what is right”, NORTHLANDS develops its education and character development platform interconnecting it with the School’s Personal and Social Education axes, with the values which sustain our culture, the IB learner profile (International Baccalaureate) and the expansion of the habits of mind.',
    
    'axes.title'            =>  'INTERACTION AXES',
    'commitment.title'      =>  'IB LEARNER PROFILE',
    'education.title'       =>  'SOCIAL DEVELOPMENT AXES',
    'social.title'          =>  'Social Axes',
    'personal.title'        =>  'Personal Axes',
    'profile.title'         =>  'IB COMMUNITY LEARNERS PROFILE',
    'habits.title'          =>  'HABITS OF MIND',
];
