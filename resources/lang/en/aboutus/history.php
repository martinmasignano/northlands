<?php

return [

	/*
    |--------------------------------------------------------------------------
    | History language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the History names.
    |
    */
	
	'header'				=>	'BRIEF HISTORY',
	'text1'				=>	'Northlands was founded in 1920 by two visionary Englishwomen, Winifred M. Brightman and Muriel Ivy Slater. It opened its doors on April 1 of that year to 16 students (both boys and girls) and within a short time it had developed a solid reputation.  Three years later, NORTHLANDS stopped receiving boys and became renowned as the best girls’ school in South America.  Its main objectives of stimulating students to achieve a high academic standard, always keeping the personal, social and moral development of each student in mind, became consolidated into the School motto of “Friendship and Service”.',
    'text2'         => 'Miss Brightman was at the head of the School for forty years, until 1961.  Since then, NORTHLANDS has been a non-profit association, Northlands Asociación Civil de Beneficencia, from whose trustees the first Board of Governors was elected.',
    'text3'         => 'As we move towards our centenary, our vision at NORTHLANDS is that we approach teaching, learning and life in the spirit of the International Baccalaureate Organization, encouraging our students to reach their full potential by discovering their talents and preparing them to become committed global citizens.',

	'timeline'				=> 'TIMELINE',
];
