<?php

return [

	/*
    |--------------------------------------------------------------------------
    | authorities language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the authorities names.
    |
    */
	
	'header'				=>	'AUTHORITIES',
	'copy'					=>	'In 1960, Miss Brightman donated the School to a Non-profit Association that watches over the institution’s good management and development.  At present, the ACB (Asociación Civil de Beneficencia) has 75 volunteer Trustees, from whom the members of the Board of Governors are elected for a period of two years, at the end of which, they may be re-elected or are free to resign from their post.',
	'boardtitle'			=>	'BOARD OF GOVERNORS',
	'president'				=>	'Chair',	
	'vice'					=>	'Vice Chair',	
	'treasurer'				=>	'Treasurer',	
	'secretary'				=>	'Secretary',	
	'members'				=>	'Members',	
	'substitutesmembers'	=>	'Alternate Members',
	'reviewer'				=>	'Auditor',	
	'substitutereviewer'	=>	'Alternate Auditor',
	'subtitleAuthorities'	=>	'SCHOOL AUTHORITIES',
	
	'headmasterName'		=>	'Nicholas Reeves',
	'headmaster'			=>	'Headmaster',
	'headmasterDescription'	=>	'Headmaster: Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the standard dummy text of industries since the 1500s, when an unknown printer (N. of T. person who is dedicated to printing) unknown took a galley of type and scrambled so that he managed to make a specimen book. Not only he survived 500 years but also entered as dummy text into electronic documents, remaining essentially unchanged. It was popularized in the 60s with the creation of the leaves "Letraset", which contained Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker, which includes versions of Lorem Ipsum.',

	'vicechancellorName'	=>	'Marisa Perazzo',
	'vicechancellor'		=>	'Deputy Head',
	'vicechancellorDescription'	=>	'Deputy Head: Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the standard dummy text of industries since the 1500s, when an unknown printer (N. of T. person who is dedicated to printing) unknown took a galley of type and scrambled so that he managed to make a specimen book. Not only he survived 500 years but also entered as dummy text into electronic documents, remaining essentially unchanged. It was popularized in the 60s with the creation of the leaves "Letraset", which contained Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker, which includes versions of Lorem Ipsum.',

	'dafName'				=>	'María Eugenia Rodriguez',
	'daf'					=>	'Head of Finance and Adm',
	'dafDescription'	=>	'Director of Administration and finance: Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the standard dummy text of industries since the 1500s, when an unknown printer (N. of T. person who is dedicated to printing) unknown took a galley of type and scrambled so that he managed to make a specimen book. Not only he survived 500 years but also entered as dummy text into electronic documents, remaining essentially unchanged. It was popularized in the 60s with the creation of the leaves "Letraset", which contained Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker, which includes versions of Lorem Ipsum.',

	'devDirectorName'		=>	'Florencia Sackmann Sala',
	'devDirector'			=>	'Head of Development',
	'devDirectorDescription'	=>	'Director of development: Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the standard dummy text of industries since the 1500s, when an unknown printer (N. of T. person who is dedicated to printing) unknown took a galley of type and scrambled so that he managed to make a specimen book. Not only he survived 500 years but also entered as dummy text into electronic documents, remaining essentially unchanged. It was popularized in the 60s with the creation of the leaves "Letraset", which contained Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker, which includes versions of Lorem Ipsum.',

	'sportsDirectorName'	=>	'Jorge Rey',
	'sportsDirector'		=>	'Head of PE and Sports',
	'sportsDirectorDescription'	=>	'Director of physical education and sports: Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the standard dummy text of industries since the 1500s, when an unknown printer (N. of T. person who is dedicated to printing) unknown took a galley of type and scrambled so that he managed to make a specimen book. Not only he survived 500 years but also entered as dummy text into electronic documents, remaining essentially unchanged. It was popularized in the 60s with the creation of the leaves "Letraset", which contained Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker, which includes versions of Lorem Ipsum.',

	'perfArtsName'			=>	'Leandro Valle',
	'perfArts'				=>	'Head of Performing Arts',
	'perfArtsDescription'	=>	'Director of performing arts: Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the standard dummy text of industries since the 1500s, when an unknown printer (N. of T. person who is dedicated to printing) unknown took a galley of type and scrambled so that he managed to make a specimen book. Not only he survived 500 years but also entered as dummy text into electronic documents, remaining essentially unchanged. It was popularized in the 60s with the creation of the leaves "Letraset", which contained Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker, which includes versions of Lorem Ipsum.',

	'kinderNDirectorName'	=>	'Alejandra Batu',
	'kinderNDirector'		=>	'Acting Head of Kindergarten Nordelta',
	'kinderNDirectorDescription'	=>	'Director Kindergarten Nordelta: Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the standard dummy text of industries since the 1500s, when an unknown printer (N. of T. person who is dedicated to printing) unknown took a galley of type and scrambled so that he managed to make a specimen book. Not only he survived 500 years but also entered as dummy text into electronic documents, remaining essentially unchanged. It was popularized in the 60s with the creation of the leaves "Letraset", which contained Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker, which includes versions of Lorem Ipsum.',

	'PrimaryNDirectorName'	=>	'Patricia Christensen de Gilbert',
	'PrimaryNDirector'		=>	'Head of Primary Nordelta',
	'PrimaryNDirectorDescription'	=>	'Director Primary Nordelta: Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the standard dummy text of industries since the 1500s, when an unknown printer (N. of T. person who is dedicated to printing) unknown took a galley of type and scrambled so that he managed to make a specimen book. Not only he survived 500 years but also entered as dummy text into electronic documents, remaining essentially unchanged. It was popularized in the 60s with the creation of the leaves "Letraset", which contained Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker, which includes versions of Lorem Ipsum.',

	'SecondaryNDirectorName'=>	'Shaun Hudson',
	'SecondaryNDirector'	=>	'Head of Secondary Nordelta',
	'SecondaryNDirectorDescription'	=>	'Secondary Nordelta Director: Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the standard dummy text of industries since the 1500s, when an unknown printer (N. of T. person who is dedicated to printing) unknown took a galley of type and scrambled so that he managed to make a specimen book. Not only he survived 500 years but also entered as dummy text into electronic documents, remaining essentially unchanged. It was popularized in the 60s with the creation of the leaves "Letraset", which contained Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker, which includes versions of Lorem Ipsum.',

	'kinderODirectorName'	=>	'Vanesa Henson',
	'kinderODirector'		=>	'Head of Kindergarten Olivos',
	'kinderODirectorDescription'	=>	'Kindergarten Olivos Director: Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the standard dummy text of industries since the 1500s, when an unknown printer (N. of T. person who is dedicated to printing) unknown took a galley of type and scrambled so that he managed to make a specimen book. Not only he survived 500 years but also entered as dummy text into electronic documents, remaining essentially unchanged. It was popularized in the 60s with the creation of the leaves "Letraset", which contained Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker, which includes versions of Lorem Ipsum.',

	'PrimaryODirectorName'	=>	'Adriana García Posadas',
	'PrimaryODirector'		=>	'Deputy Head of Primary Olivos',
	'PrimaryODirectorDescription'	=>	'Primary Olivos Director: Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the standard dummy text of industries since the 1500s, when an unknown printer (N. of T. person who is dedicated to printing) unknown took a galley of type and scrambled so that he managed to make a specimen book. Not only he survived 500 years but also entered as dummy text into electronic documents, remaining essentially unchanged. It was popularized in the 60s with the creation of the leaves "Letraset", which contained Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker, which includes versions of Lorem Ipsum.',

	'SecondaryODirectorName'=>	'Sofia Hughes',
	'SecondaryODirector'	=>	'Head of Secondary Olivos',
	'SecondaryODirectorDescription'	=>	'Secondary Olivos Director: Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the standard dummy text of industries since the 1500s, when an unknown printer (N. of T. person who is dedicated to printing) unknown took a galley of type and scrambled so that he managed to make a specimen book. Not only he survived 500 years but also entered as dummy text into electronic documents, remaining essentially unchanged. It was popularized in the 60s with the creation of the leaves "Letraset", which contained Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker, which includes versions of Lorem Ipsum.'

];
