<?php

return [

	/*
    |--------------------------------------------------------------------------
    | Faculty language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the Faculty names.
    |
    */
	
	'header'				=>	'FACULTY',
	'description1'			=> 	'NORTHLANDS selects the best human capital for our students’ personal, physical and academic development.',
	'description2'			=>	'Our faculty and administrative staff receive regular training to promote personal and professional growth. The training includes workshops on best education practices taught by our own staff as well as international conferences, research trips to other institutions abroad and in-house seminars led by experienced international specialists.',
	'positionteacher1'		=>	'Position',
	'positionteacher2'		=>	'Position',
	'positionteacher3'		=>	'Position',
	'positionteacher4'		=>	'Position',
	'positionteacher5'		=>	'Position',
	'positionteacher6'		=>	'Position',

	'section-title'			=>	'TALKING ABOUT',
];
