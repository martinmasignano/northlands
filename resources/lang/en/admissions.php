<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admissions Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the Admissions section.
    |
    */

    'header'    =>  'ADMISSIONS',

    'text1'     =>  'One of the hardest and most important decisions that a parent has to make is to choose the adequate school for his/her children. Northlands considers that each new student entering the School represents a mutual long-term commitment between the institution, the student and the family. This is a demanding relation for each of the parties, but provides unique and lifelong benefits, both for our students and for their families.',

    'text2'     =>  'To ensure that this commitment may be mutually fulfilled, the School has set up different guidelines for the admission of its students. We hope that through our admission process the applicant families as well as the School may obtain the required information to determine if Northlands is the adequate School for the applicant.',

    'schedule'  =>  'Schedule of Admissions',

    'text3'     =>  'Applications for admission at Northlands will be accepted as from the month of March for the following school year',

    'date1'     =>  'Closing date for Kindergarten: August 15',
    'date2'     =>  'Closing date for Primary and Secondary: October 1',

    'text4'     =>  'Priority in the admission to School is granted, if and when vacancies are available and the applicants pass the admission process, to',

    'priority'  =>  'Priority in the admission',
    'priority1' =>  'The children of School families',
    'priority2' =>  'The children of Old Northlanders',

    'accordiontitle1'   =>  'Admission process',
    'accordiontitle2'   =>  'Foreign families',

    'process.step1'     =>  '1. Complete the information form of the applicant',
    'process.step1.link' => 'CONTACT >>',   
    'process.step2a'     =>  '2. After the informative visit, the family should complete the Application Form',
    'process.step2.link' => 'admission On-line >>', 
    'process.step2b'     =>  'The following documents should sent to the Admissions Office:',

    'process.substep1'  =>  'a. Acceptance of the conditions for the admission process signed',
    'process.substep2'  =>  'b. Letters of introduction of the family',
    'process.substep3'  =>  'c. Copy of the last report',
    'process.substep4'  =>  'The Admission Office will assign the corresponding interviews after the requirements have been completed.',

    'comunication.title'    =>  'Communication of the final decision about admission',
    'comunication.text1'    =>  'The Admissions Office will inform the parents the final result of the admissions process.',
    'comunication.text2'    =>  'Families of applicants who have been admitted should pick up at the School Reception an envelope which contains the requirements for enrolment, including the last page of the PARENTS HANDBOOK SIGNED. >>',

    'family.text1'  =>  'Families coming from abroad must comply with the steps described in the admission process.',
    'family.text2'  =>  'They should bring the following information to the admission interview:',
    
    'family.information1'  =>  'Reports from the current school',
    'family.information2'  =>  '1.  Letter of introduction of the family, which may be from a School family or from an Old Northlander. Should there be no person related to the institution, a letter from a person who can be considered an adequate reference may be submitted (head of the current school). This letter should be sent to the Admissions Office and may not be written by a member of the School Staff or by a direct family member. Submitting the letter is a requirement for initiating the admission process and will, in no case, determine the applicant`s admission.',

    'documents.title'   =>  'Documents required for international families',

    'documents.information1'    =>  'For the registration of new students, the following documents are required:',

    'documents.requisits1'  =>  'Original <strong>Passport </strong> of the student.',
    'documents.requisits2'  =>  'Original <strong>birth certificate </strong> If it is not in Spanish it should be translated',
    'documents.requisits3'  =>  'Original <strong>certificate of previous studies.</strong> In case of students coming from abroad they should present a certificate extended by the authorities of the previous school, bearing school logo, in which they indicate the last year attended and passed by the student with seal and signature of the authorities.',

    'documents.information' =>  'The certificate should be translated into Spanish and legalized by:',
    'documents.information.text1'   =>  'a. the educational authorities of the country in which the courses were taken,',
    'documents.information.text2'   =>  'b. a)  Argentine Consul of the place where the certificate was extended,',
    'documents.information.text3'   =>  'a) In Argentina: Ministry of Foreign Affairs (except in those cases where there is the Hague Apostille)',

    'documents.requisits4'  =>  '<strong>1. Vaccination certificates</strong>',

];
