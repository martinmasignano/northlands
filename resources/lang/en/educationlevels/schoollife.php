<?php

return [

    /*
    |--------------------------------------------------------------------------
    | School life Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the School life section.
    |
    */

    'header'        =>  'SCHOOL LIFE',
    'text'          =>  'The traditions we share and respect at Northlands strengthen the feeling of identification with our roots and with our commitment to educating honorable individuals, whose ethics will lead them to freely choose what is right. The Houses, the Student Council, the Captains, the Prefects and the Head Students are intended to integrate and generate spaces of participation and healthy competence among the students, since they should assume the responsibility of organizing different activities, representing their fellows, leading their groups and setting permanent example for all the students at School.',

    'houses-title'  =>  'HOUSES',
    'houses-text1'  =>  'Upon entering Primary, each student is assigned to a House to which he or she will belong throughout his or her entire school life. Houses are inherited (children of Old Northlanders keep the House of their family member) o assigned by even distribution of the number of members. Students from the different levels at School interact within each House, a fact that allows them a growing range of friends as well as the possibility to develop and take on leadership roles.',
    'houses-text2'  =>  'Interhouse competitions include general knowledge, departmental events, community service, sports and drama, all of which, in addition to academic performance, add points for each House. Points are added at the end of the year and the House with the most points wins and has its name engraved on a shield.',

    'captains-title'    =>  'CAPTAINS',
    'captains-text1'     =>  'The Captain is a student who has excelled in academic, sport and service activities organized by her/his house. The Captains should also have the following qualities: leadership, communication, sense of justice and service.',

    'prefects-title'    =>  'PREFECTS',
    'prefects-text1'    =>  'Some of the Year 12 students are elected as Prefects by their classmates and teachers. These students are the ones who best embody the values of the School and they have earned the respect of the community and constitute a model for the other students. The Prefects are part of the Senior Council and they have certain responsibilities such us working in alliance with the tutors, cooperating with the organization of assemblies and representing the School at public events.',
    'prefects-text2'    =>  'A prefect should have reached a relatively high academic level, be involved in extra curricular activities and be somebody who is committed to the School motto of Friendship and Service.',

    'headstudent-title' =>  'HEAD STUDENT',
    'headstudent-text1' =>  'The Prefect with the largest number of votes becomes Head Girl, pending the approval of the Principal. Among the Head Student´s Duties are: organize the Student Counci, chair regular meetings with the Senior Council and represent the school on formal occasions.',

    'seniorcouncil-title'   =>  'SENIOR COUNCIL',
    'seniorcouncil-text1'   =>  'The Senior Council is composed by the students who have been elected as Prefects, Head Student and Deputy Head Student who conduct monthly meeting with the School authorities to exchange and discuss ideas relevant to the general well being of the School.',

    'studentcouncil-title'  =>  'STUDENT COUNCIL',
    'studentcouncil-text1'  =>  'Students choose one class representative and one deputy representative for the Student Council. The Student Council’s function is to bring forward any doubts, suggestions or ideas to the Heads, promoting responsible social participation.',
];
