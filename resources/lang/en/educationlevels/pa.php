<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Performing arts Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the Performing arts section.
    |
    */

    'header'    =>  'Arts',
    'text1' =>  'In Art class at NORTHLANDS, we guide our students to find alternative and different ways in which to express themselves.',
    'text2' =>  'The initial learning process is based on games to learn and experiment each different discipline.  As students mature, they have the opportunity to explore visual arts, music and drama through a variety of techniques, instruments and cultures.',
    'visual-arts-title' =>  'Visual Arts',
    'dramatic-arts-title' =>  'DRAMA',
    'musical-arts-title' =>  'Musical Arts',
];
