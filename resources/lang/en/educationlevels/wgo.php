<?php

return [

    /*
    |--------------------------------------------------------------------------
    | wgo Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the wgo section.
    |
    */

    'title' => 'What´s going on at NORTHLANDS?',
    'default' => 'FILTER BY',
    'all' => 'EVERYTHING',
    'kinder-n' => 'KINDERGARTEN NORDELTA',
    'kinder-o' => 'KINDERGARTEN OLIVOS',
    'primary-n' => 'PRIMARY NORDELTA',
    'primary-o' => 'PRIMARY OLIVOS',
    'secondary-n' => 'SECONDARY NORDELTA',
    'secondary-o' => 'SECONDARY OLIVOS',
    'institutional' => 'INSTITUTIONAL',
    'beyond' => 'BEYOND NORTHLANDS WALLS',
];
