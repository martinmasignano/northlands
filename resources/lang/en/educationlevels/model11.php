<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Model 1:1 Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the Performing arts section.
    |
    */

    'header'         =>  'Model 1:1',
    'content' 	     =>	 'Content for Model 1:1',
];
