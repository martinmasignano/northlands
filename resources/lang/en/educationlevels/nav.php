<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Education levels nav Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the Education levels nav.
    |
    */

    'pa-link'       =>  'ARTS',
    'ip-link' 	    =>  'INTERNATIONAL PROGRAMMES',
    'pse-link'      =>  'PERSONAL & SOCIAL EDUCATION',
    'aash-link'     =>  'ACTIVITIES',
    'pe-link'       =>  'PHYSICAL EDUCATION',
    'sl-link'       =>  'SCHOOL LIFE',
    '11-link'       =>  'MODEL 1:1'

];
