<?php

return [

    /*
    |--------------------------------------------------------------------------
    | PE Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the PE section.
    |
    */

    'header'         =>  'PE',

    'text1' =>  'Physical education and sports are an essential part of our students’ education.',
    'text2' =>  'They participate in National and International inter-school events which promote a team spirit and healthy competition.  Our proposal’s added value is the conviction that we teach for life, not just for sporting success. We adapt strategies to provide all our students with the possibility of overcoming challenges and connect with their bodies.',
    'text3' =>  'As IB community members, we understand the importance of physical, mental and emotional balance to achieve our own wellbeing and that of others.  With this in mind, we guide our students to develop a balanced lifestyle.',

    'sidebar-heading' =>  'What makes our classes different?',
    'sidebar-text1'  =>  '· The positive stimulus provided so that students enjoy physical activity in class',
    'sidebar-text2'  =>  '· The follow-up conducted on each student to ensure they achieve their full potential.',
    'sidebar-text3'  =>  '· Sport activity analysis and group reflection',
    'sidebar-text4'  =>  '· Self-assessment and peer teaching',
    'sidebar-text5'  =>  '· Interdisciplinary, solidarity and life projects',
    'sidebar-text6'  =>  '· The sense of belonging and commitment to sport teams, School Houses and the School',
    'sidebar-text7'  =>  'Our classes are based on teaching values and metacognition so that students acquire life skills that than can be transferred to all aspects in life and to stand out for the integrity of their values to themselves, to others and the environment.',
];
