<?php

return [

    /*
    |--------------------------------------------------------------------------
    | International programmes Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the International programmes section.
    |
    */

    'header'         =>  'INTERNATIONAL PROGRAMMES',
    'intro' 	     =>	 'We are an IB World School, part of a global community that shares learning experiences and a passion for teaching.  Our teaching staff’s ongoing professional development, allows for qualified and outstanding educators.',
    'ip-header'    => 'INTERNATIONAL AFFILIATIONS',
    'pyp-header' => 'IBO PRIMARY YEARS PROGRAMME (PYP)*',
    'pyp-text' => 'An international education programme in which teachers and students explore in a transdisciplinary way the road to knowledge. Having a central axis based on the development of the child as a whole, this programme proposes an educational framework that is interesting, stimulating, relevant and committed to a structured inquiry that encourages students to act according to new perspectives and knowledge.',
    'igcse-header' => 'CAMBRIDGE INTERNATIONAL GENERAL CERTIFICATE OF SECONDARY EDUCATION (IGCSE)*',
    'igcse-text' => 'An International programme that develops essential educational skills including recall of knowledge, oral skills, problem solving, initiative, team work and investigative skills. It is recognized as the equivalent to the last two years of compulsory education in Great Britain.',
    'ibo-header' => 'IBO DIPLOMA PROGRAMME*',
    'ibo-text' => 'A challenging and stimulating curriculum that is widely recognized by the world’s leading universities. It develops in students the ability to learn how to learn, a strong sense of their own identity and culture, and the ability to communicate with and understand people from other countries and cultures.',
    'disclaimer' => '*Compulsory programmes for all NORTHLANDS students.'

];
