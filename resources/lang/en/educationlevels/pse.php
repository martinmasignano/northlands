<?php

return [

    /*
    |--------------------------------------------------------------------------
    | PSE Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the PSE section.
    |
    */

    'header'         =>  'PSE',
    'content' 	     =>	 'Content for PSE',
];