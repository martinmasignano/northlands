<?php

return [

    /*
    |--------------------------------------------------------------------------
    | AASH Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the AASH section.
    |
    */

    'header'         =>  'Activities',
    'content' 	     =>	 'Our <em>Activities</em> enhance and develop our students’ skills, encouraging them to discover new talents and interests, combining NORTHLANDS’ values with characteristics of the IB learner profile.<br />A wide range of yearly Activities, offer an alternative space for our children to make new friends and to explore new ideas through challenging experiences.',
];
