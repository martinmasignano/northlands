<?php

return [

    /*
    |--------------------------------------------------------------------------
    | footer Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the footer links.
    |
    */

    'copyright'         =>  '&copy; 2015 NORTHLANDS SCHOOL. All rigths reserved.',
    'jobopportunities' 	=>	'Job opportunities',
    'contactus'			=>	'Contact us',
];