<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navbar Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the navbar links.
    |
    */

    'about'             =>  'About us',
    'northlandsway'     =>  'The Northlands Way',
    'authorities'       =>  'Authorities',
    'history'           =>  'History',
    'ourschool'         =>  'our School',
    'levels'            =>  'Educational Levels',
    'admissions'        =>  'Admissions',
    'wgo'               =>  'wgo',
    'alumni'            =>  'Alumni',
    'contact'           =>  'Contact us',
    'contactinfo'       =>  'Contact',
    'login'             =>  'Login',
    'job'               =>  'Job Opportunities',
    'parents'           =>  'Parents',
    'students'          =>  'Students',
    'staff'             =>  'Staff',
    'help'              =>  'Need Help?',
    'spanish'           =>  'Spanish',
    'english'           =>  'English',
    'search'            =>  'Search...',
];
