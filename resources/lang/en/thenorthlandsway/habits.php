<?php

return [

    /*
    |--------------------------------------------------------------------------
    | thoughts habits Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the thoughts habits section.
    |
    */

    'header'         =>  'thoughts habits',
    'content'        =>  'Content for thoughts habits',
];