<?php

return [

    /*
    |--------------------------------------------------------------------------
    | commitment to the values Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the commitment to the values section.
    |
    */

    'header'         =>  'Commitment to the values',
    'content'        =>  'Content for commitment to the values',
];