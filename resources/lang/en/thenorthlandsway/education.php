<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Personal and social education Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the Personal and social education section.
    |
    */

    'header'         =>  'Personal and social education',
    'content'        =>  'Content for Personal and social education',
];