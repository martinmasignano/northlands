<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Community profile IB learner Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the community profile IB learner section.
    |
    */

    'header'         =>  'Community profile IB learner',
    'content'        =>  'Content for community profile IB learner',
];