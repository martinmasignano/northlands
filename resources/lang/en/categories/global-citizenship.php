<?php

return [

	/*
    |--------------------------------------------------------------------------
    | Empowers IB learnes language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the Empowers IB learnes names.
    |
    */
	
	'header'				=>	'GLOBAL CITIZENSHIP',	
	'lead'					=>	'To become a global citizen means that we not only value our own cultural identity, we also appreciate the richness of diversity and promote international mindedness.  This understanding and intercultural respect inspires us to share responsibility and to contribute towards a more sustainable and peaceful world.',

	'caption-heading-1'		=>	'Culturas tradicionales y contextos',
	'caption-text-1'		=> 'Dentro del marco de nuestro programa de Desarrollo Personal y Social, en el Jardín buscamos desarrollar en cada uno de nuestros alumnos conciencia de sí mismo y de los demás... ',

	'caption-heading-2'		=>	'Three words to the word',
	'caption-text-2'		=> 'Students in Primary were inquiring into the concept of Identity: “what makes us individual and unique and also how we portray ourselves to others”... ',
	
	'caption-heading-3'		=>	'The International Dimension',
	'caption-text-3'		=> 'Chasing the shifting clouds students float unanchored, confused  by the ever-changing world around them…but…if given the opportunity to grow and learn within an internationally minded... ',

	'caption-heading-4'		=>	'En Primera Persona',
	'caption-text-4'		=> 'The THIMUN (The Hague International Model United Nations) conference is a five-day simulation of  the United Nations for secondary school students,... ',

	'caption-heading-5'		=>	'The Golden Rule',
	'caption-text-5'		=> 'NORTHLANDS promotes the development of global citizenship in which we "not only value our own cultural identity but also appreciate the richness of diversity and promote international-mind... ',

	'caption-heading-6'		=>	'Conciencia Ecológica',
	'caption-text-6'		=> 'Uno de los desafíos más grandes en la enseñanza de las Ciencias Naturales es lograr que los alumnos puedan pensar y analizar un tema en distintos niveles y poder relacionar lo que ocurre en su entorno con... ',
];
