<?php

return [

	/*
    |--------------------------------------------------------------------------
    | Healthy lifestyle language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the Healthy lifestyle names.
    |
    */
	
	'header'				=>	'HEALTHY LIFESTYLE',	
	'lead'					=>	'During children and teenagers’ development phase it is essential to create awareness and promote the benefits of a healthy lifestyle. Through the implementation of an institutional action plan which includes physical, neurological and psychological aspects, we build and strengthen a well-balanced psyche empowering our students to make healthy life choices.',

	'caption-heading-1'		=>	'Educación Preventiva',
	'caption-text-1'		=> 'Este proyecto de educación preventiva tiene como objetivo establecer conductas de concientización y autocuidado en los niños antes de que lleguen a la adolescencia... ',

	'caption-heading-2'		=>	'Tutoriales de Primeros Auxilios',
	'caption-text-2'		=> 'El objetivo de los primeros auxilios es actuar rápidamente con técnicas y procedimientos para asistir al accidentado hasta que venga el médico o la ambulancia.... ',

	'caption-heading-3'		=>	'Construcción de Saberes y Evaluación',
	'caption-text-3'		=> 'La evaluación de la Educación Física está integrada al proceso de enseñanza en el Nivel Inicial. Durante este proceso el docente y el alumno reflexionan acerca de las experiencias y sobre cómo éstas con... ',

	'caption-heading-4'		=>	'Enseñamos Diferente',
	'caption-text-4'		=> 'Fundamentalmente porque enseñamos para la vida y no simplemente para el éxito deportivo. Por lo tanto adecuamos las estrategias de enseñanza para que todos los alumnos encuentren un desafío de supera... ',
];
