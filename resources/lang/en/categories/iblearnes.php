<?php

return [

	/*
    |--------------------------------------------------------------------------
    | Empowers IB learnes language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the Empowers IB learnes names.
    |
    */
	
	'header'				=>	'EMPOWERS IB LEARNERS',	
	'lead'					=>	'As an IB World School, we work within a global community offering a unique standard of education to develop independent thinking, international mindedness and a sense of shared responsibility for the planet we share.',

	'caption-heading-1'		=>	'En primera persona',
	'caption-text-1'		=> 'Sybrand Veeger, graduado de Northlands 2014, estudiante nos visitó para contarnos los beneficios de ser parte del mundo IB... ',

	'caption-heading-2'		=>	'Cierre de una etapa',
	'caption-text-2'		=> 'Al llegar a Año 6 y como cierre de este ciclo, los alumnos llevan a cabo un extenso y profundo proyecto colaborativo denominado la Exposición del Programa de la Escuela Primaria (PEP) del IB. ... ',

	'caption-heading-3'		=>	'24 puntos o más',
	'caption-text-3'		=> 'Todos nuestros alumnos rinden el Diploma bilingüe completo del IB que, además de evaluar las habilidades académicas, promueve una actitud internacional y habilidades interculturales... ',

	'caption-heading-4'		=>	'Mucho más que habilidades académicas',
	'caption-text-4'		=> 'Las asignaturas de Artes en el programa permiten un alto grado de adaptabilidad a diferentes contextos culturales. ...',
];
