<?php

return [

	/*
    |--------------------------------------------------------------------------
    | Mentors of knowledge language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the Mentors of knowledge names.
    |
    */
	
	'header'				=>	'MENTORS OF KNOWLEDGE',	
	'lead'					=>	'Based on each student’s individual ability to learn and understand their environment, NORTHLANDS accompanies them to explore and investigate the world of knowledge, developing thinking skills and reflecting upon their own way of learning.',

	'caption-heading-1'		=>	'Hacer visible el pensamiento',
	'caption-text-1'		=> 'Un libro sobre metacognición Docentes, asesoras y la Dirección de nuestro Nivel Inicial de la sede Nordelta publicó, junto con la Universidad de San Andrés... ',

	'caption-heading-2'		=>	'Self Expression',
	'caption-text-2'		=> 'IB Art students work throughout the last two years at School on the creation of a personal project that not only involves the continuity of the learning art at School... ',

	'caption-heading-3'		=>	'English in real context',
	'caption-text-3'		=> 'English in Secondary school at NORTHLANDS means developing worthy skills of communication that will stay with the students for ever. It means learning to use English... ',

	'caption-heading-4'		=>	'Don`t try this at home',
	'caption-text-4'		=> 'During the first unit of inquiry on Transformations and Changes, Year 6 students were faced to the following questions: How do we learn in Natural Science? How do we find out what we want to know? How is... ',

	'caption-heading-5'		=>	'¿Matemática + Lógica = Diversión?',
	'caption-text-5'		=> 'El conteo de una colección de objetos supone: el conocimiento de la serie numérica oral, la asignación de un número –y sólo uno- a cada objeto contado, reconocer... ',

	'caption-heading-6'		=>	'Too young to learn?',
	'caption-text-6'		=> 'Teaching the habits of mind in Kindergarten can contribute significantly in the task of getting both... ',
];
