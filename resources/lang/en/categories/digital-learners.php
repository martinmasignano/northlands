<?php

return [

	/*
    |--------------------------------------------------------------------------
    | Character building language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the authorities names.
    |
    */
	
	'header'				=>	'DIGITAL LEARNERS',	
	'lead'					=>	'Learning networks enable greater interaction between theory and practice, between the classroom and the real world, at any time and place. We are part of a student community who works online and keeps interconnected in order to build up knowledge in an asynchronous world. A new interaction model between teacher/student and student/student, a new way of communicating which emphasizes active and interactive learning, research and problem solving.',

    'caption-heading-1'     =>  'A la vanguardia de una educación progresista',
    'caption-text-1'        => 'Uno de los objetivos macro de NORTHLANDS es “Ser en un Colegio de vanguardia en el uso efectivo del aprendizaje digital...',

    'caption-heading-2'     =>  'You too express',
    'caption-text-2'        => 'Un equipo multidisciplinario de NORTHLANDS guiados por el equipo de innovación Cocolab, bajo la coordinación y guía de Ezequiel Bachrach investigó... ',

    'caption-heading-3'     =>  'Changing the way we teach and learn',
    'caption-text-3'        => 'We are living in an education revolution.  Much has changed in the way students are learning and we cannot expect them to learn well if we continue to teach reading, writing and mathematics...',

    'caption-heading-4'     =>  'El modelo SAMR',
    'caption-text-4'        => 'El modelo SAMR, propuesto por el Dr. Ruben Puentedura, explica cuatro fases en el uso de las nuevas tecnologías, basándose en el tipo de relación entre las actividades que se realizan con ellas...',
];
