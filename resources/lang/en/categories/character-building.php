<?php

return [

	/*
    |--------------------------------------------------------------------------
    | Character building language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the authorities names.
    |
    */
	
	'header'				=>	'CHARACTER BUILDING',	
	'lead'					=>	'As a central part of their comprehensive education and through a well-defined programme, our students are educated to become individuals of integrity with high moral standards enabling them to freely choose what is right.',

	'caption-heading-1'		=>	'Acompañar el cambio',
	'caption-text-1'		=> 'Acompañando el proceso que implica el periodo de adaptación en el Jardín, abordamos el eje: “Conciencia de uno mismo” (desarrollar la confianza en nosotros mismos a partir de un mejor conocimiento... ',

	'caption-heading-2'		=>	'Construir otros vínculos',
	'caption-text-2'		=> 'A lo largo de su escolaridad, los alumnos de NORTHLANDS, se ven beneficiados por una experiencia significativa y enriquecedora: el reagrupamiento, que brinda a los niños la oportunidad de conocer... ',

    'caption-heading-3'     =>  '¿Diferente a mí?',
    'caption-text-3'        => 'Entendemos la Conciencia de los Demás (uno de nuestros Ejes de PSE) como la capacidad de salir de uno mismo hacia el encuentro con un otro diferente a mí... ',

    'caption-heading-4'     =>  'Arts and social issues',
    'caption-text-4'        => 'After observing and analysing examples of Realistic art pieces which evidence social issues, Year 8 students were asked to develop a realistic painting using mixed media... ',
];
