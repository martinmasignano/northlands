<?php

return [

    /*
    |--------------------------------------------------------------------------
    | info Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the info section.
    |
    */

    'header'         =>  'CONTACTO',
    'branch-nordelta'   =>  'SEDE NORDELTA',
    'branch-olivos'   =>  'SEDE OLIVOS',
    'address' => 'Dirección: ',
    'phone' => 'Teléfono: ',

];
