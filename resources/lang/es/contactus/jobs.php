<?php

return [

    /*
    |--------------------------------------------------------------------------
    | job oportunities Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the job opportunities section.
    |
    */

    'header'            =>  'OPORTUNIDADES LABORALES',

    'paragraph1'        =>	 'Buscamos personas innovadoras, apasionadas y dedicadas para sumarlas a nuestro equipo de trabajo.',

    'paragraph2'        => 'Queremos sumar a los mejores profesionales de mente abierta, flexibles y versátiles. Capaces de tener una visión global de la organización y de convertirse en guías de nuestros alumnos para que al graduarse cuenten con las herramientas necesarias para ser verdaderos agentes de cambio.',

    'paragraph3'        => 'Ofrecemos un ambiente estimulante donde la capacitación permanente, el trabajo colaborativo y la pasión por la enseñanza guían nuestra forma de trabajar.',
    
    'paragraph4'        => 'Actualmente estamos realizando una búsqueda para los siguientes puestos:',

    'joinus-text'       => 'Formá parte de nuestra Comunidad de Aprendizaje',
    'joinus-link'       => 'Ingresá tus datos aquí',


    /* AQUÍ VAN LOS TEXTOS DE LOS PUESTOS LABORALES BUSCADOS */

     /* IT teacher NORDELTA */
    'nordelta-ITteacher-title' => 'Profesor/a de informática (Nivel Inicial NORDELTA)',
    'nordelta-ITteacher-text'  => '<small>07/11/2016</small><br>NORTHLANDS hace extensiva la búsqueda de un/a Profesor/a de informática (o con título similar) para la sede de Kindergarten Nordelta.<br><br>
 <strong>Requisitos</strong>
<ul><li>- Actitud dinámica y proactiva</li>
<li>- Bilingüe (inglés castellano)</li>
<li>- Excelentes relaciones interpersonales</li>
<li>- Experiencia en nivel inicial</li></ul><br>
Enviar CV a <a href="mailto:rrhh@northlands.edu.ar">rrhh@northlands.edu.ar</a>, indicar Referencia INF.',

  /* Director General */
    'nordelta-Head-of-School-title' => 'Director General',
    'nordelta-Head-of-School-text'  => '<small>28/10/2016</small><br>La posición de Director General reporta a la Comisión Directiva. En su rol de Líder Educativo y CEO, tiene bajo su responsabilidad global la calidad del aprendizaje y la fortaleza institucional. Él/ella deberá promover y emular la misión, visión, valores y objetivos del Colegio en todos los ámbitos de la vida escolar con el objetivo de educar a sus alumnos en un ambiente de desafío académico y crecimiento personal, social y emocional en preparación para un permanente aprendizaje y contribución a la comunidad.
<br><br>
<strong>Competencias:</strong>
<ul><li>
Colaboración y Prevención: dirección, liderazgo y estrategia</li>
<li>Imaginación y Creación: una comunidad de aprendizaje permanente</li>
<li>Inspiración y Evolución: progreso institucional</li>
<li>Perfección y Ejecución: contratación, compromiso y permanencia</li>
<li>Supervisión y Resultados: alineamiento de ambas sedes</li></ul><br>
<strong>Requisitos Académicos y Profesionales:</strong>
<ul>
<li>Habilidad comprobada para ejecutar la implementación, desarrollo y revisión del currículo educativo en lineamiento con la Acreditación, el IB y las Normas Nacionales.</li>

<li>Gestión reconocida en administración de Colegios Internacionales como Director o Rector, en relacionarse con Comisiones Directivas y en implementación de políticas.</li>

<li>Trayectoria en selección de docentes capacitados y talentosos y en manejo y evaluación de personal.</li>

<li>Interculturalmente sensible,  fuerte integridad moral y predisposición para adoptar la cultura Argentina.</li>

<li>Experiencia acreditada en diseño y manejo de presupuestos, habilidad para fijar objetivos, planificación estratégica y monitoreo de progreso.</li>

<li>Liderazgo e iniciativa, habilidades de team-building, auto-motivación y carisma, aptitud para inspirar y obtener confianza de la Comunidad del Colegio.</li>

<li>Excelente manejo de relaciones interpersonales y habilidad para desarrollar relaciones de trabajo con autoridades educativas y organizaciones empresariales.</li>

<li>Adepto en el uso de tecnología en un entorno educativo y a favor de su integración en el currículo educativo.</li>

<li>Es requisito poseer título Universitario, M.A./M.Ed./M.Sc. o su equivalente.</li>

<li>Preferentemente, fluidez en el idioma español</li></ul><br>
<strong>CONTRATACIÓN</strong>
<br>
El contrato inicial es por 3 años, renovable.  Se ofrece un atractivo paquete de beneficios, acorde a la experiencia y responsabilidades inherentes al puesto.
<br><br><br>
<strong>COMO POSTULARSE</strong>
<br>
CIS tiene el agrado de comunicar que ha sido contratado por NORTHLANDS para realizar la selección de un Director General para su Colegio.  El proceso será liderado por Joe Cornacchio  y el Equipo de Liderazgo en procesos de selección.
<br><br>
Aquellos Líderes que estén interesados en postularse, deben solicitar instrucciones vía email a: leadershipsearch@cois.org  Los candidatos que ya cuentan con sus antecedentes profesionales y actualizados en CIS, deben enviar una carta de presentación.
<br><br>
Si el Colegio recibiese una postulación excepcional, el Comité de Selección se reserva el derecho de hacer una oferta y finalizar la búsqueda antes de la fecha de cierre informada a continuación.
<br><br><br>
<strong>PLAZO</strong>
<br>
Fecha de cierre: 11 de noviembre de 2016.
<br><br>
Las solicitudes serán evaluadas al momento de su recepción. Los finalistas serán seleccionados por el Comité de Selección a la mayor brevedad posible durante el proceso y serán invitados a la Argentina para una entrevista final con el Comité de Selección, El Consejo Directivo y grupos constituyentes de la Comunidad de NORTHLANDS con el fin de hacer un nombramiento en un corto plazo.
<br><br>
NORTHLANDS se rige bajo el calendario del hemisferio sur y está abierto a negociar la fecha de inicio de actividades de acuerdo a la disponibilidad del nuevo Director.',


    /* Librarian NORDELTA */
    'nordelta-librarian-title' => 'Bibliotecario (Sede NORDELTA)',
    'nordelta-librarian-text'  => '<small>09/09/2016</small><br>El dominio fluido del inglés es un factor excluyente.<br>Disponibilidad preferentemente de 12 a 16.30 hs.<br>El trabajo como bibliotecario estará centrado en las siguientes tareas:<br>Administración del material de la biblioteca<br>Asistencia a los alumnos y docentes en investigación bibliográfica<br>Apoyo en el programa de inmersión en español<br>Los interesados deben enviar CV indicando Referencia BIBL. a <a href="mailto:rrhh@northlands.edu.ar">rrhh@northlands.edu.ar</a>',

    /* Primary School Teacher NORDELTA */
    'nordelta-primary-school-teacher-title' => '',
    'nordelta-primary-school-teacher-text' => '',

    /* Geography Teacher (Secondary NORDELTA) */
    'nordelta-geography-teacher-title' => 'Profesor/a bilingüe de Geografía (Secundaria NORDELTA)',
    'nordelta-geography-teacher-text' => '<small>26/05/2016</small><br>Secundaria Nordelta está en proceso de búsqueda de un/a Profesor/a de Geografía bilingüe.<br>El/la postulante debe tener titulo oficial.<br>Por favor enviar CV indicando Referencia PROF.GEO y pretensiones salariales a: <a href="mailto:rrhh@northlands.edu.ar">rrhh@northlands.edu.ar</a>',

    /* Coordinador/a de Actividades (Sede OLIVOS) */
    'olivos-Coordinator-of-Activities-title' => 'Coordinador/a de Actividades (Sede Olivos)',
    'olivos-Coordinator-of-Activities-text' => '<small>01/11/2016</small><br>NORTHLANDS hace extensiva la búsqueda de Coordinador/a de Actividades fuera del horario escolar (AASH) para su sede de Olivos.<br>
        Fecha de inicio de actividades: febrero 2017<br>
        Horario: lunes a viernes de 10.30 a 18.00<br>
        El/la Coordinador/a de AASH es un miembro del plantel educativo de NORTHLANDS y como tal será un miembro activo de la comunidad manifestando su adhesión tanto a los valores del Colegio como a su Proyecto de formación integral.<br><br>
        <strong>Será responsable de coordinar todos los aspectos del Programa de AASH:</strong>
        <ul><li>- Comunicación y promoción de las actividades</li>
        <li>- Organización y gestión de los cursos</li>
        <li>- Articulación con los departamentos de PE y Música</li>
        <li>- Comunicación con las familias</li>
        <li>- Supervisión permanente del programa</li></ul><br>
<strong>Requisitos </strong>
        <ul><li>- Muy buenas habilidades organizativas</li>
        <li>- Actitud dinámica y proactiva</li>
        <li>- Muy buen dominio del idioma inglés</li>
        <li>- Excelentes relaciones interpersonales</li>
        <li>- Muy buen manejo del paquete MS Office</li></ul><br>
        Enviar CV a <a href="mailto:rrhh@northlands.edu.ar">rrhh@northlands.edu.ar</a>, indicar Referencia COORD AASH.<br>
        Fecha límite de presentación de antecedentes: 11/11/16
 ',

    /* English and Literature Teacher (Secondary OLIVOS) */
    'olivos-english-literature-teacher-title' => 'Profesor/a de Inglés y Literatura (Secundaria OLIVOS)',
    'olivos-english-literature-teacher-text' => '<small>13/09/2016</small><br>El/la postulante debe contar con título oficial, y experiencia en exámenes IGCSE /IB.<br>Los interesados deben enviar CV indicando Referencia PROF.INGL. a <a href="mailto:rrhh@northlands.edu.ar">rrhh@northlands.edu.ar</a>',

    /* Drama Teacher (Sede OLIVOS) */
    'olivos-drama-teacher-title' => 'Profesor/a de Teatro (Sede Olivos)',
    'olivos-drama-teacher-text' => '<small>01/09/2016</small><br>El/la postulante debe contar con título oficial, y experiencia en exámenes de Drama IGCSE (teoría y práctica).<br>Los interesados deben enviar CV indicando Referencia PROF. TEATRO a <a href="mailto:rrhh@northlands.edu.ar">rrhh@northlands.edu.ar</a>',

    /* Professor of Literature and Language (Secondary OLIVOS) */
    'olivos-literature-language-title' => 'Profesora de Lengua y Literatura (Secundaria OLIVOS)',
    'olivos-literature-language-text' => 'Docente de literatura titulo de profesor con validez nacional<br>Experiencia en programas IB y IGCSE de Literatura y de Lengua y Literatura<br>Conocimientos de inglés<br>Los interesados deben enviar CV indicando Referencia PROF.L&L a <a href="mailto:rrhh@northlands.edu.ar">rrhh@northlands.edu.ar</a>',

];
