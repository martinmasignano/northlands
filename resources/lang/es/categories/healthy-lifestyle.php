<?php

return [

	/*
    |--------------------------------------------------------------------------
    | Healthy lifestyle language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the Healthy lifestyle names.
    |
    */
	
	'header'				=>	'ESTILO DE VIDA SALUDABLE',	
	'lead'					=>	'La conciencia y el fomento de los beneficios de un estilo de vida saludables resultan fundamentales en la etapa de formación de los niños y jóvenes. A través de un plan de acción institucional, que contempla aspectos físicos, neurológicos y psicológicos, se consolida la construcción de una psique balanceada que llevará a nuestros alumnos a tomar elecciones de vida saludables.',

	'caption-heading-1'		=>	'Educación Preventiva',
	'caption-text-1'		=> 'Este proyecto de educación preventiva tiene como objetivo establecer conductas de concientización y autocuidado en los niños antes de que lleguen a la adolescencia... ',

	'caption-heading-2'		=>	'Tutoriales de Primeros Auxilios',
	'caption-text-2'		=> 'El objetivo de los primeros auxilios es actuar rápidamente con técnicas y procedimientos para asistir al accidentado hasta que venga el médico o la ambulancia.... ',

	'caption-heading-3'		=>	'Construcción de Saberes y Evaluación',
	'caption-text-3'		=> 'La evaluación de la Educación Física está integrada al proceso de enseñanza en el Nivel Inicial. Durante este proceso el docente y el alumno reflexionan acerca de las experiencias y sobre cómo éstas con... ',

	'caption-heading-4'		=>	'Enseñamos Diferente',
	'caption-text-4'		=> 'Fundamentalmente porque enseñamos para la vida y no simplemente para el éxito deportivo. Por lo tanto adecuamos las estrategias de enseñanza para que todos los alumnos encuentren un desafío de supera... ',
];
