<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Barra de navegación
    |--------------------------------------------------------------------------
    |
    | Las siguientes líneas son usadas en los enlaces de la barra de navegación.
    |
    */

    'about'             =>  'Sobre Nosotros',
    'northlandsway'     =>  'The Northlands Way',
    'authorities'       =>  'Autoridades',
    'history'           =>  'Historia',
    'ourschool'         =>  'Nuestro colegio',
    'levels'            =>  'Niveles educativos',
    'admissions'        =>  'Admisiones',
    'wgo'               =>  'wgo',
    'alumni'            =>  'Alumni',
    'contact'           =>  'Contacto',
    'contactinfo'       =>  'Contacto',
    'login'             =>  'Iniciar sesión',
    'job'               =>  'Oportunidades Laborales',
    'parents'           =>  'Padres',
    'students'          =>  'Alumnos',
    'staff'             =>  'Personal',
    'help'              =>  '¿Necesitás ayuda?',
    'spanish'           =>  'Español',
    'english'           =>  'Inglés',
    'search'            =>  'Buscar...',
];