<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Education levels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the Education levels section.
    |
    */

    'header'         =>  'Niveles educativos',
    'intro1' 	     =>	 'Nuestro proyecto curricular articula los programas internacionales con el plan de la provincia de Buenos Aires.',
    'intro2'         =>  'Nuestros alumnos reciben una educación integral bilingüe desde múltiples perspectivas en un entorno aprendizaje, contención y formación personal y social que fomenta el desarrollo de una mentalidad internacional.',
    'proyect-title'     => 'PROYECTO CURRICULAR',
    
    'level1'         => 'Nivel Inicial',
    'level1.description'    => 'De 2 a 5 años',
    'level1.text'   => 'Brindamos un entorno de bienestar académico, social y emocional para el desarrollo holístico del niño. A través de un curriculum que fomenta la mentalidad internacional y la construcción de sólidos valores personales, incentivamos la curiosidad y la autoestima. De manera natural, los niños desarrollan las habilidades necesarias para comprender, elaborar y expresar sus ideas tanto en inglés como en castellano.',
    
    'level2'        => 'Primaria',
    'level2.description'    => 'De 6 a 12 años',
    'level2.text'   => 'Un entorno estimulante que promueve el aprendizaje y el desarrollo de actitudes positivas, confianza y autodisciplina. A través de un currículo integral, centrado en la indagación y el descubrimiento, se prepara a los alumnos para que sean activos, solidarios y respetuosos de sí mismos y de los demás, y para que participen como ciudadanos responsables en el mundo que los rodea.',
    
    'level3'        => 'Secundaria', 
    'level3.description'    => 'De 13 a 18 años',
    'level3.text'   => 'Un curriculum amplio y flexible que atiende a la consolidación personal de los jóvenes. Alta calidad educativa que profundiza el interés y la curiosidad por el mundo académico, la mentalidad internacional y la apreciación por la cultura. Los estudiantes desarrollan el pensamiento crítico, la creatividad y el trabajo autónomo; se estimulan los talentos individuales y la amplitud de pensamiento para  comprender y participar del mundo interconectado en el que vivimos.',

];