<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Login Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the Login section.
    |
    */

    'header'         =>  'Iniciar sesión',
    'content' 	     =>	 'Contenido para Iniciar sesión',
];