<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admissions Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the Admissions section.
    |
    */

    'header'    =>  'ADMISIONES',

    'text1'     =>  'Una de las decisiones más importantes y difíciles que un padre debe tomar es elegir el colegio adecuado para sus hijos. NORTHLANDS considera que cada nuevo alumno que ingresa al Colegio representa un compromiso mutuo a largo plazo entre la institución, el alumno y la familia. Esta es una relación exigente en cada una de las partes pero provee beneficios únicos para toda la vida, tanto para nuestros alumnos, como para sus familias.',

    'text2'     =>  'Para asegurar que este compromiso se pueda cumplir mutuamente, el Colegio establece distintas pautas para el ingreso de sus alumnos. Esperamos que a través de nuestro proceso de admisión, tanto las familias postulantes, como el Colegio obtengan toda la información necesaria para determinar si NORTHLANDS es el Colegio adecuado para el postulante.',

    'schedule'  =>  'Cronograma de Admisiones',

    'text3'     =>  'Las solicitudes de admisión a NORTHLANDS serán aceptadas a partir del mes de marzo para el año lectivo siguiente.',

    'date1'     =>  'Cierre de inscripción para Kindergarten: 15 de agosto',
    'date2'     =>  'Cierre de inscripción para Primaria y Secundaria: 1 de octubre',

    'text4'     =>  'La prioridad en la admisión al Colegio se otorga, siempre y cuando haya vacantes disponibles y los postulantes aprueben el proceso de admisión, a:',

    'priority'  =>  'Prioridad en la Admisión',
    'priority1' =>  'Los hijos de las familias del Colegio',
    'priority2' =>  'Los hijos de Ex-Alumnas (Old Northlanders)',

    'accordiontitle1'   =>  'Proceso de Admisión',
    'accordiontitle2'   =>  'Familias que vienen del exterior',

    'process.step1'     =>  '1. Completar la ficha de los datos del postulante',
    'process.step1.link' => 'CONTACTO >>',   
    'process.step2a'     =>  '2. Después de la visita informativa la familia deberá completar el',
    'process.step2.link' => 'FORMULARIO DE DATOS DEL POSTULANTE ON-LINE >>', 
    'process.step2b'     =>  'y entregar la siguiente documentación a la oficina de Admisiones',

    'process.substep1'  =>  'a. Aceptación de las condiciones del proceso de admisión firmada',
    'process.substep2'  =>  'b. Cartas de presentación de la familia',
    'process.substep3'  =>  'c. Copia del último informe / boletín',
    'process.substep4'  =>  'Una vez recibidos los requisitos, la Oficina de Admisiones asignara las entrevistas correspondientes',

    'comunication.title'    =>  'Comunicación de la decisión final de admisión',
    'comunication.text1'    =>  'La Oficina de Admisiones informará a los padres la comunicación sobre el resultado del proceso de admisión.',
    'comunication.text2'    =>  'Las familias de los postulantes admitido, deberán retirar un sobre de la Recepción General del Colegio con los requisitos de matriculación que incluyen la última página del MANUAL DE PADRES FIRMADA >>',

    'family.text1'  =>  'Las familias que vienen del exterior deben cumplir con los pasos descriptos en el proceso de admisión.',
    'family.text2'  =>  'Deben traer la siguiente información a la entrevista de admisión:',
    
    'family.information1'  =>  'Informes / boletines del colegio actual',
    'family.information2'  =>  'Carta de presentación de la familia: puede ser de alguna familia del colegio, o de una exalumna. En caso de no conocer a ninguna persona relacionada con la institución, se puede presentar una carta de una persona que se considere un referente calificado (directivo del colegio actual). Dicha carta debe ser enviada en sobre cerrado a la Oficina de Admisiones y no puede ser escrita ni por un miembro del Personal del Colegio, ni por un familiar directo. La presentación de la carta es un requisito para comenzar el proceso de admisión y en ningún caso será determinante en la admisión del postulante.',

    'documents.title'   =>  'Documentos requeridos para la matriculación',

    'documents.information1'    =>  'La siguiente documentación es un requisito para el ingreso de un nuevo alumno:',

    'documents.requisits1'  =>  '<strong>Pasaporte</strong> del alumno nuevo (original) que será fotocopiado y devuelto inmediatamente.',
    'documents.requisits2'  =>  '<strong>Partida de Nacimiento del alumno</strong> nuevo (original) que será fotocopiada y devuelta en el momento. Si está en otro idioma que no sea el castellano, debe estar traducida, apostillada o legalizada por el Cónsul Argentino del lugar donde hayan nacido los menores.',
    'documents.requisits3'  =>  '<strong>Certificado de estudios anteriores.</strong> Este certificado será una constancia realizada por las autoridades del Colegio correspondiente, en papel con membrete de dicha institución, en el que se indique el último año cursado y aprobado por el interesado con sello y firmas de las autoridades. Debe ser presentado el original o copia legalizada, acompañado de su traducción en caso de estar en otro idioma que no sea el castellano',

    'documents.information' =>  'Este certificado de estudios debe estar legalizado por:',

    'documents.information.text1'   =>  'a. Las autoridades educacionales del país donde fueron cursados los estudios.',
    'documents.information.text2'   =>  'b. El Cónsul Argentino del lugar donde fue extendido el certificado.',
    'documents.information.text3'   =>  'En la República Argentina: Ministerio de Relaciones Exteriores y Culto (salvo en aquellos casos en que se consigne la Apostilla de LA HAYA).',

    'documents.requisits4'  =>  '<strong>Certificado de vacunación</strong>',
];
