<?php

return [

    /*
    |--------------------------------------------------------------------------
    | AASH Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the AASH section.
    |
    */

    'header'         =>  'Activities',
    'content'        =>  'Nuestras <em>Activities</em> potencian y desarrollan las habilidades de nuestros alumnos. Los animan a descubrir nuevos talentos e intereses conjugando los valores de NORTHLANDS con los atributos del perfil de la comunidad IB. La oferta anual de Activities provee un espacio alternativo donde nuestros alumnos tienen la oportunidad de hacer nuevos amigos y explorar nuevas ideas a través de experiencias desafiantes.',
];
