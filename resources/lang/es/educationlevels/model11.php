<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Model 1:1 Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the Performing arts section.
    |
    */

    'header'         =>  'Modelo 1:1',
    'content' 	     =>	 'Contenido de Modelo 1:1',
];
