<?php

return [

    /*
    |--------------------------------------------------------------------------
    | School life Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the School life section.
    |
    */

    'header'         =>  'VIDA ESCOLAR',
    'text'       =>  'Las tradiciones que compartimos y respetamos en NORTHLANDS refuerzan el sentimiento de identificación con nuestras raíces y con nuestro compromiso de educar individuos íntegros cuya moral los lleve a elegir libremente el bien. Las Houses, el Student Council, los Captains, los Prefects y los Heads Student, buscan integrar y generar espacios de participación y sana competencia entre los alumnos, pues deben asumnir la responsabilidad de organizar diferentes actividades, representar a sus compañeros, liderar a sus grupos y ser ejemplo permanente para todos los alumnos del Colegio.',

    'houses-title'  =>  'HOUSES',
    'houses-text1'  =>  'Al ingresar a la Primaria, cada alumno es asignado a una House que se convertirá en su grupo de pertenencia durante toda la vida escolar. Las Houses se asignan por herencia familiar (hijos de Old Northlanders mantienen la House de sus familiares) o por distribución equitativa del número de integrantes. Dentro de cada House interactúan alumnos de los distintos niveles del Colegio lo que permite ampliar el espectro de amistades así como desarrollar y asumir roles de liderazgo.',
    'houses-text2'  =>  'Las competencias interhouse incluyen cultura general, eventos departamentales, servicio comunitario, deportes y teatro que, junto con el rendimiento académico individual, permiten sumar puntos para cada House. Al finalizar el año se suman los puntos y la House que más puntos obtuvo es la ganadora y su nombre grabado en un escudo.',

    'captains-title'    =>  'CAPTAINS',
    'captains-text1'     =>  'El Captain de una House, es un estudiante que se ha destacado en las actividades (académicas, deportivas y de servicio) organizadas por su house. Los Captains, deberán también, poseer cualidades como: liderazgo, comunicación, sentido de la justicia y del servicio. Así como practicar el lema del Colegio "Friendship & Service".',

    'prefects-title'    =>  'PREFECTS',
    'prefects-text1'    =>  'Docentes y alumnos eligen a algunos estudiantes de año 12 como Prefects por sosotener en sus acciones los valores del Colegio, estos alumnos han ganado el respeto de la comunidad y son modelos para los demás. Los Prefects componen el Senior Council que se reúne mensualmente con las autoridades del Colegio y tienen, entre otras responsabilidades, trabajar en conjunto con los tutores, cooperar en la organización de las asambleas y representar al Colegio en eventos público.',
    'prefects-text2'    =>  'Un Prefect deberá tener un buen nivel académico, estar involucrado en actividades extracurriculares y ser un joven comprometido con el lema "Friendship & Service" que guía a nuestro Colegio.',

    'headstudent-title' =>  'HEAD STUDENT',
    'headstudent-text1' =>  'El Prefect con el mayor número de votos es nombrado Head Student previa aprobación del Headmaster. Entre sus funciones se encuentran, organizar el Student Council, mantener reuniones regulares con el Senior Council y representar al Colegio en ocasiones formales.',

    'seniorcouncil-title'   =>  'SENIOR COUNCIL',
    'seniorcouncil-text1'   =>  'El Senior Council está compuesto por los estudiantes que han sido elegidos Prefects, Head Student y Deputy Head Student, quienes se reúnen mensualmente con las autoridades del Colegio para intercambiar y discutir ideas para el mejor funcionamiento del mismo.',

    'studentcouncil-title'  =>  'STUDENT COUNCIL',
    'studentcouncil-text1'  =>  'La función de este cuerpo es canalizar inquietudes, sugerencias e ideas hacia los directivos del Colegio y la vez, promover el concepto de participación social responsable. Los miembros del Consejo son elegidos por sus compañeros de acuerdo a los siguientes atributos: Amistad y servicio, Líderazgo positivo, Comunicación efectiva .',
];
