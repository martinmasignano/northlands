<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Education levels nav Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the Education levels nav.
    |
    */

    'pa-link'       =>  'ARTES',
    'ip-link' 	    =>  'PROGRAMAS INTERNACIONALES',
    'pse-link'      =>  'ED. PERSONAL Y SOCIAL',
    'aash-link'     =>  'ACTIVITIES',
    'pe-link'       =>  'EDUCACIÓN FÍSICA',
    'sl-link'       =>  'VIDA ESCOLAR',
    '11-link'       =>  'MODELO 1:1'
];
