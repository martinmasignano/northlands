<?php

return [

    /*
    |--------------------------------------------------------------------------
    | wgo Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the wgo section.
    |
    */

    'title' => 'Qué está pasando en NORTHLANDS?',
    'default' => 'FILTRAR POR',
    'all' => 'VER TODO',
    'kinder-n' => 'NIVEL INICIAL NORDELTA',
    'kinder-o' => 'NIVEL INICIAL OLIVOS',
    'primary-n' => 'PRIMARIA NORDELTA',
    'primary-o' => 'PRIMARIA OLIVOS',
    'secondary-n' => 'SECUNDARIA NORDELTA',
    'secondary-o' => 'SECUNDARIA OLIVOS',
    'institutional' => 'INSTITUCIONAL',
    'beyond' => 'MÁS ALLÁ DE NORTHLANDS',

];
