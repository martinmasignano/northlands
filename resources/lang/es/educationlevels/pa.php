<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Performing arts Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the Performing arts section.
    |
    */

    'header'         =>  'Artes',
    'text1' =>  'La enseñanza de las artes en NORTHLANDS guía a los alumnos para que puedan encontrar nuevas alternativas y oportunidades de expresión. ',
    'text2' =>  'Durante la etapa inicial el aprendizaje está basado en juegos como una manera de experimentar con las posibilidades de cada disciplina.  A medida que el estudiante madura se le ofrece una variedad de técnicas, instrumentos y culturas para explorar a través del estudio formal de las artes dramáticas, musicales y visuales.',
    'visual-arts-title' =>  'Artes Visuales',
    'dramatic-arts-title' =>  'Artes Dramáticas',
    'musical-arts-title' =>  'Artes Musicales',
];
