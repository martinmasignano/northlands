<?php

return [

    /*
    |--------------------------------------------------------------------------
    | International programmes Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the International programmes section.
    |
    */

    'header'         =>  'PROGRAMAS INTERNACIONALES',
    'intro' 	     =>	 'Somos un Colegio del Mundo IB. Participamos de una comunidad global que comparte experiencias de aprendizaje y pasión por la enseñanza. El constante desarrollo profesional de nuestro cuerpo académico permite contar con educadores idóneos y de vanguardia.',
    'ip-header'    => 'AFILIACIONES INTERNACIONALES',
    'pyp-header' => 'PROGRAMA DE LA ESCUELA PRIMARIA DEL IB (PEP)*',
    'pyp-text' => 'Un programa de educación internacional en el que docentes y alumnos exploran transdisciplinariamente el camino del conocimiento. Con eje central en el desarrollo del niño como un todo, este programa propone un marco educativo interesante, estimulante, significativo y comprometido con la indagación estructurada que alienta a los alumnos a actuar de acuerdo a nuevas perspectivas y conocimientos.',
    'igcse-header' => 'CAMBRIDGE INTERNATIONAL GENERAL CERTIFICATE OF SECONDARY EDUCATION (IGCSE)*',
    'igcse-text' => 'Un programa internacional que desarrolla destrezas educativas esenciales como el proceso de recordar conocimientos, las habilidades de expresión oral, resolución de problemas, el trabajo en equipo y el espíritu de iniciativa e investigación. Reconocido como equivalente a los dos últimos años de educación obligatoria en Gran Bretaña.',
    'ibo-header' => 'PROGRAMA DE DIPLOMA DEL IB*',
    'ibo-text' => 'Un exigente y estimulante currículo de educación internacional reconocido por universidades de todo el mundo. Desarrolla en los alumnos la capacidad de aprender a aprender; una fuerte identidad personal y cultural; y la capacidad de comprender y comunicarse con personas de otros países y culturas.',
    'disclaimer' => '*Programas obligatorios para todos los alumnos de NORTHLANDS'
];
