<?php

return [

    /*
    |--------------------------------------------------------------------------
    | PE Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the PE section.
    |
    */

    'header'    =>  'Educación física',

    'text1' =>  'La educación física y deportiva es una parte esencial en la educación de nuestros alumnos.',
    'text2' =>  'Participamos de eventos interescolares a nivel nacional e internacional que promueven el espíritu de equipo y la sana competencia. El valor diferencial de nuestra propuesta es la convicción de que enseñamos para la vida y no simplemente para el éxito deportivo. Adecuamos las estrategias para que todos los alumnos encuentren un desafío de superación y se conecten con su cuerpo.',
    'text3' =>  'Como miembros de la comunidad IB, entendemos la importancia del equilibrio físico, mental y emocional para lograr el bienestar propio y el de los demás. Bajo esta idea guiamos a nuestros alumnos en el desarrollo de un estilo de vida equilibrado.',

    'sidebar-heading' =>  '¿Qué hace que nuestras clases sean diferentes?',
    'sidebar-text1'  =>  '· El estímulo positivo para que nuestros alumnos            disfruten de la actividad física en clase',
    'sidebar-text2'  =>  '· El seguimiento de nuestros alumnos para que cada uno logre mejorar a su máximo potencial.',
    'sidebar-text3'  =>  '· El análisis de las prácticas deportivas y las reflexiones grupales',
    'sidebar-text4'  =>  '· La autoevaluación y  la enseñanza con pares',
    'sidebar-text5'  =>  '· Los proyectos interdisciplinarios, solidarios y de vida en la naturaleza',
    'sidebar-text6'  =>  '· El sentido de pertenencia y compromiso con los equipos deportivos, con su House y con el Colegio',
    'sidebar-text7'  =>  'Nuestras clases están basadas en la enseñanza en valores y la metacognición con el propósito de que los alumnos adquieran competencias para la vida que puedan transferir a cualquier ámbito de desempeño; y se destaquen por la integridad de sus valores, para con ellos, los otros y el medio ambiente',
];
