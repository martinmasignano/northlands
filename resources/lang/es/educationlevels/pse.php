<?php

return [

    /*
    |--------------------------------------------------------------------------
    | PSE Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the PSE section.
    |
    */

    'header'         =>  'Educación Personal y Social',
    'content' 	     =>	 'Contenido para Educación Personal y Social',
];