<?php

return [

	/*
    |--------------------------------------------------------------------------
    | Our school language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the Our school names.
    |
    */
	
	'header'				=>	'UN COLEGIO, DOS SEDES',
	'content'				=> 	'NORTHLANDS es un Colegio mixto con 2 sedes ubicadas en la zona norte de  la Provincia de Buenos Aires, Argentina. Brinda educación bilingüe, no confesional, de altos estándares internacionales a niños desde los 2 a los 18 años.',
    'description'           => 'DATOS CLAVES',
];
