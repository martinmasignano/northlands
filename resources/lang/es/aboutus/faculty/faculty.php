<?php

return [

	/*
    |--------------------------------------------------------------------------
    | Faculty language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the Faculty names.
    |
    */
	
	'header'				=>	'EQUIPO DOCENTE',
	'description1'			=> 	'NORTHLANDS, selecciona al mejor capital humano para el desarrollo personal, físico y académico de nuestros estudiantes.',
	'description2'			=>	'Nuestros equipos docente y administrativo reciben periódicas capacitaciones que hacen a su crecimiento personal y profesional. Las capacitaciones incluyen workshops dictados por nuestro propio staff sobre buenas prácticas en educación así como conferencias internacionales, viajes de exploración a instituciones en el exterior, seminarios in house a cargo de especialistas de trayectoria internacional.',
	'positionteacher1'		=>	'Cargo',
	'positionteacher2'		=>	'Cargo',
	'positionteacher3'		=>	'Cargo',
	'positionteacher4'		=>	'Cargo',
	'positionteacher5'		=>	'Cargo',
	'positionteacher6'		=>	'Cargo',

	'section-title'			=>	'HABLANDO SOBRE',
];
