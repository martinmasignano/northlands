<?php

return [

	/*
    |--------------------------------------------------------------------------
    | Faculty detail language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the Faculty detail names.
    |
    */
	
	'header'				=>	'CUERPO DE PROFESORES',
	'lead'			=> 	'Angie Mallo, coordinadora de Educación Física de Secundaria, disertó en el 7o Congreso Europeo de la Fédération Internationale d’Education Physique (FIEP) que tuvo lugar en Barcelona del 7 al 9 de junio de 2012.',
	'paragraph-1'		=>	'El tema de su disertación es “La educación física escolar y la creatividad oportunidades y estrategias didácticas” aquí comparte algunos puntos de su ponencia:',
	'paragraph-2'		=>	'En el siglo pasado de la mano de la industria y la producción en serie, la enseñanza se enmarcó en ciertos parámetros enfocados a la enseñanza de contenidos y del trabajo escolar en masa. El nuevo siglo, en cambio, nos marca NO a la producción y SÍ a la diferenciación; para eso necesitamos enseñar desde la individualidad. Enseñar para observar y generar cambios. <br />Tenemos que pensar en formar seres diferentes, en potenciar las particularidades porque desde ellas se hará el cambio.',
	'paragraph-3'		=>	'La creatividad ha sido considerada por psicólogos y filósofos como una alternancia entre dos modos muy diferentes de pensar: Un modo de análisis y un modo generador*. Un individuo cuando participa en un pensamiento analítico, es enfocado, limita su atención en el análisis. Cuando pensamos creativamente generamos asociaciones, el pensamiento pareciera que es menos enfocado y la atención está más a la deriva, viajando entre conceptos que antes no se tenían en cuenta. La creatividad, entonces, puede caracterizarse por la capacidad de moverse de un modo de pensamiento a otro sin dificultad.',
	'paragraph-4'		=>	'Las habilidades que requiere el ciudadano del nuevo siglo transcienden nuestra imaginación. El mundo está cambiando a gran velocidad y las certidumbres son parte del pasado.<br />Enseñar para este paradigma es difícil y desafiante. Pero es el cambio y el futuro un tejido sin costura, somos parte y artífices, ya no bastan las buenas prácticas para tener un horizonte  “...la innovación de futuro es pensar en la brecha de oportunidad que se tendrá si se trae ese pensamiento de futuro al hoy” (Govindarajan 2011) En este marco la creatividad es una habilidad clave a desarrollar en las escuelas, tanto en enseñantes como en aprendices.',
	'footnotes'			=>	'*(Howard-Jones,Paul A./ Winfieldb, M. & Crimminsb, G., 2008)pág.4',
	'position'			=>	'Cargo',
	'description'		=>	'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.',
	'cta'				=>	'Ver perfil',
];