<?php

return [

	/*
    |--------------------------------------------------------------------------
    | authorities language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the authorities names.
    |
    */
	
	'header'				=>	'AUTORIDADES',
	'copy'					=>	'En 1960 Miss Brightman, nuestra fundadora, donó el Colegio a una Asociación Civil de Beneficencia, que vela por la buena gestión y el desarrollo de la institución. En la actualidad la ACB cuenta con 75 Asociados (Trustees) voluntarios, entre quienes se eligen a los miembros de la Comisión Directiva (Board of Governors), los que permanecen en ejercicio por dos años, cuando pueden ser reelegidos o renunciar a su cargo.',
	'boardtitle'			=>	'COMISIÓN DIRECTIVA',
	'president'				=>	'Presidente',	
	'vice'					=>	'Vicepresidente',	
	'treasurer'				=>	'Tesorero',	
	'secretary'				=>	'Secretaria',	
	'members'				=>	'Miembros',	
	'substitutesmembers'	=>	'Miembros suplentes',
	'reviewer'				=>	'Revisor de cuentas',	
	'substitutereviewer'	=>	'Revisor de cuentas suplente',
	'subtitleAuthorities'	=>	'AUTORIDADES ESCOLARES',

	'headmasterName'		=>	'Nicholas Reeves',
	'headmaster'			=>	'General Headmaster',
	'headmasterDescription'	=>	'Rector General: Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.',

	'vicechancellorName'	=>	'Marisa Perazzo',
	'vicechancellor'		=>	'Vicerrectora General',
	'vicechancellorDescription'	=>	'Vicerrectora General: Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.',

	'dafName'				=>	'María Eugenia Rodriguez',
	'daf'					=>	'Directora de Administración y Finanzas',
	'dafDescription'	=>	'Directora de Administración y Finanzas: Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.',

	'devDirectorName'		=>	'Florencia Sackmann Sala',
	'devDirector'			=>	'Directora de Desarrollo',
	'devDirectorDescription'	=>	'Directora de Desarrollo: Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.',

	'sportsDirectorName'	=>	'Jorge Rey',
	'sportsDirector'		=>	'Director de Educación Física y Deportes',
	'sportsDirectorDescription'	=>	'Director de Educación Física y Deportes: Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.',

	'perfArtsName'			=>	'Leandro Valle',
	'perfArts'				=>	'Director de Artes Interpretativas',
	'perfArtsDescription'	=>	'Director de Artes Interpretativas: Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.',

	'kinderNDirectorName'	=>	'Alejandra Batu',
	'kinderNDirector'		=>	'Directora Interina de Nivel Inicial Nordelta',
	'kinderNDirectorDescription'	=>	'Directora de Jardín de Infantes Nordelta: Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.',

	'PrimaryNDirectorName'	=>	'Patricia Christensen de Gilbert',
	'PrimaryNDirector'		=>	'Directora de Primaria Nordelta',
	'PrimaryNDirectorDescription'	=>	'Directora de Primaria Nordelta: Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.',

	'SecondaryNDirectorName'=>	'Shaun Hudson',
	'SecondaryNDirector'	=>	'Head of Secondary Nordelta',
	'SecondaryNDirectorDescription'	=>	'Director de Secundaria Nordelta: Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.',

	'kinderODirectorName'	=>	'Vanesa Henson',
	'kinderODirector'		=>	'Directora de Nivel Inicial Olivos',
	'kinderODirectorDescription'	=>	'Directora de Jardín de Infantes Olivos: Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.',

	'PrimaryODirectorName'	=>	'Adriana García Posadas',
	'PrimaryODirector'		=>	'Vicedirectora de Primaria Olivos',
	'PrimaryODirectorDescription'	=>	'Directora de Primaria Olivos: Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.',

	'SecondaryODirectorName'=>	'Sofia Hughes',
	'SecondaryODirector'	=>	'Head of Secondary Olivos',
	'SecondaryODirectorDescription'	=>	'Directora de Secundaria Olivos: Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.'
];
