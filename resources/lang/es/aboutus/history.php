<?php

return [

	/*
    |--------------------------------------------------------------------------
    | History language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the History names.
    |
    */
	
	'header'				=>	'BREVE RESEÑA HISTÓRICA',
    'text1'             =>  'NORTHLANDS fue fundado el 1 de abril de 1920 por dos visionarias inglesas, Winifred M.Brightman y Muriel I. Slater, en la localidad de Olivos, en la zona norte del Gran Buenos Aires. El Colegio abrió sus puertas con 16 alumnos (varones y mujeres) y en poco tiempo desarrolló una sólida reputación. Tres años más tarde NORTHLANDS dejó de recibir varones y comenzó a proyectarse como uno de los mejores colegios de niñas de América del Sur. Sus principios básicos de estimular a los alumnos para alcanzar un alto nivel académico tomando en cuenta la formación integral de la persona, quedaron desde entonces cimentados en el lema del Colegio, “Amistad y Servicio”.',
    'text2'         => 'Winifred Brightman tuvo a su cargo la conducción del Colegio durante cuarenta años, hasta 1961 cuando concluyó su actividad. Desde entonces el Colegio comenzó a funcionar como una asociación sin fines de lucro, Northlands Asociación Civil de Beneficencia, de cuyos miembros fiduciarios fue electa la primera Comisión Directiva.',
    'text3'         => 'A medida que avanzamos hacia nuestro centenario, nuestra visión en NORTHLANDS es que nos acercamos a la enseñanza, el aprendizaje y la vida bajo el espíritu de la Organización del Bachillerato Internacional, alentando a nuestros alumnos a alcanzar su máximo potencial descubriendo sus talentos y preparándolos para ser ciudadanos globales comprometidos, encarnando nuestro lema, Amistad y Servicio.',
	'timeline'				=> 'LÍNEA DE TIEMPO',
];
