<?php

return [

	/*
    |--------------------------------------------------------------------------
    | History language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the History names.
    |
    */
	
    'header'                =>  'Construyendo nuestra historia',
    'history1'               =>  'En 2015 cumplimos 95 años desde nuestra fundación y decidimos transitar, de manera consciente, el camino hacia nuestro centenario que se cumplirá en 2020.',
    'history2'              => 'Construir nuestra historia es recorrer juntos ese camino de historias compartidas, de amigos y compañeros, de logros y fracasos que dejan enseñanza. Es sentir con orgullo que somos parte de algo más grande, de una comunidad que crece y que se amalgama bajo el lema “Amistad y Servicio”.',
    'history3'              => 'ALGUNOS HITOS EN NUESTRA HISTORIA',

    'title1'                => 'Bicentenario 9 de Julio',
    'description1'          => '1816 - 2016',
    
    'title2'                => 'Proyecto D-Learning',
    'description2'          => '2015',
    
    'title3'                => 'Intercultural',
    'description3'          => '2015',
    
    'title4'                => 'Proyecto Chaco',
    'description4'          => '25 años - 2015',
    
    'title5'                => '95 aniversario',
    'description5'          => '2015',

    'title6'                => 'Proyecto Historia',
    'description6'          => 'Alumni',

    'title7'                => '90 aniversario',
    'description7'          => '2010',

    'title8'                => 'Bicentenario 25 de Mayo',
    'description8'          => '1810 - 2010',

];
