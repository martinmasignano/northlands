<?php

return [

	/*
    |--------------------------------------------------------------------------
    | The Northlands way language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the The Northlands way names.
    |
    */
	
	'copy'					=>	'NORTHLANDS es un colegio bilingüe, co-educacional del Mundo IB que, a través de enseñanza innovadora y dedicada, educa a los jóvenes para que alcancen el máximo de sus potencialidades individuales. Es un colegio que valora sus raíces anglo-argentinas, respetuoso de todas las culturas, religiones y nacionalidades. Ofrece un ambiente integral, cálido, cordial y amistoso que se refleja en su lema: Amistad y Servicio. Los graduados son personas cuya integridad y valores morales les llevan a elegir libremente el bien.',
	'header'				=>	'This is the<br />NORTHLANDS way',
	'description-1'			=>	'Para lograrlo brindamos una educación integral donde cuerpo, mente y espíritu son abordados como un todo desde múltiples perspectivas. NORTHLANDS es un ámbito de aprendizaje y formación personal y social con un programa internacional bilingüe que invita a pensarse como ciudadanos del mundo.',
    'description-2'         =>  'Para formar individuos “cuya moral los lleve a elegir libremente el bien” NORTHLANDS desarrolla su plataforma educativa y de desarrollo personal interconectando los ejes de formación Social y Personal, los valores que sostienen nuestra cultura, el perl de la comunidad IB (International Baccalaureat) y el desarrollo de las habilidades de pensamiento.',
    'axes.title'            =>  'INTERACCIÓN DE EJES',
    'commitment.title'      =>  'COMPROMISO CON LOS VALORES',
    'education.title'       =>  'EDUCACIÓN PERSONAL Y SOCIAL',
    'social.title'          =>  'Eje Social',
    'personal.title'        =>  'Eje Personal',
    'profile.title'         =>  'PERFIL DE LA COMUNIDAD DEL APRENDIZAJE DEL IB',
    'habits.title'          =>  'HÁBITOS DEL PENSAMIENTO',
];
