<?php

return [

	/*
    |--------------------------------------------------------------------------
    | categories language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the categories names.
    |
    */
	
	'header'	=>	'This is the NORTHLANDS way',
	'subheader'	=>	'Bases sólidas en las mentes del futuro',
	'ibheader'	=>	'POTENCIAMOS ALUMNOS IB',
	'ibtext'	=>	'Programa de lectura Online',
	'gcheader'	=>	'CIUDADANÍA GLOBAL',
	'gctext'	=>	'Conectando salones de clase',
	'cbheader'	=>	'DESARROLLO DEL CARÁCTER',
	'cbtext'	=>	'Haciendo nuevos amigos',
	'mkheader'	=>	'MENTORES DEL CONOCIMIENTO',
	'mktext'	=>	'No intentes esto en casa',
	'hlheader'	=>	'ESTILO DE VIDA SALUDABLE',
	'hltext'	=>	'Tutoriales de primeros auxilios',
	'dlheader'	=>	'APRENDICES DIGITALES',
	'dltext'	=>	'Expandir el futuro de nuestros alumnos',
];