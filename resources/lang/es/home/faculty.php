<?php

return [

	/*
    |--------------------------------------------------------------------------
    | faculty language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the faculty names.
    |
    */
	
    'header'    =>  'EQUIPO DOCENTE',
	'quote'	    =>	'NORTHLANDS, selecciona al mejor capital humano para el desarrollo personal, físico y académico de nuestros estudiantes.',
	'author'	=>	'Nicholas Reeves, Headmaster',
];
