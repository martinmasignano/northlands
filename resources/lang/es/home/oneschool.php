<?php

return [

	/*
    |--------------------------------------------------------------------------
    | one school, two sites language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the one school, two sites names.
    |
    */
	
	'header'	=>	'UN COLEGIO, DOS SEDES',
    'olivos'      =>  'SEDE OLIVOS',
    'nordelta'      =>  'SEDE NORDELTA'
];
