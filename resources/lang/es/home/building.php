<?php

return [

	/*
    |--------------------------------------------------------------------------
    | building language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the building names.
    |
    */
	
	'header'		=>	'CONSTRUYENDO NUESTRA HISTORIA',
    'title'         =>  'Construyendo nuestra historia'
];