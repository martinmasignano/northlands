<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Compromiso con los valores Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the compromiso con los valores section.
    |
    */

    'header'         =>  'Compromiso con los valores',
    'content'        =>  'Contenido de   compromiso con los valores',
];