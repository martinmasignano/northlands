<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Perfil de la comunidad de aprendizaje del IB Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the Perfil de la comunidad de aprendizaje del | IB section.
    |
    */

    'header'         =>  'Perfil de la comunidad de aprendizaje del IB',
    'content'        =>  'Contenido de Perfil de la comunidad de aprendizaje del IB',
];