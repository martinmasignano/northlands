<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Hábitos de pensamientos Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the Hábitos de pensamientos section.
    |
    */

    'header'         =>  'Hábitos de pensamientos',
    'content'        =>  'Contenido de Hábitos de pensamientos',
];