<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Educación personal y social Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the Educación personal y social section.
    |
    */

    'header'         =>  'Educación personal y social',
    'content'        =>  'Contenido de Educación personal y social',
];