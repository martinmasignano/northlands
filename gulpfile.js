var elixir = require('laravel-elixir');
elixir.config.publicPath = 'public_html';

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
elixir(function(mix) {

	/* Copio las fonts necesarias */
	mix.copy('node_modules/font-awesome/fonts','public_html/fonts');
	mix.copy('node_modules/bootstrap-sass/assets/fonts/bootstrap','public_html/build/fonts/bootstrap');
	mix.copy('node_modules/slick-carousel/slick/fonts','public_html/css/fonts');

	/* Copio las imágenes necesarias */
	mix.copy('public_html/css/img','public_html/build/css/img');
	
	/* Copio los assets necesarios */
	mix.copy(
		['resources/assets/css/vendor/vlb/*.png',
		'resources/assets/css/vendor/vlb/*.gif',
		'resources/assets/css/vendor/vlb/*.css'],
		'public_html/css');

	/* Copio las librerías JS necesarias */
	mix.copy(
		['node_modules/bootstrap-sass/assets/javascripts/bootstrap.js',
		'node_modules/masonry-layout/masonry.js',
		'node_modules/slick-carousel/slick/slick.js',
		'node_modules/jquery/dist/jquery.js',
		'resources/assets/js/vendor/jquery.eislideshow.js',
		'node_modules/jquery.easing/jquery.easing.js',
		'resources/assets/js/vendor/vlb/visuallightbox.js',
		'resources/assets/js/vendor/vlb/thumbscript1.js',
		'resources/assets/js/vendor/vlb/vlbdata1.js']
		,'public_html/js/vendor');

	mix.copy('resources/assets/js/*.js','public_html/js');

	/* Compilo sass */
    mix.sass('app.scss').version('css/app.css');
});
